// ENABLE / DISABLE SEARCH OPTIONS SELECTION

var $group = $('#select-group'), $province = $('#select-province'), $ward = $('#select-ward');

$group.change(function () {
    //enable province selection
    if ($group.val() == 'empty') {
        $province.attr('disabled', 'disabled').val('empty');
    } else {
        $province.removeAttr('disabled');
    }
}).trigger('change'); // added trigger to calculate initial state

$province.change(function () {
    //enable ward selection
    if ($province.val() == 'empty') {
        $ward.attr('disabled', 'disabled').val('empty');
    } else {
        $ward.removeAttr('disabled');
    }
}).trigger('change'); // added trigger to calculate initial state


// INFO TABS
$('.tabbed-content').tabslet({
	active: 5,
	animation: true
});

// SWITCH BETWEEN MAP AND LIST VIEWS OF SUBPROJECTS
function swapViews (mapView, listView) {
    document.getElementById(mapView).style.display = 'block';
    document.getElementById(listView).style.display = 'none';
}
document.getElementById('showMap').addEventListener('click',function(e){
    swapViews ('mapView','listView');
});
document.getElementById('showList').addEventListener('click',function(e){
    swapViews ('listView','mapView');
});