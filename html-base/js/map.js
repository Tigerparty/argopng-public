var map;
var markers = [];
var prev_infowindow = false;

function initialize()
{
    $("#google_map").css('background-image', 'none');
	  var heightAshbury = init_location();
    var mapOptions = {
    	zoom: 10,
    	center: heightAshbury,
    }
    map = new google.maps.Map(document.getElementById('google_map'),
				mapOptions);
}

function init_location()
{	
	var avgLocation;

	$.ajax({
        type : "GET",
        async : false,
        url : "/ajax/get_avg_locations",
        dataType : "json",
        success :
            function(avg_location){

                avgLocation = new google.maps.LatLng(avg_location['avg_latitude'], avg_location['avg_longitude']);
            }
    });

    return avgLocation;
}

function set_markers(this_map) 
{	
	for (var i = 0; i < markers.length; i++) 
	{
    	markers[i].marker.setMap(this_map);
  	}
}

function set_markers_by_area(villages)
{
    var latlngbounds = new google.maps.LatLngBounds();
    for (var i = 0; i < villages.length; i++) {
      var village = villages[i];
      latlngbounds.extend(new google.maps.LatLng(village['lat'], village['lng']));
    }
    map.fitBounds(latlngbounds);
}


function clear_markers()
{
	set_markers(null);
	markers = [];
}

function add_marker(villages)
{
	for (var i = 0; i < villages.length; i++) {
		var village = villages[i];

		var id = parseInt(village['id']);
  	var name = village['name'];
  	var latitude = parseFloat(village['lat']);
  	var longitude = parseFloat(village['lng']);

		var myLatLng = new google.maps.LatLng(latitude, longitude);
		var marker = new google.maps.Marker({
			position: myLatLng,
			icon: '/images/mapdot.png',
			map: map,
			title: name,
			zIndex: id
		});

    var marker_info = {
      marker: marker,
      subprojects : village['subprojects']
    };
		markers.push(marker_info);
	}

}


function add_popup_window()
{
  for(var i = 0; i < markers.length; i++)
  {
    var contentString = '<div class="map-bubble">';
    for (var j=0 ; j < markers[i].subprojects.length; j++) {
      contentString += (
        '<div class="map-bubble-row">'+
          '<div class="map-bubble-img-wrapper">'+
          '<img class="map-bubble-img" src="'+markers[i].subprojects[j]['prev_img']+'">'+
          '</div>'+
          '<p class="map-bubble-content"><a href="'+markers[i].subprojects[j]['link']+'">'+ 
          markers[i].subprojects[j]['name']+
          '</a></p></div>'
      );
    }
    contentString += '</div>';
    google.maps.event.addListener(markers[i].marker, 'click', __getInfoCallback(contentString));
  }
  
		

}

function __getInfoCallback(content) {
    var infowindow = new google.maps.InfoWindow();
    return function() {
    		if( prev_infowindow )
    		{
    			prev_infowindow.close();
    		}
    		prev_infowindow = infowindow;
        infowindow.setContent(content); 
        infowindow.open(map, this);
    };
}

function update_map(villages)
{
	clear_markers();
	if(villages.length > 0)
	{
		add_marker(villages);
		add_popup_window();
    set_markers_by_area(villages);
	}
	else
	{
		initialize();
	}
}


