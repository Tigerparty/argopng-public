var P1App = angular.module('P1App', ['ngRoute', 'ui.bootstrap', 'AttachUploader'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

P1App.factory('P1Factory', function ($http) {
	return {
		getDistricts : function (p_id){
			return $http.get('/ajax/pdlwv/' + p_id);
		},
		getLlgs : function (p_id, d_id){
			return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id);
		},
		getWards : function (p_id, d_id, l_id){
			return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id);
		},
	};
});

P1App.controller('P1CreateCtrl', function ($scope, P1Factory) {
	function Place(place, representative, total_village){
		this.place = place || null;
		this.representative = representative || null;
		this.total_village = total_village || null;
	}

	$scope.addPlace = function () {
		$scope.places.push(new Place());
	};

	$scope.MeetingDatePickerOpened = false;

	$scope.openMeetingDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.MeetingDatePickerOpened = true;
  };

  $scope.updateDistricts =  function () {
  	$scope.districts = [];
  	$scope.district_id = undefined;
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
  	if ($scope.province_id !== undefined) {
      var p_id = $scope.province_id;

      P1Factory.getDistricts(p_id).then(function(response){
        if (response.data) {
          $scope.districts = response.data.districts;
        }else {
          console.log('Warning: Get empty district list');
        }
      });
    }
    else {
      console.log('No selected province');
    }
  };

  $scope.updateLlgs =  function () {
  	$scope.llgs = [];
  	$scope.llg_id = undefined;
    $scope.ward_id = undefined;
  	if ($scope.district_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;

      P1Factory.getLlgs(p_id, d_id).then(function(response){
        if (response.data) {
          $scope.llgs = response.data.districts[0].llgs;
        }else {
          console.log('Warning: Get empty llg list');
        }
      });
    }
    else {
      console.log('No selected district');
    }
  };

  $scope.updateWards =  function () {
  	$scope.wards = [];
  	$scope.ward_id = undefined;
  	if ($scope.llg_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;
      var l_id = $scope.llg_id;

      P1Factory.getWards(p_id, d_id, l_id).then(function(response){
        if (response.data) {
          $scope.wards = response.data.districts[0].llgs[0].wards;
        }else {
          console.log('Warning: Get empty ward list');
        }
      });
    }
    else {
      console.log('No selected llg');
    }
  };

  (function init() {
    $scope.provinces = window.init_data.provinces;
    $scope.places = window.init_data.places;
    $scope.attachments = init_data.attachments;

    //Old Input
    $scope.province_id = window.init_data.locations[0];
    $scope.updateDistricts();
    $scope.district_id = window.init_data.locations[1];
    $scope.updateLlgs();
    $scope.llg_id = window.init_data.locations[2];
    $scope.updateWards();
    $scope.ward_id = window.init_data.locations[3];

    if($scope.places.length == 0){
    	$scope.places.push(new Place());
    }
	})();
});

P1App.controller('P1EditCtrl', function ($scope, P1Factory) {
	function Place(place, representative, total_village){
    this.place = place || null;
    this.count_village_representative = representative || null;
    this.count_total_village = total_village || null;
  }

  $scope.addPlace = function () {
    $scope.places.push(new Place());
  };

  $scope.MeetingDatePickerOpened = false;

  $scope.openMeetingDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.MeetingDatePickerOpened = true;
  };

  $scope.updateDistricts =  function () {
    $scope.districts = [];
    $scope.district_id = undefined;
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
    if ($scope.province_id !== undefined) {
      var p_id = $scope.province_id;

      P1Factory.getDistricts(p_id).then(function(response){
        if (response.data) {
          $scope.districts = response.data.districts;
        }else {
          console.log('Warning: Get empty district list');
        }
      });
    }
    else {
      console.log('No selected province');
    }
  };

  $scope.updateLlgs =  function () {
    $scope.llgs = [];
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
    if ($scope.district_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;

      P1Factory.getLlgs(p_id, d_id).then(function(response){
        if (response.data) {
          $scope.llgs = response.data.districts[0].llgs;
        }else {
          console.log('Warning: Get empty llg list');
        }
      });
    }
    else {
      console.log('No selected district');
    }
  };

  $scope.updateWards =  function () {
    $scope.wards = [];
    $scope.ward_id = undefined;
    if ($scope.llg_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;
      var l_id = $scope.llg_id;

      P1Factory.getWards(p_id, d_id, l_id).then(function(response){
        if (response.data) {
          $scope.wards = response.data.districts[0].llgs[0].wards;
        }else {
          console.log('Warning: Get empty ward list');
        }
      });
    }
    else {
      console.log('No selected llg');
    }
  };

  (function init() {
    $scope.provinces = window.init_data.provinces;
    $scope.places = window.init_data.places;
    $scope.attachments = init_data.attachments;

    //Old Input
    $scope.province_id = window.init_data.locations[0];
    $scope.updateDistricts();
    $scope.district_id = window.init_data.locations[1];
    $scope.updateLlgs();
    $scope.llg_id = window.init_data.locations[2];
    $scope.updateWards();
    $scope.ward_id = window.init_data.locations[3];

    if($scope.places.length == 0){
      $scope.places.push(new Place());
    }
  })();
});

P1App.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});