var P2App = angular.module('P2App', ['ngRoute', 'ui.bootstrap', 'AttachUploader'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

P2App.factory('P2Factory', function ($http) {
	return {
		getDistricts : function (p_id){
			return $http.get('/ajax/pdlwv/' + p_id);
		},
		getLlgs : function (p_id, d_id){
			return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id);
		},
		getWards : function (p_id, d_id, l_id){
			return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id);
		},
    getVillages : function (p_id, d_id, l_id, w_id){
      return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id + '/' + w_id);
    },
    getWardPrioritisation : function (ward_id){
      return $http.get('/ajax/get_ward_prioritisation/' + ward_id);
    },
	};
});

P2App.controller('P2CreateCtrl', function ($scope, P2Factory) {
  function CounterpartFunding(source, origin_amount, has_commitment){
    this.source = source || null;
    this.origin_amount = parseInt(origin_amount) || null;
    this.has_commitment = has_commitment || null;
  }

  $scope.addCounterpartFunding = function () {
    $scope.counterpart_fundings.push(new CounterpartFunding());
  };

  $scope.MeetingDatePickerOpened = false;

  $scope.openMeetingDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.MeetingDatePickerOpened = true;
  };

  $scope.updateDistricts =  function () {
    $scope.districts = [];
    $scope.district_id = undefined;
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    $scope.appraisal_committee = undefined;
    $scope.prioritisation_id = undefined;
    if ($scope.province_id !== undefined) {
      var p_id = $scope.province_id;

      P2Factory.getDistricts(p_id).then(function(response){
        if (response.data) {
          $scope.districts = response.data.districts;
        }else {
          console.log('Warning: Get empty district list');
        }
      });
    }
    else {
      console.log('No selected province');
    }
  };

  $scope.updateLlgs =  function () {
    $scope.llgs = [];
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    $scope.appraisal_committee = undefined;
    $scope.prioritisation_id = undefined;
    if ($scope.district_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;

      P2Factory.getLlgs(p_id, d_id).then(function(response){
        if (response.data) {
          $scope.llgs = response.data.districts[0].llgs;
        }else {
          console.log('Warning: Get empty llg list');
        }
      });
    }
    else {
      console.log('No selected district');
    }
  };

  $scope.updateWards =  function () {
    $scope.wards = [];
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    $scope.appraisal_committee = undefined;
    $scope.prioritisation_id = undefined;
    if ($scope.llg_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;
      var l_id = $scope.llg_id;

      P2Factory.getWards(p_id, d_id, l_id).then(function(response){
        if (response.data) {
          $scope.wards = response.data.districts[0].llgs[0].wards;
        }else {
          console.log('Warning: Get empty ward list');
        }
      });
    }
    else {
      console.log('No selected llg');
    }
  };

  $scope.updateVillages =  function () {
    $scope.villages = [];
    $scope.village_id = undefined;
    $scope.appraisal_committee = undefined;
    $scope.prioritisation_id = undefined;
    if ($scope.ward_id !== undefined && $scope.ward_id !="") {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;
      var l_id = $scope.llg_id;
      var w_id = $scope.ward_id;

      P2Factory.getVillages(p_id, d_id, l_id, w_id).then(function(response){
        if (response.data) {
          $scope.villages = response.data.districts[0].llgs[0].wards[0].villages;
          $scope.appraisal_committee = response.data.districts[0].llgs[0].wards[0].name;
        }else {
          console.log('Warning: Get empty village list');
        }
      });

      P2Factory.getWardPrioritisation(w_id).then(function(response){
        if ( response.data) {
          $scope.prioritisations = response.data;
        }
      });
    }
    else {
      console.log('No selected ward');
    }
  };

  $scope.deleteAttachment = function(index) {
    $scope.attachments.splice(index, 1);
  };

  $scope.calculateTotalAmount = function() {
    $scope.totalAmount = 0;
    if($scope.rsdplgp_funding_amount){
      $scope.totalAmount += $scope.rsdplgp_funding_amount;
    }
    if($scope.community_funding_amount){
      $scope.totalAmount += $scope.community_funding_amount;
    }

    angular.forEach($scope.counterpart_fundings, function(counterpart_funding, index){
      $scope.totalAmount += counterpart_funding.origin_amount;
      $scope.totalAmountPercent = ($scope.totalAmount/$scope.approval.fund_estimate_origin*100);
    });
  };

  $scope.isFinite = function(value) {
    return isFinite(value);
  };

  $scope.amountRound = function(value){
    return (Math.round(value*100)/100);
  };

  (function init() {
    $scope.provinces = init_data.provinces;

    $scope.approval = init_data.approval;
    $scope.rsdplgp_funding_amount = init_data.rsdplgp_funding_amount;
    $scope.community_funding_amount = init_data.community_funding_amount;
    $scope.counterpart_fundings = [];
    for (var i = 0; i < init_data.counterpart_fundings.length; i++) {
      $scope.counterpart_fundings.push(new CounterpartFunding(
        init_data.counterpart_fundings[i].source,
        init_data.counterpart_fundings[i].origin_amount,
        init_data.counterpart_fundings[i].has_commitment
      ))
    };
    $scope.attachments = init_data.attachments;

    //Old Input
    $scope.province_id = init_data.locations[0];
    $scope.updateDistricts();
    $scope.district_id = init_data.locations[1];
    $scope.updateLlgs();
    $scope.llg_id = init_data.locations[2];
    $scope.updateWards();
    $scope.ward_id = init_data.locations[3];
    $scope.updateVillages();
    $scope.village_id = init_data.locations[4];
    $scope.prioritisation_id = init_data.locations[5];

    if($scope.counterpart_fundings.length == 0){
      $scope.counterpart_fundings.push(new CounterpartFunding());
    }

    $scope.calculateTotalAmount();
  })();
});

P2App.controller('P2EditCtrl', function ($scope, P2Factory) {
  function CounterpartFunding(id, source, origin_amount, has_commitment){
    this.id = id || null;
    this.source = source || null;
    this.origin_amount = parseInt(origin_amount) || null;
    this.has_commitment = has_commitment;
  }

  $scope.addCounterpartFunding = function () {
    $scope.counterpart_fundings.push(new CounterpartFunding());
  };

  $scope.MeetingDatePickerOpened = false;

  $scope.openMeetingDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.MeetingDatePickerOpened = true;
  };

  $scope.updateDistricts =  function () {
    $scope.districts = [];
    $scope.district_id = undefined;
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    $scope.appraisal_committee = undefined;
    $scope.prioritisation_id = undefined;
    if ($scope.province_id !== undefined) {
      var p_id = $scope.province_id;

      P2Factory.getDistricts(p_id).then(function(response){
        if (response.data) {
          $scope.districts = response.data.districts;
        }else {
          console.log('Warning: Get empty district list');
        }
      });
    }
    else {
      console.log('No selected province');
    }
  };

  $scope.updateLlgs =  function () {
    $scope.llgs = [];
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    $scope.appraisal_committee = undefined;
    $scope.prioritisation_id = undefined;
    if ($scope.district_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;

      P2Factory.getLlgs(p_id, d_id).then(function(response){
        if (response.data) {
          $scope.llgs = response.data.districts[0].llgs;
        }else {
          console.log('Warning: Get empty llg list');
        }
      });
    }
    else {
      console.log('No selected district');
    }
  };

  $scope.updateWards =  function () {
    $scope.wards = [];
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    $scope.appraisal_committee = undefined;
    $scope.prioritisation_id = undefined;
    if ($scope.llg_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;
      var l_id = $scope.llg_id;

      P2Factory.getWards(p_id, d_id, l_id).then(function(response){
        if (response.data) {
          $scope.wards = response.data.districts[0].llgs[0].wards;
        }else {
          console.log('Warning: Get empty ward list');
        }
      });
    }
    else {
      console.log('No selected llg');
    }
  };

  $scope.updateVillages =  function () {
    $scope.villages = [];
    $scope.village_id = undefined;
    $scope.appraisal_committee = undefined;
    $scope.prioritisation_id = undefined;
    if ($scope.ward_id !== undefined && $scope.ward_id !="") {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;
      var l_id = $scope.llg_id;
      var w_id = $scope.ward_id;

      P2Factory.getVillages(p_id, d_id, l_id, w_id).then(function(response){
        if (response.data) {
          $scope.villages = response.data.districts[0].llgs[0].wards[0].villages;
          $scope.appraisal_committee = response.data.districts[0].llgs[0].wards[0].name;
        }else {
          console.log('Warning: Get empty village list');
        }
      });

      P2Factory.getWardPrioritisation(w_id).then(function(response){
        if ( response.data) {
          $scope.prioritisations = response.data;
        }
      });
    }
    else {
      console.log('No selected ward');
    }
  };

  $scope.deleteAttachment = function(index) {
    $scope.attachments.splice(index, 1);
  };

  $scope.calculateTotalAmount = function() {
    $scope.totalAmount = 0;
    if($scope.rsdplgp_funding_amount){
      $scope.totalAmount += $scope.rsdplgp_funding_amount;
    }
    if($scope.community_funding_amount){
      $scope.totalAmount += $scope.community_funding_amount;
    }

    angular.forEach($scope.counterpart_fundings, function(counterpart_funding, index){
      $scope.totalAmount += counterpart_funding.origin_amount;
      $scope.totalAmountPercent = ($scope.totalAmount/$scope.approval.fund_estimate_origin*100);
    });
  };

  $scope.isFinite = function(value) {
    return isFinite(value);
  };

  $scope.amountRound = function(value){
    return (Math.round(value*100)/100);
  };

  (function init() {
    $scope.provinces = init_data.provinces;

    $scope.approval = init_data.approval;
    $scope.rsdplgp_funding_amount = parseInt(init_data.approval.rdp_fund.origin_amount);
    $scope.community_funding_amount = parseInt(init_data.approval.community_fund.origin_amount);
    $scope.counterpart_fundings = [];
    for (var i = 0; i < init_data.counterpart_fundings.length; i++) {
      $scope.counterpart_fundings.push(new CounterpartFunding(
        init_data.counterpart_fundings[i].id,
        init_data.counterpart_fundings[i].source,
        init_data.counterpart_fundings[i].origin_amount,
        init_data.counterpart_fundings[i].has_commitment
      ))
    };
    $scope.attachments = init_data.attachments;

    //Origin Input & Old Input
    $scope.province_id = init_data.locations[0];
    $scope.updateDistricts();
    $scope.district_id = init_data.locations[1];
    $scope.updateLlgs();
    $scope.llg_id = init_data.locations[2];
    $scope.updateWards();
    $scope.ward_id = init_data.locations[3];
    $scope.updateVillages();
    $scope.village_id = init_data.locations[4];
    $scope.prioritisation_id = init_data.locations[5];

    if($scope.counterpart_fundings.length == 0){
      $scope.counterpart_fundings.push(new CounterpartFunding());
    }

    $scope.calculateTotalAmount();
  })();
});

P2App.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});