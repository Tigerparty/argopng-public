var P5App = angular.module('P5App', ['ngRoute', 'ui.bootstrap', 'AttachUploader'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});

P5App.controller('P5CreateCtrl', function ($scope) {
  $scope.MonitoringDatePickerOpened = false;

  $scope.openMonitoringDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.MonitoringDatePickerOpened = true;
  };

  function Report(){
    this.financial_kept_check = 0;
    this.procurement_change_check = 1;
    this.safeguard_check = 1;
    this.contribution_changing_check = 1;
  };

  function Issue(desciption){
    this.desciption = desciption || null;
  };

  $scope.addIssue = function () {
    $scope.report.issues.push(new Issue());
  };

  $scope.deleteIssue = function(index) {
    $scope.report.issues.splice(index, 1);
  };

  $scope.deleteAttachment = function(index) {
    $scope.attachments.splice(index, 1);
  };

  (function init() {
    $scope.subproject = init_data.subproject;
    $scope.report = init_data.report || new Report();
    $scope.attachments = init_data.attachments;

    $scope.report.issues = $scope.subproject.issues;

    if(!$scope.report.issues){
      $scope.report.issues = [];
      $scope.report.issues.push(new Issue());
    }
  })();
});

P5App.controller('P5ShowCtrl', function ($scope) {
  (function init() {
    $scope.report = init_data.report;
  })();
});

P5App.controller('P5EditCtrl', function ($scope) {
  $scope.MonitoringDatePickerOpened = false;

  $scope.openMonitoringDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.MonitoringDatePickerOpened = true;
  };

  function Report(){
    this.financial_kept_check = 0;
    this.procurement_change_check = 1;
    this.safeguard_check = 1;
    this.contribution_changing_check = 1;
  };

  function Issue(desciption){
    this.desciption = desciption || null;
  };

  $scope.addIssue = function () {
    $scope.report.subproject.issues.push(new Issue());
  };

  $scope.deleteIssue = function(index) {
    $scope.report.subproject.issues.splice(index, 1);
  };

  $scope.initReport = function(){
    if($scope.report.financial_kept_check == undefined){
      if(!$scope.report.financial_kept){
        $scope.report.financial_kept_check = 1;
      }else{
        $scope.report.financial_kept_check = 0;
      }
    }

    if($scope.report.is_contributions_updated_check == undefined){
      if(!$scope.report.is_contributions_updated){
        $scope.report.is_contributions_updated_check = 1;
      }else{
        $scope.report.is_contributions_updated_check = 0;
      }
    }

    if($scope.report.procurement_change_check == undefined){
      if(!$scope.report.procurement_change){
        $scope.report.procurement_change_check = 0;
      }else{
        $scope.report.procurement_change_check = 1;
      }
    }

    if($scope.report.safeguard_check == undefined){
      if(!$scope.report.safeguard){
        $scope.report.safeguard_check = 0;
      }else{
        $scope.report.safeguard_check = 1;
      }
    }

    if($scope.report.contribution_changing_check == undefined){
      if(!$scope.report.contribution_changing){
        $scope.report.contribution_changing_check = 0;
      }else{
        $scope.report.contribution_changing_check = 1;
      }
    }
  }

  $scope.deleteAttachment = function(index) {
    $scope.attachments.splice(index, 1);
  };

  (function init() {
    $scope.subproject = init_data.subproject;
    $scope.report = init_data.report;
    $scope.initReport();
    $scope.attachments = init_data.attachments;

    if(!$scope.report.subproject.issues){
      $scope.report.subproject.issues = [];
      $scope.report.subproject.issues.push(new Issue());
    }
  })();
});