//-- Need constants global variable under winodw
//-- window.constants.CSRF_TOKEN
//-- window.constants

var AttachUploader = angular.module('AttachUploader', ['ngFileUpload'])
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
})

.service('FileService', function(Upload, $timeout, $rootScope) {
    var that = this;
    this.files = [];

    this.NewFile = function(name) {
        var file = {
            index: that.files.length,
            name: name,
            upload_bar_class: "",
            progress: 0,
            progressStatus: "",
            uploadedData: false
        };

        that.files.push(file);
        return file;
    };

    this.isUploading = function() {
        $rootScope.attachment_uploader.is_uploading = true;
    }

    this.notUploading = function() {
        $rootScope.attachment_uploader.is_uploading = false;
    }

    this.removeFile = function(file) {
        that.files.splice(file.index, 1);
    }

    this.removeDuplicateFile = function(new_file) {
        var isDuplicate = false;
        angular.forEach(that.files, function(file, index) {
            if(new_file.index != index && file.uploadedData.id == new_file.uploadedData.id) {
                isDuplicate = true;
            }
        })

        if (isDuplicate) {
            that.removeFile(new_file);
        }
    };

    this.initFiles = function(files) {
        that.files = files;
    };

    this.getFiles = function() {
        return that.files;
    };
})

.controller('fileController', function ($scope, FileService, $rootScope) {
    this.scope = $scope;

    $scope.origin_attachment_count = window.init_data.attachments.length;

    $rootScope.attachment_uploader = [];

    $rootScope.attachment_uploader.is_uploading = false;

    $scope.removeFile = function(file) {
        FileService.removeFile(file);
    };
})

.directive('filegroup', function(FileService) {
    return {
        controller: 'fileController',
        scope: {
            files: '='
        },
        link: function(scope, element, attrs) {
            scope.new_files = FileService.getFiles();
        },
        templateUrl: window.constants.ABS_URI + "partials/filegroup.html"
    }
})

.directive('fileuploader', function(Upload, FileService, $timeout){
    return {
        templateUrl: window.constants.ABS_URI + "partials/fileuploader.html",
        scope: true,
        link: function(scope, element, attrs){
            scope.uploadFile = function(file) {
                if( file && !file.$error) {
                    scope.file = FileService.NewFile(file.name);

                    scope.Uploader = Upload.upload({
                    url: window.constants.ABS_URI + 'file',
                        file: file,
                        method: 'POST',
                        fields: {
                            '_token': window.constants.CSRF_TOKEN
                        }
                    }).progress(function (evt){
                        var percentage = parseInt(100.0 * evt.loaded / evt.total);
                        scope.file.upload_bar_class = "progress-bar-info";
                        scope.file.progress = percentage;
                        scope.file.progressStatus = percentage + "%";
                        FileService.isUploading();
                    }).success(function(data, status, headers, config){
                        scope.file.upload_bar_class = "progress-bar-success";
                        scope.file.progressStatus = "Upload Completed";
                        scope.file.uploadedData = data;
                        FileService.removeDuplicateFile(scope.file);
                        $timeout(function(){ scope.progress = 0; }, 2000);
                        FileService.notUploading();
                    }).error(function (data, status, headers, config){
                        scope.file.upload_bar_class = "progress-bar-danger";
                        FileService.notUploading();
                        if(status == 400){
                            scope.file.progressStatus = "Error";
                        }else{
                            scope.file.progressStatus = "Canceled";
                        }
                        $timeout(function(){
                            FileService.removeFile(scope.file);
                        }, 2000);
                    });
                }
            }

            scope.cancelUpload = function(){
                scope.Uploader.abort();
                $timeout(function(){
                    scope.file.progress = 0;
                    scope.file.upload_bar_class = "progress-bar-danger";
                    scope.file.progressStatus = "Canceled";

                }, 2000)
            };
        } //-- EOF link
    }; //-- EOF directive return obj
});