var IndexApp = angular.module('IndexApp', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

IndexApp.factory('IndexFactory', function ($http) {
	return {
		getDistricts : function (p_id){
			return $http.get('/ajax/pdlwv/' + p_id);
		},
		getLlgs : function (p_id, d_id){
			return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id);
		},
		getWards : function (p_id, d_id, l_id){
			return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id);
		},
    getVillages : function (p_id, d_id, l_id, w_id){
      return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id + '/' + w_id);
    },
    getPrioritisations : function (search_info) {
      var _token = window.constants.CSRF_TOKEN;
      var keyword = search_info.keyword;
      var province = search_info.province;
      var district = search_info.district;
      var llg = search_info.llg;
      var ward = search_info.ward;

      return $http.post('/webapi/get_prioritisations', {
        _token   : _token,
        keyword  : keyword,
        province : province,
        district : district,
        llg      : llg,
        ward     : ward
      });
    },
    getApprovals : function (search_info) {
      var _token = window.constants.CSRF_TOKEN;
      var keyword = search_info.keyword;
      var province = search_info.province;
      var district = search_info.district;
      var llg = search_info.llg;

      return $http.post('/webapi/get_approvals', {
        _token   : _token,
        keyword  : keyword,
        province : province,
        district : district,
        llg      : llg
      });
    },
    getSubprojects : function (search_info) {
      var _token = window.constants.CSRF_TOKEN;
      var keyword = search_info.keyword;
      var province = search_info.province;
      var district = search_info.district;
      var llg = search_info.llg;
      var ward = search_info.ward;
      var village = search_info.village;

      return $http.post('/webapi/get_subprojects', {
        _token   : _token,
        keyword  : keyword,
        province : province,
        district : district,
        llg      : llg,
        ward     : ward,
        village  : village
      });
    },
	};
});

IndexApp.controller('P1IndexCtrl', function ($scope, IndexFactory) {
  $scope.pagePrioritisations = [];
  $scope.totalItems = 75;
  $scope.itemsPerPage = 15
  $scope.currentPage = 1;
  $scope.maxSize = 5;
  $scope.bigTotalItems = 175;
  $scope.bigCurrentPage = 1;

  $scope.updateDistricts =  function () {
    $scope.districts = [];
    $scope.district = undefined;
    $scope.llg = undefined;
    $scope.ward = undefined;
    if ($scope.province) {
      var p_id = $scope.province.id;

      IndexFactory.getDistricts(p_id).then(function(response){
        if (response.data) {
          $scope.districts = response.data.districts;
        }else {
          console.log('Warning: Get empty district list');
        }
      });
    }
    else {
      console.log('No selected province');
    }
  };

  $scope.updateLlgs =  function () {
    $scope.llgs = [];
    $scope.llg = undefined;
    $scope.ward = undefined;
    if ($scope.district) {
      var p_id = $scope.province.id;
      var d_id = $scope.district.id;

      IndexFactory.getLlgs(p_id, d_id).then(function(response){
        if (response.data) {
          $scope.llgs = response.data.districts[0].llgs;
        }else {
          console.log('Warning: Get empty llg list');
        }
      });
    }
    else {
      console.log('No selected district');
    }
  };

  $scope.updateWards =  function () {
    $scope.wards = [];
    $scope.ward = undefined;
    if ($scope.llg) {
      var p_id = $scope.province.id;
      var d_id = $scope.district.id;
      var l_id = $scope.llg.id;

      IndexFactory.getWards(p_id, d_id, l_id).then(function(response){
        if (response.data) {
          $scope.wards = response.data.districts[0].llgs[0].wards;
        }else {
          console.log('Warning: Get empty ward list');
        }
      });
    }
    else {
      console.log('No selected llg');
    }
  };

  $scope.pageChanged = function () {
    $scope.startItemIndex = ($scope.currentPage - 1) * $scope.itemsPerPage;

    $scope.endItemIndex = ( $scope.currentPage * $scope.itemsPerPage );
    if($scope.endItemIndex > $scope.totalItems)
    {
      $scope.endItemIndex = $scope.totalItems;
    }

    $scope.pagePrioritisations.length = 0;

    for(i = $scope.startItemIndex; i < $scope.endItemIndex; i++)
    {
      $scope.pagePrioritisations.push($scope.prioritisation_list[i]);
    }
  };

  $scope.searchPrioritisationByFilter = function () {
    var search_info = {
      'keyword'  : $scope.keyword,
      'province' : $scope.province,
      'district' : $scope.district,
      'llg'      : $scope.llg,
      'ward'     : $scope.ward,
    };

    IndexFactory.getPrioritisations(search_info).then(function(response) {
      $scope.prioritisation_list = response.data;
      $scope.totalItems     = response.data.length;
      $scope.pageChanged();
    });
  };

  (function init() {
    $scope.provinces = init_data.provinces;
    $scope.searchPrioritisationByFilter();
	})();
});

IndexApp.controller('P2IndexCtrl', function ($scope, IndexFactory) {
  $scope.pageApprovals = [];
  $scope.totalItems = 75;
  $scope.itemsPerPage = 15
  $scope.currentPage = 1;
  $scope.maxSize = 5;
  $scope.bigTotalItems = 175;
  $scope.bigCurrentPage = 1;

  $scope.updateDistricts =  function () {
    $scope.districts = [];
    $scope.district = undefined;
    $scope.llg = undefined;
    $scope.ward = undefined;
    if ($scope.province) {
      var p_id = $scope.province.id;

      IndexFactory.getDistricts(p_id).then(function(response){
        if (response.data) {
          $scope.districts = response.data.districts;
        }else {
          console.log('Warning: Get empty district list');
        }
      });
    }
    else {
      console.log('No selected province');
    }
  };

  $scope.updateLlgs =  function () {
    $scope.llgs = [];
    $scope.llg = undefined;
    $scope.ward = undefined;
    if ($scope.district) {
      var p_id = $scope.province.id;
      var d_id = $scope.district.id;

      IndexFactory.getLlgs(p_id, d_id).then(function(response){
        if (response.data) {
          $scope.llgs = response.data.districts[0].llgs;
        }else {
          console.log('Warning: Get empty llg list');
        }
      });
    }
    else {
      console.log('No selected district');
    }
  };

  $scope.pageChanged = function () {
    $scope.startItemIndex = ($scope.currentPage - 1) * $scope.itemsPerPage;

    $scope.endItemIndex = ( $scope.currentPage * $scope.itemsPerPage );
    if($scope.endItemIndex > $scope.totalItems)
    {
      $scope.endItemIndex = $scope.totalItems;
    }

    $scope.pageApprovals.length = 0;

    for(i = $scope.startItemIndex; i < $scope.endItemIndex; i++)
    {
      $scope.pageApprovals.push($scope.approval_list[i]);
    }
  };

  $scope.searchApprovalByFilter = function () {
    var search_info = {
      'keyword'  : $scope.keyword,
      'province' : $scope.province,
      'district' : $scope.district,
      'llg'      : $scope.llg
    };

    IndexFactory.getApprovals(search_info).then(function(response) {
      $scope.approval_list = response.data;
      $scope.totalItems     = response.data.length;
      $scope.pageChanged();
    });
  };

  (function init() {
    $scope.provinces = init_data.provinces;
    $scope.searchApprovalByFilter();
  })();
});

IndexApp.controller('P3IndexCtrl', function ($scope, IndexFactory) {
  $scope.pageSubprojects = [];
  $scope.totalItems = 75;
  $scope.itemsPerPage = 15
  $scope.currentPage = 1;
  $scope.maxSize = 5;
  $scope.bigTotalItems = 175;
  $scope.bigCurrentPage = 1;

  $scope.updateDistricts =  function () {
    $scope.districts = [];
    $scope.district = undefined;
    $scope.llg = undefined;
    $scope.ward = undefined;
    $scope.village = undefined;
    if ($scope.province) {
      var p_id = $scope.province.id;

      IndexFactory.getDistricts(p_id).then(function(response){
        if (response.data) {
          $scope.districts = response.data.districts;
        }else {
          console.log('Warning: Get empty district list');
        }
      });
    }
    else {
      console.log('No selected province');
    }
  };

  $scope.updateLlgs =  function () {
    $scope.llgs = [];
    $scope.llg = undefined;
    $scope.ward = undefined;
    $scope.village = undefined;
    if ($scope.district) {
      var p_id = $scope.province.id;
      var d_id = $scope.district.id;

      IndexFactory.getLlgs(p_id, d_id).then(function(response){
        if (response.data) {
          $scope.llgs = response.data.districts[0].llgs;
        }else {
          console.log('Warning: Get empty llg list');
        }
      });
    }
    else {
      console.log('No selected district');
    }
  };

  $scope.updateWards =  function () {
    $scope.wards = [];
    $scope.ward = undefined;
    $scope.village = undefined;
    if ($scope.llg) {
      var p_id = $scope.province.id;
      var d_id = $scope.district.id;
      var l_id = $scope.llg.id;

      IndexFactory.getWards(p_id, d_id, l_id).then(function(response){
        if (response.data) {
          $scope.wards = response.data.districts[0].llgs[0].wards;
        }else {
          console.log('Warning: Get empty ward list');
        }
      });
    }
    else {
      console.log('No selected llg');
    }
  };

  $scope.updateVillages =  function () {
    $scope.villages = [];
    $scope.village = undefined;
    if ($scope.ward) {
      var p_id = $scope.province.id;
      var d_id = $scope.district.id;
      var l_id = $scope.llg.id;
      var w_id = $scope.ward.id;

      IndexFactory.getVillages(p_id, d_id, l_id, w_id).then(function(response){
        if (response.data) {
          $scope.villages = response.data.districts[0].llgs[0].wards[0].villages;
        }else {
          console.log('Warning: Get empty village list');
        }
      });
    }
    else {
      console.log('No selected ward');
    }
  };

  $scope.pageChanged = function () {
    $scope.startItemIndex = ($scope.currentPage - 1) * $scope.itemsPerPage;

    $scope.endItemIndex = ( $scope.currentPage * $scope.itemsPerPage );
    if($scope.endItemIndex > $scope.totalItems)
    {
      $scope.endItemIndex = $scope.totalItems;
    }

    $scope.pageSubprojects.length = 0;

    for(i = $scope.startItemIndex; i < $scope.endItemIndex; i++)
    {
      $scope.pageSubprojects.push($scope.subproject_list[i]);
    }
  };

  $scope.searchSubprojectByFilter = function () {
    var search_info = {
      'keyword'  : $scope.keyword,
      'province' : $scope.province,
      'district' : $scope.district,
      'llg'      : $scope.llg,
      'ward'     : $scope.ward,
      'village'  : $scope.village
    };

    IndexFactory.getSubprojects(search_info).then(function(response) {
      $scope.subproject_list = response.data;
      $scope.totalItems     = response.data.length;
      $scope.pageChanged();
    });
  };

  (function init() {
    $scope.provinces = init_data.provinces;
    $scope.searchSubprojectByFilter();
  })();
});