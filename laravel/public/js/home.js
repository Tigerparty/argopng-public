var HomeApp = angular.module('HomeApp', ['leaflet-directive', 'highcharts-ng'])
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

HomeApp.factory('RequestFactory', function ($http) {
	return {
        getDistricts : function (p_id){
            return $http.get('/ajax/pdlwv/' + p_id);
        },
        getLlgs : function (p_id, d_id){
            return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id);
        },
        getWards : function (p_id, d_id, l_id){
            return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id);
        },
        getVillages : function (p_id, d_id, l_id, w_id){
            return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id + '/' + w_id);
        },
        getSubprojectsWithCoordinate : function (search_info) {
            var _token = window.constants.CSRF_TOKEN;
            var keyword = search_info.keyword;
            var province = search_info.province;
            var district = search_info.district;
            var llg = search_info.llg;
            var ward = search_info.ward;
            var village = search_info.village;

            return $http.post('/webapi/get_subprojects_with_coordinate', {
                _token   : _token,
                keyword  : keyword,
                province : province,
                district : district,
                llg      : llg,
                ward     : ward,
                village  : village
            });
        },
    };
});


HomeApp.service('MapService', function() {
	var that = this;

	this.map = {
		center: {
            lat: -9.9295,
            lng: 147.9751,
            zoom: 7
        },
        defaults: {
            tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",
            zoomControlPosition: 'topright',
            tileLayerOptions: {
                opacity: 0.9,
                detectRetina: true,
                reuseTiles: true,
            },
            scrollWheelZoom: false
        },
        markers: {
        },
        events: {}
	};

	this.syncMarkers = function (subprojects) {
        that.map.markers = {};
        that.subprojects = subprojects

        angular.forEach(that.subprojects, function (subproject, index) {
            if(subproject.recent_progress_report){
                if ('lat' in subproject.recent_progress_report && 'lng' in subproject.recent_progress_report) {
                    if(subproject.recent_progress_report.lat != null && subproject.recent_progress_report.lng)
                    {
                        that.map.markers[index] = {
                            lat : parseFloat(subproject.recent_progress_report.lat),
                            lng : parseFloat(subproject.recent_progress_report.lng),
                            icon: {
                                iconUrl: '/images/mapdot.png',
                                iconSize: [18, 18],
                                iconAnchor: [9, 18],
                                popupAnchor:  [0, -18]
                            },
                            message : '<mapbubble index="'+ index +'" ></mapbubble>'
                        };
                    }
                }
            }
        });
	};

    this.getSubprojectsByIdx = function(index) {
        that.subprojects[index].link = window.constants.ABS_URI + 'subproject/' + that.subprojects[index].id;
        if(that.subprojects[index].recent_image[0] && that.subprojects[index].recent_progress_report.recent_image[0])
        {
            if(that.subprojects[index].recent_image[0].id > that.subprojects[index].recent_progress_report.recent_image[0].id)
            {
                that.subprojects[index].pre_img = window.constants.ABS_URI + 'file/' + that.subprojects[index].recent_image[0].id;
            }
            else if(that.subprojects[index].recent_image[0].id < that.subprojects[index].recent_progress_report.recent_image[0].id)
            {
                that.subprojects[index].pre_img = window.constants.ABS_URI + 'file/' + that.subprojects[index].recent_progress_report.recent_image[0].id;
            }
        }
        else if(that.subprojects[index].recent_image[0] && !that.subprojects[index].recent_progress_report.recent_image[0])
        {
            that.subprojects[index].pre_img = window.constants.ABS_URI + 'file/' + that.subprojects[index].recent_image[0].id;
        }
        else if(!that.subprojects[index].recent_image[0] && that.subprojects[index].recent_progress_report.recent_image[0])
        {
            that.subprojects[index].pre_img = window.constants.ABS_URI + 'file/' + that.subprojects[index].recent_progress_report.recent_image[0].id;
        }
        else
        {
            that.subprojects[index].pre_img = window.constants.ABS_URI + 'images/project-thumb.png';
        }
        return that.subprojects[index];
    };

	this.initMap = function () {
		return that.map;
	};

});

HomeApp.controller('MapCtrl', function ($scope, $location, MapService, RequestFactory) {
    var scope = this;

    $scope.updateDistricts =  function () {
        $scope.districts = [];
        $scope.district = undefined;
        $scope.llg = undefined;
        $scope.ward = undefined;
        $scope.village = undefined;
        if ($scope.province) {
            var p_id = $scope.province.id;

            RequestFactory.getDistricts(p_id).then(function(response){
                if(response.data) {
                    $scope.districts = response.data.districts;
                }else{
                    console.log('Warning: Get empty district list');
                }
            });
        }
        else 
        {
            console.log('No selected province');
        }
    };

    $scope.updateLlgs =  function () {
        $scope.llgs = [];
        $scope.llg = undefined;
        $scope.ward = undefined;
        $scope.village = undefined;
        if ($scope.district) {
            var p_id = $scope.province.id;
            var d_id = $scope.district.id;

            RequestFactory.getLlgs(p_id, d_id).then(function(response){
                if(response.data) {
                    $scope.llgs = response.data.districts[0].llgs;
                }else{
                    console.log('Warning: Get empty llg list');
                }
            });
        }
        else 
        {
            console.log('No selected district');
        }
    };

    $scope.updateWards =  function () {
        $scope.wards = [];
        $scope.ward = undefined;
        $scope.village = undefined;
        if ($scope.llg) {
            var p_id = $scope.province.id;
            var d_id = $scope.district.id;
            var l_id = $scope.llg.id;

            RequestFactory.getWards(p_id, d_id, l_id).then(function(response){
                if(response.data) {
                    $scope.wards = response.data.districts[0].llgs[0].wards;
                }else{
                    console.log('Warning: Get empty ward list');
                }
            });
        }
        else
        {
            console.log('No selected llg');
        }
    };

    $scope.updateVillages =  function () {
        $scope.villages = [];
        $scope.village = undefined;
        if ($scope.ward) {
            var p_id = $scope.province.id;
            var d_id = $scope.district.id;
            var l_id = $scope.llg.id;
            var w_id = $scope.ward.id;

            RequestFactory.getVillages(p_id, d_id, l_id, w_id).then(function(response){
                if(response.data) {
                    $scope.villages = response.data.districts[0].llgs[0].wards[0].villages;
                }else{
                    console.log('Warning: Get empty village list');
                }
            });
        }
        else
        {
            console.log('No selected ward');
        }
    };

    scope.searchByFilter = function () {
        var search_info = {
            'keyword'  : $scope.keyword,
            'province' : $scope.province,
            'district' : $scope.district,
            'llg'      : $scope.llg,
            'ward'     : $scope.ward,
            'village'  : $scope.village
        };

        RequestFactory.getSubprojectsWithCoordinate(search_info).then(function(response) {
            $scope.subprojects = response.data;
            MapService.syncMarkers($scope.subprojects);
        });
    };

    scope.showMap = function() {
        scope.searchByFilter();
        scope.isShowMap = true;
    };

    scope.keyDown = function(event) {
        if(event.which === 13) {
            event.preventDefault();
            scope.showMap();
        }
    };

	(function init() {
        scope.isShowMap = false;
        $scope.provinces = window.init_data.provinces;
        scope.map = MapService.initMap();
        scope.searchByFilter();

        if($location.path() == '/map')
        {
            scope.showMap();
        }
	})();
});

HomeApp.controller('HomeChartCtrl', function ($scope) {
    $scope.tabslet_ds = window.init_data.tabslet_ds;

    $scope.beneficiariesChartsConfig = {
        options: {
            colors : ['#F4C079', '#CB6A14'],
            chart : {
                backgroundColor : '#f8e0c6'
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                layout: 'vertical',
                symbolHeight: 12,
                symbolWidth: 12,
            },
        },
        title: {
            text: '',
        },
        size: {
            "height": "220"
        },
        series: [{
            type: 'pie',
            center: ['50%', '53%'],
            size: '100%',
            showInLegend: true,
            tooltip: {
                pointFormat: '<b>{point.x}</b><br/>',
            },
            dataLabels: {
                distance: -30,
                textShadow: 'none'
            },
            data: [
                {
                    name: 'Female',
                    x: parseInt($scope.tabslet_ds.total_of_bnf.fmp_amount),
                    y: parseInt($scope.tabslet_ds.total_of_bnf.fmp_amount),
                    dataLabels: {
                        enabled: true,
                        format: '{y}',
                        color: 'black',
                        style: {color: "white", textShadow: "none", fontSize: '14px'}
                    }
                },
                {
                    name: 'Male',
                    x: parseInt($scope.tabslet_ds.total_of_bnf.mp_amount),
                    y: parseInt($scope.tabslet_ds.total_of_bnf.mp_amount),
                    dataLabels: {
                        enabled: true,
                        format: '{y}',
                        color: 'white',
                        style: {color: "white", textShadow: "none", fontSize: '14px'}
                    }
                }
            ]
        }]
    };

    $scope.sectorChartConfig = {
        "options": {
            "chart": {
                "height": 250,
                "type": "areaspline",
                "backgroundColor": "#f8e0c6",
            },
            "plotOptions": {
                "series": {
                    "stacking": ""
                }
            },
            "legend": {
                "enabled": false
            },
        },
        "series": [
            {
                "name": "Number of Subprojects",
                "data": [],
                "type": "column",
                "color": "#CB6A14"
            }
        ],
        "xAxis": {
            "categories": []
        },
        "yAxis": {
            "allowDecimals": false,
            "tickAmount": 5,
            "title": {
                "text": "Number of Subprojects"
            },
            "minValue": 0,
        },
        "title": {
            "text": null
        },
        "credits": {
            "enabled": false
        },
        "loading": false,
        "size": {}
    };

    $scope.provinceCompletionChartConfig = {
        "options": {
            "chart": {
                "height": 250,
                "type": "areaspline",
                "backgroundColor": "#f8e0c6",
            },
            "plotOptions": {
                "series": {
                    "stacking": ""
                }
            },
            "legend": {
                "enabled": false
            },
        },
        "series": [
            {
                "name": "Complete Percentage",
                "data": [],
                "type": "column",
                "color": "#CB6A14"
            }
        ],
        "xAxis": {
            "categories": []
        },
        "yAxis": {
            "allowDecimals": false,
            "tickAmount": 5,
            "min": 0,
            "max": 100,
            "title": {
                "text": "Complete Percentage (%)"
            }
        },
        "title": {
            "text": null
        },
        "credits": {
            "enabled": false
        },
        "loading": false,
        "size": {}
    };

    $scope.fundChartsConfig = {
        options: {
            colors : ['#F4C079', '#CB6A14'],
            chart : {
                backgroundColor : '#f8e0c6'
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                layout: 'vertical',
                symbolHeight: 12,
                symbolWidth: 12,
            },
        },
        title: {
            text: '',
        },
        size: {
            "height": "220"
        },
        series: [{
            type: 'pie',
            center: ['50%', '53%'],
            size: '100%',
            showInLegend: true,
            tooltip: {
                pointFormat: '<b>{point.x}</b><br/>',
            },
            dataLabels: {
                distance: -30,
                textShadow: 'none'
            },
            data: []
        }]
    };

    for (var i = 0; i < $scope.tabslet_ds.sps_of_sector.length; i++) {
        $scope.sectorChartConfig.xAxis.categories[i] = i + 1;
        $scope.sectorChartConfig.series[0].data[i] = $scope.tabslet_ds.sps_of_sector[i].sp_count;
    }

    angular.forEach($scope.tabslet_ds.completion_of_provinces, function(province, key) {
        $scope.provinceCompletionChartConfig.xAxis.categories.push(province.name);
        var percentage = 0
        if(province.total_activity_count > 0)
        {
            percentage = (province.completed_activity_count/province.total_activity_count*100).toFixed(0);
        }
        $scope.provinceCompletionChartConfig.series[0].data.push(parseInt(percentage));
    });

    $scope.fund_total = 0;
    angular.forEach($scope.tabslet_ds.total_of_ctbr, function(fund, key) {
        $scope.fund_total += parseInt(fund.amount);

        var data = {
            name: fund.fund_type.toUpperCase(),
            x: parseInt(fund.amount),
            y: parseInt(fund.amount),
            dataLabels: {
                enabled: true,
                format: '{y}',
                color: fund.fund_type=='rdp' ? 'black' : 'white',
                style: {color: "white", textShadow: "none", fontSize: '14px'}
            }
        }

        $scope.fundChartsConfig.series[0].data.push(data);
    });
    $scope.fundChartsConfig.series[0].data.reverse();
});

HomeApp.directive('mapbubble', function (MapService) {
    return {
        restrict: "E",
        scope: {
            index: "="
        },
        link: function(scope, element, attrs) {
            scope.subprojects = MapService.getSubprojectsByIdx(scope.index);
        },
        templateUrl: window.constants.ABS_URI + 'partials/map_bubble.html'
    }
});


//-- JQuery Chart.js

function drawBarChart(canvasId, dataObj){
    var labels = Object.keys(dataObj);
    var data = labels.map(function(key){return dataObj[key];});

    var options = {
      animation:false,
        scaleBeginAtZero : true,
        barShowStroke : true,
        barStrokeWidth : 1,
        scaleShowHorizontalLines: false,
        scaleShowVerticalLines: false,
    };

    var data = {
        labels: labels,
        datasets: [{
                label: "My First dataset",
                fillColor: "rgba(203,106,20,1)",
                strokeColor: "rgba(162,163,89,1)",
                data: data
        }]
    };
    var ctx = document.getElementById(canvasId).getContext("2d");
    var barchart = new Chart(ctx).Bar(data, options);

}

function drawPieChart(canvasId, legend, dataObj ){
    var labels = Object.keys(dataObj);
    var data = labels.map(function(key){return dataObj[key];});

    var options = {
        animation: false,
        animateScale: true,
        animationEasing: 'easeOutBounce',
        segmentShowStroke: true,
        segmentStrokeColor: '#AD9D87',
        segmentStrokeWidth: 1,
        legendTemplate : '<table><%for (var i=0; i<segments.length; i++){%><tr><td><div class="pie-legend" style="background-color:<%=segments[i].fillColor%>"></div></td><td><%if(segments[i].label){%><%=segments[i].label%><%}%></td></tr><%}%><tr><td>Total:</td><td><%=total%></td></tr></table>'

    };

    var ctx = document.getElementById(canvasId).getContext("2d");
    var piechart = new Chart(ctx).Pie(dataObj, options);
    document.getElementById(legend).innerHTML = piechart.generateLegend();
}