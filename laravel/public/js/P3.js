var P3App = angular.module('P3App', ['ngRoute', 'ui.bootstrap', 'AttachUploader'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

P3App.factory('P3Factory', function ($http) {
	return {
		getDistricts : function (p_id){
			return $http.get('/ajax/pdlwv/' + p_id);
		},
		getLlgs : function (p_id, d_id){
			return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id);
		},
		getWards : function (p_id, d_id, l_id){
			return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id);
		},
    getVillages : function (p_id, d_id, l_id, w_id){
      return $http.get('/ajax/pdlwv/' + p_id + '/' + d_id + '/' + l_id + '/' + w_id);
    },
    getWardPrioritisation : function (ward_id){
      return $http.get('/ajax/get_ward_prioritisation/' + ward_id);
    },
    deleteReport : function (subproject_id, report_id){
      var _token = window.constants.CSRF_TOKEN;

      return $http.post('/subproject/' + subproject_id + '/report/' + report_id, {
        _token        : _token,
        _method       : 'delete',
        subproject_id : subproject_id,
        report_id     : report_id
      });
    },
	};
});

P3App.controller('P3ShowCtrl', function ($scope, P3Factory) {
  $scope.deleteReport = function (subproject_id, report_id, index){
    P3Factory.deleteReport(subproject_id, report_id).then(function(response) {
      if (response.data == 'success') {
        $scope.reports.splice(index, 1);
      }
    });
  };

  (function init() {
    $scope.reports = init_data.reports;
  })();
});

P3App.controller('P3EditCtrl', function ($scope, P3Factory) {
  function CounterpartFunding(source, origin_amount, has_commitment){
    this.source = source || null;
    this.origin_amount = parseInt(origin_amount) || null;
    this.has_commitment = has_commitment || null;
  }

  $scope.addCounterpartFunding = function () {
    $scope.counterpart_fundings.push(new CounterpartFunding());
  };

  $scope.CredentialPreparedDatePickerOpened = false;
  $scope.CredentialConfirmedDatePickerOpened = false;
  $scope.CredentialCertifiedDatePickerOpened = false;

  $scope.openCredentialPreparedDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.CredentialPreparedDatePickerOpened = true;
  };

  $scope.openCredentialConfirmedDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.CredentialConfirmedDatePickerOpened = true;
  };

  $scope.openCredentialCertifiedDatePicker = function ($event, $index) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.CredentialCertifiedDatePickerOpened = true;
  };

  $scope.updateDistricts =  function () {
    $scope.districts = [];
    $scope.district_id = undefined;
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    if ($scope.province_id !== undefined) {
      var p_id = $scope.province_id;

      P3Factory.getDistricts(p_id).then(function(response){
        if (response.data) {
          $scope.districts = response.data.districts;
        }else {
          console.log('Warning: Get empty district list');
        }
      });
    }
    else {
      console.log('No selected province');
    }
  };

  $scope.updateLlgs =  function () {
    $scope.llgs = [];
    $scope.llg_id = undefined;
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    if ($scope.district_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;

      P3Factory.getLlgs(p_id, d_id).then(function(response){
        if (response.data) {
          $scope.llgs = response.data.districts[0].llgs;
        }else {
          console.log('Warning: Get empty llg list');
        }
      });
    }
    else {
      console.log('No selected district');
    }
  };

  $scope.updateWards =  function () {
    $scope.wards = [];
    $scope.ward_id = undefined;
    $scope.village_id = undefined;
    if ($scope.llg_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;
      var l_id = $scope.llg_id;

      P3Factory.getWards(p_id, d_id, l_id).then(function(response){
        if (response.data) {
          $scope.wards = response.data.districts[0].llgs[0].wards;
        }else {
          console.log('Warning: Get empty ward list');
        }
      });
    }
    else {
      console.log('No selected llg');
    }
  };

  $scope.updateVillages =  function () {
    $scope.villages = [];
    $scope.village_id = undefined;
    if ($scope.ward_id !== undefined) {
      var p_id = $scope.province_id;
      var d_id = $scope.district_id;
      var l_id = $scope.llg_id;
      var w_id = $scope.ward_id;

      P3Factory.getVillages(p_id, d_id, l_id, w_id).then(function(response){
        if (response.data) {
          $scope.villages = response.data.districts[0].llgs[0].wards[0].villages;
          $scope.appraisal_committee = response.data.districts[0].llgs[0].wards[0].name;
        }else {
          console.log('Warning: Get empty village list');
        }
      });
    }
    else {
      console.log('No selected ward');
    }
  };

  $scope.addWorkActivity = function (stg_idx) {
    var activity_num = $scope.subproject.work_stages[stg_idx].work_activities.length + 1;
    if ( activity_num <= 5 ) {
      var activity = {
        'description' : "",
        'completed' : 0,
      };
      $scope.subproject.work_stages[stg_idx].work_activities.push(activity);
    }
    else {
      alert("Work Activity can not above to 5");
    }
  };

  $scope.deleteWorkActivity = function (stg_idx, act_idx) {
    $scope.subproject.work_stages[stg_idx].work_activities.splice(act_idx, 1);
  };

  $scope.addWorkStage = function () {
    var stage_num = $scope.subproject.work_stages.length + 1;
    if ( stage_num <= 10 ) {
      var work_stage = {
        'stage'          : "Stage" + stage_num,
        'description'    : "Stage_Description" + stage_num,
        'work_activities' : []
      };
      $scope.subproject.work_stages.push(work_stage);
    }
    else {
      alert("Work Stage can not above to 10");
    }
  };

  $scope.deleteWorkStage = function (index) {
    $scope.subproject.work_stages.splice(index, 1);
  };

  $scope.deleteAttachment = function(index) {
    $scope.attachments.splice(index, 1);
  };

  $scope.calculateTotalActualAmount = function() {
    $scope.totalActualAmount = 0;
    if(!$scope.subproject.approval)
    {
      return false;
    }

    if($scope.subproject.approval.rdp_fund){
      $scope.subproject.approval.rdp_fund.accuate_amount ?
      $scope.totalActualAmount += parseFloat($scope.subproject.approval.rdp_fund.accuate_amount) :
      $scope.totalActualAmount += 0;
    }
    if($scope.subproject.approval.community_fund){
      $scope.subproject.approval.community_fund.accuate_amount ?
      $scope.totalActualAmount += parseFloat($scope.subproject.approval.community_fund.accuate_amount) :
      $scope.totalActualAmount += 0;
    }

    angular.forEach($scope.subproject.approval.counterpart_funds, function(counterpart_fund, index){
      if($scope.isFinite(parseInt(counterpart_fund.accuate_amount))){
        $scope.totalActualAmount += parseFloat(counterpart_fund.accuate_amount);
      }
    });

    // angular.forEach($scope.counterpart_fundings, function(counterpart_funding, index){
    //   if($scope.isFinite(parseInt(counterpart_funding.origin_amount))){
    //     $scope.totalAmount += parseInt(counterpart_funding.origin_amount);
    //   }
    // });

    $scope.totalActualAmountPercent = ($scope.totalActualAmount/$scope.subproject.fund_estimate_new*100);
  };

  $scope.calculateTotalOriginAmount = function() {
    $scope.totalOriginAmount = 0;
    if(!$scope.subproject.approval)
    {
      return false;
    }

    if($scope.subproject.approval.rdp_fund){
      $scope.totalOriginAmount += parseFloat($scope.subproject.approval.rdp_fund.origin_amount);
    }
    if($scope.subproject.approval.community_fund){
      $scope.totalOriginAmount += parseFloat($scope.subproject.approval.community_fund.origin_amount);
    }

    angular.forEach($scope.subproject.approval.counterpart_funds, function(counterpart_fund, index){
      if($scope.isFinite(parseFloat(counterpart_fund.origin_amount))){
        $scope.totalOriginAmount += parseInt(counterpart_fund.origin_amount);
      }
    });

    angular.forEach($scope.counterpart_fundings, function(counterpart_funding, index){
      if($scope.isFinite(parseFloat(counterpart_funding.origin_amount))){
        $scope.totalOriginAmount += parseFloat(counterpart_funding.origin_amount);
      }
    });
  };

  $scope.isFinite = function(value) {
    return isFinite(value);
  };

  $scope.amountRound = function(value){
    return (Math.round(value*100)/100).toFixed(2);
  };

  (function init() {
    $scope.subproject = init_data.subproject;
    $scope.attachments = init_data.attachments;
    $scope.provinces = init_data.provinces;

    //Origin Input & Old Input
    $scope.province_id = init_data.locations[0];
    $scope.updateDistricts();
    $scope.district_id = init_data.locations[1];
    $scope.updateLlgs();
    $scope.llg_id = init_data.locations[2];
    $scope.updateWards();
    $scope.ward_id = init_data.locations[3];
    $scope.updateVillages();
    $scope.village_id = init_data.locations[4];

    $scope.counterpart_fundings = [];
    for (var i = 0; i < init_data.counterpart_fundings.length; i++) {
      $scope.counterpart_fundings.push(new CounterpartFunding(
        init_data.counterpart_fundings[i].source,
        init_data.counterpart_fundings[i].origin_amount,
        init_data.counterpart_fundings[i].has_commitment
      ))
    };
    if($scope.counterpart_fundings.length == 0){
      $scope.counterpart_fundings.push(new CounterpartFunding());
    }

    $scope.calculateTotalOriginAmount();
    $scope.calculateTotalActualAmount();
  })();
});

P3App.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});