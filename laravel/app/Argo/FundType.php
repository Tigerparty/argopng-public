<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class FundType extends Model
{
  protected $table = 'fund_type';

  protected $primaryKey = 'fund_type'

  public $timestamps = false;

  public function funds()
  {
  	return $this->hasMany('App\Argo\Fund', 'fund_type')
  }
}
