<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class WorkActivity extends Model
{
    protected $table = 'work_activity';

    public $timestamps = false;

    public function work_stage()
    {
        return $this->belongsTo('App\Argo\WorkStage');
    }

    public function report_modify()
    {
        return $this->belongsToMany('App\Argo\ProgressReport', 'relation_report_modify_activity', 'activity_id', 'report_id');
    }
}
