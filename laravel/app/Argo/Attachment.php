<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
  protected $table = 'attachment';

  public function progress_reports()
  {
    return $this->morphedByMany('App\Argo\ProgressReport', 'attachable');
  }

  public function subprojects()
  {
    return $this->morphedByMany('App\Argo\Subproject', 'attachable');
  }

  public function approvals()
  {
    return $this->morphedByMany('App\Argo\Approval', 'attachable');
  }

  public function prioritisations()
  {
    return $this->morphedByMany('App\Argo\Prioritisation', 'attachable');
  }
}
