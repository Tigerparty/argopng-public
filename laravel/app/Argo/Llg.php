<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Llg extends Model
{
  protected $table = 'llg';

  public $timestamps = false;

  public function district()
  {
  	return $this->belongsTo('App\Argo\District');
  }

  public function wards()
  {
  	return $this->hasMany('App\Argo\Ward');
  }
}
