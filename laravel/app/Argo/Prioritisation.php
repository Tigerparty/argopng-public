<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Prioritisation extends Model
{
  protected $table = 'prioritisation';

  public function ward()
  {
  	return $this->belongsTo('App\Argo\Ward');
  }

  public function need_identify()
  {
  	return $this->belongsTo('App\Argo\NeedIdentify');
  }

  public function user()
  {
  	return $this->belongsTo('App\User');
  }

  public function prioritisation_places()
  {
  	return $this->hasMany('App\Argo\PrioritisationPlace');
  }

  public function prioritisation_needs()
  {
  	return $this->hasMany('App\Argo\PrioritisationNeed');
  }

  public function attachments()
  {
    return $this->morphToMany('App\Argo\Attachment', 'attachable');
  }

  public function images()
  {
    return $this->morphToMany('App\Argo\Attachment', 'attachable')->where('type', 'LIKE', 'image/%');
  }
}
