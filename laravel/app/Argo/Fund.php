<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Fund extends Model
{
  protected $table = 'fund';

  public $timestamps = false;

  public function approval()
  {
  	return $this->belongsTo('App\Argo\Approval');
  }

  public function fund_type()
  {
  	return $this->belongsTo('App\Argo\FundType', 'fund_type');
  }
}
