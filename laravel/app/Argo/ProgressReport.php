<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class ProgressReport extends Model
{
    protected $table = 'progress_report';

    public $timestamps = true;

    public function subproject()
    {
        return $this->belongsTo('App\Argo\Subproject');
    }

    public function modify_activities()
    {
        return $this->belongsToMany('App\Argo\WorkActivity', 'relation_report_modify_activity', 'report_id', 'activity_id');
    }

    public function attachments()
    {
        return $this->morphToMany('App\Argo\Attachment', 'attachable');
    }

    public function images()
    {
        return $this->morphToMany('App\Argo\Attachment', 'attachable')->where('type', 'LIKE', 'image/%');
    }

    public function recent_image()
    {
        return $this->morphToMany('App\Argo\Attachment', 'attachable')->where('type', 'LIKE', 'image/%')->orderBy('id', 'DESC');
    }
}
