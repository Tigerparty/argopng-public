<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = 'village';

    public $timestamps = false;

    public function subprojects()
    {
        return $this->hasMany('App\Argo\Subproject');
    }

    public function ward()
    {
        return $this->belongsTo('App\Argo\Ward');
    }
}
