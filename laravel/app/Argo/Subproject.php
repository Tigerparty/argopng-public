<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;
use DB;
use DateTime;

class Subproject extends Model
{
    protected $table = 'subproject';

    public $timestamps = true;

    public function approval()
    {
        return $this->belongsTo('App\Argo\Approval');
    }

    public function progress_reports()
    {
        return $this->hasMany('App\Argo\ProgressReport');
    }

    public function recent_progress_report()
    {
        return $this->hasOne('App\Argo\ProgressReport')->orderBy('updated_at', 'DESC');
    }

    public function issues()
    {
        return $this->hasMany('App\Argo\Issue');
    }

    public function work_stages()
    {
        return $this->hasMany('App\Argo\WorkStage');
    }

    public function village()
    {
        return $this->belongsTo('App\Argo\Village');
    }

    public function attachments()
    {
        return $this->morphToMany('App\Argo\Attachment', 'attachable');
    }

    public function images()
    {
        return $this->morphToMany('App\Argo\Attachment', 'attachable')->where('type', 'LIKE', 'image/%');
    }

    public function recent_image()
    {
        return $this->morphToMany('App\Argo\Attachment', 'attachable')->where('type', 'LIKE', 'image/%')->orderBy('id', 'DESC');
    }

    public function scopeGetCompleteActivityCount($query, $subproject_id)
    {
        return DB::table($this->table.' AS p')
            ->select(DB::raw('
                a.id AS activity_id
            '))
            ->leftjoin(DB::raw('work_stage AS s'), 'p.id', '=', 's.subproject_id')
            ->leftjoin(DB::raw('work_activity AS a'), 's.id', '=', 'a.stage_id')
            ->where('p.id', '=', $subproject_id);
    }

    public function scopeGetTotalCompletedActivityCount($query, $subproject_id)
    {
        return DB::table($this->table.' AS p')
            ->select(DB::raw('
            a.id AS activity_id
            '))
            ->leftjoin(DB::raw('work_stage AS s'), 'p.id', '=', 's.subproject_id')
            ->leftjoin(DB::raw('work_activity AS a'), 's.id', '=', 'a.stage_id')
            ->where('p.id', '=', $subproject_id)
            ->where('a.completed', '=', '1');
    }

    public function scopeGetPreviousThirtyDaysCompletedActivityCount($query, $subproject_id)
    {
        $date = new DateTime;
        $date->modify('1 month ago');
        $thirtyDaysAgoDate = $date->format('Y-m-d');

        return DB::table($this->table.' AS sp')
            ->select(DB::raw('
            a.id AS activity_id
            '))
            ->leftjoin(DB::raw('work_stage AS s'), 'sp.id', '=', 's.subproject_id')
            ->leftjoin(DB::raw('work_activity AS a'), 's.id', '=', 'a.stage_id')
            ->where('sp.id', '=', $subproject_id)
            ->where('a.completed', '=', '1')
            ->where('a.completed_at', '>', $thirtyDaysAgoDate);
    }
}
