<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class SelectionMethod extends Model
{
	protected $table = 'selection_method';

	protected $primaryKey = 'name';

	public $timestamps = false;

	public function approvals()
	{
		return $this->hasMany('App\Argo\Approval', 'selection_method');
	}
}
