<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class NeedIdentify extends Model
{
  protected $table = 'need_identify';

  public $timestamps = false;

  public function prioritisations()
  {
  	return $this->hasMany('App\Argo\Prioritisation');
  }
}
