<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
  protected $table = 'approval';

  public function selection_method()
  {
  	return $this->belongsTo('App\Argo\SelectionMethod', 'selection_method', 'name');
  }

  public function user()
  {
  	return $this->belongsTo('App\User');
  }

  public function subproject()
  {
  	return $this->hasMany('App\Argo\Subproject', 'approval_id');
  }

  public function funds()
  {
  	return $this->hasMany('App\Argo\Fund');
  }

  public function rdp_fund()
  {
    return $this->hasOne('App\Argo\Fund', 'approval_id')->where('fund_type', '=', 'RSDLGP');
  }

  public function community_fund()
  {
    return $this->hasOne('App\Argo\Fund', 'approval_id')->where('fund_type', '=', 'Community');
  }

  public function counterpart_funds()
  {
    return $this->hasMany('App\Argo\Fund', 'approval_id')->where('fund_type', '=', 'counterpart');
  }

  public function attachments()
  {
    return $this->morphToMany('App\Argo\Attachment', 'attachable');
  }

  public function images()
  {
    return $this->morphToMany('App\Argo\Attachment', 'attachable')->where('type', 'LIKE', 'image/%');
  }
}
