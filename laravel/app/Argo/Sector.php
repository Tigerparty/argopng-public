<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
  protected $table = 'sector';

  public $timestamps = false;

  public function prioritisations()
  {
  	return $this->hasMany('App\Argo\Prioritisation');
  }

  public function prioritisation_needs()
  {
  	return $this->hasMany('App\Argo\PrioritisationNeed');
  }
}
