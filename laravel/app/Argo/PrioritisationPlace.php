<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class PrioritisationPlace extends Model
{
  protected $table = 'prioritisation_place';

  public $timestamps = false;

  public function prioritisation()
  {
  	return $this->belongsTo('App\Argo\Prioritisation');
  }
}
