<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class WorkStage extends Model
{
    protected $table = 'work_stage';

    public $timestamps = false;

    public function work_activities()
    {
        return $this->hasMany('App\Argo\WorkActivity', 'stage_id');
    }

    public function subproject()
    {
        return $this->belongsTo('App\Argo\Subproject');
    }
}
