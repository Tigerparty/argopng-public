<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $table = 'issue';

    public $timestamps = true;

    public function subproject()
    {
        return $this->belongsTo('App\Argo\Subproject');
    }
}
