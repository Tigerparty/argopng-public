<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
  protected $table = 'ward';

  public $timestamps = false;

  public function llg()
  {
  	return $this->belongsTo('App\Argo\Llg');
  }

  public function villages()
  {
  	return $this->hasMany('App\Argo\Village');
  }

  public function prioritisations()
  {
  	return $this->hasMany('App\Argo\Prioritisation');
  }
}