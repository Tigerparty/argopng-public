<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class PrioritisationNeed extends Model
{
  protected $table = 'prioritisation_need';

  public $timestamps = false;

  public function prioritisation()
  {
  	return $this->belongsTo('App\Argo\Prioritisation');
  }

  public function sector()
  {
  	return $this->belongsTo('App\Argo\Sector');
  }
}
