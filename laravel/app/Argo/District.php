<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
  protected $table = 'district';

  public $timestamps = false;

  public function province()
  {
  	return $this->belongsTo('App\Argo\Province');
  }

  public function llgs()
  {
  	return $this->hasMany('App\Argo\Llg');
  }
}