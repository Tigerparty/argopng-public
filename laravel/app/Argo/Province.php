<?php

namespace App\Argo;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
  protected $table = 'province';

  public $timestamps = false;

  public function districts()
  {
  	return $this->hasMany('App\Argo\District');
  }
}
