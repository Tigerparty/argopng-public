<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'csrf'], function()
{
  Route::group(['middleware' => 'auth'], function()
  {
  	//-- P1 controller
  	Route::get('/prioritisation/create', 'P1Controller@create');
  	Route::post('/prioritisation/', 'P1Controller@store');
    Route::get('/prioritisation/{prioritisation_id}/edit', 'P1Controller@edit');
    Route::put('/prioritisation/{prioritisation_id}', 'P1Controller@update');

    //-- P2 controller
    Route::get('/approval/create', 'P2Controller@create');
    Route::post('/approval', 'P2Controller@store');
    Route::get('/approval/{approval_id}/edit', 'P2Controller@edit');
    Route::put('/approval/{approval_id}', 'P2Controller@update');

    //-- P3 controller
    Route::get('/subproject/{subproject_id}/edit', 'P3Controller@edit');
    Route::put('/subproject/{subproject_id}', 'P3Controller@update');

    //-- P5 controller
    Route::get('/subproject/{subproject_id}/report/create', 'P5Controller@create');
    Route::post('/subproject/{subproject_id}/report', 'P5Controller@store');
    Route::get('/subproject/{subproject_id}/report/{report_id}', 'P5Controller@show');
    Route::get('/subproject/{subproject_id}/report/{report_id}/edit', 'P5Controller@edit');
    Route::put('/subproject/{subproject_id}/report/{report_id}', 'P5Controller@update');
    Route::delete('/subproject/{subproject_id}/report/{report_id}', 'P5Controller@destroy');

    //-- Attachment controller
    Route::post('/file',                  'AttachmentController@checkFile');
	});

	//-- About RDP
	Route::get('/about', 'AboutController@showAboutRDP');

	//-- P1 controller
  Route::get('/prioritisation', 'P1Controller@index');
	Route::get('/prioritisation/{prioritisation_id}', 'P1Controller@show');

  //-- P2 controller
  Route::get('/approval', 'P2Controller@index');
  Route::get('/approval/{approval_id}', 'P2Controller@show');

  //-- P3 controller
  Route::get('/subproject', 'P3Controller@index');
  Route::get('/subproject/{subproject_id}', 'P3Controller@show');

  //-- Attachment controller
  Route::get('/file/{ath_id}',          'AttachmentController@access');
  Route::get('/file/{ath_id}/download', 'AttachmentController@download');

	//-- Login controller
	Route::get('/login', 'LoginController@showLogin');
  Route::post('/login', 'LoginController@doLogin');
  Route::get('/logout', 'LoginController@doLogout');

  Route::get('/', 'HomeController@Home');

  //-- Ajax contoller
  Route::get('/ajax/pdlwv/{province_id}', 'AjaxController@getProvinceWithDistricts');
  Route::get('/ajax/pdlwv/{province_id}/{district_id}', 'AjaxController@getProvinceWithDistrictsLlgs');
  Route::get('/ajax/pdlwv/{province_id}/{district_id}/{llg_id}', 'AjaxController@getProvinceWithDistrictsLlgsWards');
  Route::get('/ajax/pdlwv/{province_id}/{district_id}/{llg_id}/{ward_id}', 'AjaxController@getProvinceWithDistrictsLlgsWardsVillages');
  Route::get('/ajax/get_ward_prioritisation/{ward_id}', 'AjaxController@getWardPrioritisation');

  //-- Webapi controller
  Route::post('/webapi/get_prioritisations', 'WebapiController@getPrioritisations');
  Route::post('/webapi/get_approvals', 'WebapiController@getApprovals');
  Route::post('/webapi/get_subprojects', 'WebapiController@getSubprojects');
  Route::post('/webapi/get_subprojects_with_coordinate', 'WebapiController@getSubprojectsWithCoordinate');

  //-- Api controller
  Route::get('/api/data_sync', 'ApiController@DataSync');
  Route::post('/api/submit_report', 'ApiController@SubmitReport');
});