<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use DateTime;

use App\Services\WorkStageActivityService;
use App\Services\FundService;
use App\Argo\Subproject;
use App\Argo\Village;
use App\Argo\Province;
use App\Argo\Fund;

class P3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::orderBy('name')->get();

        $array_data = array(
            'provinces' => $provinces,
        );

        return view('P3.index', $array_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subproject = Subproject::with(array(
                'approval' => function ($query){
                    $query->select(['id', 'fund_estimate_origin']);
                    $query->with(['rdp_fund', 'community_fund', 'counterpart_funds']);
                },
                'work_stages' => function ($query) {
                    $query->with(array(
                        'work_activities' => function ($query) {
                            $query->select(['id', 'stage_id', 'description', 'completed']);
                        }
                    ));
                },
                'attachments',
                'progress_reports' => function ($query){
                    $query->with(array(
                        'images' => function ($query){
                            $query->max('attachment.id');
                            $query->select('attachment.id');
                        }
                    ));
                    $query->select(['id', 'subproject_id', 'monitoring_date'])
                        ->orderBy('updated_at', 'DESC');
                }
            ))
            ->findOrFail($id);

        $village = Village::with([
                'ward' => function($query){
                    $query->with([
                        'llg' => function($query){
                            $query->with([
                                'district' => function($query){
                                    $query->with('province');
                                }
                            ]);
                        }
                    ]);
                }
            ])
            ->find($subproject->village_id);

        $activity_total = Subproject::GetCompleteActivityCount($id)->count();
        $completition_activity_total = Subproject::GetTotalCompletedActivityCount($id)->count();
        $completition_activity_recent = Subproject::GetPreviousThirtyDaysCompletedActivityCount($id)->count();
        $completition_activity_old = $completition_activity_total - $completition_activity_recent;

        $completition_percent = ($activity_total == 0) ? 0 : round( (float)$completition_activity_total / (float)$activity_total, 2) * 100;
        $completition_recent_percent = ($completition_activity_recent == 0) ? 0 : round( (float)$completition_activity_recent / (float)$activity_total, 2) * 100;
        $completition_old_percent = ($completition_activity_recent == 0) ? $completition_percent : round( (float)$completition_activity_old / (float)$activity_total, 2) * 100;

        $array_data = array(
            'subproject' => $subproject,
            'village' => $village,
            'activity_total' => $activity_total,
            'completition_percent' => $completition_percent,
            'completition_recent_percent' => $completition_recent_percent,
            'completition_old_percent' => $completition_old_percent,
        );

        return view('P3.show', $array_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subproject = Subproject::with(array(
                'approval' => function($query){
                    $query->select(['id', 'fund_estimate_origin']);
                    $query->with(['rdp_fund', 'community_fund', 'counterpart_funds']);
                },
                'work_stages' => function ($query) {
                    $query->with(array(
                        'work_activities' => function ($query) {
                            $query->select(['id', 'stage_id', 'description', 'completed']);
                        }
                    ));
                },
                'attachments',
            ))
            ->findOrFail($id);

        $attachments = $subproject->attachments;

        $provinces = Province::orderBy('name')->get();
        $location = Village::with([
                'ward' => function($query){
                    $query->with([
                        'llg' => function($query){
                            $query->with([
                                'district' => function($query){
                                    $query->with('province');
                                }
                            ]);
                        }
                    ]);
                }
            ])
            ->find($subproject->village_id);

        $location_ids = array(
            (string)$location->ward->llg->district->province->id,
            (string)$location->ward->llg->district->id,
            (string)$location->ward->llg->id,
            (string)$location->ward->id,
            (string)$location->id
        );

        $array_data = array(
            'subproject' => $subproject,
            'attachments' => $attachments,
            'provinces' => $provinces,
            'locations' => $location_ids,
        );
        
        return view('P3.edit', $array_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validator_rules($request), $this->validator_messages($request));

        try
        {
            DB::beginTransaction();

            $subproject = Subproject::find($id);
            $subproject->name = $request->input('subproject.name');
            $subproject->village_id = $request->input('village_id');
            $subproject->expected_duration         = $request->has('subproject.expected_duration') ? $request->input('subproject.expected_duration') : NULL;
            $subproject->fund_estimate_new         = $request->input('subproject.fund_estimate_new') == 0 ? NULL : $request->input('subproject.fund_estimate_new');
            $subproject->is_technically_feasible   = $request->input('subproject.is_technically_feasible');
            $subproject->is_financially_feasible   = $request->input('subproject.is_financially_feasible');
            $subproject->is_co_financed            = $request->input('subproject.is_co_financed');
            $subproject->credential_prepared_by    = $request->input('subproject.credential_prepared_by');
            $subproject->credential_prepared_date  = $request->has('subproject.credential_prepared_date') ? DateTime::createFromFormat('d/m/Y', $request->input('subproject.credential_prepared_date')) : NULL;
            $subproject->credential_confirmed_by   = $request->input('subproject.credential_confirmed_by');
            $subproject->credential_confirmed_date = $request->has('subproject.credential_confirmed_date') ? DateTime::createFromFormat('d/m/Y', $request->input('subproject.credential_confirmed_date')) : NULL;
            $subproject->credential_certified_by   = $request->input('subproject.credential_certified_by');
            $subproject->credential_certified_date = $request->has('subproject.credential_certified_date') ? DateTime::createFromFormat('d/m/Y', $request->input('subproject.credential_certified_date')) : NULL;
            $subproject->save();

            if($request->has('subproject.work_stages'))
            {
                WorkStageActivityService::save_stage_and_activity($request->input('subproject.work_stages'), $subproject->id);
            }

            if($request->has('subproject.approval.rdp_fund'))
            {
                //update rdp accuate
                $update_rdp_fund = Fund::find($request->input('subproject.approval.rdp_fund.id'));
                $update_rdp_fund->accuate_amount = $request->input('subproject.approval.rdp_fund.accuate_amount') == 0 ? null : $request->input('subproject.approval.rdp_fund.accuate_amount');
                $update_rdp_fund->save();
            }

            if($request->has('subproject.approval.community_fund'))
            {
                //update community accuate
                $update_community_fund = Fund::find($request->input('subproject.approval.community_fund.id'));
                $update_community_fund->accuate_amount = $request->input('subproject.approval.community_fund.accuate_amount') == 0 ? null : $request->input('subproject.approval.community_fund.accuate_amount');
                $update_community_fund->save();
            }

            if($request->has('subproject.approval.counterpart_funds'))
            {
                foreach ($request->input('subproject.approval.counterpart_funds') as $counterpart_fund) {
                    //update counterpart accuate
                    $update_counterpart_fund = Fund::find($counterpart_fund['id']);
                    $update_counterpart_fund->accuate_amount = $counterpart_fund['accuate_amount'] == 0 ? null : $counterpart_fund['accuate_amount'];
                    $update_counterpart_fund->save();

                }
            }

            if($request->has('counterpart_fundings'))
            {
                FundService::create_counterpart_fundings($request->input('counterpart_fundings'), $subproject->approval_id);
            }

            $attachment_ids = array();

            if($request->has('attachments'))
            {
                foreach ($request->input('attachments') as $index => $attachment) {
                    array_push($attachment_ids, $attachment['id']);
                }
            }

            $subproject->attachments()->sync(array_unique($attachment_ids));

            DB::commit();

            return redirect('/subproject/'.$subproject->id);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            \Log::error($e);
            abort(400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validator_rules($request)
    {
        $rules = [
            'village_id' => 'required',
            'subproject.name' => 'required',
            'subproject.expected_duration' => 'numeric|min:1',
            'subproject.fund_estimate_new' => 'numeric|min:0',
            'subproject.approval.rdp_fund.accuate_amount' => 'numeric|min:0',
            'subproject.approval.community_fund.accuate_amount' => 'numeric|min:0',
            'subproject.credential_prepared_by' => 'required_with:credential_prepared_date|max:255',
            'subproject.credential_prepared_date' => 'required_with:credential_prepared_by|date_format:d/m/Y',
            'subproject.credential_confirmed_by' => 'required_with:credential_confirmed_date|max:255',
            'subproject.credential_confirmed_date' => 'required_with:credential_confirmed_by|date_format:d/m/Y',
            'subproject.credential_certified_by' => 'required_with:credential_certified_date|max:255',
            'subproject.credential_certified_date' => 'required_with:credential_certified_by|date_format:d/m/Y',
        ];

        if($request->has('subproject.approval.counterpart_funds'))
        {
            foreach ($request->input('subproject.approval.counterpart_funds') as $index => $counterpart_fund) {
                $rules['subproject.approval.counterpart_funds.'.$index.'.accuate_amount'] = 'numeric|min:0';
            }
        }

        if($request->has('counterpart_fundings'))
        {
            foreach ($request->input('counterpart_fundings') as $index => $counterpart_funding) {
                $rules['counterpart_fundings.'.$index.'.origin_amount'] = 'numeric|min:0|required_with:counterpart_fundings.'.$index.'.source';
                $rules['counterpart_fundings.'.$index.'.source'] = 'max:255|required_with:counterpart_fundings.'.$index.'.origin_amount';
                $rules['counterpart_fundings.'.$index.'.has_commitment'] = 'required_with:counterpart_fundings.'.$index.'.source,counterpart_fundings.'.$index.'.origin_amount|boolean';
            }
        }

        return $rules;
    }

    public function validator_messages($request)
    {
        $messages = [
            'subproject.village.required' => 'Village field is required.',
            'subproject.name.required' => 'Subproject name is required.',
            'subproject.expected_duration.min' => 'The expected duration month(s) must be at least 1.',
            'subproject.expected_duration.numeric' => 'The expected duration month(s) must be a number',
            'subproject.fund_estimate_new.min' => 'The new estimate total amount must be at least 0.',
            'subproject.fund_estimate_new.numeric' => 'The new estimate total amount must be a number',
            'subproject.approval.rdp_fund.accuate_amount.min' => 'The RSDPLGP actual amount must be at least 0.',
            'subproject.approval.rdp_fund.accuate_amount.numeric' => 'The RSDPLGP actual amount must be a number',
            'subproject.approval.community_fund.accuate_amount.min' => 'The community contribution amount must be at least 0.',
            'subproject.approval.community_fund.accuate_amount.numeric' => 'The community contribution amount must be a number',
            'subproject.credential_prepared_by.required_with' => 'The name of community facilitator of credential prepared by is required when prepared date is given.',
            'subproject.credential_prepared_by.max' => 'The name of community facilitator of credential prepared by may not be greater than 255 characters.',
            'subproject.credential_prepared_date.required_with' => 'The date of credential prepared is required when name of community facilitator is given.',
            'subproject.credential_prepared_date.date_format' => 'The format of credential prepared date is wrong.',
            'subproject.credential_confirmed_by.required_with' => 'The name of LLG manager of credential confirmed by is required when confirmed date is given.',
            'subproject.credential_confirmed_by.max' => 'The name of LLG manager of credential confirmed by may not be greater than 255 characters.',
            'subproject.credential_confirmed_date.required_with' => 'The date of credential confirmed is required when name of community facilitator is given.',
            'subproject.credential_confirmed_date.date_format' => 'The format of credential confirmed date is wrong.',
            'subproject.credential_certified_by.required_with' => 'The name of PMU team leader of credential certified by is required when certified date is given.',
            'subproject.credential_certified_by.max' => 'The name of PMU team leader of credential certified by may not be greater than 255 characters.',
            'subproject.credential_certified_date.required_with' => 'The date of credential certified is required when name of community facilitator is given.',
            'subproject.credential_certified_date.date_format' => 'The format of credential certified date is wrong.',
        ];

        if($request->has('subproject.approval.counterpart_funds'))
        {
            foreach ($request->input('subproject.approval.counterpart_funds') as $index => $counterpart_fund) {
                $messages['subproject.approval.counterpart_funds.'.$index.'.accuate_amount.numeric'] = 'The actual amount of '.$counterpart_fund['source'].' must be a number';
                $messages['subproject.approval.counterpart_funds.'.$index.'.accuate_amount.min'] = 'The actual amount of '.$counterpart_fund['source'].' must be at least 0';
            }
        }

        if($request->has('counterpart_fundings'))
        {
            foreach ($request->input('counterpart_fundings') as $index => $counterpart_funding) {
                $messages['counterpart_fundings.'.$index.'.origin_amount.required_with'] = 'Please enter the amount for counterpart funding.';
                $messages['counterpart_fundings.'.$index.'.origin_amount.numeric'] = 'The origin amount of other counterpart funding must be a number';
                $messages['counterpart_fundings.'.$index.'.origin_amount.min'] = 'The origin amount of other counterpart funding must be at least 0';
                $messages['counterpart_fundings.'.$index.'.source.required_with'] = 'Please provide the source of counterpart funding.';
                $messages['counterpart_fundings.'.$index.'.source.max'] = 'The source of other counterpart funding may not be greater than 255 characters.';
                $messages['counterpart_fundings.'.$index.'.has_commitment.required_with'] = 'Please confirm whether letters of commitment for counterpart funding/co-financing is provided.';
                $messages['counterpart_fundings.'.$index.'.has_commitment.boolean'] = 'Has commitment field must be true or false.';
            }
        }

        return $messages;
    }
}
