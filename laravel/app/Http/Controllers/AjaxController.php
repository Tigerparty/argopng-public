<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Argo\Province;
use App\Argo\Prioritisation;

class AjaxController extends Controller
{
    public function getProvinceWithDistricts($p_id)
    {
        $data = Province::with('districts')->findOrFail($p_id);

        return $data;
    }

    public function getProvinceWithDistrictsLlgs($p_id, $d_id)
    {
        $data = Province::with(array(
                'districts' => function($query) use ($d_id){
                    $query->with('llgs');
                    $query->where('id', '=', $d_id);
                }
            ))
            ->findOrFail($p_id);

        return $data;
    }

    public function getProvinceWithDistrictsLlgsWards($p_id, $d_id, $l_id)
    {
        $data = Province::with(array(
                'districts' => function($query) use ($d_id, $l_id){
                    $query->with(array(
                        'llgs' => function($query) use ($l_id){
                            $query->with('wards');
                            $query->where('id', '=', $l_id);
                        }
                    ));
                    $query->where('id', '=', $d_id);
                }
            ))
            ->findOrFail($p_id);

        return $data;
    }

    public function getProvinceWithDistrictsLlgsWardsVillages($p_id, $d_id, $l_id, $w_id)
    {
        $data = Province::with(array(
                'districts' => function($query) use ($d_id, $l_id, $w_id){
                    $query->with(array(
                        'llgs' => function($query) use ($l_id, $w_id){
                            $query->with(array(
                                'wards' => function($query) use ($w_id){
                                    $query->with('villages');
                                    $query->where('id', '=', $w_id);
                                }
                            ));
                            $query->where('id', '=', $l_id);
                        }
                    ));
                    $query->where('id', '=', $d_id);
                }
            ))
            ->findOrFail($p_id);

        return $data;
    }

    public function getWardPrioritisation($w_id)
    {
        $prioritisations = Prioritisation::with([
                'prioritisation_needs' => function($query){
                    $query->where('priority', '=', 1);
                    $query->select(['prioritisation_id', 'description']);
                }
            ])
            ->select(['id', 'meeting_date'])
            ->where('ward_id', '=', $w_id)
            ->orderBy('meeting_date')
            ->get();

        return $prioritisations;
    }
}
