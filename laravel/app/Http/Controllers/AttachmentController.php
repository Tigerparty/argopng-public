<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use DateTime;
use Storage;

use App\Argo\Attachment;

class AttachmentController extends Controller
{
    public function checkFile(Request $request)
    {
        if($request->hasFile('file'))
        {
            return $this->doUpload($request->file('file'));
        }

        Log::warning("File upload fail, forgot to setup upload_max_filesize or post_max_size?");

        return response()->json(array(), 400);
    }

    public function doUpload($file)
    {
        $origin_name = $file->getClientOriginalName();
        $sha1_name = sha1_file($file);
        $folder_name = $this->get_folder_name($file);
        $mime = $file->getMimeType();

        $save_path_name = $folder_name.'/'.$sha1_name;

        if(Storage::disk('local')->has($save_path_name))
        {
            $exist_attach = Attachment::where('path', '=', $save_path_name)->first();

            if($exist_attach)
            {
                Log::warning("Uploading exist file: [$exist_attach->id] $save_path_name");

                return response()->json($this->get_response_array($exist_attach));
            }
        }

        try
        {
            DB::beginTransaction();

            //-- move file
            Storage::put($save_path_name, file_get_contents($file));

            $attach = new Attachment();
            $attach->name = $origin_name;
            $attach->path = $save_path_name;
            $attach->type = $mime;
            $attach->save();

            DB::commit();

            return response()->json($this->get_response_array($attach));
        }
        catch(\Exception $e)
        {
            DB::rollback();

            Log::error($e);
        }
    }

    public function access($ath_id)
    {
        try
        {
            $attach = Attachment::findOrFail($ath_id);
            if(!Storage::disk('local')->has($attach->path))
            {
                throw new Exception("File Not Exists in path: $attach->path");
            }
        }
        catch(\Exception $e)
        {
            Log::error($e);

            App::abort(404);
        }

        $file = Storage::get($attach->path);
        $mime = $attach->type;

        $response = response($file, 200);
        $response->header('Content-Type', $mime);

        return $response;
    }

    public function download($ath_id)
    {
        try
        {
            $attach = Attachment::findOrFail($ath_id);

            if(!Storage::disk('local')->has($attach->path))
            {
                throw new \Exception("File Not Exists in path: $attach->path");
            }
        }
        catch(\Exception $e)
        {
            Log::error($e);

            abort(404);
        }

        $file = Storage::get($attach->path);
        $file_name = $attach->name;
        $mime = $attach->type;

        $response = response($file, 200);
        $response->header('Content-Type', $mime);
        $response->header('COntent-Disposition', "attachment; filename=$file_name");

        return $response;
    }

    private function get_folder_name($fileObj)
    {
        $sub_dir = substr(sha1_file($fileObj), 0, 2);
        $storage_folder = $sub_dir;

        return $storage_folder;
    }

    private function get_response_array($attach_orm)
    {
        $id = $attach_orm->id;
        return array(
            "id" => $id,
            "name" => $attach_orm->name,
            "mime" => $attach_orm->type,
            "asset_path" => asset("/file/$id"),
            "download_path" => asset("/file/$id/download")
        );
    }
}
