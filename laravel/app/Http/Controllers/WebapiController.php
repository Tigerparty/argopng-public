<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repository\PrioritisationRepository;
use App\Repository\ApprovalRepository;
use App\Repository\SubprojectRepository;

class WebapiController extends Controller
{
    public function getPrioritisations(Request $request)
    {
        $keyword = $request->input('keyword', '');

        $prioritisation_list = PrioritisationRepository::getPrioritisationList()
            ->where(function($query) use ($keyword) {
                $query->where('p.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('d.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('l.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('w.name', 'LIKE', '%'. trim($keyword) .'%' );
            });

        if($request->has('province'))
        {
            $prioritisation_list = $prioritisation_list->where('p.id', '=' , $request->input('province.id', ''));
        }

        if($request->has('district'))
        {
            $prioritisation_list = $prioritisation_list->where('d.id', '=' , $request->input('district.id', ''));
        }

        if($request->has('llg'))
        {
            $prioritisation_list = $prioritisation_list->where('l.id', '=' , $request->input('llg.id', ''));
        }

        if($request->has('ward'))
        {
            $prioritisation_list = $prioritisation_list->where('w.id', '=' , $request->input('ward.id', ''));
        }

        $prioritisation_list = $prioritisation_list->get();

        return $prioritisation_list;
    }

    public function getApprovals(Request $request)
    {
        $keyword = $request->input('keyword', '');

        $approval_list = ApprovalRepository::getApprovalList()
            ->where(function($query) use ($keyword) {
                $query->where('p.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('d.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('l.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('w.name', 'LIKE', '%'. trim($keyword) .'%' );
            });

        if($request->has('province'))
        {
            $approval_list = $approval_list->where('p.id', '=' , $request->input('province.id', ''));
        }

        if($request->has('district'))
        {
            $approval_list = $approval_list->where('d.id', '=' , $request->input('district.id', ''));
        }

        if($request->has('llg'))
        {
            $approval_list = $approval_list->where('l.id', '=' , $request->input('llg.id', ''));
        }

        if($request->has('ward'))
        {
            $approval_list = $approval_list->where('w.id', '=' , $request->input('ward.id', ''));
        }

        $approval_list = $approval_list->get();

        return $approval_list;
    }

    public function getSubprojects(Request $request)
    {
        $keyword = $request->input('keyword', '');

        $subproject_list = SubprojectRepository::getSubprojectList()
            ->where(function($query) use ($keyword) {
                $query->where('subproject.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('p.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('d.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('l.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('w.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('v.name', 'LIKE', '%'. trim($keyword) .'%' );
            });

        if($request->has('province'))
        {
            $subproject_list = $subproject_list->where('p.id', '=' , $request->input('province.id', ''));
        }

        if($request->has('district'))
        {
            $subproject_list = $subproject_list->where('d.id', '=' , $request->input('district.id', ''));
        }

        if($request->has('llg'))
        {
            $subproject_list = $subproject_list->where('l.id', '=' , $request->input('llg.id', ''));
        }

        if($request->has('ward'))
        {
            $subproject_list = $subproject_list->where('w.id', '=' , $request->input('ward.id', ''));
        }

        if($request->has('village'))
        {
            $subproject_list = $subproject_list->where('v.id', '=' , $request->input('village.id', ''));
        }

        $subproject_list = $subproject_list->get();

        return $subproject_list;
    }

    public function getSubprojectsWithCoordinate(Request $request)
    {
        $keyword = $request->input('keyword', '');

        $subproject_list = SubprojectRepository::getSubprojectsWithCoordinate()
            ->where(function($query) use ($keyword) {
                $query->where('subproject.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('p.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('d.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('l.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('w.name', 'LIKE', '%'. trim($keyword) .'%' )
                      ->orWhere('v.name', 'LIKE', '%'. trim($keyword) .'%' );
            });

        if($request->has('province'))
        {
            $subproject_list = $subproject_list->where('p.id', '=' , $request->input('province.id', ''));
        }

        if($request->has('district'))
        {
            $subproject_list = $subproject_list->where('d.id', '=' , $request->input('district.id', ''));
        }

        if($request->has('llg'))
        {
            $subproject_list = $subproject_list->where('l.id', '=' , $request->input('llg.id', ''));
        }

        if($request->has('ward'))
        {
            $subproject_list = $subproject_list->where('w.id', '=' , $request->input('ward.id', ''));
        }

        if($request->has('village'))
        {
            $subproject_list = $subproject_list->where('v.id', '=' , $request->input('village.id', ''));
        }

        $subproject_list = $subproject_list->get();

        return $subproject_list;
    }
}
