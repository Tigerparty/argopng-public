<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AttachmentController;

use DB;
use DateTime;

use App\User;
use App\Argo\Subproject;
use App\Argo\ProgressReport;
use App\Argo\WorkActivity;
use App\Argo\Issue;
use App\Argo\Attachment;

class ApiController extends Controller
{
    public function DataSync()
    {
        try{
            $users = User::all();
            foreach ($users as $user) {
                $user->pwd = $user->password;
            }

            $array_subprojects = array();
            $subprojects = Subproject::all();
            foreach($subprojects as $subproject){
                foreach($subproject->work_stages as $work_stage){
                    $work_stage->work_activities;
                }
                $subproject->issues;

                array_push($array_subprojects, array(
                    'id' => $subproject->id,
                    'name' => $subproject->name,
                    'created_by' => $subproject->created_by,
                    'village' => $subproject->village->name,
                    'ward' => $subproject->village->ward->name,
                    'llg' => $subproject->village->ward->llg->name,
                    'district' => $subproject->village->ward->llg->district->name,
                    'province' => $subproject->village->ward->llg->district->province->name,
                    'work_stages' => $subproject->work_stages,
                    "issues" => $subproject->issues
                ));
            }

            $array_data = array(
                'User' => $users,
                'Subproject' => $array_subprojects
            );

            $encode_data = $this->encodeString(json_encode($array_data));

            return response()->json(['result' => 'success', 'info' => $encode_data]);
        }
        catch(Exception $e){
            return response()->json(['result' => 'fail']);
        }
    }

    public function SubmitReport(Request $request)
    {
        try{
            $results = $request->all();
            if(!empty($results)){

                DB::beginTransaction();

                $pr_id = 0;
                $array_attachment_id = array();

                foreach($results as $key => $value){
                    if($key == 'info'){
                        $info = (array) json_decode($value, true)[0];

                        $pr = new ProgressReport;
                        $pr->subproject_id = $info['subproject_id'];
                        $pr->monitoring_date = DateTime::createFromFormat('d/m/Y', $info['monitoring_date']);
                        $pr->completed_by = $info['completed_by'];
                        $pr->financial_kept = $info['financial_kept'];
                        $pr->is_contributions_updated = $info['is_contributions_updated'];
                        $pr->procurement_change = $info['procurement_change'];
                        $pr->safeguard = $info['safeguard'];
                        $pr->contribution_changing = $info['contribution_changing'];
                        $pr->comment = $info['comment'];
                        $pr->lat = $info['lat'];
                        $pr->lng = $info['lng'];
                        $pr->created_by = $info['created_by'];
                        $pr->save();

                        $pr_id = $pr->id;

                        foreach ($info['stage'] as $key => $stage){
                            foreach ($stage['activity'] as $key => $activity){
                                $work_activity = WorkActivity::find($activity['id']);
                                if($work_activity->completed == 0){
                                    $work_activity->completed = $activity['completed'];
                                    if($activity['completed_at']){
                                        $work_activity->completed_at = DateTime::createFromFormat('d/m/Y', $activity['completed_at']);
                                        $work_activity->report_modify()->attach($pr_id);
                                    }
                                    $work_activity->save();
                                }
                            }
                        }

                        //-- check issue is deleted in app or not
                        $issues = Subproject::find($info['subproject_id'])->issues;
                        foreach($issues as $db_issue){
                            $is_issue_exist = false;
                            foreach ($info['issue'] as $app_issue){
                                if($db_issue->id == $app_issue['id']){
                                    $is_issue_exist = true;
                                    break;
                                }
                            }
                            //-- delete db issue which is deleted in app
                            if(!$is_issue_exist){
                                $db_issue->delete();
                            }
                        }

                        foreach ($info['issue'] as $key => $issue){
                            if($issue['id']){
                                $exist_issue = Issue::find($issue['id']);
                                if($exist_issue){
                                    $exist_issue->desc = $issue['desc'];
                                    $exist_issue->is_resolved = $issue['is_resolved'];
                                    if($issue['is_resolved'])
                                    {
                                        $exist_issue->resolved_at = DateTime::createFromFormat('d/m/Y', $issue['resolved_at']);
                                    }
                                    $exist_issue->save();
                                }
                            }else{
                                $new_issue = new Issue;
                                $new_issue->subproject_id = $issue['subproject_id'];
                                $new_issue->desc = $issue['desc'];
                                $new_issue->is_resolved = $issue['is_resolved'];
                                if($issue['is_resolved'])
                                {
                                    $new_issue->resolved_at = DateTime::createFromFormat('d/m/Y', $issue['resolved_at']);
                                }
                                $new_issue->created_by = $issue['created_by'];
                                $new_issue->save();
                            }
                        }

                        //-- return issue list to app to update all issues
                        $subproject = Subproject::find($info['subproject_id']);
                        $array_data = array(
                            'Issue' => $subproject->issues
                        );

                        $encode_data = $this->encodeString(json_encode($array_data));
                    }
                    else{
                        $response = (new AttachmentController)->doUpload($value);
                        $result = json_decode($response->getContent(), true);
                        array_push($array_attachment_id, $result['id']);
                    }
                }

                //-- sync attachment id into database
                if(count($array_attachment_id) > 0){
                    $pr->images()->sync($array_attachment_id);
                }

                DB::commit();
            }
            return response()->json(['result' => 'success', 'info' => $encode_data]);
        }
        catch(Exception $e){
            DB::rollback();
            Log::error("Submit report fail, due to : " + $e);
            return response()->json(['result' => 'fail']);
        }
    }

    function encodeString($string)
    {
        return base64_encode($string);
    }
}
