<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Auth;
use Input;
use Validator;

class LoginController extends Controller
{
    public function showLogin()
    {
        if(Auth::check())
        {
            Auth::logout();
        }

        return View::make('login');
    }

    public function doLogin(Request $request)
    {
        $rules = array(
            'username' => 'required',
            'password' => 'required'
        );

        $messages = array(
            'username.required' => 'Username is required.',
            'password.required' => 'Password is required.'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        if($validator->fails())
        {
            $messages = $validator->messages();

            return redirect('/login')->withErrors($validator)->withInput(Input::except('password'));
        }

        $userdata = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );

        if(Auth::attempt($userdata))
        {
            return redirect()->intended('/');
        }
        else
        {
            return redirect('/login')->with('login_fail_message', 'Login failed. You have the wrong username or password.');
        }
    }

    public function doLogout()
    {
        Auth::logout();

        return redirect('/login');
    }
}
