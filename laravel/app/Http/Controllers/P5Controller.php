<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DateTime;
use Gate;

use App\Services\WorkStageActivityService;
use App\Services\UserService;
use App\Services\IssueService;
use App\Argo\Subproject;
use App\Argo\ProgressReport;
use App\Argo\WorkActivity;
use App\Argo\Issue;

class P5Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($subproject_id)
    {
        $subproject = Subproject::with(array(
                'work_stages' => function ($query) {
                    $query->with(array(
                        'work_activities' => function ($query) {
                            $query->select(['id', 'stage_id', 'description', 'completed', 'completed as origin_completed', 'completed_at']);
                        }
                    ));
                },
                'issues'
            ))
            ->select(['id', 'name'])
            ->findOrFail($subproject_id);

        $array_data = array(
            'subproject' => $subproject,
            'subproject_id' => $subproject_id,
            'subproject_name' => $subproject->name
        );

        return view('P5.create', $array_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $subproject_id)
    {
        $this->validate($request, $this->validator_rules($request), $this->validator_messages($request));

        try
        {
            DB::beginTransaction();

            $new_report = new ProgressReport;
            $new_report->subproject_id = $subproject_id;
            $new_report->created_by = Auth::user()->id;
            $new_report->monitoring_date = DateTime::createFromFormat('d/m/Y', $request->input('report.monitoring_date'));
            $new_report->completed_by = $request->input('report.completed_by');
            $new_report->financial_kept = $request->input('report.financial_kept_check') ? NULL : $request->input('report.financial_kept');
            $new_report->is_contributions_updated = $request->input('report.is_contributions_updated');
            if (Gate::allows('view-procurement-safeguard')) 
            { 
                $new_report->procurement_change = $request->input('report.procurement_change_check') ? $request->input('report.procurement_change') : NULL;
                $new_report->safeguard = $request->input('report.safeguard_check') ? $request->input('report.safeguard') : NULL;
            }
            $new_report->contribution_changing = $request->input('report.contribution_changing_check') ? $request->input('report.contribution_changing') : NULL;
            $new_report->comment = $request->input('report.comment');
            $new_report->save();

            if($request->has('subproject.work_stages'))
            {
                WorkStageActivityService::update_work_activities($request->input('subproject.work_stages'), $new_report->id);
            }

            if($request->has('report.issues'))
            {
                IssueService::create_and_update_issue($request->input('report.issues'), $subproject_id);
            }

            if($request->has('attachments'))
            {
                $attachment_ids = array();

                foreach ($request->input('attachments') as $index => $attachment) {
                    array_push($attachment_ids, $attachment['id']);
                }

                $new_report->attachments()->sync(array_unique($attachment_ids));
            }

            DB::commit();

            return redirect(asset('/subproject/'.$subproject_id.'/report/'.$new_report->id));
        }
        catch(\Exception $e)
        {
            DB::rollback();

            abort(400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($subproject_id, $report_id)
    {
        $report = ProgressReport::with(array(
                'subproject' => function($query){
                    $query->with(array(
                        'work_stages' => function($query){
                            $query->with('work_activities');
                        },
                        'issues'
                    ));
                },
                'attachments'
            ))
            ->findOrFail($report_id);

        $array_data = array(
            'report' => $report,
        );

        return view('P5.show', $array_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($subproject_id, $report_id)
    {
        $report = ProgressReport::with(array(
                'subproject' => function($query){
                    $query->with(array(
                        'work_stages' => function($query){
                            $query->with('work_activities');
                        },
                        'issues'
                    ));
                },
                'attachments'
            ))
            ->findOrFail($report_id);

        $array_data = array(
            'report' => $report,
            'subproject_id' => $subproject_id,
            'subproject_name' => $report->subproject->name,
            'report_id' => $report->id
        );

        return view('P5.edit', $array_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $subproject_id, $report_id)
    {
        $this->validate($request, $this->validator_rules($request), $this->validator_messages($request));

        try
        {
            DB::beginTransaction();

            $update_report = ProgressReport::find($report_id);
            $update_report->subproject_id = $subproject_id;
            $update_report->monitoring_date = DateTime::createFromFormat('d/m/Y', $request->input('report.monitoring_date'));
            $update_report->completed_by = $request->input('report.completed_by');
            $update_report->financial_kept = $request->input('report.financial_kept_check') ? NULL : $request->input('report.financial_kept');
            $update_report->is_contributions_updated = $request->input('report.is_contributions_updated');
            if (Gate::allows('view-procurement-safeguard')) 
            { 
                $update_report->procurement_change = $request->input('report.procurement_change_check') ? $request->input('report.procurement_change') : NULL;
                $update_report->safeguard = $request->input('report.safeguard_check') ? $request->input('report.safeguard') : NULL;
            }
            $update_report->contribution_changing = $request->input('report.contribution_changing_check') ? $request->input('report.contribution_changing') : NULL;
            $update_report->comment = $request->input('report.comment');
            $update_report->save();

            if($request->has('subproject.work_stages'))
            {
                WorkStageActivityService::update_work_activities($request->input('subproject.work_stages'), $report_id);
            }

            if($request->has('report.subproject.issues'))
            {
                IssueService::create_and_update_issue($request->input('report.subproject.issues'), $subproject_id);
            }

            $attachment_ids = array();

            if($request->has('attachments'))
            {
                foreach ($request->input('attachments') as $index => $attachment) {
                    array_push($attachment_ids, $attachment['id']);
                }
            }

            $update_report->attachments()->sync(array_unique($attachment_ids));

            DB::commit();

            return redirect(asset('/subproject/'.$subproject_id.'/report/'.$report_id));
        }
        catch(\Exception $e)
        {
            DB::rollback();

            abort(400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($subproject_id, $report_id)
    {
        if(Auth::user()->can('destroy'))
        {
            try
            {
                DB::beginTransaction();

                $report = ProgressReport::findOrFail($report_id);
                $report->attachments()->detach();

                #--Reset activity status
                $modify_activities = $report->modify_activities()->select('id')->get();
                $report->modify_activities()->detach();
                WorkStageActivityService::reset_work_activity_once_delete_report($modify_activities);

                $report->delete();

                DB::commit();

                return response()->json('success');
            }
            catch(\Exception $e)
            {
                DB::rollback();

                abort(400);
            }
        }
        else
        {
            abort(403);
        }
    }

    public function validator_rules($request)
    {
        $rules = [
            'report.monitoring_date' => 'required|date_format:d/m/Y',
            'report.financial_kept' => 'required_if:report.financial_kept_check,0',
            'report.is_contributions_updated' => 'required',
            'report.procurement_change' => 'required_if:report.procurement_change_check,1',
            'report.safeguard' => 'required_if:report.safeguard_check,1',
            'report.contribution_changing' => 'required_if:report.contribution_changing_check,1'
        ];

        return $rules;
    }

    public function validator_messages($request)
    {
        $messages = [
            'report.monitoring_date.required' => 'The monitoring date field is required.',
            'report.financial_kept.required_if' => 'The details are required when financial records not being kept up to date.',
            'report.is_contributions_updated.required' => 'Please provide the contributions is updated or not.',
            'report.procurement_change.required_if' => 'The details are required when there are any changes with the procurement plan.',
            'report.safeguard.required_if' => 'The details are required when there are any environmental or social issues emerging with this subproject.',
            'report.contribution_changing.required_if' => 'The details are required when there are any changes to other contributions.'
        ];

        return $messages;
    }
}
