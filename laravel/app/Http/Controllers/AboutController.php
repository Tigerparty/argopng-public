<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

class AboutController extends Controller {

    public function showAboutRDP()
    {
        return View::make('about');
    }
}
