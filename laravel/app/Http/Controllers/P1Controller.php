<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use DateTime;
use Auth;
use Validator;

use App\Argo\Province;
use App\Argo\Ward;
use App\Argo\Sector;
use App\Argo\NeedIdentify;
use App\Argo\Prioritisation;
use App\Argo\PrioritisationNeed;
use App\Argo\PrioritisationPlace;

class P1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::orderBy('name')->get();

        $array_data = array(
            'provinces' => $provinces,
        );

        return view('P1.index', $array_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::orderBy('name')->get();
        $sectors = Sector::all();
        $need_identifies = NeedIdentify::all();

        $array_data = array(
            'provinces'       => $provinces,
            'sectors'         => $sectors,
            'need_identifies' => $need_identifies
        );

        return view('P1.create', $array_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->input();
        $this->validate($request, $this->validator_rules($request), $this->validator_messages($request));

        try
        {
            DB::beginTransaction();

            $new_prioritisation = new Prioritisation;
            $new_prioritisation->ward_id                      = $request->ward_id;
            $new_prioritisation->need_identify_id             = $request->has('need_identify_id') ? $request->need_identify_id : null;
            $new_prioritisation->meeting_date                 = DateTime::createFromFormat('d/m/Y', $request->meeting_date);
            $new_prioritisation->participants_male_0_14       = $request->participants_male_0_14;
            $new_prioritisation->participants_female_0_14     = $request->participants_female_0_14;
            $new_prioritisation->participants_disable_0_14    = $request->participants_disable_0_14;
            $new_prioritisation->participants_male_15_24      = $request->participants_male_15_24;
            $new_prioritisation->participants_female_15_24    = $request->participants_female_15_24;
            $new_prioritisation->participants_disable_15_24   = $request->participants_disable_15_24;
            $new_prioritisation->participants_male_25_59      = $request->participants_male_25_59;
            $new_prioritisation->participants_female_25_59    = $request->participants_female_25_59;
            $new_prioritisation->participants_disable_25_59   = $request->participants_disable_25_59;
            $new_prioritisation->participants_male_60_plus    = $request->participants_male_60_plus;
            $new_prioritisation->participants_female_60_plus  = $request->participants_female_60_plus;
            $new_prioritisation->participants_disable_60_plus = $request->participants_disable_60_plus;
            $new_prioritisation->beneficiaries_male_0_14      = $request->beneficiaries_male_0_14;
            $new_prioritisation->beneficiaries_female_0_14    = $request->beneficiaries_female_0_14;
            $new_prioritisation->beneficiaries_plwds_0_14     = $request->beneficiaries_plwds_0_14;
            $new_prioritisation->beneficiaries_male_15_24     = $request->beneficiaries_male_15_24;
            $new_prioritisation->beneficiaries_female_15_24   = $request->beneficiaries_female_15_24;
            $new_prioritisation->beneficiaries_plwds_15_24    = $request->beneficiaries_plwds_15_24;
            $new_prioritisation->beneficiaries_male_25_59     = $request->beneficiaries_male_25_59;
            $new_prioritisation->beneficiaries_female_25_59   = $request->beneficiaries_female_25_59;
            $new_prioritisation->beneficiaries_plwds_25_59    = $request->beneficiaries_plwds_25_59;
            $new_prioritisation->beneficiaries_male_60_plus   = $request->beneficiaries_male_60_plus;
            $new_prioritisation->beneficiaries_female_60_plus = $request->beneficiaries_female_60_plus;
            $new_prioritisation->beneficiaries_plwds_60_plus  = $request->beneficiaries_plwds_60_plus;
            $new_prioritisation->created_by                   = Auth::user()->id;
            $new_prioritisation->save();

            $new_place = new PrioritisationPlace;
            $new_place->prioritisation_id = $new_prioritisation->id;
            $new_place->place = $request->input('prioritisation_places.0.place');
            $new_place->count_village_representative = $request->input('prioritisation_places.0.count_village_representative');
            $new_place->count_total_village = $request->input('prioritisation_places.0.count_total_village');
            $new_place->save();

            for ($i=0; $i < 5 ; $i++) { 
                if($request->has('prioritisation_needs.'.$i.'.description')){
                    $new_need = new PrioritisationNeed;
                    $new_need->prioritisation_id = $new_prioritisation->id;
                    $new_need->sector_id = $request->input('prioritisation_needs.'.$i.'.sector_id');
                    $new_need->priority = $i + 1;
                    $new_need->description = $request->input('prioritisation_needs.'.$i.'.description');
                    $new_need->save();
                }else{
                    $new_need = new PrioritisationNeed;
                    $new_need->prioritisation_id = $new_prioritisation->id;
                    $new_need->priority = $i + 1;
                    $new_need->save();
                }
            }

            if($request->has('attachments'))
            {
                $attachment_ids = array();

                foreach ($request->input('attachments') as $index => $attachment) {
                    array_push($attachment_ids, $attachment['id']);
                }

                $new_prioritisation->attachments()->sync(array_unique($attachment_ids));
            }

            DB::commit();

            return redirect('/prioritisation/'.$new_prioritisation->id);
        }
        catch(\Exception $e)
        {
            DB::rollback();

            abort(400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prioritisation = Prioritisation::with([
                'prioritisation_places', 
                'prioritisation_needs' => function($query){
                    $query->with('sector');
                },
                'need_identify',
            ])->findOrFail($id);

        $location = Ward::with([
                'llg' => function($query){
                    $query->with([
                        'district' => function($query){
                            $query->with('province');
                        }
                    ]);
                }
            ])
            ->findOrFail($prioritisation->ward_id);

        $array_data = array(
            'prioritisation' => $prioritisation,
            'location'       => $location
        );

        return view('P1.show', $array_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provinces = Province::orderBy('name')->get();
        $sectors = Sector::all();
        $need_identifies = NeedIdentify::all();

        $prioritisation = Prioritisation::with([
                'prioritisation_places', 
                'prioritisation_needs' => function($query){
                    $query->with('sector');
                },
                'need_identify',
            ])->findOrFail($id);

        $location = Ward::with([
                'llg' => function($query){
                    $query->with([
                        'district' => function($query){
                            $query->with('province');
                        }
                    ]);
                }
            ])
            ->find($prioritisation->ward_id);

        $location_ids = array((string)$location->llg->district->province->id, (string)$location->llg->district->id, (string)$location->llg->id, (string)$location->id, );

        $array_data = array(
            'provinces'      => $provinces,
            'sectors'         => $sectors,
            'need_identifies' => $need_identifies,
            'prioritisation' => $prioritisation,
            'locations'       => $location_ids
        );

        return view('P1.edit', $array_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->input();
        $this->validate($request, $this->validator_rules($request), $this->validator_messages($request));

        try
        {
            DB::beginTransaction();

            $update_prioritisation = Prioritisation::findOrFail($id);
            $update_prioritisation->ward_id                      = $request->ward_id;
            $update_prioritisation->need_identify_id             = $request->has('need_identify_id') ? $request->need_identify_id : null;
            $update_prioritisation->meeting_date                 = DateTime::createFromFormat('d/m/Y', $request->meeting_date);
            $update_prioritisation->participants_male_0_14       = $request->participants_male_0_14;
            $update_prioritisation->participants_female_0_14     = $request->participants_female_0_14;
            $update_prioritisation->participants_disable_0_14    = $request->participants_disable_0_14;
            $update_prioritisation->participants_male_15_24      = $request->participants_male_15_24;
            $update_prioritisation->participants_female_15_24    = $request->participants_female_15_24;
            $update_prioritisation->participants_disable_15_24   = $request->participants_disable_15_24;
            $update_prioritisation->participants_male_25_59      = $request->participants_male_25_59;
            $update_prioritisation->participants_female_25_59    = $request->participants_female_25_59;
            $update_prioritisation->participants_disable_25_59   = $request->participants_disable_25_59;
            $update_prioritisation->participants_male_60_plus    = $request->participants_male_60_plus;
            $update_prioritisation->participants_female_60_plus  = $request->participants_female_60_plus;
            $update_prioritisation->participants_disable_60_plus = $request->participants_disable_60_plus;
            $update_prioritisation->beneficiaries_male_0_14      = $request->beneficiaries_male_0_14;
            $update_prioritisation->beneficiaries_female_0_14    = $request->beneficiaries_female_0_14;
            $update_prioritisation->beneficiaries_plwds_0_14     = $request->beneficiaries_plwds_0_14;
            $update_prioritisation->beneficiaries_male_15_24     = $request->beneficiaries_male_15_24;
            $update_prioritisation->beneficiaries_female_15_24   = $request->beneficiaries_female_15_24;
            $update_prioritisation->beneficiaries_plwds_15_24    = $request->beneficiaries_plwds_15_24;
            $update_prioritisation->beneficiaries_male_25_59     = $request->beneficiaries_male_25_59;
            $update_prioritisation->beneficiaries_female_25_59   = $request->beneficiaries_female_25_59;
            $update_prioritisation->beneficiaries_plwds_25_59    = $request->beneficiaries_plwds_25_59;
            $update_prioritisation->beneficiaries_male_60_plus   = $request->beneficiaries_male_60_plus;
            $update_prioritisation->beneficiaries_female_60_plus = $request->beneficiaries_female_60_plus;
            $update_prioritisation->beneficiaries_plwds_60_plus  = $request->beneficiaries_plwds_60_plus;
            $update_prioritisation->save();

            if($request->has('prioritisation_places.0.place'))
            {
                if($request->has('prioritisation_places.0.id'))
                {
                    $update_place = PrioritisationPlace::findOrFail($request->input('prioritisation_places.0.id'));
                    $update_place->place = $request->input('prioritisation_places.0.place');
                    $update_place->count_village_representative = $request->input('prioritisation_places.0.count_village_representative');
                    $update_place->count_total_village = $request->input('prioritisation_places.0.count_total_village');
                    $update_place->save();
                }
                else
                {
                    $new_place = new PrioritisationPlace;
                    $new_place->prioritisation_id = $id;
                    $update_place->place = $request->input('prioritisation_places.0.place');
                    $update_place->count_village_representative = $request->input('prioritisation_places.0.count_village_representative');
                    $update_place->count_total_village = $request->input('prioritisation_places.0.count_total_village');
                    $update_place->save();
                }
            }

            for ($i=0; $i < 5 ; $i++)
            { 
                if($request->has('prioritisation_needs.'.$i.'.id'))
                {
                    $update_need = PrioritisationNeed::findOrFail($request->input('prioritisation_needs.'.$i.'.id'));
                    $update_need->prioritisation_id = $id;
                    $update_need->sector_id = $request->input('prioritisation_needs.'.$i.'.sector_id') ? $request->input('prioritisation_needs.'.$i.'.sector_id') : NULL;
                    $update_need->priority = $i + 1;
                    $update_need->description = $request->input('prioritisation_needs.'.$i.'.description');
                    $update_need->save();
                }
            }

            $attachment_ids = array();

            if($request->has('attachments'))
            {
                foreach ($request->input('attachments') as $index => $attachment)
                {
                    array_push($attachment_ids, $attachment['id']);
                }
            }

            $update_prioritisation->attachments()->sync(array_unique($attachment_ids));

            DB::commit();

            return redirect('/prioritisation/'.$id);
        }
        catch(Exception $e)
        {
            DB::rollback();

            abort(400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validator_rules($request)
    {
        $rules = [
            'ward_id' => 'required',
            'meeting_date' => 'date_format:d/m/Y',
            'prioritisation_needs.0.description' => 'required',
            'participants_male_0_14' => 'integer|min:0',
            'participants_female_0_14' => 'integer|min:0',
            'participants_disable_0_14' => 'integer|min:0',
            'participants_male_15_24' => 'integer|min:0',
            'participants_female_15_24' => 'integer|min:0',
            'participants_disable_15_24' => 'integer|min:0',
            'participants_male_25_59' => 'integer|min:0',
            'participants_female_25_59' => 'integer|min:0',
            'participants_disable_25_59' => 'integer|min:0',
            'participants_male_60_plus' => 'integer|min:0',
            'participants_female_60_plus' => 'integer|min:0',
            'participants_disable_60_plus' => 'integer|min:0',
            'beneficiaries_male_0_14' => 'integer|min:0',
            'beneficiaries_female_0_14' => 'integer|min:0',
            'beneficiaries_plwds_0_14' => 'integer|min:0',
            'beneficiaries_male_15_24' => 'integer|min:0',
            'beneficiaries_female_15_24' => 'integer|min:0',
            'beneficiaries_plwds_15_24' => 'integer|min:0',
            'beneficiaries_male_25_59' => 'integer|min:0',
            'beneficiaries_female_25_59' => 'integer|min:0',
            'beneficiaries_plwds_25_59' => 'integer|min:0',
            'beneficiaries_male_60_plus' => 'integer|min:0',
            'beneficiaries_female_60_plus' => 'integer|min:0',
            'beneficiaries_plwds_60_plus' => 'integer|min:0',
        ];

        foreach ((array)$request->input('prioritisation_places') as $index => $value) {
            $rules['prioritisation_places.'.$index.'.place'] = 'required_with:prioritisation_places.'.$index.'.count_total_village,prioritisation_places.'.$index.'.count_village_representative|max:255';
            $rules['prioritisation_places.'.$index.'.count_village_representative'] = 'required_with:prioritisation_places.'.$index.'.count_total_village,prioritisation_places.'.$index.'.place|integer|min:0';
            $rules['prioritisation_places.'.$index.'.count_total_village'] = 'required_with:prioritisation_places.'.$index.'.place,prioritisation_places.'.$index.'.count_village_representative|integer|min:0';
        }

        for ($i = 1; $i <= count($request->input('prioritisation_needs')); $i++) {
            $rules['prioritisation_needs.'.$i.'.description'] = 'required_with:prioritisation_needs.'.$i.'.sector_id|max:255';
            $rules['prioritisation_needs.'.$i.'.sector_id'] = 'required_with:prioritisation_needs.'.$i.'.description|integer|min:0';
        }

        return $rules;
    }

    public function validator_messages($request)
    {
        $messages = [
            'ward_id.required' => 'The Ward field is required.',
            'prioritisation_needs.0.description.required' => 'The top 1 of community priority need description field is required',
            'prioritisation_needs.0.sector_id.required' => 'The top 1 of community priority need sector field is required'
        ];

        foreach ((array)$request->input('prioritisation_places') as $index => $value) {
            $messages['prioritisation_places.'.$index.'.place.required_with'] = 'The place of meeting field is required when other fields is present in same row';
            $messages['prioritisation_places.'.$index.'.count_village_representative.required_with'] = 'The # of villages who have representative in the meeting field is required when other fields is present in same row';
            $messages['prioritisation_places.'.$index.'.count_total_village.required_with'] = 'The # of villages in the ward field is required when other fields is present in same row';
        }

        for ($i = 1; $i <= count($request->input('prioritisation_needs')); $i++) {
            $messages['prioritisation_needs.'.$i.'.description.required_with'] = 'The description field is required when sector is present in same row.';
            $messages['prioritisation_needs.'.$i.'.sector_id.required_with'] = 'The sector field is required when description is present in same row.';
        }

        return $messages;
    }
}
