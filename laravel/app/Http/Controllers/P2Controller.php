<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use DateTime;
use Auth;

use App\Services\FundService;
use App\Argo\SelectionMethod;
use App\Argo\Province;
use App\Argo\Ward;
use App\Argo\Village;
use App\Argo\Approval;
use App\Argo\Fund;
use App\Argo\Subproject;
use App\Argo\Prioritisation;

use App\Services\AutomationService;

class P2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::orderBy('name')->get();

        $array_data = array(
            'provinces' => $provinces,
        );

        return view('P2.index', $array_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selection_methods = SelectionMethod::orderBy('order')->get();
        $provinces = Province::orderBy('name')->get();

        $array_data = array(
            'selection_methods' => $selection_methods,
            'provinces'         => $provinces
        );

        return view('P2.create', $array_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->input();

        $this->validate($request, $this->validator_rules($request), $this->validator_messages($request));

        try
        {
            DB::beginTransaction();

            $new_approval = new Approval;
            $new_approval->prioritisation_id    = $request->prioritisation_id;
            $new_approval->selection_method     = $request->selection_method;
            $new_approval->meeting_date         = DateTime::createFromFormat('d/m/Y', $request->input('approval.meeting_date'));
            $new_approval->rsdlgp_funding_limit = $request->has('approval.rsdlgp_funding_limit') ? $request->input('approval.rsdlgp_funding_limit') : NULL;
            $new_approval->fund_estimate_origin = $request->has('approval.fund_estimate_origin') ? $request->input('approval.fund_estimate_origin') : NULL;
            $new_approval->created_by           = Auth::user()->id;
            $new_approval->save();

            AutomationService::create_p3_for_p2(
                $new_approval->id,
                $request->village_id,
                $request->subproject_name,
                $request->subproject_description
            );

            $new_rsdplgp_funding = new Fund;
            $new_rsdplgp_funding->approval_id   = $new_approval->id;
            $new_rsdplgp_funding->fund_type     = 'rdp';
            $new_rsdplgp_funding->origin_amount = $request->rsdplgp_funding_amount;
            $new_rsdplgp_funding->save();

            $new_community_funding = new Fund;
            $new_community_funding->approval_id   = $new_approval->id;
            $new_community_funding->fund_type     = 'community';
            $new_community_funding->origin_amount = $request->community_funding_amount;
            $new_community_funding->save();

            if($request->has('counterpart_fundings'))
            {
                FundService::create_counterpart_fundings($request->input('counterpart_fundings'), $new_approval->id);
            }

            if($request->has('attachments'))
            {
                $attachment_ids = array();

                foreach ($request->input('attachments') as $index => $attachment) {
                    array_push($attachment_ids, $attachment['id']);
                }

                $new_approval->attachments()->sync(array_unique($attachment_ids));
            }

            DB::commit();

            return redirect('/approval/'.$new_approval->id);
        }
        catch(Exception $e)
        {
            DB::rollback();

            abort(400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $approval = Approval::with(array(
                'funds',
                'attachments',
            ))
            ->findOrFail($id);

        $total_fund = Fund::where('approval_id', $approval->id)->sum('origin_amount');

        $subproject = Subproject::where('approval_id', $approval->id)->first();
        $priority = Prioritisation::with([
                'prioritisation_needs' => function($query) {
                    $query->where('priority', 1);
                    $query->select(['prioritisation_id', 'description']);
                }
            ])
            ->select(['id', 'ward_id'])
            ->findOrFail($approval->prioritisation_id);

        try
        {
            $location = Village::with([
                    'ward' => function($query){
                        $query->with([
                            'llg' => function($query){
                                $query->with([
                                    'district' => function($query){
                                        $query->with('province');
                                    }
                                ]);
                            }
                        ]);
                    }
                ])
                ->find($subproject->village_id);
        }
        catch(\Exception $e)
        {
            $location = Ward::with([
                'llg' => function($query){
                    $query->with([
                        'district' => function($query){
                            $query->with('province');
                        }
                    ]);
                }
            ])
            ->find($priority->ward_id);
        }

        $array_data = array(
            'approval'   => $approval,
            'total_fund' => $total_fund,
            'subproject' => $subproject,
            'priority'   => $priority,
            'location'   => $location
        );

        return view('P2.show', $array_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $approval = Approval::with(array(
                'rdp_fund',
                'community_fund',
                'counterpart_funds'
            ))
            ->findOrFail($id);

        $subproject = Subproject::where('approval_id', '=', $id)
            ->first();

        $location = Village::with([
                'ward' => function($query){
                    $query->with([
                        'llg' => function($query){
                            $query->with([
                                'district' => function($query){
                                    $query->with('province');
                                }
                            ]);
                        }
                    ]);
                }
            ])
            ->findOrFail($subproject->village_id);

        $location_ids = array(
            (string)$location->ward->llg->district->province->id,
            (string)$location->ward->llg->district->id,
            (string)$location->ward->llg->id,
            (string)$location->ward->id,
            (string)$location->id,
            (string)$approval->prioritisation_id
        );

        $selection_methods = SelectionMethod::orderBy('order')->get();
        $provinces = Province::orderBy('name')->get();

        $array_data = array(
            'approval'          => $approval,
            'subproject'        => $subproject,
            'locations'         => $location_ids,
            'selection_methods' => $selection_methods,
            'provinces'         => $provinces,
        );

        return view('P2.edit', $array_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validator_rules($request), $this->validator_messages($request));

        // return $request->input();

        try
        {
            DB::beginTransaction();

            $update_approval = Approval::find($id);
            $update_approval->prioritisation_id    = $request->prioritisation_id;
            $update_approval->selection_method     = $request->selection_method;
            $update_approval->meeting_date         = DateTime::createFromFormat('d/m/Y', $request->input('approval.meeting_date'));
            $update_approval->rsdlgp_funding_limit = $request->has('approval.rsdlgp_funding_limit') ? $request->input('approval.rsdlgp_funding_limit') : NULL;
            $update_approval->fund_estimate_origin = $request->has('approval.fund_estimate_origin') ? $request->input('approval.fund_estimate_origin') : NULL;
            $update_approval->save();

            AutomationService::edit_p3_for_p2(
                $update_approval->id,
                $request->village_id,
                $request->subproject_name,
                $request->subproject_description
            );

            if($request->has('approval.rdp_fund.origin_amount')){
                $update_fund = Fund::where('approval_id', '=', $update_approval->id)
                    ->where('fund_type', '=', 'rdp')
                    ->first();
                $update_fund->origin_amount = $request->input('approval.rdp_fund.origin_amount');
                $update_fund->save();
            }

            if($request->has('approval.community_fund.origin_amount')){
                $update_fund = Fund::where('approval_id', '=', $update_approval->id)
                    ->where('fund_type', '=', 'community')
                    ->first();
                $update_fund->origin_amount = $request->input('approval.community_fund.origin_amount');
                $update_fund->save();
            }

            foreach ($request->input('counterpart_fundings') as $key => $counterpart_funding) {
                if($counterpart_funding['source'] != "" and $counterpart_funding['origin_amount'] > 0)
                {
                    if($request->has('counterpart_fundings.'.$key.'.id'))
                    {
                        $update_fund = Fund::find($request->input('counterpart_fundings.'.$key.'.id'));
                        $update_fund->source         = $counterpart_funding['source'];
                        $update_fund->has_commitment = $counterpart_funding['has_commitment'];
                        $update_fund->origin_amount  = $counterpart_funding['origin_amount'];
                        $update_fund->save();
                    }
                    else
                    {
                        $new_fund = new Fund;
                        $new_fund->approval_id    = $update_approval->id;
                        $new_fund->fund_type      = 'counterpart';
                        $new_fund->source         = $counterpart_funding['source'];
                        $new_fund->has_commitment = $counterpart_funding['has_commitment'];
                        $new_fund->origin_amount  = $counterpart_funding['origin_amount'];
                        $new_fund->save();
                    }
                }
            }

            $attachment_ids = array();

            if($request->has('attachments'))
            {
                foreach ($request->input('attachments') as $index => $attachment) {
                    array_push($attachment_ids, $attachment['id']);
                }
            }

            $update_approval->attachments()->sync(array_unique($attachment_ids));

            DB::commit();

            return redirect('/approval/'.$update_approval->id);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return $e;
            abort(400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validator_rules($request)
    {
        $rules = [
            'approval.meeting_date' => 'required|date_format:d/m/Y',
            'approval.rsdplgp_funding_limit' => 'numeric|min:0',
            'approval.fund_estimate_origin' => 'numeric|min:0',
            'village_id' => 'required|integer',
            'prioritisation_id' => 'required|integer',
            'subproject_name' => 'required|max:255',
            'rsdplgp_funding_amount' => 'numeric|min:0',
            'community_funding_amount' => 'numeric|min:0',
        ];

        foreach ((array)$request->input('counterpart_fundings') as $index => $value) {
            $rules['counterpart_fundings.'.$index.'.source'] = 'required_with:counterpart_fundings.'.$index.'.origin_amount|max:255';
            $rules['counterpart_fundings.'.$index.'.origin_amount'] = 'required_with:counterpart_fundings.'.$index.'.source|numeric|min:0';
            $rules['counterpart_fundings.'.$index.'.has_commitment'] = 'required_with:counterpart_fundings.'.$index.'.source,counterpart_fundings.'.$index.'.origin_amount|boolean';
        }

        return $rules;
    }

    public function validator_messages($request)
    {
        $messages = [
            'approval.meeting_date.required' => 'The meeting date field is required.',
            'approval.meeting_date.date_format' => 'The date format of meeting date is wrong.',
            'approval.rsdplgp_funding_limit.numeric' => 'RSDLGP Funding limit must be a number.',
            'approval.rsdplgp_funding_limit.min' => 'RSDLGP Funding limit must be at least 0.',
            'approval.fund_estimate_origin.numeric' => 'Estimated cost of requested subproject must be a number.',
            'approval.fund_estimate_origin.min' => 'Estimated cost of requested subproject must be at least 0.',
            'village_id.required' => 'The village field is required.',
            'prioritisation_id.required' => 'The select priority field is required.',
            'subproject_name' => 'The subproject name field is required.',
            'rsdplgp_funding_amount.numeric' => 'RSDLGP fund amount must be a number.',
            'rsdplgp_funding_amount.min' => 'RSDLGP fund amount must be at least 0.',
            'community_funding_amount.numeric' => 'community fund amount must be a number.',
            'community_funding_amount.min' => 'community fund amount must be at least 0.',
        ];

        foreach ((array)$request->input('counterpart_fundings') as $index => $value) {
            $messages['counterpart_fundings.'.$index.'.source.required_with'] = 'Please provide the source of counterpart funding.';
            $messages['counterpart_fundings.'.$index.'.source.max'] = 'The source name of counterpart funding may not be greater than 255 characters.';
            $messages['counterpart_fundings.'.$index.'.origin_amount.required_with'] = 'Please enter the amount for counterpart funding.';
            $messages['counterpart_fundings.'.$index.'.origin_amount.numeric'] = 'The amount of counterpart funding field must be a number.';
            $messages['counterpart_fundings.'.$index.'.origin_amount.min'] = 'The amount of counterpart funding field must be at least 0.';
            $messages['counterpart_fundings.'.$index.'.has_commitment.required_with'] = 'Please confirm whether letters of commitment for counterpart funding/co-financing is provided.';
            $messages['counterpart_fundings.'.$index.'.has_commitment.boolean'] = 'Has commitment field must be true or false.';
        }

        return $messages;
    }
}
