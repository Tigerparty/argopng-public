<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\Argo\Province;
use App\Argo\ProgressReport;
use App\Argo\Prioritisation;
use App\Argo\Sector;
use App\Argo\Fund;

class HomeController extends Controller
{
    public function home()
    {
        //-- get provinces
        $provinces = Province::orderBy('name')->get(['id', 'name']);

        $reports = ProgressReport::orderBy('created_at', 'DESC')->take(5)->get();

        $array_data = array(
            'provinces' => $provinces,
            'reports'   => $reports,
            'tabslet_ds' => $this->getTabSletInformations(),
        );

        return view('home', $array_data);
    }

    private function getTabSletInformations()
    {
        $populations = \App\Argo\Prioritisation::select([
                DB::raw('sum(participants_male_0_14) AS male_0_14'),
                DB::raw('sum(participants_male_15_24) AS male_15_24'),
                DB::raw('sum(participants_male_25_59) AS male_25_59'),
                DB::raw('sum(participants_male_60_plus) AS male_60_plus'),
                DB::raw('sum(participants_female_0_14) AS female_0_14'),
                DB::raw('sum(participants_female_15_24) AS female_15_24'),
                DB::raw('sum(participants_female_25_59) AS female_25_59'),
                DB::raw('sum(participants_female_60_plus) AS female_60_plus'),
            ])
            ->first();

        $total_of_bnf = (object)array(
            'mp_amount' => $populations->male_0_14 + $populations->male_15_24 + $populations->male_25_59 + $populations->male_60_plus,
            'fmp_amount' => $populations->female_0_14 + $populations->female_15_24 + $populations->female_25_59 + $populations->female_60_plus
        );

        $sps_of_sector = Sector::select([
                'sector.name',
                DB::raw('count(subproject.id) AS sp_count')
            ])
            ->where('sector.name', '!=', 'Unknown')
            ->groupBy('sector.id')
            ->leftJoin('prioritisation_need', function($join){
                $join->on('sector.id', '=', 'prioritisation_need.sector_id')
                    ->on('prioritisation_need.priority', '=', DB::raw('1'));
            })
            ->leftJoin('prioritisation', 'prioritisation_need.prioritisation_id', '=', 'prioritisation.id')
            ->leftJoin('approval', 'prioritisation.id', '=', 'approval.prioritisation_id')
            ->leftJoin('subproject', 'approval.id', '=', 'subproject.approval_id')
            ->get();

        $completion_of_provinces = Province::select([
                'province.name',
                DB::raw('count(work_activity.id) AS total_activity_count')
            ])
            ->groupBy('province.id')
            ->leftJoin('district', 'province.id', '=', 'district.province_id')
            ->leftJoin('llg', 'district.id', '=', 'llg.district_id')
            ->leftJoin('ward', 'llg.id', '=', 'ward.llg_id')
            ->leftJoin('village', 'ward.id', '=', 'village.ward_id')
            ->leftJoin('subproject', 'village.id', '=', 'subproject.village_id')
            ->leftJoin('work_stage', 'subproject.id', '=', 'work_stage.subproject_id')
            ->leftJoin('work_activity', 'work_stage.id', '=', 'work_activity.stage_id')
            ->get();

        $completed_activity_of_provinces = Province::select([
                'province.name',
                DB::raw('count(work_activity.id) AS activity_count')
            ])
            ->groupBy('province.id')
            ->leftJoin('district', 'province.id', '=', 'district.province_id')
            ->leftJoin('llg', 'district.id', '=', 'llg.district_id')
            ->leftJoin('ward', 'llg.id', '=', 'ward.llg_id')
            ->leftJoin('village', 'ward.id', '=', 'village.ward_id')
            ->leftJoin('subproject', 'village.id', '=', 'subproject.village_id')
            ->leftJoin('work_stage', 'subproject.id', '=', 'work_stage.subproject_id')
            ->leftJoin('work_activity', function($join){
                $join->on('work_stage.id', '=', 'work_activity.stage_id')
                    ->on('work_activity.completed', '=', DB::raw('1'));
            })
            ->get();

        foreach ($completion_of_provinces as $key => $completion_of_province) {
            $completion_of_province->completed_activity_count = $completed_activity_of_provinces[$key]->activity_count;
        }

        $total_of_ctbr = Fund::select(['fund_type', DB::raw('SUM(accuate_amount) AS amount')])
            ->groupBy('fund_type')
            ->whereIn('fund_type', ['rdp', 'community'])
            ->orderBy('fund_type', 'DESC')
            ->get();

        $tabslet_data_set = (object)array(
            'total_of_bnf' => $total_of_bnf,
            'sps_of_sector' => $sps_of_sector,
            'completion_of_provinces' => $completion_of_provinces,
            'total_of_ctbr' => $total_of_ctbr
        );

        return $tabslet_data_set;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
