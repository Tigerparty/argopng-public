<?php

namespace App\Services;

use Auth;
use App\Argo\Issue;

class IssueService
{
	public static function create_and_update_issue($issues, $subproject_id)
	{
		foreach ($issues as $index => $issue)
    {
      if(array_key_exists('id', $issue))
      {
        $update_issue = Issue::find($issue['id']);
        if($update_issue->is_resolved == 0 and 
        	 $issue['is_resolved'] == 1 and
        	 $update_issue->resolved_at == NULL)
        {
          $update_issue->is_resolved = 1;
          $update_issue->resolved_at = date('Y-m-d');
          $update_issue->save();
        }
      }
      else
      {
        if($issue['desc'] != "")
        {
          $new_issue = new Issue;
          $new_issue->subproject_id = $subproject_id;
          $new_issue->desc = $issue['desc'];
          $new_issue->is_resolved = 0;
          $new_issue->created_by = Auth::user()->id;
          $new_issue->save();
        }   
      }
    }
	}
}