<?php 

namespace App\Services;

use App\Argo\Subproject;

class AutomationService 
{
	public static function create_p3_for_p2($approval_id, $village_id, $name, $description = null)
	{
		$new_subproject = new Subproject;
    $new_subproject->approval_id = $approval_id;
    $new_subproject->village_id = $village_id;
    $new_subproject->name = $name;
    $new_subproject->description = $description;
    $new_subproject->save();
	}

	public static function edit_p3_for_p2($approval_id, $village_id, $name, $description = null)
	{
		$update_subproject = Subproject::where('approval_id', '=', $approval_id)->first();
		$update_subproject->village_id = $village_id;
		$update_subproject->name = $name;
		$update_subproject->description = $description;
		$update_subproject->save();
	}
}