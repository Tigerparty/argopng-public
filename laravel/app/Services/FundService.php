<?php 

namespace App\Services;

use App\Argo\Fund;

class FundService 
{
	public static function create_counterpart_fundings($counterpart_fundings, $approval_id)
	{
		foreach ($counterpart_fundings as $counterpart_funding) {
      if($counterpart_funding['source'] != "" and 
      	 $counterpart_funding['origin_amount'] != "" and
      	 array_key_exists('has_commitment', $counterpart_funding))
      {
        $new_fund = new Fund;
        $new_fund->approval_id    = $approval_id;
        $new_fund->fund_type      = 'counterpart';
        $new_fund->source         = $counterpart_funding['source'];
        $new_fund->has_commitment = $counterpart_funding['has_commitment'];
        $new_fund->origin_amount  = $counterpart_funding['origin_amount'];
        $new_fund->save();
      }
  	}
	}
}