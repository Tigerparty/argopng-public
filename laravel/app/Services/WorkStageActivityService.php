<?php

namespace App\Services;

use App\Argo\WorkStage;
use App\Argo\WorkActivity;

class WorkStageActivityService
{
	public static function save_stage_and_activity($work_stages, $subproject_id)
	{
    $origin_work_stage_ids = WorkStage::where('subproject_id', '=', $subproject_id)->lists('id');
    $update_work_stage_ids = array();

		foreach ($work_stages as $index => $work_stage) {
      array_push($update_work_stage_ids, $work_stage['id']);

      if($work_stage['id'] != "")
      {
        //update stg
        $update_work_stage = WorkStage::find($work_stage['id']);
        $update_work_stage->subproject_id = $subproject_id;
        $update_work_stage->stage 			  = "Stage" . ( $index + 1 );
        $update_work_stage->description   = "Stage_Description" . ( $index + 1);
        $update_work_stage->save();

        $origin_work_activity_ids = WorkActivity::where('stage_id', '=', $update_work_stage->id)->lists('id');

        if(array_key_exists('work_activities', $work_stage))
        {
          $update_work_activity_ids = array();

          foreach ($work_stage['work_activities'] as $index => $work_activity) {
            if($work_activity['id'] != "")
            {
              array_push($update_work_activity_ids, $work_activity['id']);
              //update act
              $update_work_activity = WorkActivity::find($work_activity['id']);
              $update_work_activity->description = $work_activity['description'];
              $update_work_stage->work_activities()->save($update_work_activity);
            }
            else
            {
              //create act
              $update_work_activity = new WorkActivity;
              $update_work_activity->description = $work_activity['description'];
              $update_work_stage->work_activities()->save($update_work_activity);
            }
          }
        }

        $delete_work_activity_ids = array();

        foreach ($origin_work_activity_ids as $origin_work_activity_id) {
          if(!in_array($origin_work_activity_id, $update_work_activity_ids))
          {
            array_push($delete_work_activity_ids, $origin_work_activity_id);
          }
        }

        if(count($delete_work_activity_ids) > 0)
        {
          $delete_work_activities = WorkActivity::where('id', '=', $delete_work_activity_ids)->get();

          foreach ($delete_work_activities as $delete_work_activity) {
            $delete_work_activity->report_modify()->detach();
            $delete_work_activity->delete();
          }
        }
      }
      else
      {
        //create stg
        $new_work_stage = new WorkStage;
        $new_work_stage->subproject_id = $subproject_id;
        $new_work_stage->stage 				 = "Stage" . ( $index + 1 );
        $new_work_stage->description 	 = "Stage_Description" . ( $index + 1);
        $new_work_stage->save();

        if(array_key_exists('work_activities', $work_stage))
        {
          foreach ($work_stage['work_activities'] as $index => $work_activity) {
          	//create act
            $new_work_activity = new WorkActivity;
            $new_work_activity->description = $work_activity['description'];
            $new_work_stage->work_activities()->save($new_work_activity);
          }
        }
      }
  	}

    $delete_work_stage_ids = array();

    foreach ($origin_work_stage_ids as $origin_work_stage_id) {
      if(!in_array($origin_work_stage_id, $update_work_stage_ids))
      {
        array_push($delete_work_stage_ids, $origin_work_stage_id);
      }
    }

    if(count($delete_work_stage_ids) > 0)
    {
      $delete_work_activities = WorkActivity::whereIn('stage_id', $delete_work_stage_ids)->get();

      foreach ($delete_work_activities as $delete_work_activity) {
        $delete_work_activity->report_modify()->detach();
        $delete_work_activity->delete();
      }

      WorkStage::whereIn('id', $delete_work_stage_ids)->delete();
    }
	}

  public static function update_work_activities($work_stages, $report_id)
  {
    foreach ($work_stages as $stg_idx => $work_stages)
    {
      if(array_key_exists('work_activities', $work_stages))
      {
        foreach ($work_stages['work_activities'] as $act_idx => $work_activity)
        {
          $update_work_activity = WorkActivity::find($work_activity['id']);

          if($update_work_activity->completed == 0 and 
             $work_activity['completed'] == 1 and
             $update_work_activity->completed_at == NULL)
          {
            $update_work_activity->completed = 1;
            $update_work_activity->completed_at = date('Y-m-d');
            $update_work_activity->save();
            $update_work_activity->report_modify()->attach($report_id);
          }
        }
      }
    }
  }

  public static function reset_work_activity_once_delete_report($activities)
  {
    foreach ($activities as $modify_activity)
    {
      $activity = WorkActivity::find($modify_activity['id']);
      $report_modify = $activity->report_modify()->select('id')->get();

      if(count($report_modify) == 0){
        $activity->completed = 0;
        $activity->completed_at = null;
      }

      $activity->save();
    }
  }
}