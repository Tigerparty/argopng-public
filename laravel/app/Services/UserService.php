<?php 

namespace App\Services;

use Auth;

class UserService 
{
	public static function getIdentity() 
	{
		if(Auth::check())
		{
			return Auth::user()->identity;
		}
		else
		{
			return config('argo.default_identity');
		}
	}

	public static function isDeleteAble()
	{
		return self::getIdentity() == config('argo.admin_identity');
	}
}