<?php

namespace App\Repository;

use App\Argo\Subproject;
use DB;

class SubprojectRepository {
	public static function getSubprojectList()
	{
		$subproject = Subproject::select([
				'subproject.id',
				'subproject.name',
				'p.name as p_name',
				'd.name as d_name',
				'l.name as l_name',
				'w.name as w_name',
				'v.name as v_name',
				'd.province_id',
				'l.district_id',
				'w.llg_id',
				'v.ward_id'
			])
			->leftJoin('village as v', 'subproject.village_id', '=', 'v.id')
			->leftJoin('ward as w', 'v.ward_id', '=', 'w.id')
			->leftJoin('llg as l', 'w.llg_id', '=', 'l.id')
			->leftJoin('district as d', 'l.district_id', '=', 'd.id')
			->leftJoin('province as p', 'd.province_id', '=', 'p.id')
			->orderBy('updated_at', 'DESC');

		return $subproject;
	}

	public static function getSubprojectsWithCoordinate($subproject_id = NULL)
	{
		$subproject = Subproject::with(array(
				'recent_progress_report' => function ($query){
					$query->select([
						'id',
						'subproject_id',
						'monitoring_date',
						'lat',
						'lng'
					]);
					$query->with(array(
						'recent_image' => function ($query){
							$query->select('attachment.id');
						},
					));
				},
				'recent_image' => function ($query){
					$query->select('attachment.id');
				},
			))
			->leftJoin('village as v', 'subproject.village_id', '=', 'v.id')
			->leftJoin('ward as w', 'v.ward_id', '=', 'w.id')
			->leftJoin('llg as l', 'w.llg_id', '=', 'l.id')
			->leftJoin('district as d', 'l.district_id', '=', 'd.id')
			->leftJoin('province as p', 'd.province_id', '=', 'p.id')
			->select(DB::raw('
				subproject.id,
				subproject.name,
				subproject.description,
				p.name as p_name,
				d.name as d_name,
				l.name as l_name,
				w.name as w_name,
				v.name as v_name
			'));

		if($subproject_id)
		{
			$subproject = $subproject->find($subproject_id);
		}

		return $subproject;
	}
}