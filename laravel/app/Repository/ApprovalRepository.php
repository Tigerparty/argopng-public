<?php

namespace App\Repository;

use App\Argo\Approval;

class ApprovalRepository {
	public static function getApprovalList()
	{
		$approval = Approval::select([
				'approval.id',
				'approval.meeting_date',
				'p.name as p_name',
				'd.name as d_name',
				'l.name as l_name',
				'w.name as w_name',
				'd.province_id',
				'l.district_id',
				'w.llg_id',
			])
			->leftJoin('prioritisation', 'approval.prioritisation_id', '=', 'prioritisation.id')
			->leftJoin('ward as w', 'prioritisation.ward_id', '=', 'w.id')
			->leftJoin('llg as l', 'w.llg_id', '=', 'l.id')
			->leftJoin('district as d', 'l.district_id', '=', 'd.id')
			->leftJoin('province as p', 'd.province_id', '=', 'p.id')
			->orderBy('approval.updated_at', 'DESC');

		return $approval;
	}
}