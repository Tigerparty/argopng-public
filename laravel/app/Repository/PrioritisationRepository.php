<?php

namespace App\Repository;

use App\Argo\Prioritisation;

class PrioritisationRepository {
	public static function getPrioritisationList()
	{
		$prioritisations = Prioritisation::select([
				'prioritisation.id',
				'prioritisation.meeting_date',
				'p.name as p_name',
				'd.name as d_name',
				'l.name as l_name',
				'w.name as w_name',
				'd.province_id',
				'l.district_id',
				'w.llg_id',
			])
			->leftJoin('ward as w', 'prioritisation.ward_id', '=', 'w.id')
			->leftJoin('llg as l', 'w.llg_id', '=', 'l.id')
			->leftJoin('district as d', 'l.district_id', '=', 'd.id')
			->leftJoin('province as p', 'd.province_id', '=', 'p.id')
			->orderBy('updated_at', 'DESC');

		return $prioritisations;
	}
}