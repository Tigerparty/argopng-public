<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('approval_id', false, 11)->nullable();
            $table->string('fund_type', 255);
            $table->string('source', 255)->nullable();
            $table->boolean('has_commitment')->nullable();
            $table->double('origin_amount', 15, 8)->nullable();
            $table->double('accuate_amount', 15, 8)->nullable();

            $table->foreign('approval_id')->references('id')->on('approval');
            $table->foreign('fund_type')->references('fund_type')->on('fund_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund');
    }
}
