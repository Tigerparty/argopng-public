<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubprojectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subproject', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('approval_id', false, 11)->nullable();
            $table->integer('village_id', false, 11)->nullable();
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->integer('expected_duration', false, 5)->nullable();
            $table->double('fund_estimate_new', 15, 8)->nullable();
            $table->boolean('is_technically_feasible')->nullable();
            $table->boolean('is_financially_feasible')->nullable();
            $table->string('is_co_financed', 20)->nullable();
            $table->string('credential_prepared_by', 255)->nullable();
            $table->date('credential_prepared_date')->nullable();
            $table->string('credential_confirmed_by', 255)->nullable();
            $table->date('credential_confirmed_date')->nullable();
            $table->string('credential_certified_by', 255)->nullable();
            $table->date('credential_certified_date')->nullable();
            $table->integer('created_by', false, 11)->nullable();
            $table->timestamps();

            $table->foreign('approval_id')->references('id')->on('approval');
            $table->foreign('village_id')->references('id')->on('village');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subproject');
    }
}
