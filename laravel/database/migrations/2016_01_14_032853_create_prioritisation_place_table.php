<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrioritisationPlaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prioritisation_place', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prioritisation_id', false, 11)->nullable();
            $table->string('place', 225)->nullable();
            $table->integer('count_village_representative', false, 11)->nullable();
            $table->integer('count_total_village', false, 11)->nullable();

            $table->foreign('prioritisation_id')->references('id')->on('prioritisation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prioritisation_place');
    }
}
