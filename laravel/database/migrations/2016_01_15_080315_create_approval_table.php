<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prioritisation_id', false ,11)->nullable();
            $table->string('selection_method', 255)->nullable();
            $table->date('meeting_date')->nullable();
            $table->double('rsdlgp_funding_limit', 15, 8)->nullable();
            $table->double('fund_estimate_origin', 15, 8)->nullable();
            $table->integer('created_by', false, 11)->nullable();
            $table->timestamps();

            $table->foreign('prioritisation_id')->references('id')->on('prioritisation');
            $table->foreign('selection_method')->references('name')->on('selection_method');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval');
    }
}
