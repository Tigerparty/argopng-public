<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrioritisationNeedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prioritisation_need', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prioritisation_id', false, 11)->nullable();
            $table->integer('sector_id', false, 11)->nullable();
            $table->integer('priority', false, 11)->nullable();
            $table->text('description')->nullable();

            $table->foreign('prioritisation_id')->references('id')->on('prioritisation');
            $table->foreign('sector_id')->references('id')->on('sector');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prioritisation_need');
    }
}
