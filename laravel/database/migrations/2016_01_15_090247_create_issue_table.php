<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subproject_id', false, 11);
            $table->text('desc');
            $table->boolean('is_resolved');
            $table->date('resolved_at')->nullable();
            $table->integer('created_by', false, 11);
            $table->timestamps();

            $table->foreign('subproject_id')->references('id')->on('subproject');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue');
    }
}
