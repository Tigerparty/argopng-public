<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationReportModifyActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_report_modify_activity', function (Blueprint $table) {
            $table->integer('report_id', false, 11);
            $table->integer('activity_id', false, 11);

            $table->primary(['report_id', 'activity_id']);
            $table->foreign('report_id')->references('id')->on('progress_report');
            $table->foreign('activity_id')->references('id')->on('work_activity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relation_report_modify_activity');
    }
}
