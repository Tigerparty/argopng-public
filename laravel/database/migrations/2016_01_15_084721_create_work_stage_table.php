<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkStageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_stage', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subproject_id', false, 11)->nullable();
            $table->string('stage', 255)->nullable();
            $table->text('description')->nullable();

            $table->foreign('subproject_id')->references('id')->on('subproject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_stage');
    }
}
