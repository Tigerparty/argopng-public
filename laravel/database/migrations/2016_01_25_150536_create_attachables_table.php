<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachables', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('attachment_id', false, 11)->nullable();
            $table->morphs('attachable');

            $table->foreign('attachment_id')->references('id')->on('attachment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attachables');
    }
}
