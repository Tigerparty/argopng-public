<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stage_id', false, 11)->nullable();
            $table->text('description')->nullable();
            $table->boolean('completed');
            $table->date('completed_at')->nullable();

            $table->foreign('stage_id')->references('id')->on('work_stage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_activity');
    }
}
