<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrioritisationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prioritisation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ward_id', false, 11);
            $table->integer('need_identify_id', false, 11)->nullable();
            $table->date('meeting_date')->nullable();

            $table->integer('participants_male_0_14', false, 5)->nullable();
            $table->integer('participants_female_0_14', false, 5)->nullable();
            $table->integer('participants_disable_0_14', false, 5)->nullable();
            $table->integer('participants_male_15_24', false, 5)->nullable();
            $table->integer('participants_female_15_24', false, 5)->nullable();
            $table->integer('participants_disable_15_24', false, 5)->nullable();
            $table->integer('participants_male_25_59', false, 5)->nullable();
            $table->integer('participants_female_25_59', false, 5)->nullable();
            $table->integer('participants_disable_25_59', false, 5)->nullable();
            $table->integer('participants_male_60_plus', false, 5)->nullable();
            $table->integer('participants_female_60_plus', false, 5)->nullable();
            $table->integer('participants_disable_60_plus', false, 5)->nullable();

            $table->integer('beneficiaries_male_0_14', false, 5)->nullable();
            $table->integer('beneficiaries_female_0_14', false, 5)->nullable();
            $table->integer('beneficiaries_plwds_0_14', false, 5)->nullable();
            $table->integer('beneficiaries_male_15_24', false, 5)->nullable();
            $table->integer('beneficiaries_female_15_24', false, 5)->nullable();
            $table->integer('beneficiaries_plwds_15_24', false, 5)->nullable();
            $table->integer('beneficiaries_male_25_59', false, 5)->nullable();
            $table->integer('beneficiaries_female_25_59', false, 5)->nullable();
            $table->integer('beneficiaries_plwds_25_59', false, 5)->nullable();
            $table->integer('beneficiaries_male_60_plus', false, 5)->nullable();
            $table->integer('beneficiaries_female_60_plus', false, 5)->nullable();
            $table->integer('beneficiaries_plwds_60_plus', false, 5)->nullable();

            $table->integer('created_by', false, 11)->nullable();
            $table->timestamps();

            $table->foreign('ward_id')->references('id')->on('ward');
            $table->foreign('need_identify_id')->references('id')->on('need_identify');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prioritisation');
    }
}
