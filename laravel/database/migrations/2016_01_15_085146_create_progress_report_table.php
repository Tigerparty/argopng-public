<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progress_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subproject_id', false, 11);
            $table->integer('created_by', false, 11);
            $table->date('monitoring_date')->nullable();
            $table->string('completed_by', 255)->nullable();
            $table->text('financial_kept')->nullable();
            $table->boolean('is_contributions_updated')->nullable();
            $table->text('procurement_change')->nullable();
            $table->text('safeguard')->nullable();
            $table->text('contribution_changing')->nullable();
            $table->text('comment')->nullable();
            $table->double('lat', 15, 12)->nullable();
            $table->double('lng', 15, 12)->nullable();
            $table->timestamps();

            $table->foreign('subproject_id')->references('id')->on('subproject');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progress_report');
    }
}
