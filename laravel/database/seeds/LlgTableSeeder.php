<?php

use Illuminate\Database\Seeder;

class LlgTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $record = array(
        array('id' => 1, 'name' => "Cloudy Bay",    'district_id' => 1),
        array('id' => 2, 'name' => "Tapini",        'district_id' => 2),
        array('id' => 3, 'name' => "Kairuku",       'district_id' => 3),
        array('id' => 4, 'name' => "Rigo Inland",   'district_id' => 4),
        array('id' => 5, 'name' => "Lake Murray",   'district_id' => 5),
        array('id' => 6, 'name' => "Kiunga Rural",  'district_id' => 6),
        array('id' => 7, 'name' => "Kiwai",         'district_id' => 7),
        array('id' => 8, 'name' => "Oriomo-Bituri", 'district_id' => 7),
    );

        DB::table('llg')->insert($record);
  }
}
