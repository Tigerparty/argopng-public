<?php

use Illuminate\Database\Seeder;

class FundTypeTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array('fund_type' => 'rdp'),
            array('fund_type' => 'community'),
            array('fund_type' => 'counterpart'),
        );

        DB::table('fund_type')->insert($record);
    }
}
