<?php

use Illuminate\Database\Seeder;

class SelectionMethodTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array('order' => 1, 'name' => 'Open competition'),
            array('order' => 2, 'name' => 'Need-based'),
        );

        DB::table('selection_method')->insert($record);
    }
}
