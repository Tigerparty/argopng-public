<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UsersTableSeeder::class);
        $this->call(ProvinceTableSeeder::class);
        $this->call(DistrictTableSeeder::class);
        $this->call(LlgTableSeeder::class);
        $this->call(WardTableSeeder::class);
        $this->call(VillageTableSeeder::class);
        $this->call(FundTypeTableSeeder::class);
        $this->call(SectorTableSeeder::class);
        $this->call(NeedIdentifyTableSeeder::class);
        $this->call(SelectionMethodTableSeeder::class);

        Model::reguard();
    }
}
