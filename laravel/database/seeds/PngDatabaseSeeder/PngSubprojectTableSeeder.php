<?php

use Illuminate\Database\Seeder;

class PngSubprojectTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$record = array(
      array('id' => 1, 'approval_id' => 1, 'name' => 'Solar Lighting', 'village_id' => 1, 'fund_estimate_new' => 9269800),
      array('id' => 2, 'approval_id' => 2, 'name' => 'Village Couthouse', 'village_id' => 2, 'fund_estimate_new' => 7500000),
      array('id' => 3, 'approval_id' => 3, 'name' => 'Water Catchment', 'village_id' => 4, 'fund_estimate_new' => 7394400),
      array('id' => 4, 'approval_id' => 4, 'name' => 'Aid Post', 'village_id' => 5, 'fund_estimate_new' => 7500000),
      array('id' => 5, 'approval_id' => 5, 'name' => 'Classroom Building', 'village_id' => 6, 'fund_estimate_new' => 7500000),
      array('id' => 6, 'approval_id' => 6, 'name' => 'Community Hall', 'village_id' => 7, 'fund_estimate_new' => 7466800),
      array('id' => 7, 'approval_id' => 7, 'name' => 'Classroom Building', 'village_id' => 8, 'fund_estimate_new' => 10000000),
      array('id' => 8, 'approval_id' => 8, 'name' => 'Health Worker\'s House', 'village_id' => 10, 'fund_estimate_new' => 66800.2),
      array('id' => 9, 'approval_id' => 9, 'name' => 'Aid Post', 'village_id' => 11, 'fund_estimate_new' => 7500000),
      array('id' => 10, 'approval_id' => 10, 'name' => 'Water Catchment', 'village_id' => 12, 'fund_estimate_new' => 9045600),
      array('id' => 11, 'approval_id' => 11, 'name' => 'Water Catchment', 'village_id' => 13, 'fund_estimate_new' => null),
      array('id' => 12, 'approval_id' => 12, 'name' => 'Water Catchment', 'village_id' => 14, 'fund_estimate_new' => null),
      array('id' => 13, 'approval_id' => 13, 'name' => 'Aid Post', 'village_id' => 15, 'fund_estimate_new' => null),
      array('id' => 14, 'approval_id' => 14, 'name' => 'Aid Post', 'village_id' => 16, 'fund_estimate_new' => null),
      array('id' => 15, 'approval_id' => 15, 'name' => 'Water Catchment', 'village_id' => 17, 'fund_estimate_new' => null),
      array('id' => 16, 'approval_id' => 16, 'name' => 'Classroom Building', 'village_id' => 18, 'fund_estimate_new' => null),
      array('id' => 17, 'approval_id' => 17, 'name' => 'Aid Post', 'village_id' => 23, 'fund_estimate_new' => null),
      array('id' => 18, 'approval_id' => 18, 'name' => 'Solar Lighting', 'village_id' => 25, 'fund_estimate_new' => 5000000),
      array('id' => 19, 'approval_id' => 19, 'name' => 'Classroom Building', 'village_id' => 26, 'fund_estimate_new' => null),
      array('id' => 20, 'approval_id' => 20, 'name' => 'Classroom Building', 'village_id' => 27, 'fund_estimate_new' => null),
      array('id' => 21, 'approval_id' => 21, 'name' => 'Aid Post', 'village_id' => 29, 'fund_estimate_new' => null),
      array('id' => 22, 'approval_id' => 22, 'name' => 'Classroom Building', 'village_id' => 30, 'fund_estimate_new' => null),
      array('id' => 23, 'approval_id' => 23, 'name' => 'School Library', 'village_id' => 31, 'fund_estimate_new' => null),
      array('id' => 24, 'approval_id' => 24, 'name' => 'Aid Post Upgrade', 'village_id' => 32, 'fund_estimate_new' => null),
      array('id' => 25, 'approval_id' => 25, 'name' => 'Water Catchment', 'village_id' => 33, 'fund_estimate_new' => null),
      array('id' => 26, 'approval_id' => 26, 'name' => 'Water Catchment', 'village_id' => 34, 'fund_estimate_new' => null),
      array('id' => 27, 'approval_id' => 27, 'name' => 'Water Catchment', 'village_id' => 35, 'fund_estimate_new' => null),
      array('id' => 28, 'approval_id' => 28, 'name' => 'Water Catchment', 'village_id' => 36, 'fund_estimate_new' => null),
      array('id' => 29, 'approval_id' => 29, 'name' => 'Water Catchment', 'village_id' => 37, 'fund_estimate_new' => null),
      array('id' => 30, 'approval_id' => 30, 'name' => 'Aid Post', 'village_id' => 38, 'fund_estimate_new' => null),
      array('id' => 31, 'approval_id' => 31, 'name' => 'Aid Post', 'village_id' => 39, 'fund_estimate_new' => null),
      array('id' => 32, 'approval_id' => 32, 'name' => 'Water Catchment', 'village_id' => 41, 'fund_estimate_new' => null),
      array('id' => 33, 'approval_id' => 33, 'name' => 'Water Catchment', 'village_id' => 42, 'fund_estimate_new' => null),
      array('id' => 34, 'approval_id' => 34, 'name' => 'Water Catchment', 'village_id' => 43, 'fund_estimate_new' => null),
      array('id' => 35, 'approval_id' => 35, 'name' => 'Aid Post', 'village_id' => 44, 'fund_estimate_new' => null),
      array('id' => 36, 'approval_id' => 36, 'name' => 'Aid Post', 'village_id' => 48, 'fund_estimate_new' => null),
      array('id' => 37, 'approval_id' => 37, 'name' => 'Foot Bridge', 'village_id' => 50, 'fund_estimate_new' => null),
      array('id' => 38, 'approval_id' => 38, 'name' => 'Teacher\'s House', 'village_id' => 51, 'fund_estimate_new' => null),
      array('id' => 39, 'approval_id' => 39, 'name' => 'Single Classroom with Office & Water', 'village_id' => 52, 'fund_estimate_new' => null),
      array('id' => 40, 'approval_id' => 40, 'name' => 'Teacher\'s House', 'village_id' => 53, 'fund_estimate_new' => null),
      array('id' => 41, 'approval_id' => 41, 'name' => 'Aid Post', 'village_id' => 54, 'fund_estimate_new' => null),
      array('id' => 42, 'approval_id' => 42, 'name' => 'Aid Post', 'village_id' => 55, 'fund_estimate_new' => null),
      array('id' => 43, 'approval_id' => 43, 'name' => 'Resource Centre', 'village_id' => 57, 'fund_estimate_new' => null),
      array('id' => 44, 'approval_id' => 44, 'name' => 'Aid Post', 'village_id' => 58, 'fund_estimate_new' => null),
      array('id' => 45, 'approval_id' => 45, 'name' => 'Aid Post', 'village_id' => 59, 'fund_estimate_new' => null),
      array('id' => 46, 'approval_id' => 46, 'name' => 'Community Health Worker\'s (CHWs) House', 'village_id' => 60, 'fund_estimate_new' => null),
      array('id' => 47, 'approval_id' => 47, 'name' => 'Aid Post', 'village_id' => 61, 'fund_estimate_new' => null),
      array('id' => 48, 'approval_id' => 48, 'name' => 'Aid Post', 'village_id' => 62, 'fund_estimate_new' => null),
      array('id' => 49, 'approval_id' => 49, 'name' => 'Classroom Building', 'village_id' => 63, 'fund_estimate_new' => null),
      array('id' => 50, 'approval_id' => 50, 'name' => 'Aid Post', 'village_id' => 64, 'fund_estimate_new' => null),
      array('id' => 51, 'approval_id' => 51, 'name' => 'Aid Post', 'village_id' => 65, 'fund_estimate_new' => null),
      array('id' => 52, 'approval_id' => 52, 'name' => 'Aid Post', 'village_id' => 66, 'fund_estimate_new' => null),
      array('id' => 53, 'approval_id' => 53, 'name' => 'Aid Post', 'village_id' => 67, 'fund_estimate_new' => null),
      array('id' => 54, 'approval_id' => 54, 'name' => 'Water Catchment', 'village_id' => 68, 'fund_estimate_new' => null),
      array('id' => 55, 'approval_id' => 55, 'name' => 'Water Catchment', 'village_id' => 69, 'fund_estimate_new' => null),
      array('id' => 56, 'approval_id' => 56, 'name' => 'Water Catchment', 'village_id' => 70, 'fund_estimate_new' => null),
      array('id' => 57, 'approval_id' => 57, 'name' => 'Water Catchment', 'village_id' => 71, 'fund_estimate_new' => null),
      array('id' => 58, 'approval_id' => 58, 'name' => 'Water Catchment', 'village_id' => 72, 'fund_estimate_new' => null),
      array('id' => 59, 'approval_id' => 59, 'name' => 'Water Catchment', 'village_id' => 73, 'fund_estimate_new' => null),
      array('id' => 60, 'approval_id' => 60, 'name' => 'Water Catchment', 'village_id' => 74, 'fund_estimate_new' => null),
      array('id' => 61, 'approval_id' => 61, 'name' => 'Water Catchment', 'village_id' => 75, 'fund_estimate_new' => null),
      array('id' => 62, 'approval_id' => 62, 'name' => 'Water Catchment', 'village_id' => 76, 'fund_estimate_new' => null),
      array('id' => 63, 'approval_id' => 63, 'name' => 'Solar Lighting', 'village_id' => 77, 'fund_estimate_new' => null),
      array('id' => 64, 'approval_id' => 64, 'name' => 'Aid Post', 'village_id' => 78, 'fund_estimate_new' => null),
      array('id' => 65, 'approval_id' => 65, 'name' => 'Motor & Dinghy', 'village_id' => 79, 'fund_estimate_new' => null),
      array('id' => 66, 'approval_id' => 66, 'name' => 'Solar Lighting', 'village_id' => 80, 'fund_estimate_new' => null),
      array('id' => 67, 'approval_id' => 67, 'name' => 'Community Hall/Water Catchment', 'village_id' => 81, 'fund_estimate_new' => null),
      array('id' => 68, 'approval_id' => 68, 'name' => 'Water Catchment', 'village_id' => 82, 'fund_estimate_new' => null),
      array('id' => 69, 'approval_id' => 69, 'name' => 'Aid Post', 'village_id' => 83, 'fund_estimate_new' => null),
      array('id' => 70, 'approval_id' => 70, 'name' => 'Classroom Building', 'village_id' => 84, 'fund_estimate_new' => null),
      array('id' => 71, 'approval_id' => 71, 'name' => 'Foot Bridge', 'village_id' => 85, 'fund_estimate_new' => null),
      array('id' => 72, 'approval_id' => 72, 'name' => 'Classroom Building', 'village_id' => 86, 'fund_estimate_new' => null),
      array('id' => 73, 'approval_id' => 73, 'name' => 'Commmunity Hall', 'village_id' => 87, 'fund_estimate_new' => null),
      array('id' => 74, 'approval_id' => 74, 'name' => 'Classroom Building', 'village_id' => 88, 'fund_estimate_new' => null),
  	);

    DB::table('subproject')->insert($record);
  }
}