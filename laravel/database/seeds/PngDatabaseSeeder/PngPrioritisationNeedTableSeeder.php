<?php

use Illuminate\Database\Seeder;

class PngPrioritisationNeedTableSeeder extends Seeder
{
      /**
      * Run the database seeds.
      *
      * @return void
      */
      public function run()
      {
            $subproject_count = \App\Argo\Prioritisation::all()->count();

            $record = array();

            for ($i = 0; $i < $subproject_count; $i++) { 
                  for ($j=1; $j < 6; $j++) { 
                        array_push($record, array('prioritisation_id' => $i+1 , 'sector_id' => NULL, 'priority' => $j, 'description' => ""));
                  }
            }

            DB::table('prioritisation_need')->insert($record);
      }
}
