<?php

use Illuminate\Database\Seeder;

class PngWorkActivityTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$record = array(
  		array('stage_id' => 1, 'description' => 'Laying of Foundation', 'completed' => 0, 'completed_at' => null),
      array('stage_id' => 1, 'description' => 'Pouring Concrete', 'completed' => 0, 'completed_at' => null),
      array('stage_id' => 2, 'description' => 'Act 2-1', 'completed' => 0, 'completed_at' => null),
      array('stage_id' => 2, 'description' => 'Act 2-2', 'completed' => 0, 'completed_at' => null),
  	);

    DB::table('work_activity')->insert($record);
  }
}