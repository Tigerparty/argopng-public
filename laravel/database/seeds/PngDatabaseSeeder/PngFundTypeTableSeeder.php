<?php

use Illuminate\Database\Seeder;

class PngFundTypeTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $record = array(
    	array('fund_type' => 'RSDLGP'),
    	array('fund_type' => 'Community'),
      array('fund_type' => 'District'),
      array('fund_type' => 'LLG'),
      array('fund_type' => 'Province'),
      array('fund_type' => 'Provincial Administration'),
      array('fund_type' => 'Ward (ikc)'),
    	array('fund_type' => 'counterpart')
    );

    DB::table('fund_type')->insert($record);
  }
}
