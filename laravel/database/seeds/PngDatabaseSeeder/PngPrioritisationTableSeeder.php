<?php

use Illuminate\Database\Seeder;

class PngPrioritisationTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$record = array(
      array('id' => 1, 'ward_id' => 1),
      array('id' => 2, 'ward_id' => 2),
      array('id' => 3, 'ward_id' => 3),
      array('id' => 4, 'ward_id' => 4),
      array('id' => 5, 'ward_id' => 5),
      array('id' => 6, 'ward_id' => 6),
      array('id' => 7, 'ward_id' => 7),
      array('id' => 8, 'ward_id' => 8),
      array('id' => 9, 'ward_id' => 9),
      array('id' => 10, 'ward_id' => 10),
      array('id' => 11, 'ward_id' => 11),
      array('id' => 12, 'ward_id' => 12),
      array('id' => 13, 'ward_id' => 13),
      array('id' => 14, 'ward_id' => 14),
      array('id' => 15, 'ward_id' => 15),
      array('id' => 16, 'ward_id' => 16),
      array('id' => 17, 'ward_id' => 17),
      array('id' => 18, 'ward_id' => 18),
      array('id' => 19, 'ward_id' => 19),
      array('id' => 20, 'ward_id' => 20),
      array('id' => 21, 'ward_id' => 21),
      array('id' => 22, 'ward_id' => 22),
      array('id' => 23, 'ward_id' => 23),
      array('id' => 24, 'ward_id' => 24),
      array('id' => 25, 'ward_id' => 25),
      array('id' => 26, 'ward_id' => 26),
      array('id' => 27, 'ward_id' => 27),
      array('id' => 28, 'ward_id' => 28),
      array('id' => 29, 'ward_id' => 29),
      array('id' => 30, 'ward_id' => 30),
      array('id' => 31, 'ward_id' => 31),
      array('id' => 32, 'ward_id' => 32),
      array('id' => 33, 'ward_id' => 33),
      array('id' => 34, 'ward_id' => 34),
      array('id' => 35, 'ward_id' => 35),
      array('id' => 36, 'ward_id' => 36),
      array('id' => 37, 'ward_id' => 37),
      array('id' => 38, 'ward_id' => 38),
      array('id' => 39, 'ward_id' => 39),
      array('id' => 40, 'ward_id' => 40),
      array('id' => 41, 'ward_id' => 41),
      array('id' => 42, 'ward_id' => 42),
      array('id' => 43, 'ward_id' => 43),
      array('id' => 44, 'ward_id' => 44),
      array('id' => 45, 'ward_id' => 45),
      array('id' => 46, 'ward_id' => 46),
      array('id' => 47, 'ward_id' => 47),
      array('id' => 48, 'ward_id' => 48),
      array('id' => 49, 'ward_id' => 49),
      array('id' => 50, 'ward_id' => 50),
      array('id' => 51, 'ward_id' => 51),
      array('id' => 52, 'ward_id' => 52),
      array('id' => 53, 'ward_id' => 53),
      array('id' => 54, 'ward_id' => 54),
      array('id' => 55, 'ward_id' => 55),
      array('id' => 56, 'ward_id' => 56),
      array('id' => 57, 'ward_id' => 57),
      array('id' => 58, 'ward_id' => 58),
      array('id' => 59, 'ward_id' => 59),
      array('id' => 60, 'ward_id' => 60),
      array('id' => 61, 'ward_id' => 61),
      array('id' => 62, 'ward_id' => 62),
      array('id' => 63, 'ward_id' => 63),
      array('id' => 64, 'ward_id' => 64),
      array('id' => 65, 'ward_id' => 65),
      array('id' => 66, 'ward_id' => 66),
      array('id' => 67, 'ward_id' => 67),
      array('id' => 68, 'ward_id' => 68),
      array('id' => 69, 'ward_id' => 69),
      array('id' => 70, 'ward_id' => 70),
      array('id' => 71, 'ward_id' => 71),
      array('id' => 72, 'ward_id' => 72),
      array('id' => 73, 'ward_id' => 73),
      array('id' => 74, 'ward_id' => 74),
  	);

    DB::table('prioritisation')->insert($record);
  }
}