<?php

use Illuminate\Database\Seeder;

class PngProvinceTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $record = array(
    	array('id' => 1, 'name' => 'Central'),
    	array('id' => 2, 'name' => 'Western')
    );

    DB::table('province')->insert($record);
  }
}
