<?php

use Illuminate\Database\Seeder;

class PngApprovalTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array('id' => 1, 'prioritisation_id' => 1, 'fund_estimate_origin' => 4874000),
            array('id' => 2, 'prioritisation_id' => 2, 'fund_estimate_origin' => 5945901),
            array('id' => 3, 'prioritisation_id' => 3, 'fund_estimate_origin' => 3457500),
            array('id' => 4, 'prioritisation_id' => 4, 'fund_estimate_origin' => 0),
            array('id' => 5, 'prioritisation_id' => 5, 'fund_estimate_origin' => 5562333),
            array('id' => 6, 'prioritisation_id' => 6, 'fund_estimate_origin' => 5256000),
            array('id' => 7, 'prioritisation_id' => 7, 'fund_estimate_origin' => 6118100),
            array('id' => 8, 'prioritisation_id' => 8, 'fund_estimate_origin' => 4106900),
            array('id' => 9, 'prioritisation_id' => 9, 'fund_estimate_origin' => 6225524),
            array('id' => 10, 'prioritisation_id' => 10, 'fund_estimate_origin' => 6345600),
            array('id' => 11, 'prioritisation_id' => 11, 'fund_estimate_origin' => 1650000),
            array('id' => 12, 'prioritisation_id' => 12, 'fund_estimate_origin' => 1650000),
            array('id' => 13, 'prioritisation_id' => 13, 'fund_estimate_origin' => 0),
            array('id' => 14, 'prioritisation_id' => 14, 'fund_estimate_origin' => 0),
            array('id' => 15, 'prioritisation_id' => 15, 'fund_estimate_origin' => 1650000),
            array('id' => 16, 'prioritisation_id' => 16, 'fund_estimate_origin' => 0),
            array('id' => 17, 'prioritisation_id' => 17, 'fund_estimate_origin' => 0),
            array('id' => 18, 'prioritisation_id' => 18, 'fund_estimate_origin' => 3250000),
            array('id' => 19, 'prioritisation_id' => 19, 'fund_estimate_origin' => 0),
            array('id' => 20, 'prioritisation_id' => 20, 'fund_estimate_origin' => 0),
            array('id' => 21, 'prioritisation_id' => 21, 'fund_estimate_origin' => 0),
            array('id' => 22, 'prioritisation_id' => 22, 'fund_estimate_origin' => 0),
            array('id' => 23, 'prioritisation_id' => 23, 'fund_estimate_origin' => 0),
            array('id' => 24, 'prioritisation_id' => 24, 'fund_estimate_origin' => 0),
            array('id' => 25, 'prioritisation_id' => 25, 'fund_estimate_origin' => 1650000),
            array('id' => 26, 'prioritisation_id' => 26, 'fund_estimate_origin' => 1650000),
            array('id' => 27, 'prioritisation_id' => 27, 'fund_estimate_origin' => 1650000),
            array('id' => 28, 'prioritisation_id' => 28, 'fund_estimate_origin' => 1650000),
            array('id' => 29, 'prioritisation_id' => 29, 'fund_estimate_origin' => 0),
            array('id' => 30, 'prioritisation_id' => 30, 'fund_estimate_origin' => 0),
            array('id' => 31, 'prioritisation_id' => 31, 'fund_estimate_origin' => 0),
            array('id' => 32, 'prioritisation_id' => 32, 'fund_estimate_origin' => 1650000),
            array('id' => 33, 'prioritisation_id' => 33, 'fund_estimate_origin' => 1650000),
            array('id' => 34, 'prioritisation_id' => 34, 'fund_estimate_origin' => 1650000),
            array('id' => 35, 'prioritisation_id' => 35, 'fund_estimate_origin' => 0),
            array('id' => 36, 'prioritisation_id' => 36, 'fund_estimate_origin' => 0),
            array('id' => 37, 'prioritisation_id' => 37, 'fund_estimate_origin' => 0),
            array('id' => 38, 'prioritisation_id' => 38, 'fund_estimate_origin' => 0),
            array('id' => 39, 'prioritisation_id' => 39, 'fund_estimate_origin' => 0),
            array('id' => 40, 'prioritisation_id' => 40, 'fund_estimate_origin' => 0),
            array('id' => 41, 'prioritisation_id' => 41, 'fund_estimate_origin' => 0),
            array('id' => 42, 'prioritisation_id' => 42, 'fund_estimate_origin' => 0),
            array('id' => 43, 'prioritisation_id' => 43, 'fund_estimate_origin' => 0),
            array('id' => 44, 'prioritisation_id' => 44, 'fund_estimate_origin' => 0),
            array('id' => 45, 'prioritisation_id' => 45, 'fund_estimate_origin' => 0),
            array('id' => 46, 'prioritisation_id' => 46, 'fund_estimate_origin' => 0),
            array('id' => 47, 'prioritisation_id' => 47, 'fund_estimate_origin' => 0),
            array('id' => 48, 'prioritisation_id' => 48, 'fund_estimate_origin' => 0),
            array('id' => 49, 'prioritisation_id' => 49, 'fund_estimate_origin' => 0),
            array('id' => 50, 'prioritisation_id' => 50, 'fund_estimate_origin' => 0),
            array('id' => 51, 'prioritisation_id' => 51, 'fund_estimate_origin' => 0),
            array('id' => 52, 'prioritisation_id' => 52, 'fund_estimate_origin' => 0),
            array('id' => 53, 'prioritisation_id' => 53, 'fund_estimate_origin' => 0),
            array('id' => 54, 'prioritisation_id' => 54, 'fund_estimate_origin' => 0),
            array('id' => 55, 'prioritisation_id' => 55, 'fund_estimate_origin' => 0),
            array('id' => 56, 'prioritisation_id' => 56, 'fund_estimate_origin' => 0),
            array('id' => 57, 'prioritisation_id' => 57, 'fund_estimate_origin' => 0),
            array('id' => 58, 'prioritisation_id' => 58, 'fund_estimate_origin' => 0),
            array('id' => 59, 'prioritisation_id' => 59, 'fund_estimate_origin' => 0),
            array('id' => 60, 'prioritisation_id' => 60, 'fund_estimate_origin' => 0),
            array('id' => 61, 'prioritisation_id' => 61, 'fund_estimate_origin' => 0),
            array('id' => 62, 'prioritisation_id' => 62, 'fund_estimate_origin' => 0),
            array('id' => 63, 'prioritisation_id' => 63, 'fund_estimate_origin' => 0),
            array('id' => 64, 'prioritisation_id' => 64, 'fund_estimate_origin' => 0),
            array('id' => 65, 'prioritisation_id' => 65, 'fund_estimate_origin' => 0),
            array('id' => 66, 'prioritisation_id' => 66, 'fund_estimate_origin' => 0),
            array('id' => 67, 'prioritisation_id' => 67, 'fund_estimate_origin' => 0),
            array('id' => 68, 'prioritisation_id' => 68, 'fund_estimate_origin' => 0),
            array('id' => 69, 'prioritisation_id' => 69, 'fund_estimate_origin' => 0),
            array('id' => 70, 'prioritisation_id' => 70, 'fund_estimate_origin' => 0),
            array('id' => 71, 'prioritisation_id' => 71, 'fund_estimate_origin' => 0),
            array('id' => 72, 'prioritisation_id' => 72, 'fund_estimate_origin' => 0),
            array('id' => 73, 'prioritisation_id' => 73, 'fund_estimate_origin' => 0),
            array('id' => 74, 'prioritisation_id' => 74, 'fund_estimate_origin' => 0),
        );

        \DB::table('approval')->insert($record);
    }
}