<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PngDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PngUsersTableSeeder::class);
        $this->call(PngProvinceTableSeeder::class);
        $this->call(PngDistrictTableSeeder::class);
        $this->call(PngLlgTableSeeder::class);
        $this->call(PngWardTableSeeder::class);
        $this->call(PngVillageTableSeeder::class);
        $this->call(PngFundTypeTableSeeder::class);
        $this->call(PngSectorTableSeeder::class);
        $this->call(PngNeedIdentifyTableSeeder::class);
        $this->call(PngSelectionMethodTableSeeder::class);
        $this->call(PngPrioritisationTableSeeder::class);
        $this->call(PngPrioritisationNeedTableSeeder::class);
        // $this->call(PngPrioritisationPlaceTableSeeder::class);
        $this->call(PngApprovalTableSeeder::class);
        $this->call(PngFundTableSeeder::class);
        $this->call(PngSubprojectTableSeeder::class);
        $this->call(PngWorkStageTableSeeder::class);
        $this->call(PngWorkActivityTableSeeder::class);
        // $this->call(PngProgressReportTableSeeder::class);
        // $this->call(PngIssueTableSeeder::class);

        Model::reguard();
    }
}