<?php

use Illuminate\Database\Seeder;

class PngNeedIdentifyTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array('id' => 1, 'name' => "Ward Development Planning Meeting"),
            array('id' => 2, 'name' => "Community Planning Meeting"),
            array('id' => 3, 'name' => "Ward Development Committee Meeting"),
            array('id' => 4, 'name' => "Special Interests Needs Meeting"),
        );

        DB::table('need_identify')->insert($record);
    }
}
