<?php

use Illuminate\Database\Seeder;

class PngSectorTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $record = array(
    	array('name' => 'Recreation, Culture and Religion'),
      array('name' => 'Public Order and Safety'),
      array('name' => 'Economic Affairs'),
      array('name' => 'Social Protection'),
      array('name' => 'Housing and Community'),
      array('name' => 'Education'),
      array('name' => 'Defence'),
      array('name' => 'Environmental Protection'),
      array('name' => 'Health'),
      array('name' => 'General Public Services'),
      array('name' => 'Unknown'),
    );

    DB::table('sector')->insert($record);
  }
}
