<?php

use Illuminate\Database\Seeder;

class PngWorkStageTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$record = array(
  		array('id' => 1, 'subproject_id' => 1, 'stage' => 'Stage 1'),
      array('id' => 2, 'subproject_id' => 1, 'stage' => 'Stage 2'),
  	);

    DB::table('work_stage')->insert($record);
  }
}