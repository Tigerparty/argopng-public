<?php

use Illuminate\Database\Seeder;

class PrioritisationNeedTableSeeder extends Seeder
{
      /**
      * Run the database seeds.
      *
      * @return void
      */
      public function run()
      {
            $record = array(
                  array('prioritisation_id' => 1 , 'sector_id' => 1,    'priority' => 1, 'description' => "Classroom Building"), // Classroom Building (1)
                  array('prioritisation_id' => 1 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 1 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 1 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 1 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 2 , 'sector_id' => 1,    'priority' => 1, 'description' => "Health Worker's House"), // Health Worker's House (2)
                  array('prioritisation_id' => 2 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 2 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 2 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 2 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 3 , 'sector_id' => 1,    'priority' => 1, 'description' => "Solar Lighting"), // Solar Lighting (3)
                  array('prioritisation_id' => 3 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 3 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 3 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 3 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 4 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (4)
                  array('prioritisation_id' => 4 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 4 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 4 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 4 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 5 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (5)
                  array('prioritisation_id' => 5 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 5 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 5 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 5 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 6 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (6)
                  array('prioritisation_id' => 6 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 6 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 6 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 6 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 7 , 'sector_id' => 1,    'priority' => 1, 'description' => "Classroom Building"), // Classroom Building (7)
                  array('prioritisation_id' => 7 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 7 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 7 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 7 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 8 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (8)
                  array('prioritisation_id' => 8 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 8 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 8 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 8 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 9 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (9)
                  array('prioritisation_id' => 9 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 9 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 9 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 9 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 10 , 'sector_id' => 1,    'priority' => 1, 'description' => "Community Hall"), // Community Hall (10)
                  array('prioritisation_id' => 10 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 10 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 10 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 10 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 11 , 'sector_id' => 1,    'priority' => 1, 'description' => "Village Couthouse"), // Village Couthouse (11)
                  array('prioritisation_id' => 11 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 11 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 11 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 11 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 12 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (12)
                  array('prioritisation_id' => 12 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 12 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 12 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 12 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 13 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (13)
                  array('prioritisation_id' => 13 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 13 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 13 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 13 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 14 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (14)
                  array('prioritisation_id' => 14 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 14 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 14 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 14 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 15 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (15)
                  array('prioritisation_id' => 15 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 15 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 15 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 15 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 16 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (16)
                  array('prioritisation_id' => 16 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 16 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 16 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 16 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 17 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (17)
                  array('prioritisation_id' => 17 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 17 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 17 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 17 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 18 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (18)
                  array('prioritisation_id' => 18 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 18 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 18 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 18 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 19 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (19)
                  array('prioritisation_id' => 19 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 19 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 19 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 19 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 20 , 'sector_id' => 1,    'priority' => 1, 'description' => "School Library"), // School Library (20)
                  array('prioritisation_id' => 20 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 20 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 20 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 20 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 21 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (21)
                  array('prioritisation_id' => 21 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 21 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 21 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 21 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 22 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (22)
                  array('prioritisation_id' => 22 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 22 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 22 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 22 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 23 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (23)
                  array('prioritisation_id' => 23 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 23 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 23 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 23 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 24 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post Upgrade"), // Aid Post Upgrade (24)
                  array('prioritisation_id' => 24 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 24 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 24 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 24 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 25 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (25)
                  array('prioritisation_id' => 25 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 25 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 25 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 25 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 26 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post Upgrade"), // Aid Post Upgrade (26)
                  array('prioritisation_id' => 26 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 26 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 26 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 26 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 27 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (27)
                  array('prioritisation_id' => 27 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 27 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 27 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 27 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 28 , 'sector_id' => 1,    'priority' => 1, 'description' => "Classroom Building"), // Classroom Building (28)
                  array('prioritisation_id' => 28 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 28 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 28 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 28 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 29 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (29)
                  array('prioritisation_id' => 29 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 29 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 29 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 29 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 30 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (30)
                  array('prioritisation_id' => 30 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 30 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 30 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 30 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 31 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (31)
                  array('prioritisation_id' => 31 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 31 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 31 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 31 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 32 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (32)
                  array('prioritisation_id' => 32 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 32 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 32 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 32 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 33 , 'sector_id' => 1,    'priority' => 1, 'description' => "Solar Lighting"), // Solar Lighting (33)
                  array('prioritisation_id' => 33 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 33 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 33 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 33 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 34 , 'sector_id' => 1,    'priority' => 1, 'description' => "Solar Lighting"), // Solar Lighting (34)
                  array('prioritisation_id' => 34 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 34 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 34 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 34 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 35 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (35)
                  array('prioritisation_id' => 35 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 35 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 35 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 35 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 36 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (36)
                  array('prioritisation_id' => 36 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 36 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 36 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 36 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 37 , 'sector_id' => 1,    'priority' => 1, 'description' => "Gravity Fed/Bore Water Supply "), // Gravity Fed/Bore Water Supply  (37)
                  array('prioritisation_id' => 37 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 37 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 37 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 37 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 38 , 'sector_id' => 1,    'priority' => 1, 'description' => "Solar Lighting"), // Solar Lighting (38)
                  array('prioritisation_id' => 38 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 38 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 38 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 38 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 39 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (39)
                  array('prioritisation_id' => 39 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 39 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 39 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 39 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 40 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (40)
                  array('prioritisation_id' => 40 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 40 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 40 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 40 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 41 , 'sector_id' => 1,    'priority' => 1, 'description' => "Classroom Building"), // Classroom Building (41)
                  array('prioritisation_id' => 41 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 41 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 41 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 41 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 42 , 'sector_id' => 1,    'priority' => 1, 'description' => "Teacher's House"), // Teacher's House (42)
                  array('prioritisation_id' => 42 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 42 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 42 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 42 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 43 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (43)
                  array('prioritisation_id' => 43 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 43 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 43 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 43 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 44 , 'sector_id' => 1,    'priority' => 1, 'description' => "Teacher's House"), // Teacher's House (44)
                  array('prioritisation_id' => 44 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 44 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 44 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 44 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 45 , 'sector_id' => 1,    'priority' => 1, 'description' => "Single Classroom with Office & Water"), // Single Classroom with Office & Water (45)
                  array('prioritisation_id' => 45 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 45 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 45 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 45 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 46 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (46)
                  array('prioritisation_id' => 46 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 46 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 46 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 46 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 47 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (47)
                  array('prioritisation_id' => 47 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 47 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 47 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 47 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 48 , 'sector_id' => 1,    'priority' => 1, 'description' => "Foot Bridge"), // Foot Bridge (48)
                  array('prioritisation_id' => 48 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 48 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 48 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 48 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 49 , 'sector_id' => 1,    'priority' => 1, 'description' => "Resource Centre"), // Resource Centre (49)
                  array('prioritisation_id' => 49 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 49 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 49 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 49 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 50 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (50)
                  array('prioritisation_id' => 50 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 50 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 50 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 50 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 51 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (51)
                  array('prioritisation_id' => 51 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 51 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 51 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 51 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 52 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (52)
                  array('prioritisation_id' => 52 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 52 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 52 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 52 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 53 , 'sector_id' => 1,    'priority' => 1, 'description' => "Classroom Building"), // Classroom Building (53)
                  array('prioritisation_id' => 53 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 53 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 53 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 53 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 54 , 'sector_id' => 1,    'priority' => 1, 'description' => "Community Health Worker's (CHWs) House"), // Community Health Worker's (CHWs) House (54)
                  array('prioritisation_id' => 54 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 54 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 54 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 54 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 55 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (55)
                  array('prioritisation_id' => 55 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 55 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 55 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 55 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 56 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (56)
                  array('prioritisation_id' => 56 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 56 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 56 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 56 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 57 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (57)
                  array('prioritisation_id' => 57 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 57 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 57 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 57 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 58 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (58)
                  array('prioritisation_id' => 58 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 58 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 58 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 58 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 59 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (59)
                  array('prioritisation_id' => 59 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 59 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 59 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 59 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 60 , 'sector_id' => 1,    'priority' => 1, 'description' => "Classroom Building"), // Classroom Building (60)
                  array('prioritisation_id' => 60 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 60 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 60 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 60 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 61 , 'sector_id' => 1,    'priority' => 1, 'description' => "Classroom Building"), // Classroom Building (61)
                  array('prioritisation_id' => 61 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 61 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 61 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 61 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 62 , 'sector_id' => 1,    'priority' => 1, 'description' => "Classroom Building"), // Classroom Building (62)
                  array('prioritisation_id' => 62 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 62 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 62 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 62 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 63 , 'sector_id' => 1,    'priority' => 1, 'description' => "Commmunity Hall"), // Commmunity Hall (63)
                  array('prioritisation_id' => 63 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 63 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 63 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 63 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 64 , 'sector_id' => 1,    'priority' => 1, 'description' => "Foot Bridge"), // Foot Bridge (64)
                  array('prioritisation_id' => 64 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 64 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 64 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 64 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 65 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (65)
                  array('prioritisation_id' => 65 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 65 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 65 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 65 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 66 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (66)
                  array('prioritisation_id' => 66 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 66 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 66 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 66 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 67 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (67)
                  array('prioritisation_id' => 67 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 67 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 67 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 67 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 68 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (68)
                  array('prioritisation_id' => 68 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 68 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 68 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 68 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 69 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (69)
                  array('prioritisation_id' => 69 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 69 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 69 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 69 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 70 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (70)
                  array('prioritisation_id' => 70 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 70 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 70 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 70 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 71 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (71)
                  array('prioritisation_id' => 71 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 71 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 71 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 71 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 72 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (72)
                  array('prioritisation_id' => 72 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 72 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 72 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 72 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 73 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (73)
                  array('prioritisation_id' => 73 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 73 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 73 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 73 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 74 , 'sector_id' => 1,    'priority' => 1, 'description' => "Solar Lighting"), // Solar Lighting (74)
                  array('prioritisation_id' => 74 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 74 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 74 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 74 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 75 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (75)
                  array('prioritisation_id' => 75 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 75 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 75 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 75 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 76 , 'sector_id' => 1,    'priority' => 1, 'description' => "Community Hall/Water Catchment"), // Community Hall/Water Catchment (76)
                  array('prioritisation_id' => 76 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 76 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 76 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 76 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 77 , 'sector_id' => 1,    'priority' => 1, 'description' => "Solar Lighting"), // Solar Lighting (77)
                  array('prioritisation_id' => 77 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 77 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 77 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 77 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 78 , 'sector_id' => 1,    'priority' => 1, 'description' => "Aid Post"), // Aid Post (78)
                  array('prioritisation_id' => 78 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 78 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 78 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 78 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 79 , 'sector_id' => 1,    'priority' => 1, 'description' => "Water Catchment"), // Water Catchment (79)
                  array('prioritisation_id' => 79 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 79 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 79 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 79 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),

                  array('prioritisation_id' => 80 , 'sector_id' => 1,    'priority' => 1, 'description' => "Motor & Dinghy"), // Motor & Dinghy (80)
                  array('prioritisation_id' => 80 , 'sector_id' => NULL, 'priority' => 2, 'description' => ""),
                  array('prioritisation_id' => 80 , 'sector_id' => NULL, 'priority' => 3, 'description' => ""),
                  array('prioritisation_id' => 80 , 'sector_id' => NULL, 'priority' => 4, 'description' => ""),
                  array('prioritisation_id' => 80 , 'sector_id' => NULL, 'priority' => 5, 'description' => ""),
            );

            DB::table('prioritisation_need')->insert($record);
      }
}
