<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DemoDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PrioritisationTableSeeder::class);
        $this->call(PrioritisationNeedTableSeeder::class);
        $this->call(PrioritisationPlaceTableSeeder::class);
        $this->call(ApprovalTableSeeder::class);
        $this->call(FundTableSeeder::class);
        $this->call(SubprojectTableSeeder::class);
        $this->call(WorkStageTableSeeder::class);
        $this->call(WorkActivityTableSeeder::class);
        $this->call(ProgressReportTableSeeder::class);
        $this->call(IssueTableSeeder::class);

        Model::reguard();
    }
}