<?php

use Illuminate\Database\Seeder;

class IssueTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$record = array(
  		array(
  			'id' => 1,
  			'subproject_id' => 1,
        'desc' => 'Unstoppable issue',
        'is_resolved' => 0,
        'resolved_at' => NULL,
        'created_by' => 1,
        'created_at' => '2012-12-21 08:34:43'
  		),
      array(
        'id' => 2,
        'subproject_id' => 1,
        'desc' => 'Dominating issue',
        'is_resolved' => 1,
        'resolved_at' => '2012-12-21',
        'created_by' => 1,
        'created_at' => '2012-12-21 08:34:43'
      ),
      array(
        'id' => 3,
        'subproject_id' => 2,
        'desc' => 'Unbelieveble issue',
        'is_resolved' => 1,
        'resolved_at' => '2014-12-12',
        'created_by' => 1,
        'created_at' => '2012-12-21 08:34:43'
      ),
      array(
        'id' => 4,
        'subproject_id' => 2,
        'desc' => 'Perfect issue',
        'is_resolved' => 0,
        'resolved_at' => NULL,
        'created_by' => 1,
        'created_at' => '2012-12-21 08:34:43'
      ),
      array(
        'id' => 5,
        'subproject_id' => 3,
        'desc' => 'Romantic issue',
        'is_resolved' => 1,
        'resolved_at' => '2015-08-12',
        'created_by' => 1,
        'created_at' => '2012-12-21 08:34:43'
      ),
      array(
        'id' => 6,
        'subproject_id' => 3,
        'desc' => 'Kind issue',
        'is_resolved' => 0,
        'resolved_at' => NULL,
        'created_by' => 1,
        'created_at' => '2012-12-21 08:34:43'
      ),
  	);

    DB::table('issue')->insert($record);
  }
}