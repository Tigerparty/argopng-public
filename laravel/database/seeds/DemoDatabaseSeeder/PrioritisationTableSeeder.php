<?php
use Illuminate\Database\Seeder;
class PrioritisationTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array('id' =>  1, 'ward_id' =>  1, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Classroom Building
            array('id' =>  2, 'ward_id' =>  2, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Health Worker's House
            array('id' =>  3, 'ward_id' =>  3, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Solar Lighting
            array('id' =>  4, 'ward_id' =>  4, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' =>  5, 'ward_id' =>  5, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' =>  6, 'ward_id' =>  6, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' =>  7, 'ward_id' =>  7, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Classroom Building
            array('id' =>  8, 'ward_id' =>  8, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' =>  9, 'ward_id' =>  9, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 10, 'ward_id' => 10, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Community Hall
            array('id' => 11, 'ward_id' => 11, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Village Couthouse
            array('id' => 12, 'ward_id' => 12, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 13, 'ward_id' => 13, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 14, 'ward_id' => 14, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 15, 'ward_id' => 15, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 16, 'ward_id' => 16, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 17, 'ward_id' => 17, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 18, 'ward_id' => 18, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 19, 'ward_id' => 19, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 20, 'ward_id' => 20, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // School Library
            array('id' => 21, 'ward_id' => 21, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 22, 'ward_id' => 22, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 23, 'ward_id' => 23, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 24, 'ward_id' => 24, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post Upgrade
            array('id' => 25, 'ward_id' => 25, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 26, 'ward_id' => 26, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post Upgrade
            array('id' => 27, 'ward_id' => 27, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 28, 'ward_id' => 28, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Classroom Building
            array('id' => 29, 'ward_id' => 29, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 30, 'ward_id' => 30, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 31, 'ward_id' => 31, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 32, 'ward_id' => 32, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 33, 'ward_id' => 33, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Solar Lighting
            array('id' => 34, 'ward_id' => 34, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Solar Lighting
            array('id' => 35, 'ward_id' => 35, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 36, 'ward_id' => 36, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 37, 'ward_id' => 37, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Gravity Fed/Bore Water Supply
            array('id' => 38, 'ward_id' => 38, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Solar Lighting
            array('id' => 39, 'ward_id' => 39, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 40, 'ward_id' => 40, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 41, 'ward_id' => 41, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Classroom Building
            array('id' => 42, 'ward_id' => 42, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Teacher's House
            array('id' => 43, 'ward_id' => 43, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 44, 'ward_id' => 44, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Teacher's House
            array('id' => 45, 'ward_id' => 45, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Single Classroom with Office & Water
            array('id' => 46, 'ward_id' => 46, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 47, 'ward_id' => 47, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 48, 'ward_id' => 48, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Foot Bridge
            array('id' => 49, 'ward_id' => 49, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Resource Centre
            array('id' => 50, 'ward_id' => 50, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 51, 'ward_id' => 51, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 52, 'ward_id' => 52, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 53, 'ward_id' => 53, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Classroom Building
            array('id' => 54, 'ward_id' => 54, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Community Health Worker's (CHWs) House
            array('id' => 55, 'ward_id' => 55, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 56, 'ward_id' => 56, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 57, 'ward_id' => 57, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 58, 'ward_id' => 58, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 59, 'ward_id' => 59, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 60, 'ward_id' => 60, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Classroom Building
            array('id' => 61, 'ward_id' => 61, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Classroom Building
            array('id' => 62, 'ward_id' => 62, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Classroom Building
            array('id' => 63, 'ward_id' => 63, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Commmunity Hall
            array('id' => 64, 'ward_id' => 64, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Foot Bridge
            array('id' => 65, 'ward_id' => 65, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 66, 'ward_id' => 66, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 67, 'ward_id' => 67, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 68, 'ward_id' => 68, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 69, 'ward_id' => 69, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 70, 'ward_id' => 70, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 71, 'ward_id' => 71, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 72, 'ward_id' => 72, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 73, 'ward_id' => 73, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 74, 'ward_id' => 74, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Solar Lighting
            array('id' => 75, 'ward_id' => 75, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 76, 'ward_id' => 76, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Community Hall/Water Catchment
            array('id' => 77, 'ward_id' => 77, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Solar Lighting
            array('id' => 78, 'ward_id' => 78, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Aid Post
            array('id' => 79, 'ward_id' => 79, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Water Catchment
            array('id' => 80, 'ward_id' => 80, 'need_identify_id' => 1, 'meeting_date' => '2016-04-01', 'created_by' => 1, 'created_at' => '2016-04-01', 'updated_at' => '2016-04-01'), // Motor & Dinghy
        );
        DB::table('prioritisation')->insert($record);
    }
}