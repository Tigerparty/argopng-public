<?php

use Illuminate\Database\Seeder;

class WorkStageTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$record = array(
  		array('id' => 1, 'subproject_id' => 1, 'stage' => 'Stage 1'),
      array('id' => 2, 'subproject_id' => 1, 'stage' => 'Stage 2'),
      array('id' => 3, 'subproject_id' => 1, 'stage' => 'Stage 3'),
  	);

    DB::table('work_stage')->insert($record);
  }
}