<?php

use Illuminate\Database\Seeder;

class WorkActivityTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$record = array(
  		array('id' => 1, 'stage_id' => 1, 'description' => 'Fantasy activity', 'completed' => 1, 'completed_at' => '2015-10-30'),
      array('id' => 2, 'stage_id' => 1, 'description' => 'Great activity', 'completed' => 0, 'completed_at' => null),
      array('id' => 3, 'stage_id' => 1, 'description' => 'Best activity', 'completed' => 0, 'completed_at' => null),

      array('id' => 4, 'stage_id' => 2, 'description' => 'Incredible activity', 'completed' => 1, 'completed_at' => '2015-10-30'),
      array('id' => 5, 'stage_id' => 2, 'description' => 'Well activity', 'completed' => 0, 'completed_at' => null),
      array('id' => 6, 'stage_id' => 2, 'description' => 'Delicious activity', 'completed' => 0, 'completed_at' => null),
      array('id' => 7, 'stage_id' => 2, 'description' => 'Bad activity', 'completed' => 0, 'completed_at' => null),

      array('id' => 8, 'stage_id' => 3, 'description' => 'Hard activity', 'completed' => 1, 'completed_at' => '2015-10-30'),
      array('id' => 9, 'stage_id' => 3, 'description' => 'Serious activity', 'completed' => 1, 'completed_at' => date('Y-m-d')),
      array('id' => 10, 'stage_id' => 3, 'description' => 'Legendary activity', 'completed' => 0, 'completed_at' => null),
      array('id' => 11, 'stage_id' => 3, 'description' => 'Holy activity', 'completed' => 0, 'completed_at' => null),
  	);

    DB::table('work_activity')->insert($record);
  }
}