<?php

use Illuminate\Database\Seeder;

class ProgressReportTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$record = array(
      array(
    		'subproject_id' => 1,
    		'created_by' => 1,
    		'monitoring_date' => '2016-02-18',
    		'completed_by' => 'Tony Stark',
        'financial_kept' => 'I don\'t know',
        'is_contributions_updated' => 1,
        'procurement_change' => NULL,
        'safeguard' => NULL,
        'contribution_changing' => 'What do you think?',
        'comment' => 'Good!',
        'lat' => -6.972806,
        'lng' => 142.302772,
        'created_at' => '2016-02-01 13:34:50',
        'updated_at' => '2016-02-02 14:43:34',
      ),
      array(
        'subproject_id' => 1,
        'created_by' => 1,
        'monitoring_date' => '2016-01-18',
        'completed_by' => 'Pepper Pott',
        'financial_kept' => 'Magic',
        'is_contributions_updated' => 1,
        'procurement_change' => NULL,
        'safeguard' => NULL,
        'contribution_changing' => 'Boom',
        'comment' => 'Nice!',
        'lat' => -7.972806,
        'lng' => 143.302772,
        'created_at' => '2016-02-01 13:34:50',
        'updated_at' => '2016-02-03 14:43:34',
      ),
      array(
        'subproject_id' => 2,
        'created_by' => 1,
        'monitoring_date' => '2016-02-23',
        'completed_by' => 'Emma Watson',
        'financial_kept' => NULL,
        'is_contributions_updated' => 0,
        'procurement_change' => 'No reason',
        'safeguard' => 'High classified!',
        'contribution_changing' => NULL,
        'comment' => 'Got cha!',
        'lat' => -4.793251,
        'lng' => 143.6903903,
        'created_at' => '2016-02-01 13:34:50',
        'updated_at' => '2016-02-02 14:43:34',
      ),
      array(
        'subproject_id' => 2,
        'created_by' => 1,
        'monitoring_date' => '2016-02-12',
        'completed_by' => 'Taylor Swift',
        'financial_kept' => NULL,
        'is_contributions_updated' => 0,
        'procurement_change' => 'No reason',
        'safeguard' => 'High classified!',
        'contribution_changing' => NULL,
        'comment' => 'Got cha!',
        'lat' => -4.793251,
        'lng' => 143.6903903,
        'created_at' => '2016-02-01 13:34:50',
        'updated_at' => '2016-02-04 14:43:34',
      ),
      array(
        'subproject_id' => 3,
        'created_by' => 1,
        'monitoring_date' => '2016-02-21',
        'completed_by' => 'Ayumi Hamasaki',
        'financial_kept' => NULL,
        'is_contributions_updated' => 1,
        'procurement_change' => NULL,
        'safeguard' => 'That is an exception!',
        'contribution_changing' => 'What do you think?',
        'comment' => 'No reason',
        'lat' => -6.931056,
        'lng' => 145.439711,
        'created_at' => '2016-02-01 13:34:50',
        'updated_at' => '2016-02-02 14:43:34',
      ),
      array(
        'subproject_id' => 3,
        'created_by' => 1,
        'monitoring_date' => '2016-02-25',
        'completed_by' => 'Yokohama',
        'financial_kept' => NULL,
        'is_contributions_updated' => 1,
        'procurement_change' => NULL,
        'safeguard' => 'That is an accident!',
        'contribution_changing' => 'Unknow?',
        'comment' => 'Peace and joy',
        'lat' => -7.931056,
        'lng' => 143.439711,
        'created_at' => '2016-02-01 13:34:50',
        'updated_at' => '2016-02-07 14:43:34',
      ),
  	);

    DB::table('progress_report')->insert($record);
  }
}