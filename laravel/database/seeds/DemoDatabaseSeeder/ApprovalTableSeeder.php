<?php

use Illuminate\Database\Seeder;

class ApprovalTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array(
                'id' =>  1,
                'prioritisation_id' =>  1,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' =>  2,
                'prioritisation_id' =>  2,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' =>  3,
                'prioritisation_id' =>  3,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' =>  4,
                'prioritisation_id' =>  4,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' =>  5,
                'prioritisation_id' =>  5,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' =>  6,
                'prioritisation_id' =>  6,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' =>  7,
                'prioritisation_id' =>  7,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' =>  8,
                'prioritisation_id' =>  8,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' =>  9,
                'prioritisation_id' =>  9,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 10,
                'prioritisation_id' => 10,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 11,
                'prioritisation_id' => 11,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 12,
                'prioritisation_id' => 12,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 13,
                'prioritisation_id' => 13,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 14,
                'prioritisation_id' => 14,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 15,
                'prioritisation_id' => 15,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 16,
                'prioritisation_id' => 16,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 17,
                'prioritisation_id' => 17,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 18,
                'prioritisation_id' => 18,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 19,
                'prioritisation_id' => 19,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 20,
                'prioritisation_id' => 20,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 21,
                'prioritisation_id' => 21,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 22,
                'prioritisation_id' => 22,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 23,
                'prioritisation_id' => 23,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 24,
                'prioritisation_id' => 24,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 25,
                'prioritisation_id' => 25,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 26,
                'prioritisation_id' => 26,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 27,
                'prioritisation_id' => 27,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 28,
                'prioritisation_id' => 28,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 29,
                'prioritisation_id' => 29,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 30,
                'prioritisation_id' => 30,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 31,
                'prioritisation_id' => 31,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 32,
                'prioritisation_id' => 32,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 33,
                'prioritisation_id' => 33,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 34,
                'prioritisation_id' => 34,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 35,
                'prioritisation_id' => 35,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 36,
                'prioritisation_id' => 36,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 37,
                'prioritisation_id' => 37,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 38,
                'prioritisation_id' => 38,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 39,
                'prioritisation_id' => 39,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 40,
                'prioritisation_id' => 40,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 41,
                'prioritisation_id' => 41,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 42,
                'prioritisation_id' => 42,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 43,
                'prioritisation_id' => 43,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 44,
                'prioritisation_id' => 44,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 45,
                'prioritisation_id' => 45,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 46,
                'prioritisation_id' => 46,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 47,
                'prioritisation_id' => 47,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 48,
                'prioritisation_id' => 48,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 49,
                'prioritisation_id' => 49,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 50,
                'prioritisation_id' => 50,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 51,
                'prioritisation_id' => 51,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 52,
                'prioritisation_id' => 52,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 53,
                'prioritisation_id' => 53,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 54,
                'prioritisation_id' => 54,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 55,
                'prioritisation_id' => 55,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 56,
                'prioritisation_id' => 56,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 57,
                'prioritisation_id' => 57,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 58,
                'prioritisation_id' => 58,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 59,
                'prioritisation_id' => 59,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 60,
                'prioritisation_id' => 60,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 61,
                'prioritisation_id' => 61,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 62,
                'prioritisation_id' => 62,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 63,
                'prioritisation_id' => 63,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 64,
                'prioritisation_id' => 64,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 65,
                'prioritisation_id' => 65,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 66,
                'prioritisation_id' => 66,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 67,
                'prioritisation_id' => 67,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 68,
                'prioritisation_id' => 68,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 69,
                'prioritisation_id' => 69,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 70,
                'prioritisation_id' => 70,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 71,
                'prioritisation_id' => 71,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 72,
                'prioritisation_id' => 72,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 73,
                'prioritisation_id' => 73,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 74,
                'prioritisation_id' => 74,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 75,
                'prioritisation_id' => 75,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 76,
                'prioritisation_id' => 76,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 77,
                'prioritisation_id' => 77,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 78,
                'prioritisation_id' => 78,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 79,
                'prioritisation_id' => 79,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),

            array(
                'id' => 80,
                'prioritisation_id' => 80,
                'selection_method' => 'Need-based',
                'meeting_date' => '2016-04-01',
                'rsdlgp_funding_limit' => 0,
                'fund_estimate_origin' => 0,
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
        );

        DB::table('approval')->insert($record);
    }
}