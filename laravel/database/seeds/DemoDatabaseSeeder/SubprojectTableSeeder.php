<?php

use Illuminate\Database\Seeder;

class SubprojectTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array(
                'id' =>  1,
                'approval_id' =>  1,
                'village_id' =>  1,
                'name' => "Classroom Building",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' =>  2,
                'approval_id' =>  2,
                'village_id' =>  2,
                'name' => "Health Worker's House",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' =>  3,
                'approval_id' =>  3,
                'village_id' =>  3,
                'name' => "Solar Lighting",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' =>  4,
                'approval_id' =>  4,
                'village_id' =>  4,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' =>  5,
                'approval_id' =>  5,
                'village_id' =>  5,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' =>  6,
                'approval_id' =>  6,
                'village_id' =>  6,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' =>  7,
                'approval_id' =>  7,
                'village_id' =>  7,
                'name' => "Classroom Building",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' =>  8,
                'approval_id' =>  8,
                'village_id' =>  8,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' =>  9,
                'approval_id' =>  9,
                'village_id' =>  9,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 10,
                'approval_id' => 10,
                'village_id' => 10,
                'name' => "Community Hall",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 11,
                'approval_id' => 11,
                'village_id' => 11,
                'name' => "Village Couthouse",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 12,
                'approval_id' => 12,
                'village_id' => 12,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 13,
                'approval_id' => 13,
                'village_id' => 13,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 14,
                'approval_id' => 14,
                'village_id' => 14,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 15,
                'approval_id' => 15,
                'village_id' => 15,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 16,
                'approval_id' => 16,
                'village_id' => 16,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 17,
                'approval_id' => 17,
                'village_id' => 17,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 18,
                'approval_id' => 18,
                'village_id' => 18,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 19,
                'approval_id' => 19,
                'village_id' => 19,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 20,
                'approval_id' => 20,
                'village_id' => 20,
                'name' => "School Library",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 21,
                'approval_id' => 21,
                'village_id' => 21,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 22,
                'approval_id' => 22,
                'village_id' => 22,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 23,
                'approval_id' => 23,
                'village_id' => 23,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 24,
                'approval_id' => 24,
                'village_id' => 24,
                'name' => "Aid Post Upgrade",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 25,
                'approval_id' => 25,
                'village_id' => 25,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 26,
                'approval_id' => 26,
                'village_id' => 26,
                'name' => "Aid Post Upgrade",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 27,
                'approval_id' => 27,
                'village_id' => 27,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 28,
                'approval_id' => 28,
                'village_id' => 28,
                'name' => "Classroom Building",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 29,
                'approval_id' => 29,
                'village_id' => 29,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 30,
                'approval_id' => 30,
                'village_id' => 30,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 31,
                'approval_id' => 31,
                'village_id' => 31,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 32,
                'approval_id' => 32,
                'village_id' => 32,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 33,
                'approval_id' => 33,
                'village_id' => 33,
                'name' => "Solar Lighting",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 34,
                'approval_id' => 34,
                'village_id' => 34,
                'name' => "Solar Lighting",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 35,
                'approval_id' => 35,
                'village_id' => 35,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 36,
                'approval_id' => 36,
                'village_id' => 36,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 37,
                'approval_id' => 37,
                'village_id' => 37,
                'name' => "Gravity Fed/Bore Water Supply ",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 38,
                'approval_id' => 38,
                'village_id' => 38,
                'name' => "Solar Lighting",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 39,
                'approval_id' => 39,
                'village_id' => 39,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 40,
                'approval_id' => 40,
                'village_id' => 40,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 41,
                'approval_id' => 41,
                'village_id' => 41,
                'name' => "Classroom Building",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 42,
                'approval_id' => 42,
                'village_id' => 42,
                'name' => "Teacher's House",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 43,
                'approval_id' => 43,
                'village_id' => 43,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 44,
                'approval_id' => 44,
                'village_id' => 44,
                'name' => "Teacher's House",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 45,
                'approval_id' => 45,
                'village_id' => 45,
                'name' => "Single Classroom with Office & Water",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 46,
                'approval_id' => 46,
                'village_id' => 46,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 47,
                'approval_id' => 47,
                'village_id' => 47,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 48,
                'approval_id' => 48,
                'village_id' => 48,
                'name' => "Foot Bridge",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 49,
                'approval_id' => 49,
                'village_id' => 49,
                'name' => "Resource Centre",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 50,
                'approval_id' => 50,
                'village_id' => 50,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 51,
                'approval_id' => 51,
                'village_id' => 51,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 52,
                'approval_id' => 52,
                'village_id' => 52,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 53,
                'approval_id' => 53,
                'village_id' => 53,
                'name' => "Classroom Building",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 54,
                'approval_id' => 54,
                'village_id' => 54,
                'name' => "Community Health Worker's (CHWs) House",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 55,
                'approval_id' => 55,
                'village_id' => 55,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 56,
                'approval_id' => 56,
                'village_id' => 56,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 57,
                'approval_id' => 57,
                'village_id' => 57,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 58,
                'approval_id' => 58,
                'village_id' => 58,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 59,
                'approval_id' => 59,
                'village_id' => 59,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 60,
                'approval_id' => 60,
                'village_id' => 60,
                'name' => "Classroom Building",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 61,
                'approval_id' => 61,
                'village_id' => 61,
                'name' => "Classroom Building",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 62,
                'approval_id' => 62,
                'village_id' => 62,
                'name' => "Classroom Building",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 63,
                'approval_id' => 63,
                'village_id' => 63,
                'name' => "Commmunity Hall",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 64,
                'approval_id' => 64,
                'village_id' => 64,
                'name' => "Foot Bridge",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 65,
                'approval_id' => 65,
                'village_id' => 65,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 66,
                'approval_id' => 66,
                'village_id' => 66,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 67,
                'approval_id' => 67,
                'village_id' => 67,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 68,
                'approval_id' => 68,
                'village_id' => 68,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 69,
                'approval_id' => 69,
                'village_id' => 69,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 70,
                'approval_id' => 70,
                'village_id' => 70,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 71,
                'approval_id' => 71,
                'village_id' => 71,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 72,
                'approval_id' => 72,
                'village_id' => 72,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 73,
                'approval_id' => 73,
                'village_id' => 73,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 74,
                'approval_id' => 74,
                'village_id' => 74,
                'name' => "Solar Lighting",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 75,
                'approval_id' => 75,
                'village_id' => 75,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 76,
                'approval_id' => 76,
                'village_id' => 76,
                'name' => "Community Hall/Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 77,
                'approval_id' => 77,
                'village_id' => 77,
                'name' => "Solar Lighting",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 78,
                'approval_id' => 78,
                'village_id' => 78,
                'name' => "Aid Post",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 79,
                'approval_id' => 79,
                'village_id' => 79,
                'name' => "Water Catchment",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
            array(
                'id' => 80,
                'approval_id' => 80,
                'village_id' => 80,
                'name' => "Motor & Dinghy",
                'created_by' => 1,
                'created_at' => '2016-04-01 00:00:00',
                'updated_at' => '2016-04-01 00:00:00'
            ),
        );

        DB::table('subproject')->insert($record);
    }
}