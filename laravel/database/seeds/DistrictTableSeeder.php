<?php

use Illuminate\Database\Seeder;

class DistrictTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array('id' => 1, 'name' => "Abau",          'province_id' => 1),
            array('id' => 2, 'name' => "Goilala",       'province_id' => 1),
            array('id' => 3, 'name' => "Kairuku/Hiri",  'province_id' => 1),
            array('id' => 4, 'name' => "Rigo",          'province_id' => 1),
            array('id' => 5, 'name' => "Middle Fly",    'province_id' => 2),
            array('id' => 6, 'name' => "North Fly",     'province_id' => 2),
            array('id' => 7, 'name' => "South Fly",     'province_id' => 2),
        );

        DB::table('district')->insert($record);
    }
}
