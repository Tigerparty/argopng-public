<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $record = array(
      array('id' => 1, 'username' => 'admin', 'password' => Hash::make('happyEveryday'), 'identity' => 'superadmin'),
      array('id' => 2, 'username' => 'developer', 'password' => Hash::make('developer'), 'identity' => 'superadmin'),
      array('id' => 3, 'username' => 'regular', 'password' => Hash::make('goregulargo'), 'identity' => 'admin'),
    );

    DB::table('users')->insert($record);
  }
}
