<?php

use Illuminate\Database\Seeder;

class WardTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $record = array(
            array('id' => 1, 'name' => "Amau",                      'llg_id' => 1),
            array('id' => 2, 'name' => "Baramatta",                 'llg_id' => 1),
            array('id' => 3, 'name' => "Boru",                      'llg_id' => 1),
            array('id' => 4, 'name' => "Dom",                       'llg_id' => 1),
            array('id' => 5, 'name' => "Domara",                    'llg_id' => 1),
            array('id' => 6, 'name' => "Duramu",                    'llg_id' => 1),
            array('id' => 7, 'name' => "Ganai",                     'llg_id' => 1),
            array('id' => 8, 'name' => "Ianu",                      'llg_id' => 1),
            array('id' => 9, 'name' => "Manabo",                    'llg_id' => 1),
            array('id' => 10, 'name' => "Merani",                   'llg_id' => 1),
            array('id' => 11, 'name' => "Moreguina",                'llg_id' => 1),
            array('id' => 12, 'name' => "Robinson",                 'llg_id' => 1),
            array('id' => 13, 'name' => "Si'ini",                   'llg_id' => 1),
            array('id' => 14, 'name' => "Tutubu",                   'llg_id' => 1),
            array('id' => 15, 'name' => "Abiara Oreke",             'llg_id' => 3),
            array('id' => 16, 'name' => "Babiko",                   'llg_id' => 3),
            array('id' => 17, 'name' => "Bereina Urban",            'llg_id' => 3),
            array('id' => 18, 'name' => "Biotou",                   'llg_id' => 3),
            array('id' => 19, 'name' => "Chiria",                   'llg_id' => 3),
            array('id' => 20, 'name' => "Delena",                   'llg_id' => 3),
            array('id' => 21, 'name' => "Hisiu",                    'llg_id' => 3),
            array('id' => 22, 'name' => "Kivori",                   'llg_id' => 3),
            array('id' => 23, 'name' => "Malati",                   'llg_id' => 3),
            array('id' => 24, 'name' => "Mou",                      'llg_id' => 3),
            array('id' => 25, 'name' => "Nabua",                    'llg_id' => 3),
            array('id' => 26, 'name' => "Nara",                     'llg_id' => 3),
            array('id' => 27, 'name' => "Pinu",                     'llg_id' => 3),
            array('id' => 28, 'name' => "Rapa",                     'llg_id' => 3),
            array('id' => 29, 'name' => "Veimauri",                 'llg_id' => 3),
            array('id' => 30, 'name' => "Waima Abiara",             'llg_id' => 3),
            array('id' => 31, 'name' => "Waima Kore",               'llg_id' => 3),
            array('id' => 32, 'name' => "Diabi",                    'llg_id' => 6),
            array('id' => 33, 'name' => "Drimdamasuk",              'llg_id' => 6),
            array('id' => 34, 'name' => "Drimgas",                  'llg_id' => 6),
            array('id' => 35, 'name' => "Drimskai/Smipen",          'llg_id' => 6),
            array('id' => 36, 'name' => "Gasuke",                   'llg_id' => 6),
            array('id' => 37, 'name' => "Gusiore",                  'llg_id' => 6),
            array('id' => 38, 'name' => "Kmom",                     'llg_id' => 6),
            array('id' => 39, 'name' => "Temipen/Hupen",            'llg_id' => 6),
            array('id' => 40, 'name' => "Tmingodok/Giponai",        'llg_id' => 6),
            array('id' => 41, 'name' => "Tminsriap",                'llg_id' => 6),
            array('id' => 42, 'name' => "Buzi-Berr",                'llg_id' => 7),
            array('id' => 43, 'name' => "Mabudawan",                'llg_id' => 7),
            array('id' => 44, 'name' => "Old Mowata",               'llg_id' => 7),
            array('id' => 45, 'name' => "Sigabaduru",               'llg_id' => 7),
            array('id' => 46, 'name' => "Tureture",                 'llg_id' => 7),
            array('id' => 47, 'name' => "Boboa Station",            'llg_id' => 5),
            array('id' => 48, 'name' => "Buseki",                   'llg_id' => 5),
            array('id' => 49, 'name' => "Kapikam",                  'llg_id' => 5),
            array('id' => 50, 'name' => "Kusikina",                 'llg_id' => 5),
            array('id' => 51, 'name' => "Magipopo",                 'llg_id' => 5),
            array('id' => 52, 'name' => "Miwa",                     'llg_id' => 5),
            array('id' => 53, 'name' => "Pangoa",                   'llg_id' => 5),
            array('id' => 54, 'name' => "Upovia",                   'llg_id' => 5),
            array('id' => 55, 'name' => "Usukof",                   'llg_id' => 5),
            array('id' => 56, 'name' => "Bisuaka",                  'llg_id' => 8),
            array('id' => 57, 'name' => "Irupi, Drageli, Tati",     'llg_id' => 8),
            array('id' => 58, 'name' => "Kurunti",                  'llg_id' => 8),
            array('id' => 59, 'name' => "Kuruwonnie",               'llg_id' => 8),
            array('id' => 60, 'name' => "Podare",                   'llg_id' => 8),
            array('id' => 61, 'name' => "To be added",              'llg_id' => 8),
            array('id' => 62, 'name' => "Waidoro",                  'llg_id' => 8),
            array('id' => 63, 'name' => "Yamega",                   'llg_id' => 8),
            array('id' => 64, 'name' => "Central Boku/Dorumu",      'llg_id' => 4),
            array('id' => 65, 'name' => "Central Maria",            'llg_id' => 4),
            array('id' => 66, 'name' => "Central Mt.Brown",         'llg_id' => 4),
            array('id' => 67, 'name' => "Central Mt.Obree",         'llg_id' => 4),
            array('id' => 68, 'name' => "East Maria",               'llg_id' => 4),
            array('id' => 69, 'name' => "Lower Boku/Dorumu",        'llg_id' => 4),
            array('id' => 70, 'name' => "Lower Mt. Obree",          'llg_id' => 4),
            array('id' => 71, 'name' => "Lower Mt.Brown",           'llg_id' => 4),
            array('id' => 72, 'name' => "Ormand Central",           'llg_id' => 4),
            array('id' => 73, 'name' => "Ormand East",              'llg_id' => 4),
            array('id' => 74, 'name' => "Ormand West",              'llg_id' => 4),
            array('id' => 75, 'name' => "Upper Boku/Dorumu",        'llg_id' => 4),
            array('id' => 76, 'name' => "Upper Maria",              'llg_id' => 4),
            array('id' => 77, 'name' => "Upper Mt.Brown",           'llg_id' => 4),
            array('id' => 78, 'name' => "Upper Mt.Obree",           'llg_id' => 4),
            array('id' => 79, 'name' => "West Maria",               'llg_id' => 4),
            array('id' => 80, 'name' => "Aiwara",                   'llg_id' => 2),
            array('id' => 81, 'name' => "Central Ivane",            'llg_id' => 2),
            array('id' => 82, 'name' => "Ivane",                    'llg_id' => 2),
            array('id' => 83, 'name' => "Jowa",                     'llg_id' => 2),
            array('id' => 84, 'name' => "Kataipi",                  'llg_id' => 2),
            array('id' => 85, 'name' => "Loloipa",                  'llg_id' => 2),
            array('id' => 86, 'name' => "Pilitu",                   'llg_id' => 2),
            array('id' => 87, 'name' => "Pilitu Central",           'llg_id' => 2),
            array('id' => 88, 'name' => "Sopu",                     'llg_id' => 2),
            array('id' => 89, 'name' => "Tapini Urban",             'llg_id' => 2),
        );

        DB::table('ward')->insert($record);
    }
}
