<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Rural Service Delivery And Local Governance Project</title>
          
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- bunches of app icons -->
  <link rel="apple-touch-icon" sizes="57x57" href="{!! asset('images/favicon/apple-touch-icon-57x57.png') !!}">
  <link rel="apple-touch-icon" sizes="60x60" href="{!! asset('images/favicon/apple-touch-icon-60x60.png') !!}">
  <link rel="apple-touch-icon" sizes="72x72" href="{!! asset('images/favicon/apple-touch-icon-72x72.png') !!}">
  <link rel="apple-touch-icon" sizes="76x76" href="{!! asset('images/favicon/apple-touch-icon-76x76.png') !!}">
  <link rel="apple-touch-icon" sizes="114x114" href="{!! asset('images/favicon/apple-touch-icon-114x114.png') !!}">
  <link rel="apple-touch-icon" sizes="120x120" href="{!! asset('images/favicon/apple-touch-icon-120x120.png') !!}">
  <link rel="apple-touch-icon" sizes="144x144" href="{!! asset('images/favicon/apple-touch-icon-144x144.png') !!}">
  <link rel="apple-touch-icon" sizes="152x152" href="{!! asset('images/favicon/apple-touch-icon-152x152.png') !!}">
  <link rel="apple-touch-icon" sizes="180x180" href="{!! asset('images/favicon/apple-touch-icon-180x180.png') !!}">
  <link rel="icon" type="image/png" href="{!! asset('images/favicon/favicon-32x32.png') !!}" sizes="32x32">
  <link rel="icon" type="image/png" href="{!! asset('images/favicon/android-chrome-192x192.png') !!}" sizes="192x192">
  <link rel="icon" type="image/png" href="{!! asset('images/favicon/favicon-96x96.png') !!}" sizes="96x96">
  <link rel="icon" type="image/png" href="{!! asset('images/favicon/favicon-16x16.png') !!}" sizes="16x16">
  <link rel="manifest" href="{!! asset('images/favicon/manifest.json') !!}">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="{!! asset('images/favicon/mstile-144x144.png') !!}">
  <meta name="theme-color" content="#ffffff">

  <!-- css -->
  {!! Html::style('css/bootstrap.min.css') !!}
  {!! Html::style('css/refactoring-main.css') !!}
  {!! Html::style('css/home.css') !!}

  <!-- js -->
  {!! Html::script('js/jquery.min.js') !!}
  {!! Html::script('js/bootstrap.min.js') !!}

  <script>
    window.constants = {
      CSRF_TOKEN : '{!! csrf_token() !!}',
      ABS_URI: {!! json_encode(asset('/')) !!}
    }
  </script>

  @yield("css_block")
  @yield("js_block")
</head>

<body>
  <div id="overall-wrapper" class="containter">
    @include('master/header')
    <!-- body -->
    <div id="overall-body">
      @yield("content")
    </div>

    @include('master/footer')
  </div>
</body>
</html>
