<!-- Header -->
<div class="row" id="overall-header">
  <div class="col-md-12">
    <div id="overall-header-logo">
      <img src="{!! asset('/images/banner-logo.png') !!}" alt=""/>
    </div>

    <div id="overall-header-title">
      <h1>Rural Service Delivery and Local Governance Project</h1>
      <p>The Department of Provincial and Local Government Affairs</p>
    </div>
  </div>

  <div class="col-md-12 col-xs-12 tagline">
    <p>The objective of this programme is to improve access to basic services in rural areas</p>
  </div>
</div>

<nav class="navbar navbar-default hidden-print" id="overall-menu">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>
  <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav" id="navbar_container">
      <li id="home-tab"><a href="{!! asset('/') !!}">Home</a></li>
      <li id="about_rdp-tab"><a href="{!! asset('/about')!!}">About RSDLGP</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Forms <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li id="priority-tab"><a href="{!! asset('/prioritisation') !!}">Ward Prioritisation</a></li>
          <li id="ward-tab"><a href="{!! asset('/approval') !!}">LLG Approval</a></li>
          <li id="subproject-tab"><a href="{!! asset('/subproject') !!}">Subproject Implementation</a></li>
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="{!! asset('/apk/argo.apk') !!}">Download App</a></li>
      @if(Auth::check())
      <li>
        <a href="#" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
        Create Form<span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="{!! asset('/prioritisation/create') !!}">Ward Prioritisation</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="{!! asset('/approval/create') !!}">LLG Appraisal Committee Subproject Assessment and Approval</a></li>
        </ul>
      </li>
      <li><a href="{!! asset('/logout') !!}" id="overall-header-logout-link">Logout</a></li>
      @else
        <li><a id="overall-header-logout-link" href="{!! asset('/login') !!}">Login</a></li>
      @endif
    </ul>
  </div><!--/.nav-collapse -->
</nav>

<script type="text/javascript">
  $(document).ready(function()
  {
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    var page_name = $(location).attr("pathname");

    if(page_name.indexOf('about') > -1)
    {
        $("#navbar_container").children().removeClass('active');
        $("#about_rdp-tab").addClass("active");
    }
    else
    {
        $("#navbar_container").children().removeClass('active');
        $("#home-tab").addClass("active");
    }
  });
</script>
