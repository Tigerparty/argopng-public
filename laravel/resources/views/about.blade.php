<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Rural Service Delivery And Local Governance Project</title>
          
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- bunches of app icons -->
  <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="/images/favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/images/favicon/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="/images/favicon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="/images/favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/images/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="/images/favicon/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <!-- css -->
  {!! Html::style('css/bootstrap.min.css') !!}
  {!! Html::style('css/refactoring-main.css') !!}
  {!! Html::style('css/home.css') !!}

  <!-- js -->
  {!! Html::script('js/jquery.min.js') !!}
  {!! Html::script('js/bootstrap.min.js') !!}

  @yield("css_block")
  @yield("js_block")
</head>

<body>
  <div id="overall-wrapper" class="containter">
    @include('master/header')
    <!-- body -->
    <div id="overall-body">
      <!-- .about start -->
      <div id="about" class="about clearfix">

        <!-- <div class="section-title clearfix">
            <h2>About RDP</h2>
        </div> -->

        <div class="info-block">
          <h3>ABOUT RDP</h3>
          <p>The objective of RDP is to improve access to basic services in rural areas and the production and productivity of farmers. It does so through two main components:</p>
          <div class="component"><h5>Component 1</h5> Community Infrastructure and Services, aims to improve access to basic services in rural areas by providing Community Development Grants to each rural ward in the country. Using the Community-Driven-Development (CDD) approach, communities identify priority needs, design, construct and operate their own projects and services. </div><div class="component"><h5>Component 2</h5> Agriculture Partnerships and Support, assists farming households to engage in productive partnerships with commercial enterprises; builds the capacity of the Ministry of Agriculture and Livestock to deliver its core functions of regulation, research and sector coordination; and restores the productive assets of households critically affected by natural disaster. This website focuses mainly on the projects financed through Community Development Grants and implemented by the communities themselves.</div>
        </div>
      <!-- </div> -->

      <div class="row"><br></div>
        <div class="row text-center" id="overall-footer">
      Rural Service Delivery &amp; Local Governance Project<br>
      3rd Floor Kitpeng Building Dept of Provincial and Local Government Affairs P.O. Box 1287, BOROKO, National Capital District
        </div>
      </div>
      <!-- .about end -->
    </div>
  </div>
</body>
</html>
