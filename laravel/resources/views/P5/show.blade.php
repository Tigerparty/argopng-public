@extends('master.main')

@section('css_block')
  <style>
    .attachement-img-wrapper{
        height: 200px;
        text-align: center;
        background-color: #FFF;
        margin: 10px 0px;
        padding: 10px 40px;
        border-radius: 6px;
      }
      .attachement-img-inner-wrapper{
        height: 100%;
        background-color:ghostwhite;
      }
      .attachement-img{
        max-width: 100%;
        height: 100%;
      }
      .row{
        margin: 0px;
        padding: 0px 15px;
        overflow: hidden;
      }
      .report-preview-image-wrapper{
        text-align: center;
        width: 50px;
        height: 35px;
      }
      .report-preview-image{
        max-height: 100%;
        width: 100%;
      }
  </style>
@stop

@section('js_block')
  {!! Html::script('js/angular.min.js') !!}
  {!! Html::script('js/angular-route.min.js') !!}
  {!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
  {!! HTML::script('js/ng-file-upload.min.js') !!}
  {!! HTML::script('js/attach-uploader.js') !!}
  {!! Html::script('js/P5.js') !!}
  <script>
    window.init_data = {
      'report'  : {!! $report !!}
    };
  </script>
@stop

@section('content')
  <div ng-app="P5App" ng-controller="P5ShowCtrl">
    <h3 class="col-md-10 col-xs-9">
      <a href="{!! asset('/subproject/'.$report->subproject->id) !!}">{!! $report->subproject->name !!}</a> / Monitoring report / <% report.monitoring_date | date: 'dd/MM/yyyy' %>
    </h3>
    <h3 class="col-md-2 col-xs-3">
      <a class="btn pull-right btn-default" href="{!! asset('/subproject/'.$report->subproject->id.'/report/'.$report->id.'/edit') !!}">Edit</a>
      <a href="#" class="btn btn-default">Print</a>
    </h3>
    
    <strong class="col-md-5 col-sm-6 col-xs-7">
      Name of person completing the report:
    </strong>
    <p class="col-md-7 col-sm-6 col-xs-5">
      <% report.completed_by %>
    </p>
    
    <div class="row">
      <p class="col-md-12 bg-section">Work plan stages and activities:</p>
    </div>
    
    <div class="row" ng-repeat="(stg_idx, work_stage) in report.subproject.work_stages">
      <div class="col-md-12">
        <table class="table table-condensed table-hover">
          <thead>
            <tr><th class="col-md-10">Stage <% stg_idx +1 %></th><th class="col-md-2 text-center">Completed</th></tr>
          </thead>
          <tbody ng-repeat="(act_idx, work_activity) in work_stage.work_activities">
            <tr>
              <td><% work_activity.description %></td>
              <td ng-show="work_activity.completed == 1">yes</td>
              <td ng-show="work_activity.completed != 1">no</td>
            </tr>
          </tbody>
      </table>
      </div>
    </div>

    <div class="row">
      <p class="col-md-12 bg-section">Implementation Problem or Procurement issues</p>
    </div>
    
    <div class="row">
      <div class="col-md-12">
        <table class="table table-condensed table-hover">
          <thead>
            <tr>
              <th class="col-md-8">Issue</th>
              <th class="col-md-2">Date Created</th>
              <th class="col-md-2">Date Resolved</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="issue in report.subproject.issues">
              <td><% issue.desc %></td>
              <td><% issue.created_at.substring(0, 10) | date : 'dd/MM/yyyy' %></td>
              <td ng-show="issue.is_resolved != 1">N/A</td>
              <td ng-show="issue.is_resolved == 1"><% issue.resolved_at | date : 'dd/MM/yyyy' %></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    
    <div class="col-md-12">
      <hr>
    </div>
    
    <div class="row">
      <strong class="col-md-6">
        Are Financial records being kept up to date:
      </strong>
      <span class="col-md-6" ng-show="report.financial_kept == null">Yes</span>
      <span class="col-md-6" ng-show="report.financial_kept != null">No</span>
    </div>
    <div class="row" ng-if="report.financial_kept != NULL">
      <p class="col-md-12">
        <% report.financial_kept %>
      </p>
    </div>

    <div>
      <br>
    </div>

    <div class="row">
      <strong class="col-md-7">
        Have records of community contributions been updated:
      </strong>
      <span class="col-md-5" ng-show="report.is_contributions_updated == 1">Yes</span>
      <span class="col-md-5" ng-show="report.is_contributions_updated != 1">No</span>
    </div>

    <div>
      <br>
    </div>
    
    @can('view-procurement-safeguard')
      <div class="row">
        <strong class="col-md-8">
          Procurement: Are there any changes with the procurement plan:
        </strong>
        <span class="col-md-4" ng-show="report.procurement_change != null">Yes</span>
        <span class="col-md-4" ng-show="report.procurement_change == null">No</span>
      </div>
      <div class="row">
        <p class="col-md-12">
          <% report.procurement_change %>
        </p>
      </div>
      
      <div>
        <br>
      </div>
    @endcan

    @can('view-procurement-safeguard')
      <div class="row">
        <strong class="col-md-11">
          Safeguards: Are there any environmental or social issues emerging with this subproject:
        </strong>
        <span class="col-md-1" ng-show="report.safeguard != null">Yes</span>
        <span class="col-md-1" ng-show="report.safeguard == null">No</span>
      </div>
      <div class="row">
        <p class="col-md-12">
          <% report.safeguard %>
        </p>
      </div>
    
      <div>
        <br>
      </div>
    @endcan

    <div class="row">
      <strong class="col-md-6">
        Are there any changes to other contributions?
      </strong>
      <span class="col-md-6" ng-show="report.contribution_changing != null">Yes</span>
      <span class="col-md-6" ng-show="report.contribution_changing == null">No</span>
    </div>
      <div class="row">
        <p class="col-md-12">
          <% report.contribution_changing %>
        </p>
      </div>

    <div class="col-md-12">
      <hr>
    </div>
    
    <div class="row">
      <strong class="col-md-12">
        Additional Comments:
      </strong>
    </div>
    <div class="row">
      <p class="col-md-12">
        <% report.comment %>
      </p>
    </div>
      
    <div class="row"><textarea class="col-xs-12 form-control visible-print-block" rows="5" name="brief_description"></textarea></div>
      
    <div class="row">
      <p class="col-md-12 bg-section">Other Attachments</p>
    </div>

    @foreach($report->attachments as $attachment)
      <div class="row">
        <a class="col-md-12 col-xs-12" href="{!! asset('/file/'.$attachment->id.'/download') !!}">{!! $attachment->name !!}</a>
      </div>
    @endforeach

    <div class="col-md-12">
      <hr>
    </div>


    <div class="row pull-right hidden-print">
      <a class="btn btn-default" href="{!! asset('/subproject/'.$report->subproject->id.'/report/'.$report->id.'/edit') !!}">Edit</a>
    </div>

    <div class="row"><br></div>

    <div class="col-md-12 hidden-print">
      <hr>
    </div>
  </div>
@stop