@extends('master.main')

@section('css_block')
  <style>
    #wrong-image-label{
      display: none;
    }
    #file_load_gif{
      display: none;
    }
    #ui-datepicker-div
    {
      display: none;
    }
    ul.ui-autocomplete.ui-menu{
      width:50px;
    }
    .row{
        margin: 0px;
        padding: 0px 15px;
    }
    @media screen and (min-width: 992px) {
      .dollar-sign{
        position: relative;
        top: 7px;
        left: 10px;
      }
    }
  </style>
@stop

@section('js_block')
  {!! Html::script('js/angular.min.js') !!}
  {!! Html::script('js/angular-route.min.js') !!}
  {!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
  {!! HTML::script('js/ng-file-upload.min.js') !!}
  {!! HTML::script('js/attach-uploader.js') !!}
  {!! Html::script('js/P5.js') !!}
  <script>
    window.init_data = {
      'subproject'  : {!! old('subproject') ? json_encode(old('subproject')) : $subproject !!},
      'report'      : {!! old('report') ? json_encode(old('report')) : "''" !!},
      'attachments' : {!! old('attachments') ? json_encode(old('attachments')) : "[]" !!}
    };
  </script>
@stop

@section('content')
  <div ng-app="P5App" ng-controller="P5CreateCtrl">
    <div class="row">
      <h3 class="col-md-12"><a href="{!! asset("/subproject/$subproject_id") !!}">{!! $subproject->name !!}</a> / Progress Monitoring Report</h3>
    </div>

    @if($errors->has())
      <div class="row">
        <div class="alert alert-danger">
          @foreach ($errors->all() as $error)
            {{ $error }}<br>
          @endforeach
        </div>
      </div>
    @endif

    <div class="row">
      <p class="col-md-12 bg-section">General Info</p>
    </div>

    {!! Form::open(['url' => asset('/subproject/'.$subproject_id.'/report'), 'method' => 'POST']) !!}
      <div class="content">

        <div class="row">
          <label class="col-md-4 col-xs-12 control-label">Subproject:</label>
          <div class="col-md-4 col-xs-12">
            {!! $subproject_name !!}
          </div>
        </div>

        <div><br></div>

        <div class="row">
          <label class="col-md-4 col-xs-12 control-label">Date of monitoring visit :</label>
          <div class="col-md-4 col-xs-12">
            <div class="input-group" ng-click="openMonitoringDatePicker($event)">
              <input type="text"
                     class="form-control"
                     name="report[monitoring_date]"
                     id="monitoring_date"
                     ng-required="true"
                     ng-readonly="true"
                     ng-model="report.monitoring_date"
                     ng-init="monitoring_date = '{!! old('monitoring_date') ? old('monitoring_date') : '' !!}'"
                     datepicker-popup="dd/MM/yyyy"
                     is-open="MonitoringDatePickerOpened">
              <div class="input-group-addon"><img src="/images/calendar_icon.png" alt="calendar" ></div>
            </div>
          </div>
        </div>

        <div><br></div>

        <div class="row">
          <label class="col-md-4 col-xs-12">Name of person completing the report :</label>
          <div class="col-md-4 col-xs-12">
            <input class="form-control" type="text" name="report[completed_by]" ng-model="report.completed_by">
          </div>
        </div>

        <div class="row">
          <!-- word plan stage and activities are created in P3, here use can only answer if the activities are completed -->
          <p class="col-md-12 bg-section">Work plan Stages and Activities</p>
        </div>

        <div class="col-md-12" ng-repeat="(stg_idx, work_stage) in subproject.work_stages track by $index">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Stage <% stg_idx + 1 %></h3>

            </div>
            <div class="panel-body">
              <div ng-repeat="(act_idx, activity) in work_stage.work_activities track by $index">
                <div class="activity-row">
                  <span class="col-md-9 col-xs-12">
                    <% activity.description %>
                  </span>
                  <input type="hidden" name="subproject[work_stages][<% stg_idx %>][work_activities][<% act_idx %>][init_completed]" ng-model="activity.init_completed" ng-init="activity.init_completed = activity.completed" value="<% activity.init_completed %>">
                  <input type="hidden" name="subproject[work_stages][<% stg_idx %>][work_activities][<% act_idx %>][completed]" value="<% activity.completed %>">
                  <input type="hidden" name="subproject[work_stages][<% stg_idx %>][work_activities][<% act_idx %>][description]" value="<% activity.description %>">
                  <input type="hidden" name="subproject[work_stages][<% stg_idx %>][work_activities][<% act_idx %>][id]" value="<% activity.id %>">
                  <span class="col-md-3 col-xs-12" ng-show="activity.init_completed == 0">Completed
                    <label class="radio-inline">
                      <input type="radio" value="1" ng-model="activity.completed">Yes
                    </label>
                    <label class="radio-inline">
                      <input type="radio" value="0" ng-model="activity.completed">No
                    </label>
                  </span>
                  <span class="col-md-3 col-xs-12" ng-show="activity.init_completed == 1">Completed:yes
                  </span>
                </div>

                <div class="row" ng-hide="act_idx+1 == work_stage.work_activities.length"><br></div>
                <div ng-hide="act_idx+1 == work_stage.work_activities.length"><br class="hidden-md hidden-lg"></div>
              </div>
            </div><!-- .panel-body -->
          </div><!-- .panel -->
        </div>

        <div class="row">
          <p class="col-md-12 bg-section">Implementation Problem or Procurement issues:</p>
        </div>

        <div class="row hidden-xs hidden-sm">
          <label class="col-md-8">Issues</label>
          <label class="col-md-2">Date Created</label>
          <label class="col-md-2">Resolved?</label>
        </div>

        <span ng-repeat="(index, issue) in report.issues">
          <div ng-if="issue.id">
            <div class="form-group row">
              <input type="hidden" name="report[issues][<% index %>][id]" value="<% issue.id %>">
              <input type="hidden" name="report[issues][<% index %>][created_at]" value="<% issue.created_at %>">
              <div class="col-md-8 col-xs-12">
                <p><% issue.desc %></p>
              </div>
              <div class="col-md-2 col-xs-6">
                <p class="date-created"><% issue.created_at.substring(0, 10) | date : 'dd/MM/yyyy' %></p>
              </div>
              <input type="hidden" name="report[issues][<% index %>][init_is_resolved]" ng-model="issue.init_is_resolved" ng-init="issue.init_is_resolved = issue.is_resolved" value="<% issue.init_is_resolved %>">
              <div class="col-md-2 col-xs-6" ng-show="issue.init_is_resolved == 1">
                <p class="date-resolved"><% issue.resolved_at | date : 'dd/MM/yyyy' %></p>
              </div>
              <div class="col-md-2 col-xs-6 yetResolved" ng-show="issue.init_is_resolved != 1">
                <label class="radio-inline">
                  <input type="radio" name="report[issues][<% index %>][is_resolved]" ng-model="issue.is_resolved" value="1">Yes
                </label>
                <label class="radio-inline">
                  <input type="radio" name="report[issues][<% index %>][is_resolved]" ng-model="issue.is_resolved" value="0">No
                </label>
              </div>
            </div>

            <div>
              <br class="hidden-md hidden-lg">
            </div>
          </div>

          <div ng-show="!issue.id">
            <div action="" class="form-group row">
              <div class="col-md-10 col-xs-10">
                <input type="text" name="report[issues][<% index %>][desc]" ng-model="issue.desc" class="form-control" placeholder="New issue description">
              </div>
              <a href="" class="col-md-2 col-xs-2 text-center" ng-click="deleteIssue(index)">Delete</a>
            </div>

            <div><br></div>
          </div>
        </span>

        <div class="row">
          <div class="col-md-2">
            <input class="btn btn-default btn-xs" type="button" value="Add another issue" ng-click="addIssue()">
          </div>
        </div>

        <div class="row">
          <hr>
        </div>

        <div class="row">
          <label class="col-md-5 col-xs-9">Are financial records being kept up to date?</label>
          <div class="col-md-2 col-xs-3">
            <label class="radio-inline">
              <input type="radio" name="report[financial_kept_check]" ng-model="report.financial_kept_check" value="1">
              Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="report[financial_kept_check]" ng-model="report.financial_kept_check" value="0">
              No
            </label>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12" ng-show="report.financial_kept_check == 0">
            <label class="control-label">If no, please give details:</label>
            <textarea class="form-control" rows="5" name="report[financial_kept]" ng-model="report.financial_kept" placeholder="Description"></textarea>
          </div>
        </div>

        <div><br></div>

        <div class="row">
          <label class="col-md-6 col-xs-9">Have records of community contributions been updated?</label>
          <div class="col-md-2 col-xs-3">
            <label class="radio-inline">
              <input type="radio" name="report[is_contributions_updated]" ng-model="report.is_contributions_updated" value="1">
              Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="report[is_contributions_updated]" ng-model="report.is_contributions_updated" value="0">
              No
            </label>
          </div>
        </div>

        <div>
          <br>
        </div>

        @can('view-procurement-safeguard')
          <div class="row">
            <label class="col-md-6 col-xs-9">Procurement: Are there any changes with the procurement plan?</label>
            <div class="col-md-2 col-xs-3">
              <label class="radio-inline">
                <input type="radio" name="report[procurement_change_check]" ng-model="report.procurement_change_check" value="1">
                Yes
              </label>
              <label class="radio-inline">
                <input type="radio" name="report[procurement_change_check]" ng-model="report.procurement_change_check" value="0">
                No
              </label>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12" ng-show="report.procurement_change_check == 1">
              <label class="control-label">If yes, please give details:</label>
              <textarea class="form-control" rows="5" name="report[procurement_change]" ng-model="report.procurement_change" placeholder="Description"></textarea>
            </div>
          </div>

          <div>
            <br>
          </div>
        @endcan

        @can('view-procurement-safeguard')
          <div class="row">
            <label class="col-md-8 col-xs-9">Safeguards: Are there any environmental or social issues emerging with this subproject?</label>
            <div class="col-md-2 col-xs-3">
              <label class="radio-inline">
                <input type="radio" name="report[safeguard_check]" ng-model="report.safeguard_check" value="1">
                Yes
              </label>
              <label class="radio-inline">
                <input type="radio" name="report[safeguard_check]" ng-model="report.safeguard_check" value="0">
                No
              </label>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12" ng-show="report.safeguard_check == 1">
              <label class="control-label">If yes, please give details:</label>
              <textarea class="form-control" rows="5" name="report[safeguard]" ng-model="report.safeguard" placeholder="Description"></textarea>
            </div>
          </div>

          <div><br></div>
        @endcan

        <div class="row">
          <label class="col-md-5 col-xs-9">Are there any changes to other contributions?</label>
          <div class="col-md-2 col-xs-3">
            <label class="radio-inline">
              <input type="radio" name="report[contribution_changing_check]" ng-model="report.contribution_changing_check" value="1">
              Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="report[contribution_changing_check]" ng-model="report.contribution_changing_check" value="0">
              No
            </label>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12" ng-show="report.contribution_changing_check == 1">
            <label class="control-label">If yes, please give details:</label>
            <textarea class="form-control" rows="5" name="report[contribution_changing]" ng-model="report.contribution_changing" placeholder="Description"></textarea>
          </div>
        </div>

        <div><br></div>

        <div class="row">
          <div class="col-md-12">
            <label class="control-label">Add any other comments:</label>
            <textarea class="form-control" rows="5" name="report[comment]" ng-model="report.comment" placeholder="Additional Comments"></textarea>
          </div>
        </div>


        <div class="row">
          <p class="col-md-12 bg-section">Other Attachments</p>
        </div>

        <div class="row">
          <div ng-repeat="(index, attachment) in attachments">
            <input type="hidden" name="attachments[<% $index %>][id]" value="<% attachment.id %>">
            <input type="hidden" name="attachments[<% $index %>][name]" value="<% attachment.name %>">
            <strong class="col-md-12">
              <a href="{!! asset('/file') !!}/<% attachment.id %>/download"><% attachment.name %></a>&emsp;
              <a href="" ng-click="deleteAttachment(index)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></strong>
          </div>
        </div>

        <filegroup files=""></filegroup>
        <fileuploader></fileuploader>

        <div class="row">
          <hr>
        </div>

        <div class="row">
          <button type="button" class="btn btn-default col-md-2 col-xs-5" onclick="location.href=window.constants.ABS_URI+'/subproject/{!! $subproject_id !!}'">Cancel</button>
          <input type="submit" id="edit_submit" class="btn btn-default col-md-2 col-md-offset-1 hidden-sm hidden-xs" value="Submit" ng-disabled="attachment_uploader.is_uploading">
          <input type="submit" id="edit_submit" class="btn btn-default col-xs-5 col-xs-offset-2 visible-sm visible-xs" value="Save" ng-disabled="attachment_uploader.is_uploading">
        </div>
      </div><!-- .content -->
    {!! Form::close() !!}

    <div>
      <br>
    </div>

    <div class="row">
      <hr>
    </div>
  </div>
@stop