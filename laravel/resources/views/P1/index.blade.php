@extends('master.main')

@section('css_block')
<style>
  .row{
    margin: 0px;
    padding: 0px 15px;
    overflow: hidden;
  }
</style>
@stop

@section('js_block')
{!! Html::script('js/angular.min.js') !!}
{!! Html::script('js/angular-route.min.js') !!}
{!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
{!! Html::script('js/index.js') !!}
<script>
  window.init_data = {
    'provinces': {!! $provinces !!},
  };
</script>
@stop

@section('content')
  <div ng-app="IndexApp" ng-controller="P1IndexCtrl" style="min-height: 400px">
    <div class="col-xs-12 col-md-3 search-parameters">
      <p>Search the RSDLGP database by ward.</p>
      <form>

        <div class="input-group">
          <input type="text" ng-model="keyword" class="form-control" id="search-sp" placeholder="Search by keyword">
          <a href="" ng-click="searchPrioritisationByFilter()" class="input-group-addon"><img src="images/icon-search.png" alt="search"></a>
        </div>

        <br>  

        <div>
          <select name="province" ng-model="province" id="select-province" class="form-control" ng-change="updateDistricts()" ng-options="province.name for province in provinces">
            <option value="">-- Select Province --</option>
          </select>
        </div>

        <br>

        <div>
          <select name="district" ng-model="district" id="select-district" class="form-control" ng-change="updateLlgs()" ng-options="district.name for district in districts" ng-disabled="!province">
            <option value="">-- Select District --</option>
          </select>
        </div>

        <br>

        <div>
            <select name="llg" ng-model="llg" id="select-llg" class="form-control" ng-change="updateWards()" ng-options="llg.name for llg in llgs" ng-disabled="!district">
              <option value="">-- Select LLG --</option>
            </select>
        </div>

        <br>

        <div>
            <select name="ward" ng-model="ward" id="select-ward" class="form-control" ng-options="ward.name for ward in wards" ng-disabled="!llg">
              <option value="" selected>-- Select Ward --</option>
            </select>
        </div>

        <br>

        <a href="#" class="btn btn-primary col-md-12" ng-click="searchPrioritisationByFilter()">SEARCH</a>

        <br>
          
      </form>
    </div>

    <div class="projects-list col-xs-12 col-md-9">
      <p class="col-md-12 bg-section">Ward Priorisation Forms</p>
      <table class="table">
        <thead>
          <tr>
            <th>Province</th>
            <th>District</th>
            <th>LLG</th>
            <th>Ward</th>
            <th>Meeting Date</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr class="list-item" ng-repeat="prioritisation in pagePrioritisations">
            <td><% prioritisation.p_name %></td>
            <td><% prioritisation.d_name %></td>
            <td><% prioritisation.l_name %></td>
            <td><% prioritisation.w_name %></td>
            <td><% prioritisation.meeting_date | date : 'dd/MM/yyyy' %></td>
            <td><a href="{!! asset('/prioritisation') !!}/<% prioritisation.id %>">View</a></td>
          </tr>
        </tbody>
      </table>

    </div><!-- .project-list -->
    
    <div class="row">
      <div class="col-md-3 col-md-offset-3" style="margin-top: 8px;">
        <span ng-bind="startItemIndex + 1"></span>
        <span > - </span>
        <span ng-bind="endItemIndex"></span>
        <span> of </span>
        <span ng-bind="totalItems"></span>
        <span> Prioritisation forms </span>
      </div>

      <pagination class="pull-right" boundary-links="true" ng-change="pageChanged()" total-items="totalItems" items-per-page="itemsPerPage" max-size="maxSize" ng-model="currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination> 
    </div>
    
  </div>

  <div>
    <br>
  </div>

  <div class="row">
    <hr>
  </div>
@stop