@extends('master.main')

@section('css_block')
<style>
  #wrong-image-label{
    display: none;
  }
  #file_load_gif{
    display: none;
  }
  #ui-datepicker-div
  {
    display: none;
  }
  ul.ui-autocomplete.ui-menu{
    width:50px;
  }
  .row{
    margin: 0px;
    padding: 0px 15px;
  }
  @media screen and (min-width: 992px) {
    .dollar-sign{
      position: relative;
      top: 7px;
      left: 10px;
    }
  }
</style>
@stop

@section('js_block')
{!! Html::script('js/angular.min.js') !!}
{!! Html::script('js/angular-route.min.js') !!}
{!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
{!! HTML::script('js/ng-file-upload.min.js') !!}
{!! HTML::script('js/attach-uploader.js') !!}
{!! Html::script('js/P1.js') !!}
<script>
  window.init_data = {
    'provinces': {!! $provinces !!},
    'places': {!! old() ? json_encode(old('prioritisation_places')) : '[]' !!},
    'attachments' : {!! old('attachments') ? json_encode(old('attachments')) : '[]' !!},
    'locations': {!! old('ward_id') ? json_encode([old('province_id'), old('district_id'), old('llg_id'), old('ward_id')]) : '[]' !!}
  };
</script>
@stop

@section('content')
<div ng-app="P1App" ng-controller="P1CreateCtrl">
	<div class="row">
    <h3 class="col-md-12">
      Ward Prioritisation
    </h3>
  </div>

  @if($errors->has())
    <div class="row">
      <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
          {{ $error }}<br>
        @endforeach
      </div>
    </div>
  @endif

  <div class="row">
    <p class="col-md-12 bg-section">General Information</p>
  </div>

  {!! Form::open(['url' => 'prioritisation', 'method' => 'POST']) !!}
  	<div class="content">

  		<div class="row">
        <label class="col-md-2 control-label col-xs-3">Province:
        </label>
        <div class="col-md-4 col-xs-9">
          <select id="province" class="form-control" name="province_id"
                  ng-model="province_id" ng-change="updateDistricts()">
            <option value="">-- Select Province --</option>
              @foreach($provinces as $province)
                <option value="{!! $province->id !!}" ng-selected="province_id == {!! $province->id !!}">{!! $province->name !!}</option>
              @endforeach
          </select>
        </div>
        <div class="row visible-xs"><br></div>
        <label class="col-md-1 col-md-offset-1 control-label col-xs-3">District:
        </label>
        <div class="col-md-4 col-xs-9">
          <select id="district" class="form-control" name="district_id"
                  ng-model="district_id" ng-disabled="!province_id" ng-change="updateLlgs()">
            <option value="">-- Select District --</option>
            <option ng-repeat="district in districts" value="<% district.id %>" ng-selected="district_id == district.id"><% district.name %></option>
          </select>
        </div>
      </div>
      <div><br></div>

      <div class="row">
        <label class="col-md-2 control-label col-xs-3">LLG:
        </label>
        <div class="col-md-4 col-xs-9">
          <select id="llg" class="form-control" name="llg_id"
                  ng-model="llg_id" ng-disabled="!district_id" ng-change="updateWards()">
            <option value="">-- Select LLG --</option>
            <option ng-repeat="llg in llgs" value="<% llg.id %>" ng-selected="llg_id == llg.id"><% llg.name %></option>
          </select>
        </div>
        <div class="row visible-xs"><br></div>
        <label class="col-md-1 col-md-offset-1 control-label col-xs-3">Ward:</label>
        <div class="col-md-4 col-xs-9">
          <select name="ward_id" id="ward" class="form-control"
                  ng-model="ward_id" ng-disabled="!llg_id">
            <option selected="selected" value="">-- Select Ward --</option>
            <option ng-repeat="ward in wards" value="<% ward.id %>" ng-selected="ward_id == ward.id"><% ward.name %></option>
          </select>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <p class="col-md-12 bg-section">Meeting, date, time, place and participants</p>
      </div>

      <div class="row">
        <label class="col-md-2 col-xs-3">Date of meeting:</label>
        <div class="col-md-4 col-xs-9">
          <div class="input-group" ng-click="openMeetingDatePicker($event)">
            <input type="text"
                   class="form-control"
                   name="meeting_date"
                   id="meeting_date"
                   ng-required="true"
                   ng-readonly="true"
                   ng-model="meeting_date"
                   ng-init="meeting_date = '{!! old('meeting_date') ? old('meeting_date') : '' !!}'"
                   datepicker-popup="dd/MM/yyyy"
                   is-open="MeetingDatePickerOpened">
            <div class="input-group-addon"><img src="/images/calendar_icon.png" alt="calendar" ></div>
          </div>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <strong class="col-md-12">Place of the meeting and the number (#) of participating villages</strong>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-4 col-xs-3">Place of meeting</label>
        <label class="col-md-4 col-xs-3 text-center"># of villages represented in the meeting</label>
        <label class="col-md-4 col-xs-3 text-center">Total # of villages in the ward</label>
      </div>

      <div><br></div>

      <div ng-repeat="place in places track by $index">
        <div class="row">
          <div class="col-md-4 col-xs-3">
            <input type="text" name="prioritisation_places[<% $index %>][place]" class="form-control" id="meetingPlace1" value="<% place.place %>">
          </div>
          <div class="col-md-4 col-xs-3">
            <input type="number" min=0 step="1" name="prioritisation_places[<% $index %>][count_village_representative]" class="form-control" id="villageNumber1" value="<% place.count_village_representative %>">
          </div>
          <div class="col-md-4 col-xs-3">
            <input type="number" min=0 step="1" name="prioritisation_places[<% $index %>][count_total_village]" class="form-control" id="totalVillageInWard1" value="<% place.count_total_village %>">
          </div>
        </div>

        <div><br></div>
      </div>

      <div><br></div>

    	<div class="row">
        <div class="col-md-12">
          <label class="control-label">Summary of meeting participants:</label>
        </div>
      </div>

      <div class="row">
        <label class="col-md-2 col-xs-2">Age Group</label>
        <label class="col-md-2 col-xs-2 text-center">Male</label>
        <label class="col-md-2 col-xs-2 text-center">Female</label>
        <label class="col-md-2 col-xs-2 text-center">Total</label>
        <label class="col-md-4 col-xs-4 text-center"># of participants with disabilities</label>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">0 - 14 yrs</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="participants_male_0_14" class="form-control" id="" ng-init="participants_male_0_14 = {!! old('participants_male_0_14') ? old('participants_male_0_14') : '0' !!}" value="<% participants_male_0_14 %>" ng-model="participants_male_0_14">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="participants_female_0_14" class="form-control" id="" ng-init="participants_female_0_14 = {!! old('participants_female_0_14') ? old('participants_female_0_14') : 0 !!}" value="<% participants_female_0_14 %>" ng-model="participants_female_0_14">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (participants_male_0_14*1)+(participants_female_0_14*1) %>" disabled>
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" string-to-number name="participants_disable_0_14" class="form-control" id="" ng-init="participants_disable_0_14 = {!! old('participants_disable_0_14') ? old('participants_disable_0_14') : 0 !!}" value="<% participants_disable_0_14 %>" ng-model="participants_disable_0_14">
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">15 - 24 yrs</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="participants_male_15_24" class="form-control" id="" ng-init="participants_male_15_24 = {!! old('participants_male_15_24') ? old('participants_male_15_24') : 0 !!}" value="<% participants_male_15_24 %>" ng-model="participants_male_15_24">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="participants_female_15_24" class="form-control" id="" ng-init="participants_female_15_24 = {!! old('participants_female_15_24') ? old('participants_female_15_24') : 0 !!}" value="<% participants_female_15_24 %>" ng-model="participants_female_15_24">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (participants_male_15_24*1)+(participants_female_15_24*1) %>" disabled>
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" string-to-number name="participants_disable_15_24" class="form-control" id="" ng-init="participants_disable_15_24 = {!! old('participants_disable_15_24') ? old('participants_disable_15_24') : 0 !!}" value="<% participants_disable_15_24 %>" ng-model="participants_disable_15_24">
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">25 - 59 yrs</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="participants_male_25_59" class="form-control" id="" ng-init="participants_male_25_59 = {!! old('participants_male_25_59') ? old('participants_male_25_59') : 0 !!}" value="<% participants_male_25_59 %>" ng-model="participants_male_25_59">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="participants_female_25_59" class="form-control" id="" ng-init="participants_female_25_59 = {!! old('participants_female_25_59') ? old('participants_female_25_59') : 0 !!}" value="<% participants_female_25_59 %>" ng-model="participants_female_25_59">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (participants_male_25_59*1)+(participants_female_25_59*1) %>" disabled>
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" string-to-number name="participants_disable_25_59" class="form-control" id="" ng-init="participants_disable_25_59 = {!! old('participants_disable_25_59') ? old('participants_disable_25_59') : 0 !!}" value="<% participants_disable_25_59 %>" ng-model="participants_disable_25_59">
        </div>
      </div>


      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">60 yrs +</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="participants_male_60_plus" class="form-control" id="" ng-init="participants_male_60_plus = {!! old('participants_male_60_plus') ? old('participants_male_60_plus') : 0 !!}" value="<% participants_male_60_plus %>" ng-model="participants_male_60_plus">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="participants_female_60_plus" class="form-control" id="" ng-init="participants_female_60_plus = {!! old('participants_female_60_plus') ? old('participants_female_60_plus') : 0 !!}" value="<% participants_female_60_plus %>" ng-model="participants_female_60_plus">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (participants_male_60_plus*1)+(participants_female_60_plus*1) %>" disabled>
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" string-to-number name="participants_disable_60_plus" class="form-control" id="" ng-init="participants_disable_60_plus = {!! old('participants_disable_60_plus') ? old('participants_disable_60_plus') : 0 !!}" value="<% participants_disable_60_plus %>" ng-model="participants_disable_60_plus">
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">Total</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (participants_male_0_14*1)+(participants_male_15_24*1)+(participants_male_25_59*1)+(participants_male_60_plus*1) %>" ng-model="participants_male_total" disabled>
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (participants_female_0_14*1)+(participants_female_15_24*1)+(participants_female_25_59*1)+(participants_female_60_plus*1) %>" ng-model="participants_female_total" disabled>
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (participants_female_0_14*1)+(participants_female_15_24*1)+(participants_female_25_59*1)+(participants_female_60_plus*1)+(participants_male_0_14*1)+(participants_male_15_24*1)+(participants_male_25_59*1)+(participants_male_60_plus*1) %>" disabled>
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" class="form-control" value="<% (participants_disable_0_14*1)+(participants_disable_15_24*1)+(participants_disable_25_59*1)+(participants_disable_60_plus*1) %>" disabled>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <p class="col-md-12 bg-section">Priority needs identified during the community participatory meeting process</p>
      </div>

      <div class="row">
        <label class="col-md-4 control-label col-xs-6">Priority Needs Identified during:
        </label>
        <div class="col-md-4 col-xs-6">
          <select name="need_identify_id" class="form-control">
            <option value="">- Select -</option>
              @foreach($need_identifies as $need_identify)
                <option value="{!! $need_identify->id !!}" ng-selected="{!! old('need_identify_id') ? old('need_identify_id') : '0' !!} == {!! $need_identify->id !!}">{!! $need_identify->name !!}</option>
              @endforeach
          </select>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <strong class="col-md-12">Rank top 5 community priority needs:</strong>
      </div>

      <div><br></div>

      <div id="priorities">
        @for ($i = 0; $i < 5; $i++)
          <div class="form-group" id="priority_{!! $i !!}" data-id="0">
            <div class="row">
              <label class="col-md-1 col-xs-1">{!! $i+1 !!}</label>
              <div class="col-md-6 col-xs-6">
                <input type="text" class="form-control" name="prioritisation_needs[{!! $i !!}][description]" value="{!! old('prioritisation_needs.'.$i.'.description') ? old('prioritisation_needs.'.$i.'.description') : '' !!}" placeholder="Community need description">
              </div>
              <div class="col-md-5 col-xs-5">
                <select name="prioritisation_needs[{!! $i !!}][sector_id]" class="form-control">
                  <option value="">- Select sector-</option>
                  @foreach($sectors as $sector)
                    <option value="{!! $sector->id !!}" ng-selected="{!! old('prioritisation_needs.'.$i.'.sector_id') ? old('prioritisation_needs.'.$i.'.sector_id') : '0' !!} == {!! $sector->id !!}">{!! $sector->name !!}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        @endfor

      <div class="row">
        <p class="col-md-12 bg-section">Subproject beneficiaries</p>
      </div>

      <div class="row">
        <strong class="col-md-12">Estimated number of beneficiaries:</strong>
      </div>

      <div class="row">
        <label class="col-md-2 col-xs-2">Age Group</label>
        <label class="col-md-2 col-xs-2 text-center">Male</label>
        <label class="col-md-2 col-xs-2 text-center">Female</label>
        <label class="col-md-2 col-xs-2 text-center">PLWDs</label>
        <label class="col-md-4 col-xs-4 text-center">Total</label>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">0 - 14 yrs</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_male_0_14" class="form-control" id="" ng-init="beneficiaries_male_0_14 = {!! old('beneficiaries_male_0_14') ? old('beneficiaries_male_0_14') : 0 !!}" value="<% beneficiaries_male_0_14 %>" ng-model="beneficiaries_male_0_14">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_female_0_14" class="form-control" id="" ng-init="beneficiaries_female_0_14 = {!! old('beneficiaries_female_0_14') ? old('beneficiaries_female_0_14') : 0 !!}" value="<% beneficiaries_female_0_14 %>" ng-model="beneficiaries_female_0_14">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_plwds_0_14" class="form-control" id="" ng-init="beneficiaries_plwds_0_14 = {!! old('beneficiaries_plwds_0_14') ? old('beneficiaries_plwds_0_14') : 0 !!}" value="<% beneficiaries_plwds_0_14 %>" ng-model="beneficiaries_plwds_0_14">
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" class="form-control" value="<% (beneficiaries_male_0_14*1)+(beneficiaries_female_0_14*1)+(beneficiaries_plwds_0_14*1) %>" id="" disabled="" ng-model="beneficiaries_total_0_14">
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">15 - 24 yrs</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_male_15_24" class="form-control" id="" ng-init="beneficiaries_male_15_24 = {!! old('beneficiaries_male_15_24') ? old('beneficiaries_male_15_24') : 0 !!}" value="<% beneficiaries_male_15_24 %>" ng-model="beneficiaries_male_15_24">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_female_15_24" class="form-control" id="" ng-init="beneficiaries_female_15_24 = {!! old('beneficiaries_female_15_24') ? old('beneficiaries_female_15_24') : 0 !!}" value="<% beneficiaries_female_15_24 %>" ng-model="beneficiaries_female_15_24">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_plwds_15_24" class="form-control" id="" ng-init="beneficiaries_plwds_15_24 = {!! old('beneficiaries_plwds_15_24') ? old('beneficiaries_plwds_15_24') : 0 !!}" value="<% beneficiaries_plwds_15_24 %>" ng-model="beneficiaries_plwds_15_24">
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" class="form-control" value="<% (beneficiaries_male_15_24*1)+(beneficiaries_female_15_24*1)+(beneficiaries_plwds_15_24*1) %>" id="" disabled="" ng-model="beneficiaries_total_15_24">
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">25 - 59 yrs</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_male_25_59" class="form-control" id="" ng-init="beneficiaries_male_25_59 = {!! old('beneficiaries_male_25_59') ? old('beneficiaries_male_25_59') : 0 !!}" value="<% beneficiaries_male_25_59 %>" ng-model="beneficiaries_male_25_59">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_female_25_59" class="form-control" id="" ng-init="beneficiaries_female_25_59 = {!! old('beneficiaries_female_25_59') ? old('beneficiaries_female_25_59') : 0 !!}" value="<% beneficiaries_female_25_59 %>" ng-model="beneficiaries_female_25_59">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_plwds_25_59" class="form-control" id="" ng-init="beneficiaries_plwds_25_59 = {!! old('beneficiaries_plwds_25_59') ? old('beneficiaries_plwds_25_59') : 0 !!}" value="<% beneficiaries_plwds_25_59 %>" ng-model="beneficiaries_plwds_25_59">
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" class="form-control" value="<% (beneficiaries_male_25_59*1)+(beneficiaries_female_25_59*1)+(beneficiaries_plwds_25_59*1) %>" id="" disabled="" ng-model="beneficiaries_total_25_29">
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 col-xs-2">60 yrs +</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_male_60_plus" class="form-control" id="" ng-init="beneficiaries_male_60_plus = {!! old('beneficiaries_male_60_plus') ? old('beneficiaries_male_60_plus') : 0 !!}" value="<% beneficiaries_male_60_plus %>" ng-model="beneficiaries_male_60_plus">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_female_60_plus" class="form-control" id="" ng-init="beneficiaries_female_60_plus = {!! old('beneficiaries_female_60_plus') ? old('beneficiaries_female_60_plus') : 0 !!}" value="<% beneficiaries_female_60_plus %>" ng-model="beneficiaries_female_60_plus">
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" string-to-number name="beneficiaries_plwds_60_plus" class="form-control" id="" ng-init="beneficiaries_plwds_60_plus = {!! old('beneficiaries_plwds_60_plus') ? old('beneficiaries_plwds_60_plus') : 0 !!}" value="<% beneficiaries_plwds_60_plus %>" ng-model="beneficiaries_plwds_60_plus">
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" class="form-control" value="<% (beneficiaries_male_60_plus*1)+(beneficiaries_female_60_plus*1)+(beneficiaries_plwds_60_plus*1) %>" id="" disabled="" ng-model="beneficiaries_total_60_plus">
        </div>
      </div>

      <div><br></div>

       <div class="row">
        <label class="col-md-2 col-xs-2">Total</label>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (beneficiaries_male_0_14*1)+(beneficiaries_male_15_24*1)+(beneficiaries_male_25_59*1)+(beneficiaries_male_60_plus*1) %>" disabled>
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (beneficiaries_female_0_14*1)+(beneficiaries_female_15_24*1)+(beneficiaries_female_25_59*1)+(beneficiaries_female_60_plus*1) %>" disabled>
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="number" min=0 step="1" class="form-control" value="<% (beneficiaries_plwds_0_14*1)+(beneficiaries_plwds_15_24*1)+(beneficiaries_plwds_25_59*1)+(beneficiaries_plwds_60_plus*1) %>" disabled>
        </div>
        <div class="col-md-4 col-xs-4">
          <input type="number" min=0 step="1" class="form-control" value="<% (beneficiaries_male_0_14*1)+(beneficiaries_male_15_24*1)+(beneficiaries_male_25_59*1)+(beneficiaries_male_60_plus*1)+(beneficiaries_female_0_14*1)+(beneficiaries_female_15_24*1)+(beneficiaries_female_25_59*1)+(beneficiaries_female_60_plus*1)+(beneficiaries_plwds_0_14*1)+(beneficiaries_plwds_15_24*1)+(beneficiaries_plwds_25_59*1)+(beneficiaries_plwds_60_plus*1) %>" disabled>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <p class="col-md-12 bg-section">Attachments:</p>
      </div>

      <div class="row">
        <div ng-repeat="(index, attachment) in attachments">
          <input type="hidden" name="attachments[<% $index %>][id]" value="<% attachment.id %>">
          <input type="hidden" name="attachments[<% $index %>][name]" value="<% attachment.name %>">
          <strong class="col-md-12">
            <a href="{!! asset('/file') !!}/<% attachment.id %>/download"><% attachment.name %></a>&emsp;
            <a href="" ng-click="deleteAttachment(index)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></strong>
        </div>
      </div>

      <filegroup files=""></filegroup>
      <fileuploader></fileuploader>

      <div class="row">
        <hr>
      </div>

      <div class="row">
        <a class="btn btn-default" href="{!! asset('/prioritisation') !!}">Cancel</a>
        <input type="submit" id="create_submit" class="btn btn-default" value="Submit" ng-disabled="attachment_uploader.is_uploading">
      </div>

  	</div>
  {!! Form::close() !!}
</div>
@stop