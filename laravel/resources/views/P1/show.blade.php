@extends('master.main')

@section('css_block')
<style>
  .attachement-img-wrapper{
    height: 200px;
    text-align: center;
    background-color: #FFF;
    margin: 10px 0px;
    padding: 10px 40px;
    border-radius: 6px;
  }
  .attachement-img-inner-wrapper{
    height: 100%;
    background-color:ghostwhite;
  }
  .attachement-img{
    max-width: 100%;
    height: 100%;
  }
  .row{
    margin: 0px;
    padding: 0px 15px;
    overflow: hidden;
  }
  .report-preview-image-wrapper{
    text-align: center;
    width: 50px;
    height: 35px;
  }
  .report-preview-image{
    max-height: 100%;
    width: 100%;
  }
</style>
@stop

@section('js_block')
{!! Html::script('js/angular.min.js') !!}
{!! Html::script('js/angular-route.min.js') !!}
{!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
{!! Html::script('js/P1.js') !!}
@stop

@section('content')
<div ng-app="P1App">
	<div class="row">
    <h3 class="col-md-10 col-xs-10">
      Ward Prioritisation Form
    </h3>
    <h3 class="col-md-2 col-xs-2">
      <a class="pull-right btn btn-default" href="{!! asset('prioritisation/'.$prioritisation->id.'/edit') !!}">Edit</a>
    </h3>
  </div>

  <div class="row">
    <p class="col-md-12 col-xs-12 bg-section">General Information</p>
  </div>

  <div class="row">
    <strong class="col-md-2 col-xs-4">
      Province:
    </strong>
    <span class="col-md-6 col-xs-8">
      {!! $location->llg->district->province->name !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-2 col-xs-4">
      District:
    </strong>
    <span class="col-md-10 col-xs-8">
      {!! $location->llg->district->name !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-2 col-xs-4">
      LLG:
    </strong>
    <span class="col-md-10 col-xs-8">
      {!! $location->llg->name !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-2 col-xs-4">
      Ward:
    </strong>
    <span class="col-md-10 col-xs-8">
      {!! $location->name !!}
    </span>
  </div>

  <div class="row">
    <p class="col-md-12 col-xs-12 bg-section">Meeting, date, time, place and participants</p>
  </div>

  <div class="row">
    <strong class="col-md-2 col-xs-4">
      Date of meeting:
    </strong>
    <span class="col-md-10 col-xs-8">
      {!! date_format(date_create($prioritisation->meeting_date), 'l d/m/Y') !!}
    </span>
  </div>
  <div><br></div>
  <div class="row">
    <strong class="col-md-12">Place of the meeting and the number (#) of participating villages:</strong>
  </div>

  <div class="row">
    <div class="col-md-12 table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Place of meeting</th>
            <th>Number of villages represented in the meeting</th>
            <th>Total number of villages in the Ward</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>{!! count($prioritisation->prioritisation_places)>0 ? $prioritisation->prioritisation_places[0]->place : '--' !!}</td>
            <td>{!! count($prioritisation->prioritisation_places)>0 ? $prioritisation->prioritisation_places[0]->count_village_representative : '--' !!}</td>
            <td>{!! count($prioritisation->prioritisation_places)>0 ? $prioritisation->prioritisation_places[0]->count_total_village : '--' !!}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>  

  <div class="row">
    <strong class="col-md-12 col-xs-12">Summary of meeting participants</strong>
  </div>

  <div class="row">
    <div class="col-md-12 table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Age Group</th>
            <th>Male</th>
            <th>Female</th>
            <th>Total</th>
            <th class="col-md-3 col-xs-4">Number of Participants with Disabilities</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>0 - 14 yrs</td>
            <td>{!! $prioritisation->participants_male_0_14 !!}</td>
            <td>{!! $prioritisation->participants_female_0_14 !!}</td>
            <td>{!! $prioritisation->participants_male_0_14+$prioritisation->participants_female_0_14 !!}</td>
            <td>{!! $prioritisation->participants_disable_0_14 !!}</td>
          </tr>
          <tr>
            <td>15 - 24 yrs</td>
            <td>{!! $prioritisation->participants_male_15_24 !!}</td>
            <td>{!! $prioritisation->participants_female_15_24 !!}</td>
            <td>{!! $prioritisation->participants_male_15_24+$prioritisation->participants_female_15_24 !!}</td>
            <td>{!! $prioritisation->participants_disable_15_24 !!}</td>
          </tr>
          <tr>
            <td>25 - 59 yrs</td>
            <td>{!! $prioritisation->participants_male_25_59 !!}</td>
            <td>{!! $prioritisation->participants_female_25_59 !!}</td>
            <td>{!! $prioritisation->participants_male_25_59+$prioritisation->participants_female_25_59 !!}</td>
            <td>{!! $prioritisation->participants_disable_25_59 !!}</td>
          </tr>
          <tr>
            <td>60+ yrs</td>
            <td>{!! $prioritisation->participants_male_60_plus !!}</td>
            <td>{!! $prioritisation->participants_female_60_plus !!}</td>
            <td>{!! $prioritisation->participants_male_60_plus+$prioritisation->participants_female_60_plus !!}</td>
            <td>{!! $prioritisation->participants_disable_60_plus !!}</td>
          </tr>
          <tr>
            <td>Total</td>
            <td>{!! $prioritisation->participants_male_0_14+$prioritisation->participants_male_15_24+$prioritisation->participants_male_25_59+$prioritisation->participants_male_60_plus !!}</td>
            <td>{!! $prioritisation->participants_female_0_14+$prioritisation->participants_female_15_24+$prioritisation->participants_female_25_59+$prioritisation->participants_female_60_plus !!}</td>
            <td>{!! $prioritisation->participants_male_0_14+$prioritisation->participants_male_15_24+$prioritisation->participants_male_25_59+$prioritisation->participants_male_60_plus+$prioritisation->participants_female_0_14+$prioritisation->participants_female_15_24+$prioritisation->participants_female_25_59+$prioritisation->participants_female_60_plus !!}</td>
            <td>{!! $prioritisation->participants_disable_0_14+$prioritisation->participants_disable_15_24+$prioritisation->participants_disable_25_59+$prioritisation->participants_disable_60_plus !!}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <p class="col-md-12 bg-section">Priority needs identified during the community participatory meeting process</p>
  </div>

  <div class="row">
    <strong class="col-md-12">Priority Needs Identified during: <span>{!! $prioritisation->need_identify->name or '' !!}</span></strong>
  </div>    

  <div class="row">
    <strong class="col-md-12">Top 5 Community Priority Needs:</strong>
  </div>

  <div class="row">
    <div class="col-md-12 table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th class="col-md-1">Rank</th>
            <th>Identified need</th>
            <th>Sector</th>
          </tr>
        </thead>

        <tbody>
          @foreach($prioritisation->prioritisation_needs as $need)
            <tr>
              <td>{!! $need->priority !!}</td>
              <td>{!! $need->description !!}</td>
              <td>{!! $need->sector->name or '' !!}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <p class="col-md-12 bg-section">Subproject beneficiaries</p>
  </div>

  <div class="row">
    <strong class="col-md-12">Estimated number of beneficiaries:</strong>
  </div>
  <div class="row">
    <div class="col-md-12 table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Age Group</th>
            <th>Male</th>
            <th>Female</th>
            <th>PLWDs</th>
            <th>Total</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>0 - 14 yrs</td>
            <td>{!! $prioritisation->beneficiaries_male_0_14 !!}</td>
            <td>{!! $prioritisation->beneficiaries_female_0_14 !!}</td>
            <td>{!! $prioritisation->beneficiaries_plwds_0_14 !!}</td>
            <td>{!! $prioritisation->beneficiaries_male_0_14+$prioritisation->beneficiaries_female_0_14+$prioritisation->beneficiaries_plwds_0_14 !!}</td>
          </tr>
          <tr>
            <td>15 - 24 yrs</td>
            <td>{!! $prioritisation->beneficiaries_male_15_24 !!}</td>
            <td>{!! $prioritisation->beneficiaries_female_15_24 !!}</td>
            <td>{!! $prioritisation->beneficiaries_plwds_15_24 !!}</td>
            <td>{!! $prioritisation->beneficiaries_male_15_24+$prioritisation->beneficiaries_female_15_24+$prioritisation->beneficiaries_plwds_15_24 !!}</td>
          </tr>
          <tr>
            <td>25 - 59 yrs</td>
            <td>{!! $prioritisation->beneficiaries_male_25_59 !!}</td>
            <td>{!! $prioritisation->beneficiaries_female_25_59 !!}</td>
            <td>{!! $prioritisation->beneficiaries_plwds_25_59 !!}</td>
            <td>{!! $prioritisation->beneficiaries_male_25_59+$prioritisation->beneficiaries_female_25_59+$prioritisation->beneficiaries_plwds_25_59 !!}</td>
          </tr>
          <tr>
            <td>60+ yrs</td>
            <td>{!! $prioritisation->beneficiaries_male_60_plus !!}</td>
            <td>{!! $prioritisation->beneficiaries_female_60_plus !!}</td>
            <td>{!! $prioritisation->beneficiaries_plwds_60_plus !!}</td>
            <td>{!! $prioritisation->beneficiaries_male_60_plus+$prioritisation->beneficiaries_female_60_plus+$prioritisation->beneficiaries_plwds_60_plus !!}</td>
          </tr>
          <tr>
            <td>Total</td>
            <td>{!! $prioritisation->beneficiaries_male_0_14+$prioritisation->beneficiaries_male_15_24+$prioritisation->beneficiaries_male_25_59+$prioritisation->beneficiaries_male_60_plus !!}</td>
            <td>{!! $prioritisation->beneficiaries_female_0_14+$prioritisation->beneficiaries_female_15_24+$prioritisation->beneficiaries_female_25_59+$prioritisation->beneficiaries_female_60_plus !!}</td>
            <td>{!! $prioritisation->beneficiaries_plwds_0_14+$prioritisation->beneficiaries_plwds_15_24+$prioritisation->beneficiaries_plwds_25_59+$prioritisation->beneficiaries_plwds_60_plus !!}</td>
            <td>{!! $prioritisation->beneficiaries_male_0_14+$prioritisation->beneficiaries_male_15_24+$prioritisation->beneficiaries_male_25_59+$prioritisation->beneficiaries_male_60_plus+$prioritisation->beneficiaries_female_0_14+$prioritisation->beneficiaries_female_15_24+$prioritisation->beneficiaries_female_25_59+$prioritisation->beneficiaries_female_60_plus+$prioritisation->beneficiaries_plwds_0_14+$prioritisation->beneficiaries_plwds_15_24+$prioritisation->beneficiaries_plwds_25_59+$prioritisation->beneficiaries_plwds_60_plus !!}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

   <div class="row">
    <p class="col-md-12 bg-section">Attachments:</p>
  </div>

  @foreach($prioritisation->attachments as $attachment)
    <div class="row">
      <a class="col-md-12 col-xs-12" href="{!! asset('/file/'.$attachment->id.'/download') !!}">{!! $attachment->name !!}</a>
    </div>
  @endforeach

  <div class="row"><br></div>

  <div class="row"><hr></div>

  <div class="row" style="text-align: right;">
    <a class="btn btn-default" href="{!! asset('prioritisation/'.$prioritisation->id.'/edit') !!}">Edit</a>
  </div>

  <div class="row">
    <hr>
  </div>
</div>
@stop