@extends('master.main')

@section('css_block')
<style>
  .attachement-img-wrapper{
    height: 200px;
    text-align: center;
    background-color: #FFF;
    margin: 10px 0px;
    padding: 10px 40px;
    border-radius: 6px;
  }
  .attachement-img-inner-wrapper{
    height: 100%;
    background-color:ghostwhite;
  }
  .attachement-img{
    max-width: 100%;
    height: 100%;
  }
  .row{
    margin: 0px;
    padding: 0px 15px;
    overflow: hidden;
  }
  .report-preview-image-wrapper{
    text-align: center;
    width: 50px;
    height: 35px;
  }
  .report-preview-image{
    max-height: 100%;
    width: 100%;
  }
</style>
@stop

@section('js_block')
{!! Html::script('js/angular.min.js') !!}
{!! Html::script('js/angular-route.min.js') !!}
{!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
{!! HTML::script('js/ng-file-upload.min.js') !!}
{!! HTML::script('js/attach-uploader.js') !!}

{!! Html::script('js/P3.js') !!}
<script>
  window.init_data = {
    'reports' : {!! $subproject->progress_reports !!}
  };
</script>
@stop

@section('content')
<div ng-app="P3App" ng-controller="P3ShowCtrl">
  <h3 class="col-md-10 col-xs-10">
    {!! $subproject->name !!}
  </h3>

  <h3 class="col-md-2 col-xs-2">
    @if(Auth::check())
      <a class="pull-right btn btn-default" href="{!! asset('/subproject/'.$subproject->id.'/edit') !!}">Edit</a>
    @else
      &nbsp;
    @endif
  </h3>

  <strong class="col-md-2 col-xs-2">Progress:</strong>
  <div class="col-md-6 col-xs-6">
    <div class="progress subproject-progress">
      <div class="progress-bar" role="progressbar" aria-valuenow="25"
           aria-valuemin="0" aria-valuemax="100" style="width: {{ $completition_old_percent }}%; background-color: #337ab7 !important; border:none; -webkit-print-color-adjust: exact;"
           data-brix_readonly="true" data-toggle="tooltip" data-placement="top" title="past">{!! $completition_old_percent !!}&percnt;</div>
      @if($completition_recent_percent>0)
        <div class="progress-bar progress-bar-success" role="progressbar"
             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
             style="width: {{ $completition_recent_percent }}%; background-color: #5cb85c !important; border:none; -webkit-print-color-adjust: exact;" data-brix_readonly="true" data-toggle="tooltip"
             data-placement="top" title="last month">{!! $completition_recent_percent !!}&percnt;</div>
      @endif
    </div>
  </div>
  <div class="col-md-4 col-xs-4">
    <p>{!! $completition_percent !!}% Completion</p>
  </div>


  <div class="row">
    <p class="col-md-12 col-xs-12 bg-section">Location and name of requested subproject</p>
  </div>

  <div class="row">
    <strong class="col-md-2 col-xs-6">
      Province:
    </strong>
    <span class="col-md-4 col-xs-6">
      {!! $village->ward->llg->district->province->name !!}
    </span>
    <strong class="col-md-2 col-xs-6">
      District:
    </strong>
    <span class="col-md-4 col-xs-6">
      {!! $village->ward->llg->district->name !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-2 col-xs-6">
      LLG:
    </strong>
    <span class="col-md-4 col-xs-6">
      {!! $village->ward->llg->name !!}
    </span>
    <strong class="col-md-2 col-xs-6">
      Ward:
    </strong>
    <span class="col-md-4 col-xs-6">
      {!! $village->ward->name !!}
    </span>
  </div>



  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Village (subproject site):
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $village->name !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-8 col-xs-6">
      Expected duration of implementation of requested subproject:
    </strong>
    <span class="col-md-4 col-xs-6">
      {!! $subproject->expected_duration == NULL ? '' : $subproject->expected_duration.' months' !!}
    </span>
  </div>

  <div class="row">
    <p class="col-md-12 bg-section">Work plan stages and activities</p>
  </div>

  @foreach($subproject->work_stages as $work_stage)
    <div class="row">
      <div class="col-md-12">
        <table class="table table-condensed table-hover">
          <thead>
            <tr><th class="col-md-10">{!! $work_stage->stage !!}</th><th class="col-md-2 text-center">Completed</th></tr>
          </thead>
          <tbody>
            @foreach($work_stage->work_activities as $work_activity)
              <tr>
                <td>{!! $work_activity->description !!}</td>
                @if($work_activity->completed == 1)
                  <td>yes</td>
                @else
                  <td>no</td>
                @endif
              </tr>
            @endforeach
          </tbody>
      </table>
      </div>
    </div>
  @endforeach

  <div class="row">
    <p class="col-md-12 bg-section">Subproject Funding Details</p>
  </div>

  <div class="row">
    <strong class="col-md-12 col-xs-12">
      Total amount required to implement the subproject:
    </strong>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Original estimate:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $subproject->approval ? number_format($subproject->approval->fund_estimate_origin, 2, ".", "") : 0 !!} PGK
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      New estimate:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $subproject->fund_estimate_new == NULL ? '--' : number_format($subproject->fund_estimate_new, 2, ".", "").' PGK' !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-12 col-xs-6">
      Funding Resources for this Project:
    </strong>
  </div>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th></th>
            <th>Original amount</th>
            <th>Actual amount</th>
            <th>% of total</th>
            <th>Letter of commitment</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>RSDLGP</td>
            <td>{!! ($subproject->approval and $subproject->approval->rdp_fund) ? number_format($subproject->approval->rdp_fund->origin_amount, 2, ".", "") : 0 !!} PGK</td>
            <td>{!! ($subproject->approval and $subproject->approval->rdp_fund) ? number_format($subproject->approval->rdp_fund->accuate_amount, 2, ".", "").' PGK' : '--' !!}</td>
            @if($subproject->fund_estimate_new > 0 and $subproject->approval->rdp_fund)
              <td>{!! round($subproject->approval->rdp_fund->accuate_amount/$subproject->fund_estimate_new*100, 2) !!}%</td>
            @else
              <td></td>
            @endif
            <td>--</td>
          </tr>
          <tr>
            <td>Community Contribution</td>
            <td>{!! ($subproject->approval and $subproject->approval->community_fund) ? number_format($subproject->approval->community_fund->origin_amount, 2, ".", "") : 0 !!} PGK</td>
            <td>{!! ($subproject->approval and $subproject->approval->community_fund) ? number_format($subproject->approval->community_fund->accuate_amount, 2, ".", "").' PGK' : '--' !!}</td>
            @if($subproject->fund_estimate_new > 0 and $subproject->approval->community_fund)
              <td>{!! round($subproject->approval->community_fund->accuate_amount/$subproject->fund_estimate_new*100, 2) !!}%</td>
            @else
              <td></td>
            @endif
            <td>--</td>
          </tr>
          @if($subproject->approval)
            @foreach($subproject->approval->counterpart_funds as $counterpart_fund)
              <tr>
                <td>{!! $counterpart_fund->source !!}</td>
                <td>{!! number_format($counterpart_fund->origin_amount, 2, ".", "") !!} PGK</td>
                <td>{!! $counterpart_fund->accuate_amount != NULL ? number_format($counterpart_fund->accuate_amount, 2, ".", "").' PGK' : '--' !!}</td>
                @if($subproject->fund_estimate_new > 0 and $subproject->approval)
                  <td>{!! round($counterpart_fund->accuate_amount/$subproject->fund_estimate_new*100, 2) !!}%</td>
                @else
                  <td></td>
                @endif
                @if($counterpart_fund->has_commitment == 1)
                  <td>yes</td>
                @elseif($counterpart_fund->has_commitment == 0)
                  <td>no</td>
                @else
                  <td>--</td>
                @endif
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div> 

  <div class="row">
    <strong class="col-md-6 col-xs-10">
      Is the subproject technically feasible:
    </strong>
    <span class="col-md-6 col-xs-2">
      {!! $subproject->is_technically_feasible == 1 ? 'Yes' : 'No' !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-6 col-xs-10">
      Is the subproject financially feasible:
    </strong>
    <span class="col-md-6 col-xs-2">
      {!! $subproject->is_financially_feasible == 1 ? 'Yes' : 'No' !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-11 col-xs-10">
      If the subproject is to be co-financed or getting counterpart funding, what is the likelihood of the funds being available:
    </strong>
    <span class="col-md-1 col-xs-2">
      {!! $subproject->is_co_financed == NULL ? '--' : $subproject->is_co_financed !!}
    </span>
  </div>

  <div class="row">
    <p class="col-md-12 bg-section">Credentials</p>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-4">
      Prepared by:
    </strong>
    <span class="col-md-5 col-xs-6">
      {!! $subproject->credential_prepared_by !!}
    </span>
    <span class="col-md-3 col-xs-2">
      {!! $subproject->credential_prepared_date != NULL ? date_format(date_create($subproject->credential_prepared_date), 'd/m/Y') : '' !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-4">
      Confirmed by:
    </strong>
    <span class="col-md-5 col-xs-6">
      {!! $subproject->credential_confirmed_by !!}
    </span>
    <span class="col-md-3 col-xs-2">
      {!! $subproject->credential_confirmed_date != NULL ? date_format(date_create($subproject->credential_confirmed_date), 'd/m/Y') : '' !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-4">
      Assessment results certified by:
    </strong>
    <span class="col-md-5 col-xs-6">
      {!! $subproject->credential_certified_by !!}
    </span>
    <span class="col-md-3 col-xs-2">
      {!! $subproject->credential_certified_date != NULL ? date_format(date_create($subproject->credential_certified_date), 'd/m/Y') : '' !!}
    </span>
  </div>

  <div class="row">
    <p class="col-md-12 bg-section">Attachments (Map or sketch of village showing natural resource base, social &amp; infra services):</p>
  </div>

  @foreach($subproject->attachments as $attachment)
    <div class="row">
      <a class="col-md-12 col-xs-12" href="{!! asset('/file/'.$attachment->id.'/download') !!}">{!! $attachment->name !!}</a>
    </div>
  @endforeach

  <div class="row">
    <p class="col-md-12 bg-section">Monitoring Reports: <span class="pull-right"><a href="{!! asset('/subproject/'.$subproject->id.'/report/create') !!}">create new report</a></span></p>
  </div>

  <div class="col-md-12">
    <table class="table table-hover table-condensed">
      <tr ng-repeat="(index, report) in reports">
        <td>
          <div class="report-preview-image-wrapper" ng-show="report.images[0].id">
            <img class="report-preview-image" src="{!! asset('/file') !!}/<% report.images[0].id %>" alt="">
          </div>
          <div class="report-preview-image-wrapper" ng-show="!report.images[0].id">
            <img class="report-preview-image" src="{!! asset('/images/project-thumb.png') !!}" alt="">
          </div>
        </td>
        <td>
          <a href="{!! asset('/subproject/'.$subproject->id.'/report') !!}/<% report.id %>"><% report.monitoring_date | date: 'dd/MM/yyyy' %></a>
        </td>
        @can('destroy')
          <td style="text-align:center;">
            <a href="" ng-click="deleteReport(report.subproject_id, report.id, index)">delete</a>
          </td>
        @endcan
      </tr>
    </table>
  </div>    


  <div class="row"><hr></div>

  @if(Auth::check())
    <div class="row" style="text-align: right;">
      <a class="pull-right btn btn-default" href="{!! asset('/subproject/'.$subproject->id.'/edit') !!}">Edit</a>
    </div>
  @endif

  <div class="row">
    <hr>
  </div>
</div>
@stop