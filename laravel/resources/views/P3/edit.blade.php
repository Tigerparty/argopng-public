@extends('master.main')

@section('css_block')
<style>
  #ui-datepicker-div
  {
    display: none;
  }
  ul.ui-autocomplete.ui-menu{
    width:50px;
  }
  .row{
      margin: 0px;
      padding: 0px 15px;
  }
  @media screen and (min-width: 992px) {
    .dollar-sign{
      position: relative;
      top: 7px;
      left: 10px;
    }
  }
</style>
@stop

@section('js_block')
{!! Html::script('js/angular.min.js') !!}
{!! Html::script('js/angular-route.min.js') !!}
{!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
{!! HTML::script('js/ng-file-upload.min.js') !!}
{!! HTML::script('js/attach-uploader.js') !!}
{!! Html::script('js/P3.js') !!}
<script>
  window.init_data = {
    'subproject': {!! old() ? json_encode(old('subproject')) : $subproject !!},
    'attachments': {!! old('attachments') ? json_encode(old('attachments')) : $attachments !!},
    'provinces': {!! $provinces !!},
    'locations': {!! old() ? json_encode([old('province_id'), old('district_id'), old('llg_id'), old('ward_id'), old('village_id')]) : json_encode($locations) !!},
    'counterpart_fundings': {!! old('counterpart_fundings') ? json_encode(old('counterpart_fundings')) : '[]' !!}
  };
</script>
@stop

@section('content')
<div ng-app="P3App" ng-controller="P3EditCtrl">
  <div class="row">
    <h3 class="col-md-12">
      Subproject Implementation Form
    </h3>
  </div>

  @if($errors->has())
    <div class="row">
      <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
          {{ $error }}<br>
        @endforeach
      </div>
    </div>
  @endif

  <div class="row">
    <p class="col-md-12 bg-section">Location and name of requested subproject</p>
  </div>

  {!! Form::open(['url' => '/subproject/'.$subproject->id, 'method' => 'PUT']) !!}
    <div class="content">

      <div class="row">
        <label for="" class="col-md-3 col-sm-5 col-xs-12 control-label">Name of subproject</label>
        <div class="col-md-9 col-sm-7 col-xs-12">
          <input type="text" class="form-control" name="subproject[name]" ng-model="subproject.name" placeholder="Enter name of subproject">
        </div>
      </div>

      <div><br class="hidden-xs"></div>

      <div class="row">
        <label class="col-md-3 control-label col-sm-5 col-xs-12">Province:
        </label>
        <div class="col-md-3 col-sm-7 col-xs-12">
          <input type="hidden" name="province_id" value="<% province_id %>">
          <select id="province" class="form-control"
                  ng-model="province_id" ng-change="updateDistricts()" ng-disabled="true">
            <option value="">-- Select Province --</option>
              @foreach($provinces as $province)
                <option value="{!! $province->id !!}" ng-selected="province_id == {!! $province->id !!}">{!! $province->name !!}</option>
              @endforeach
          </select>
        </div>

        <div class="row visible-sm">&emsp;</div> <!-- spacer for smaller device -->
      
        <label class="col-md-3 control-label col-sm-5 col-xs-12">District:
        </label>
        <div class="col-md-3 col-sm-7 col-xs-12">
          <input type="hidden" name="district_id" value="<% district_id %>">
          <select id="district" class="form-control" 
                  ng-model="district_id" ng-disabled="true" ng-change="updateLlgs()">
            <option value="">-- Select District --</option>
            <option ng-repeat="district in districts" value="<% district.id %>" ng-selected="district_id == district.id"><% district.name %></option>
          </select>
        </div>
      </div>

      <div><br class="hidden-xs"></div>


      <div class="row">
        <label class="col-md-3 control-label col-sm-5 col-xs-12">LLG:
        </label>
        <div class="col-md-3 col-sm-7 col-xs-12">
          <input type="hidden" name="llg_id" value="<% llg_id %>">
          <select id="llg" class="form-control"
                  ng-model="llg_id" ng-disabled="true" ng-change="updateWards()">
            <option value="">-- Select LLG --</option>
            <option ng-repeat="llg in llgs" value="<% llg.id %>" ng-selected="llg_id == llg.id"><% llg.name %></option>
          </select>
        </div>
      <div class="row visible-sm">&emsp;</div> <!-- spacer for smaller device -->
        <label class="col-md-3 control-label col-sm-5 col-xs-12">Ward:
        </label>
        <div class="col-md-3 col-sm-7 col-xs-12">
          <input type="hidden" name="ward_id" value="<% ward_id %>">
          <select id="ward" class="form-control"
                  ng-model="ward_id" ng-disabled="true" ng-change="updateVillages()">
            <option selected="selected" value="">-- Select Ward --</option>
            <option ng-repeat="ward in wards" value="<% ward.id %>" ng-selected="ward_id == ward.id"><% ward.name %></option>
          </select>
        </div>
      </div>

      <div><br class="hidden-xs"></div>

      <div class="row">
        <label class="col-md-3 control-label col-sm-5 col-xs-12">Village (subproject site):
        </label>
        <div class="col-md-3 col-sm-7 col-xs-12">
          <select name="village_id" id="village" class="form-control"
                  ng-model="village_id" ng-disabled="!ward_id">
            <option selected="selected" value="">-- Select Village --</option>
            <option ng-repeat="village in villages" value="<% village.id %>" ng-selected="village_id == village.id"><% village.name %></option>
          </select>
        </div>
      </div>

      <div>
        <br class="hidden-xs">
      </div>

      <div class="row">
        <label class="col-md-8 col-sm-7 col-xs-12">What is the expected duration of implementation of requested subproject:</label>
        <div class="col-md-4 col-sm-5 col-xs-12">
          <div class="input-group">
          <input type="number" min=1 step="any" string-to-number class="form-control" name="subproject[expected_duration]" ng-model="subproject.expected_duration" placeholder="number of months">
          <div class="input-group-addon">months</div>
          </div>
        </div>
      </div>

      <div class="row">
        <p class="col-md-12 bg-section">Work plan stages and activities:</p>
      </div>

      <div class="row" ng-repeat="(stg_idx, work_stage) in subproject.work_stages">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <input type="hidden" name="subproject[work_stages][<% stg_idx %>][id]"
                value="<% work_stage.id %>">
              Stage <% stg_idx + 1 %>
              <button type="button" class="close pull-right" aria-label="Close"
                ng-click="deleteWorkStage(stg_idx)">
                <span aria-hidden="true">
                  &times;
                </span>
              </button>
            </div>
            <div class="panel-body">
              <div ng-repeat="(act_idx, activity) in work_stage.work_activities">
                <div class="activity-row" ng-show="activity.completed == 1">
                  <p class="col-md-9 col-xs-8"><% activity.description %></p>
                  <p class="col-md-3 col-xs-4">Completed: <span>Yes</span></p>
                </div>

                <div class="activity-row" ng-show="activity.completed != 1">
                  <div class="col-md-11 col-xs-10">
                    <input type="hidden" name="subproject[work_stages][<% stg_idx %>][work_activities][<% act_idx %>][id]" value="<% activity.id %>">
                    <input type="text" class="form-control" 
                        placeholder="Activity description"
                        name="subproject[work_stages][<% stg_idx %>][work_activities][<% act_idx %>][description]"
                        ng-model="activity.description">
                  </div>
                  <a class="col-md-1 col-xs-2 position-adjust" href="" ng-click="deleteWorkActivity(stg_idx, act_idx)">Delete</a>
                </div>

                <div><br></div>
              </div>

              <a class="col-md-3" href="" ng-click="addWorkActivity(stg_idx)">Add Activities</a>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-2">
          <input class="btn btn-default btn-xs" type="button" value="Add Stage" ng-click="addWorkStage()">
        </div>
      </div>

      <div class="row">
        <p class="col-md-12 bg-section">Subproject Funding Details</p>
      </div>

      <div class="row">
        <strong class="col-md-12 col-xs-12">Total amount required to implement the subproject:</strong>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-3 col-xs-6">Original estimate</label>
        <div class="col-md-3 col-xs-6">
          <div class="input-group">
            <input type="hidden" name="subproject[approval][fund_estimate_origin]" value="<% subproject.approval.fund_estimate_origin %>">
            <input type="number" string-to-number class="form-control" ng-model="subproject.approval.fund_estimate_origin" disabled>
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
        <div class="row visible-xs-block visible-sm-block">&emsp;</div> <!-- spacer for smaller device -->
        <label class="col-md-3 col-xs-6">New estimate</label>
        <div class="col-md-3 col-xs-6">
          <div class="input-group">
            <input type="number" min=0 step="any" string-to-number name="subproject[fund_estimate_new]" ng-model="subproject.fund_estimate_new" class="form-control" ng-KeyUp="calculateTotalActualAmount()">
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <strong class="col-md-12 col-xs-12">Funding Resources for this Project::</strong>
      </div>

      <div><br></div>


      <div class="row">
        <label class="col-md-4 col-xs-4"></label>
        <label class="col-md-3 col-xs-3 text-center">Original amount</label>
        <label class="col-md-3 col-xs-3 text-center">Actual amount</label>
        <label class="col-md-2 col-xs-2 text-center">% of total</label>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-4 col-xs-4">RSDPLGP</label>
        <div class="col-md-3 col-xs-3">
          <div class="input-group">
            <input type="hidden" name="subproject[approval][rdp_fund][origin_amount]" value="<% subproject.approval.rdp_fund.origin_amount %>">
            <input type="number" class="form-control" value="<% amountRound(subproject.approval.rdp_fund.origin_amount) %>" disabled>
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
        <div class="col-md-3 col-xs-3">
          <input type="hidden" name="subproject[approval][rdp_fund][id]" value="<% subproject.approval.rdp_fund.id %>">
          <div class="input-group">
            <input type="number" min=0 step="any" string-to-number class="form-control" name="subproject[approval][rdp_fund][accuate_amount]" ng-model="subproject.approval.rdp_fund.accuate_amount" ng-KeyUp="calculateTotalActualAmount()">
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="text" class="form-control" value="<% isFinite(subproject.approval.rdp_fund.accuate_amount/subproject.fund_estimate_new) ? amountRound(subproject.approval.rdp_fund.accuate_amount/subproject.fund_estimate_new*100) : 0 %>%" disabled>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-4 col-xs-4">Community Contribution</label>
        <div class="col-md-3 col-xs-3">
          <div class="input-group">
            <input type="hidden" name="subproject[approval][community_fund][origin_amount]" value="<% subproject.approval.community_fund.origin_amount %>">
            <input type="number" class="form-control" value="<% amountRound(subproject.approval.community_fund.origin_amount) %>" disabled>
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
        <div class="col-md-3 col-xs-3">
          <input type="hidden" name="subproject[approval][community_fund][id]" value="<% subproject.approval.community_fund.id %>">
          <div class="input-group">
            <input type="number" min=0 step="any" string-to-number class="form-control" name="subproject[approval][community_fund][accuate_amount]" ng-model="subproject.approval.community_fund.accuate_amount" ng-KeyUp="calculateTotalActualAmount()">
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="text" class="form-control" value="<% isFinite(subproject.approval.community_fund.accuate_amount/subproject.fund_estimate_new) ? amountRound(subproject.approval.community_fund.accuate_amount/subproject.fund_estimate_new*100) : 0 %>%" disabled>
        </div>
      </div>

      <div><br></div>

      <div ng-repeat="counterpart_fund in subproject.approval.counterpart_funds track by $index">
        <div class="row">
          <input type="hidden" name="subproject[approval][counterpart_funds][<% $index %>][source]" value="<% counterpart_fund.source %>">
          <label class="col-md-4 col-xs-4"><% counterpart_fund.source %></label>
          <div class="col-md-3 col-xs-3">
            <div class="input-group">
              <input type="hidden" name="subproject[approval][counterpart_funds][<% $index %>][origin_amount]" value="<% counterpart_fund.origin_amount %>">
              <input type="number" class="form-control" value="<% amountRound(counterpart_fund.origin_amount) %>" disabled>
              <div class="input-group-addon">PGK</div>
            </div>
          </div>
          <div class="col-md-3 col-xs-3">
            <input type="hidden" name="subproject[approval][counterpart_funds][<% $index %>][id]" value="<% counterpart_fund.id %>">
            <div class="input-group">
              <input type="number" min=0 step="any" string-to-number class="form-control" name="subproject[approval][counterpart_funds][<% $index %>][accuate_amount]" ng-model="counterpart_fund.accuate_amount" ng-KeyUp="calculateTotalActualAmount()">
              <div class="input-group-addon">PGK</div>
            </div>
          </div>
          <div class="col-md-2 col-xs-2">
            <input type="text" class="form-control" value="<% isFinite(counterpart_fund.accuate_amount/subproject.fund_estimate_new) ? (counterpart_fund.accuate_amount/subproject.fund_estimate_new*100).toFixed(0) : 0 %>%" disabled>
          </div>
        </div>

        <div><br></div>
      </div>

      <div class="row">
        <strong class="col-xs-12">Other potential sources of counterpart funding:</strong>
      </div>

      <div><br></div>

      <div ng-repeat="counterpart_funding in counterpart_fundings track by $index">
        <div class="row">
          <div class="col-md-7 col-sm-4 col-xs-5">
            <input type="text" class="form-control" name="counterpart_fundings[<% $index %>][source]" ng-model="counterpart_funding.source" placeholder="Source of counterpart funding">
          </div>
          <div class="col-md-3 col-sm-4 col-xs-4">
            <input type="number" min=0 step="any" string-to-number class="form-control" name="counterpart_fundings[<% $index %>][origin_amount]" ng-model="counterpart_funding.origin_amount" ng-KeyUp="calculateTotalOriginAmount()">
          </div>
          <div class="col-md-2 col-sm-4 col-xs-3">
            <input type="text" class="form-control" value="<% isFinite(counterpart_funding.origin_amount/subproject.fund_estimate_new) ? amountRound(counterpart_funding.origin_amount/subproject.fund_estimate_new*100) : 0 %>%" disabled>
          </div>
        </div>

        <div><br></div>

        <div class="row">
          <strong class="col-md-10 col-xs-9">Letters of commitment for counterpart funding / co-financing provided:</strong>
          <div class="col-md-2 col-xs-3 text-center">
            <label class="radio-inline">
              <input type="radio" name="counterpart_fundings[<% $index %>][has_commitment]" ng-model="counterpart_funding.has_commitment" value="1">Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="counterpart_fundings[<% $index %>][has_commitment]" ng-model="counterpart_funding.has_commitment" value="0">No
            </label>
          </div>
        </div>

        <div><br></div>
      </div>

      <div class="row">
        <div class="col-md-2">
          <input class="btn btn-default btn-xs" type="button" value="Add Row" ng-click="addCounterpartFunding()">
        </div>
      </div>

      <div class="row">
        <hr>
      </div>

      <div class="row">
        <label class="col-md-4 col-xs-4">Total</label>
        <div class="col-md-3 col-xs-3">
          <div class="input-group">
            <input type="text" class="form-control" value="<% totalOriginAmount.toFixed(2) %>" disabled>
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
        <div class="col-md-3 col-xs-3">
          <div class="input-group">
            <input type="text" class="form-control" value="<% totalActualAmount.toFixed(2) %>" disabled>
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
        <div class="col-md-2 col-xs-2">
          <input type="text" class="form-control" value="<% isFinite(totalActualAmountPercent) ? amountRound(totalActualAmountPercent) : 0 %>%" disabled>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <strong class="col-md-7 col-sm-9 col-xs-9">Is the subproject technically feasible:</strong>
        <div class="col-md-5 col-sm-3 col-xs-3 text-center">
          <label class="radio-inline">
            <input type="radio" name="subproject[is_technically_feasible]" ng-model="subproject.is_technically_feasible" value="1">Yes
          </label>
          <label class="radio-inline">
            <input type="radio" name="subproject[is_technically_feasible]" ng-model="subproject.is_technically_feasible" value="0">No
          </label>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <strong class="col-md-7 col-sm-9 col-xs-9">Is the subproject financially feasible:</strong>
        <div class="col-md-5 col-sm-3 col-xs-3 text-center">
          <label class="radio-inline">
            <input type="radio" name="subproject[is_financially_feasible]" ng-model="subproject.is_financially_feasible" value="1">Yes
          </label>
          <label class="radio-inline">
            <input type="radio" name="subproject[is_financially_feasible]" ng-model="subproject.is_financially_feasible" value="0">No
          </label>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <strong class="col-md-8 col-xs-12">If the subproject is to be co-financed or getting counterpart funding, what is the likelihood of the funds being available:</strong>
        <div class="col-md-4 col-xs-12">
          <label class="radio-inline">
            <input type="radio" name="subproject[is_co_financed]" ng-model="subproject.is_co_financed" value="High">High
          </label>
          <label class="radio-inline">
            <input type="radio" name="subproject[is_co_financed]" ng-model="subproject.is_co_financed" value="Medium">Medium
          </label>
          <label class="radio-inline">
            <input type="radio" name="subproject[is_co_financed]" ng-model="subproject.is_co_financed" value="Low">Low
          </label>
        </div>
      </div>

      <div class="row">
        <p class="col-md-12 bg-section">Credentials</p>
      </div>

      <div class="row">
        <label class="col-md-4 control-label col-xs-12">Prepared by:
        </label>
        <div class="col-md-4 col-xs-6">
          <input type="text" class="form-control" name="subproject[credential_prepared_by]" ng-model="subproject.credential_prepared_by" placeholder="Name of community facilitator">
        </div>
        <div class="col-md-4 col-xs-6">
          <div class="input-group" ng-click="openCredentialPreparedDatePicker($event)">
            <input type="text"
                   class="form-control"
                   name="subproject[credential_prepared_date]"
                   id="credential_prepared_date"
                   ng-required="true"
                   ng-readonly="true"
                   ng-model="subproject.credential_prepared_date"
                   datepicker-popup="dd/MM/yyyy"
                   is-open="CredentialPreparedDatePickerOpened">
            <div class="input-group-addon"><img src="/images/calendar_icon.png" alt="calendar"></div>
          </div>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-4 control-label col-xs-12">Confirmed by:
        </label>
        <div class="col-md-4 col-xs-6">
          <input type="text" class="form-control" name="subproject[credential_confirmed_by]" ng-model="subproject.credential_confirmed_by" placeholder="Name of LLG Manager">
        </div>
        <div class="col-md-4 col-xs-6">
          <div class="input-group" ng-click="openCredentialConfirmedDatePicker($event)">
            <input type="text"
                   class="form-control"
                   name="subproject[credential_confirmed_date]"
                   id="credential_confirmed_date"
                   ng-required="true"
                   ng-readonly="true"
                   ng-model="subproject.credential_confirmed_date"
                   datepicker-popup="dd/MM/yyyy"
                   is-open="CredentialConfirmedDatePickerOpened">
            <div class="input-group-addon"><img src="/images/calendar_icon.png" alt="calendar"></div>
          </div>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-4 control-label col-xs-12">Assessment results certified by:
        </label>
        <div class="col-md-4 col-xs-6">
          <input type="text" class="form-control" name="subproject[credential_certified_by]" ng-model="subproject.credential_certified_by" placeholder="Name of PMU team leader">
        </div>
        <div class="col-md-4 col-xs-6">
          <div class="input-group" ng-click="openCredentialCertifiedDatePicker($event)">
            <input type="text"
                   class="form-control"
                   name="subproject[credential_certified_date]"
                   id="credential_certified_date"
                   ng-required="true"
                   ng-readonly="true"
                   ng-model="subproject.credential_certified_date"
                   datepicker-popup="dd/MM/yyyy"
                   is-open="CredentialCertifiedDatePickerOpened">
            <div class="input-group-addon"><img src="/images/calendar_icon.png" alt="calendar"></div>
          </div>
        </div>
      </div>
     
      <div class="row">
        <p class="col-md-12 bg-section">Attach Implementation Status (Map or sketch of village showing natural resource base, social &amp; infra services):</p>
      </div>

      <div class="row">
        <div ng-repeat="(index, attachment) in attachments">
          <input type="hidden" name="attachments[<% $index %>][id]" value="<% attachment.id %>">
          <input type="hidden" name="attachments[<% $index %>][name]" value="<% attachment.name %>">
          <strong class="col-md-12">
            <a href="{!! asset('/file') !!}/<% attachment.id %>/download"><% attachment.name %></a>&emsp;
            <a href="" ng-click="deleteAttachment(index)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></strong>
        </div>
      </div>

      <filegroup files=""></filegroup>
      <fileuploader></fileuploader>

      <div class="row">
        <hr>
      </div>

      <div class="row">
        <button type="button" class="btn btn-default" onclick="location.href='/subproject/{!! $subproject->id !!}'">Cancel</button>
        <input type="submit" id="edit_submit" class="btn btn-default" value="Submit" ng-disabled="attachment_uploader.is_uploading">
      </div>
    </div>
  {!! Form::close() !!}
</div>
@stop