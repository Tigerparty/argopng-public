@extends('master.main')

@section("css_block")
  <style>
    .map-bubble{
      position: relative;
      width: 300px;
      min-width: 130px;
      margin: 0px;
    }
    .map-bubble-row{
      vertical-align: middle;
      padding: 3px 0px 3px 0px;
    }
    .map-bubble-img-wrapper{
      position: relative;
      display: inline-block;
      text-align: center;
      width: 50px;
    }
    .map-bubble-img{
      width: 100%;
      height: 35px;
      object-fit: contain;
    }
    .map-bubble-content{
      display: inline-block;
      line-height: 0px;
      padding: 0px 0px 0px 15px;
      font-weight: bolder;
    }
    .map-bubble-content a{
      text-decoration: none;
    }
    .row{
        margin: 0px;
        padding: 0px 15px;
        overflow: hidden;
    }
  </style>
  {!! Html::style('css/leaflet.css') !!}
@stop

@section("js_block")
  <script>
    window.init_data = {
      'provinces' : {!! $provinces !!},
      'tabslet_ds' : JSON.parse('{!! json_encode($tabslet_ds, JSON_NUMERIC_CHECK) !!}')
    };
  </script>
  {!! Html::script('js/leaflet.js') !!}
  {!! Html::script('js/angular.min.js') !!}
  {!! Html::script('js/angular-simple-logger.min.js') !!}
  {!! Html::script('js/angular-leaflet-directive.min.js') !!}
  {!! Html::script('js/home.js') !!}
  {!! Html::script('js/jquery.tabslet.min.js') !!}
  {!! HTML::script('js/highcharts.js') !!}
  {!! Html::script('js/highcharts-ng.min.js') !!}
@stop

@section("content")
  <!-- Content -->
  <div ng-app="HomeApp">
    <!--project-search start -->
    <div class="search" ng-controller="MapCtrl as map_ctrl">
      <div class="subproject_search">
        <div class="col-xs-12 col-md-3 search-parameters">
          <p>Search the RSDLGP database.</p>
          <div class="input-group">
            <input type="text" ng-model="keyword" class="form-control" id="search-sp" placeholder="Search by keyword">
            <a href="" class="input-group-addon" ng-click="map_ctrl.showMap();"><img src="images/icon-search.png" alt="search"></a>
          </div>

          <br>

          <div>
            <select name="province" ng-model="province" id="select-province" class="form-control" ng-change="updateDistricts()" ng-options="province.name for province in provinces">
              <option value="">-- Select Province --</option>
            </select>
          </div>

          <br>

          <div>
            <select name="district" ng-model="district" id="select-district" class="form-control" ng-change="updateLlgs()" ng-options="district.name for district in districts" ng-disabled="!province">
              <option value="">-- Select District --</option>
            </select>
          </div>

          <br>

          <div>
            <select name="llg" ng-model="llg" id="select-llg" class="form-control" ng-change="updateWards()" ng-options="llg.name for llg in llgs" ng-disabled="!district">
              <option value="">-- Select LLG --</option>
            </select>
          </div>

          <br>

          <div>
            <select name="ward" ng-model="ward" id="select-ward" class="form-control" ng-change="updateVillages()" ng-options="ward.name for ward in wards" ng-disabled="!llg">
              <option value="" selected>-- Select Ward --</option>
            </select>
          </div>

          <br>

          <div>
            <select name="village" ng-model="village" id="select-village" class="form-control" ng-options="village.name for village in villages" ng-disabled="!ward">
              <option value="" selected>-- Select Village --</option>
            </select>
          </div>

          <br>

          <button class="btn btn-primary col-md-12" ng-click="map_ctrl.showMap();">SEARCH</button>

          <br>

          <a class="btn btn-warning show-map" style="font-size:13px;" ng-click="map_ctrl.showMap();">Click for map of subprojects</a>
        </div>
      </div>

      <div class="col-xs-12 col-md-9">
         <div id="carousel" class="carousel slide" data-ride="carousel" data-interval="5000"
          ng-show="!map_ctrl.isShowMap">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="images/slide_0.jpg" alt="slide_1">
            </div>
            <div class="item">
              <img src="images/slide_1.jpg" alt="slide_2">
            </div>
            <div class="item">
              <img src="images/slide_2.jpg" alt="slide_3">
            </div>
            <div class="item">
              <img src="images/slide_3.jpg" alt="slide_4">
            </div>
          </div>
        </div>
        <div id="" class="projects-map" ng-show="map_ctrl.isShowMap">
          <leaflet
            center="map_ctrl.map.center"
            defaults="map_ctrl.map.defaults"
            markers="map_ctrl.map.markers"
            width="100%" height="500px">
          </leaflet>
        </div>
      </div>

    </div>

    <div class="info row">
          
        
      <!-- .tabbed-content start -->
      <div class="tabbed-content" ng-controller="HomeChartCtrl">
      
        <ul class="tab-links clearfix">
          <li class="tab-beneficiaries">
            <a href="#tab-1" title="Beneficiaries">
              <span class="tab-icon"></span>
              <h2>Beneficiaries</h2>
            </a>
          </li>
          <li class="tab-sectors">
            <a href="#tab-2" title="Sectors">
              <span class="tab-icon"></span>
              <h2>Sectors</h2>
            </a>
          </li>
          <li class="tab-status">
            <a href="#tab-3" title="Subproject Status">
              <span class="tab-icon"></span>
              <h2>Subproject Status</h2>
            </a>
          </li>
          <li class="tab-finances">
            <a href="#tab-4" title="Subproject Finances">
              <span class="tab-icon"></span>
              <h2>Subproject Finances</h2>
            </a>
          </li>
        </ul>
               
        <div class="tab beneficiaries" id='tab-1'>
          <div class="row">
            <p class="col-md-6 col-md-offset-2">Aggregated Beneficiaries across all the Subprojects for Rural Service Delivery and Local Governance Project in Papua New Guinea.<br><br>Total Beneficiaries: {{ $tabslet_ds->total_of_bnf->fmp_amount+$tabslet_ds->total_of_bnf->mp_amount }}<br>Male Beneficiaries: {{ $tabslet_ds->total_of_bnf->mp_amount }}<br>Female Beneficiaries: {{ $tabslet_ds->total_of_bnf->fmp_amount }}</p>
            <div class="col-md-4">
              <highchart id="chart1" config="beneficiariesChartsConfig"></highchart>
            </div>
          </div>
        </div>
        <div class="tab sectors" id='tab-2'>
          <div class="row">
            <p class="col-md-6 col-md-offset-2">
              Subproject Distribution by Sector across all the Subprojects for Rural Service Delivery and Local Governance Project in Papua New Guinea. Sectors:<br>
              <br>
              @foreach($tabslet_ds->sps_of_sector as $index => $sector)
                {!! $index + 1 !!} &ndash; {!! $sector->name !!}: {!! $sector->sp_count !!}<br>
              @endforeach
            </p>
            <div class="col-md-4">
              <highchart id="chart1" config="sectorChartConfig"></highchart>
            </div>
          </div>
        </div>
        <div class="tab status" id='tab-3'>
          <div class="row">
              <p class="col-md-6 col-md-offset-2">
                Subproject Status across all the Subprojects for Rural Service Delivery and Local Governance Project in Papua New Guinea.<br><br>
                @foreach($tabslet_ds->completion_of_provinces as $index => $province)
                  {!! $index + 1 !!} &ndash; {!! $province->name !!}: {!! $province->total_activity_count > 0 ? round($province->completed_activity_count/$province->total_activity_count*100, 0) : 0 !!}%<br>
                @endforeach
              </p>
              <div class="col-md-4">
                <highchart id="chart1" config="provinceCompletionChartConfig"></highchart>
              </div>
            </div>
        </div>            
        <div class="tab finances" id='tab-4'>
          <div class="row">
              <p class="col-md-6 col-md-offset-2">
                Subproject Contributions across all the Subprojects for Rural Service Delivery and Local Governance Project in Papua New Guinea. <br><br>
                @foreach($tabslet_ds->total_of_ctbr as $fund)
                  {!! ucfirst($fund->fund_type) !!} fund: &dollar;{!! $fund->amount !!}<br>
                @endforeach
                Total fund: &dollar;<% fund_total %><br>
              </p>
              <div class="col-md-4">
                <highchart id="chart1" config="fundChartsConfig"></highchart>
              </div>
            </div>
        </div>            
      
      </div>
      <!-- .tabbed-content end -->

    </div> 
    <!-- .info end -->

    <!-- .reporting start -->
    <div class="reporting">
      <div class="section-title clearfix">
        <h2>Field Reports</h2>
      </div>
      <table class="table">
        <thead>
          <tr>
            <th>Subproject</th>
            <th>Village</th>
            <th>Ward</th>
            <th>Province</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          @foreach($reports as $report)
            <tr>
              <td><a href="/subproject/{{ $report->subproject->id }}/report/{{ $report->id }}">{{ $report->subproject->name }}</a></td>
              <td>{{ $report->subproject->village->name }}</td>
              <td>{{ $report->subproject->village->ward->name }}</td>
              <td>{{ $report->subproject->village->ward->llg->district->province->name }}</td>
              <td>{{ date_format(date_create($report->monitoring_date), 'd/m/Y') }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div><!-- .reporting end -->
    <div class="row">
      <hr>
    </div>
  </div>
  <script>
    $(document).ready(function()
    {
      $('.carousel').carousel({
        interval: 7000
      })



      // INFO TABS
      $('.tabbed-content').tabslet({
        active: 5,
        animation: true
      });

      // $('.tab-beneficiaries').on('click', function(attr) {
      //   var total_of_bnf = init_data.tabslet_ds.total_of_bnf;
      //   var bf_ds = [
      //     {
      //         value: total_of_bnf.mp_amount,
      //         color:"#CB6A14",
      //         highlight: "#CB6A14",
      //         label: "Male",
      //         labelColor: 'white',
      //         labelFontSize: '16'
      //     },
      //     {
      //         value: total_of_bnf.fmp_amount,
      //         color: "#F4C079",
      //         highlight: "#F4C079",
      //         label: "Female",
      //         labelColor: 'white',
      //         labelFontSize: '16'
      //     }
      // ];
      //   drawPieChart('bf_pie', 'bf_legend', bf_ds);
      // });
      // $('.tab-sectors').on('click', function(attr) {
      //   var sps_of_sector = init_data.tabslet_ds.sps_of_sector;
      //   var sct_ds = {};
      //   sps_of_sector.forEach(function(value, index) {
      //     sct_ds[index+1] = value.sp_count;
      //   });
      //   drawBarChart('sct_bar', sct_ds);
      // });
      // $('.tab-status').on('click', function(attr) {
      //   var sps_of_status = init_data.tabslet_ds.sps_of_status;
      //   var sta_ds = {};
      //   sps_of_status.forEach(function(value, index) {
      //     sta_ds[index+1] = value.sp_count;
      //   });
      //   drawBarChart('sta_bar', sta_ds);
      // });
      // $('.tab-finances').on('click', function(attr) {
      //   var total_of_ctbr = init_data.tabslet_ds.total_of_ctbr;
      //   var f_ds = [
      //     {
      //         value: total_of_ctbr[1].total,
      //         color: "#CB6A14",
      //         highlight: "#CB6A14",
      //         label: "RDP",
      //         labelColor: 'white',
      //         labelFontSize: '16'
      //     },
      //     {
      //         value: total_of_ctbr[0].total,
      //         color:"#F4C079",
      //         highlight: "#F4C079",
      //         label: "Community",
      //         labelColor: 'white',
      //         labelFontSize: '16'
      //     }
      // ];
      //   drawPieChart('f_pie', 'f_legend' , f_ds);
      // });
    });

  </script>
@stop
