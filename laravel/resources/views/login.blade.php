@extends('master.main')

@section('css_block')
  <style>
    .row{
        margin: 0px;
        padding: 0px 15px;
        overflow: hidden;
    }
  </style>
@stop

@section("content")

    <div class="row">
      <h3 class="col-md-12">Please Login to Your Account</h3>
    </div>

    @if($errors->has())
      <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
          {!! $error !!}<br>
        @endforeach
      </div>
    @endif

    @if(Session::get('login_fail_message'))
      <div class="alert alert-danger">
        {!! Session::get('login_fail_message') !!}
      </div>
    @endif

    <div class="row">
      <hr>
    </div>
    {!! Form::open(array('url' => '/login', 'method' => 'POST')) !!}
      <div class="form-horizontal">
        <div class="form-group">
          <label for="userName" class="col-sm-2 control-label">User Name</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="username" name="username" placeholder="User Name" value="{!! Input::old('username') !!}">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Password</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-4 col-md-4">
            <input type="submit" class="btn btn-default" value="Sign in">
          </div>
        </div>
      </div>
    {!! Form::close() !!}
    <div class="row">
      <hr>
    </div>
@stop