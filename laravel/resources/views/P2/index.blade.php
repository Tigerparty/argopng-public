@extends('master.main')

@section('css_block')
<style>
  .row{
    margin: 0px;
    padding: 0px 15px;
    overflow: hidden;
  }
</style>
@stop

@section('js_block')
{!! Html::script('js/angular.min.js') !!}
{!! Html::script('js/angular-route.min.js') !!}
{!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
{!! Html::script('js/index.js') !!}
<script>
  window.init_data = {
    'provinces': {!! $provinces !!},
  };
</script>
@stop

@section('content')
  <div ng-app="IndexApp" ng-controller="P2IndexCtrl">
    <div class="col-xs-12 col-md-3 search-parameters">
      <p>Search the RSDLGP database by LLG.</p>
      <div class="input-group">
        <input type="text" ng-model="keyword" class="form-control" id="search-sp" placeholder="Search by keyword">
        <a href="" ng-click="searchApprovalByFilter()" class="input-group-addon"><img src="images/icon-search.png" alt="search"></a>
      </div>

      <br>  

      <div>
        <select name="province" ng-model="province" id="select-province" class="form-control" ng-change="updateDistricts()" ng-options="province.name for province in provinces">
          <option value="">-- Select Province --</option>
        </select>
      </div>

      <br>

      <div>
        <select name="district" ng-model="district" id="select-district" class="form-control" ng-change="updateLlgs()" ng-options="district.name for district in districts" ng-disabled="!province">
          <option value="">-- Select District --</option>
        </select>
      </div>

      <br>

      <div>
          <select name="llg" ng-model="llg" id="select-llg" class="form-control" ng-options="llg.name for llg in llgs" ng-disabled="!district">
            <option value="">-- Select LLG --</option>
          </select>
      </div>

      <br>

      <a href="#" class="btn btn-primary col-md-12" ng-click="searchApprovalByFilter()">SEARCH</a>

      <br>
    </div>

    <div class="projects-list col-xs-12 col-md-9">
      <p class="col-md-12 bg-section">LLG Appraisal Committee Subproject Assessment and Approval Forms</p>
      <table class="table">
        <thead>
          <tr>
            <th>Province</th>
            <th>District</th>
            <th>LLG</th>
            <th>Meeting Date</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr class="list-item" ng-repeat="approval in pageApprovals">
            <td><% approval.p_name %></td>
            <td><% approval.d_name %></td>
            <td><% approval.l_name %></td>
            <td><% approval.meeting_date | date : 'dd/MM/yyyy' %></td>
            <td><a href="{!! asset('/approval') !!}/<% approval.id %>">View</a></td>
          </tr>
        </tbody>
      </table>

    </div><!-- .project-list -->
    
    <div class="row">
      <div class="col-md-3 col-md-offset-3" style="margin-top: 8px;">
        <span ng-bind="startItemIndex + 1"></span>
        <span > - </span>
        <span ng-bind="endItemIndex"></span>
        <span> of </span>
        <span ng-bind="totalItems"></span>
        <span> Approval forms </span>
      </div>

      <pagination class="pull-right" boundary-links="true" ng-change="pageChanged()" total-items="totalItems" items-per-page="itemsPerPage" max-size="maxSize" ng-model="currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination> 
    </div>
    
  </div>

  <div>
    <br>
  </div>

  <div class="row">
    <hr>
  </div>
@stop