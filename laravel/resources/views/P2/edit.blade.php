@extends('master.main')

@section('css_block')
<style>
  #wrong-image-label{
    display: none;
  }
  #file_load_gif{
    display: none;
  }
  #ui-datepicker-div
  {
    display: none;
  }
  ul.ui-autocomplete.ui-menu{
    width:50px;
  }
  .row{
    margin: 0px;
    padding: 0px 15px;
  }
  @media screen and (min-width: 992px) {
    .dollar-sign{
      position: relative;
      top: 7px;
      left: 10px;
    }
  }
</style>
@stop

@section('js_block')
{!! Html::script('js/angular.min.js') !!}
{!! Html::script('js/angular-route.min.js') !!}
{!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
{!! HTML::script('js/ng-file-upload.min.js') !!}
{!! HTML::script('js/attach-uploader.js') !!}
{!! Html::script('js/P2.js') !!}
<script>
  window.init_data = {
    'provinces': {!! $provinces !!},
    'locations': {!! old() ? json_encode([old('province_id'), old('district_id'), old('llg_id'), old('ward_id'), old('village_id'), old('prioritisation_id')]) : json_encode($locations) !!},
    'approval': {!! old() ? json_encode(old('approval')) : $approval !!},
    'counterpart_fundings': {!! old('counterpart_fundings') ? json_encode(old('counterpart_fundings')) : json_encode($approval->counterpart_funds) !!},
    'attachments' : {!! old('attachments') ? json_encode(old('attachments')) : $approval->attachments !!},
  };
</script>
@stop

@section('content')
<div ng-app="P2App" ng-controller="P2EditCtrl">
  <div class="row">
    <h3 class="col-md-12">
      LLG Appraisal Committee Subproject Assessment and Approval
    </h3>
  </div>

  @if($errors->has())
    <div class="row">
      <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
          {{ $error }}<br>
        @endforeach
      </div>
    </div>
  @endif

  <div class="row">
    <p class="col-md-12 bg-section">General Information</p>
  </div>

  {!! Form::open(['url' => asset('/approval/'.$approval->id), 'method' => 'PUT']) !!}
    <div class="content">

      <div class="row">
        <label class="col-md-3 control-label col-xs-5">Ward Selection Method:
        </label>
        <div class="col-md-3 col-xs-7">
          <select name="selection_method" id="" class="form-control">
            @foreach($selection_methods as $selection_method)
              <option value="{!! $selection_method->name !!}" ng-selected="'{!! $selection_method->name !!}' == '{!! old('selection_method') ? old('selection_method') : 0 !!}'">{!! $selection_method->name !!}</option>
            @endforeach
          </select>
        </div>
        <div class="row visible-xs-block visible-sm-block">&emsp;</div>
        <label class="col-md-2 col-xs-5">Date of meeting:</label>
        <div class="col-md-4 col-xs-7">
          <div class="input-group" ng-click="openMeetingDatePicker($event)">
            <input type="text"
                   class="form-control"
                   name="approval[meeting_date]"
                   id="meeting_date"
                   ng-required="true"
                   ng-readonly="true"
                   ng-model="approval.meeting_date"
                   datepicker-popup="dd/MM/yyyy"
                   is-open="MeetingDatePickerOpened">
            <div class="input-group-addon"><img src="/images/calendar_icon.png" alt="calendar" ></div>
          </div>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 control-label col-xs-5">Province:
        </label>
        <div class="col-md-4 col-xs-7">
          <select id="province" class="form-control" name="province_id"
                  ng-model="province_id" ng-change="updateDistricts()">
            <option value="">-- Select Province --</option>
              @foreach($provinces as $province)
                <option value="{!! $province->id !!}" ng-selected="province_id == {!! $province->id !!}">{!! $province->name !!}</option>
              @endforeach
          </select>
        </div>
        <div class="row visible-sm-block visible-xs-block">&emsp;</div> <!-- spacer for smaller device -->
        <label class="col-md-2 control-label col-xs-5">District:
        </label>
        <div class="col-md-4 col-xs-7">
          <select id="district" class="form-control" name="district_id"
                  ng-model="district_id" ng-disabled="!province_id" ng-change="updateLlgs()">
            <option value="">-- Select District --</option>
            <option ng-repeat="district in districts" value="<% district.id %>" ng-selected="district_id == district.id"><% district.name %></option>
          </select>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-2 control-label col-xs-5">LLG:
        </label>
        <div class="col-md-4 col-xs-7">
          <select id="llg" class="form-control" name="llg_id"
                  ng-model="llg_id" ng-disabled="!district_id" ng-change="updateWards()">
            <option value="">-- Select LLG --</option>
            <option ng-repeat="llg in llgs" value="<% llg.id %>" ng-selected="llg_id == llg.id"><% llg.name %></option>
          </select>
        </div>
        <div class="row visible-sm-block visible-xs-block">&emsp;</div> <!-- spacer for smaller device -->
        <label class="col-md-2 control-label col-xs-5">Ward:</label>
        <div class="col-md-4 col-xs-7">
          <select name="ward_id" id="ward" class="form-control"
                  ng-model="ward_id" ng-disabled="!llg_id" ng-change="updateVillages()">
            <option selected="selected" value="">-- Select Ward --</option>
            <option ng-repeat="ward in wards" value="<% ward.id %>" ng-selected="ward_id == ward.id"><% ward.name %></option>
          </select>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <p class="col-md-12 bg-section">Assessment</p>
      </div>

      <div class="row">
        <label class="col-md-2 control-label col-xs-5">Village (subproject site):
        </label>
        <div class="col-md-4 col-xs-7">
          <select name="village_id" id="village" class="form-control"
                  ng-model="village_id" ng-disabled="!ward_id">
            <option selected="selected" value="">-- Select Village --</option>
            <option ng-repeat="village in villages" value="<% village.id %>" ng-selected="village_id == village.id"><% village.name %></option>
          </select>
        </div>
        <div class="row visible-sm-block visible-xs-block">&emsp;</div> <!-- spacer for smaller device -->
        <label class="col-md-2 control-label col-xs-5">Select Priority:
        </label>
        <div class="col-md-4 col-xs-7">
          <select name="prioritisation_id" id="district" class="form-control" ng-model="prioritisation_id">
            <option value="">-- Select Priority --</option>
            <option ng-repeat="prioritisation in prioritisations" value="<% prioritisation.id %>"><% prioritisation.prioritisation_needs[0].description %></option>
          </select>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-3 control-label col-xs-5">The appraisal committee of:
        </label>
        <div class="col-md-9 col-xs-7">
          <input type="text" class="form-control" id="" value="<% appraisal_committee %>" disabled>
        </div>
      </div>

      <div><br></div>

      <div class="row">
      <label class="col-md-3 col-xs-5">Subproject name:</label>
      <div class="col-md-9 col-xs-7">
        <input type="text" name="subproject_name" class="form-control" id="" value="{!! old() ? old('subproject_name') : $subproject->name !!}">
      </div>
      </div>

      <div><br></div>

      <div class="row">
        <label for="" class="col-md-3 col-xs-5">Subproject Description:</label>
        <div class="col-md-9 col-xs-7">
          <textarea class="form-control" rows="5" name="subproject_description" placeholder="Description">{!! old() ? old('subproject_description') : $subproject->description !!}</textarea>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label for="" class="col-md-6 col-xs-5">RSDLGP Funding limit:</label>
        <div class="col-md-6 col-xs-7">
          <div class="input-group">
            <input type="number" min=0 step="all" name="approval[rsdlgp_funding_limit]" string-to-number ng-model="approval.rsdlgp_funding_limit" class="form-control">
            <div class="input-group-addon">PGK</div>              
          </div>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-6 col-xs-5">Estimated cost of requested subproject:</label>
        <div class="col-md-6 col-xs-7">
          <div class="input-group">
            <input type="number" min=0 step="all" name="approval[fund_estimate_origin]" string-to-number ng-model="approval.fund_estimate_origin" class="form-control" ng-KeyUp="calculateTotalAmount()">
            <div class="input-group-addon">PGK</div>
          </div>
        </div>
      </div>

      <div>
        <br class="hidden-xs">
      </div>

      <div class="row">
        <strong class="col-md-12">Estimated sources of funds:</strong>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-6 col-xs-5"></label>
        <label class="col-md-4 col-xs-4 text-center">Amount</label>
        <label class="col-md-2 col-xs-3 text-center">% of estimated cost</label>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-6 col-xs-5">RSDPLGP</label>
        <div class="col-md-4 col-xs-4">
          <div class="input-group">
            <input type="number" min=0 step="all" name="approval[rdp_fund][origin_amount]" ng-model="rsdplgp_funding_amount" class="form-control" ng-KeyUp="calculateTotalAmount()">
            <div class="input-group-addon">PGK</div>            
          </div>
        </div>
        <div class="col-md-2 col-xs-3">
          <input type="text" class="form-control" value="<% isFinite(rsdplgp_funding_amount/approval.fund_estimate_origin) ? amountRound(rsdplgp_funding_amount/approval.fund_estimate_origin*100) : 0 %>%" disabled>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <label class="col-md-6 col-xs-5">Community Contribution</label>
        <div class="col-md-4 col-xs-4">
          <div class="input-group">
            <input type="number" min=0 step="all" name="approval[community_fund][origin_amount]" ng-model="community_funding_amount" class="form-control" ng-KeyUp="calculateTotalAmount()">
            <div class="input-group-addon">PGK</div>              
          </div>
        </div>
        <div class="col-md-2 col-xs-3">
          <input type="text" class="form-control" value="<% isFinite(community_funding_amount/approval.fund_estimate_origin) ? amountRound(community_funding_amount/approval.fund_estimate_origin*100) : 0 %>%" disabled>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <strong class="col-xs-12">Other potential sources of counterpart funding:</strong>
      </div>

      <div><br></div>

      <div ng-repeat="counterpart_funding in counterpart_fundings track by $index">
        <div class="row">
          <input type="hidden" name="counterpart_fundings[<% $index %>][id]" value="<% counterpart_funding.id %>">
          <div class="col-md-5 col-md-offset-1 col-xs-4 col-xs-offset-1">
            <input type="text" name="counterpart_fundings[<% $index %>][source]" ng-model="counterpart_funding.source" class="form-control" id=""  placeholder="Source of counterpart funding">
          </div>
          <div class="col-md-4 col-xs-4">
            <div class="input-group">
              <input type="number" min=0 step="all" name="counterpart_fundings[<% $index %>][origin_amount]" ng-model="counterpart_funding.origin_amount" class="form-control" ng-KeyUp="calculateTotalAmount()">
              <div class="input-group-addon">PGK</div>
            </div>
          </div>
          <div class="col-md-2 col-xs-3">
            <input type="text" class="form-control" value="<% isFinite(counterpart_funding.origin_amount/approval.fund_estimate_origin) ? amountRound(counterpart_funding.origin_amount/approval.fund_estimate_origin*100) : 0 %>%" disabled>
          </div>
        </div>

        <div><br></div>

        <div class="row">
          <p class="col-md-9 col-md-offset-1 col-xs-8 col-xs-offset-1 closer-top">Letters of commitment for counterpart funding/co-financing provided:</p>
          <div class="col-md-2 col-xs-3 text-center closer-top">
            <label class="radio-inline">
              <input type="radio" ng-model="counterpart_funding.has_commitment" name="counterpart_fundings[<% $index %>][has_commitment]" ng-value="1">Yes
            </label>
            <label class="radio-inline">
              <input type="radio" ng-model="counterpart_funding.has_commitment" name="counterpart_fundings[<% $index %>][has_commitment]" ng-value="0">No
            </label>
          </div>
        </div>

        <div><br></div>
      </div>

      <div class="row">
        <div class="col-md-2 col-md-offset-1 col-xs-3 col-xs-offset-1">
          <input class="btn btn-default btn-xs" type="button" value="Add Row" ng-click="addCounterpartFunding()">
        </div>
      </div>

      <div class="row">
        <hr>
      </div>

      <div class="row">
        <label class="col-md-6 col-xs-5">Total</label>
        <div class="col-md-4 col-xs-4">
          <div class="input-group">
            <input type="number" min=0 step="all" class="form-control" ng-model="totalAmount" disabled>
            <div class="input-group-addon">PGK</div>              
          </div>
        </div>
        <div class="col-md-2 col-xs-3">
          <input type="text" class="form-control" id="" value="<% isFinite(totalAmountPercent) ? amountRound(totalAmountPercent) : 0 %>%" disabled>
        </div>
      </div>

      <div><br></div>

      <div class="row">
        <p class="col-md-12 bg-section">Attachments:</p>
      </div>

      <div class="row">
        <div ng-repeat="(index, attachment) in attachments">
          <input type="hidden" name="attachments[<% $index %>][id]" value="<% attachment.id %>">
          <input type="hidden" name="attachments[<% $index %>][name]" value="<% attachment.name %>">
          <strong class="col-md-12">
            <a href="{!! asset('/file') !!}/<% attachment.id %>/download"><% attachment.name %></a>&emsp;
            <a href="" ng-click="deleteAttachment(index)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></strong>
        </div>
      </div>

      <filegroup files=""></filegroup>
      <fileuploader></fileuploader>

      <div class="row">
        <hr>
      </div>

      <div class="row">
        <button type="button" class="btn btn-default" onclick="location.href='{!! asset('/approval') !!}'">Cancel</button>
        <input type="submit" id="edit_submit" class="btn btn-default" value="Submit" ng-disabled="attachment_uploader.is_uploading">
      </div>

    </div>
  {!! Form::close() !!}
</div>
@stop