@extends('master.main')

@section('css_block')
<style>
  .attachement-img-wrapper{
    height: 200px;
    text-align: center;
    background-color: #FFF;
    margin: 10px 0px;
    padding: 10px 40px;
    border-radius: 6px;
  }
  .attachement-img-inner-wrapper{
    height: 100%;
    background-color:ghostwhite;
  }
  .attachement-img{
    max-width: 100%;
    height: 100%;
  }
  .row{
    margin: 0px;
    padding: 0px 15px;
    overflow: hidden;
  }
  .report-preview-image-wrapper{
    text-align: center;
    width: 50px;
    height: 35px;
  }
  .report-preview-image{
    max-height: 100%;
    width: 100%;
  }
</style>
@stop

@section('js_block')
{!! Html::script('js/angular.min.js') !!}
{!! Html::script('js/angular-route.min.js') !!}
{!! Html::script('js/ui-bootstrap-tpls.min.js') !!}
{!! Html::script('js/P2.js') !!}
<script>
  window.init_data = {
  };
</script>
@stop

@section('content')
<div ng-app="P2App" ng-controller="P2CreateCtrl">
  <div class="row">
    <h3 class="col-md-10 col-xs-10">
      LLG Appraisal Committee Subproject Assessment and Approval
    </h3>
    <h3 class="col-md-2 col-xs-2">
      <a class="pull-right btn btn-default" href="{!! asset('/approval/'.$approval->id.'/edit') !!}">Edit</a>
    </h3>
  </div>

  <div class="row">
    <p class="col-md-12 col-xs-12 bg-section">General Information</p>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Ward Selection Method:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $approval->selection_method !!}
    </span>
  </div>


  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Date of meeting:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! date_format(date_create($approval->meeting_date), 'd/m/Y') !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Province:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $location->ward ? $location->ward->llg->district->province->name : $location->llg->district->province->name !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      District:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $location->ward ? $location->ward->llg->district->name : $location->llg->district->name !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      LLG:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $location->ward ? $location->ward->llg->name : $location->llg->name!!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Ward:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $location->ward ? $location->ward->name : $location->name !!}
    </span>
  </div>

  <div class="row">
    <p class="col-md-12 bg-section">Assessment</p>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Village (subproject site):
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $location->ward ? $location->name : '' !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Priority:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $priority->prioritisation_needs[0]->description !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      The appraisal committee of:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $location->ward ? $location->ward->name : $location->name !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      Subproject name:
    </strong>
    <span class="col-md-8 col-xs-6">
      {!! $subproject ? $subproject->name : '' !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-12">
      Subproject description:
    </strong>
    <span class="col-md-8 col-xs-12">
      {!! $subproject ? $subproject->description : '' !!}
    </span>
  </div>

  <div class="row">
    <strong class="col-md-4 col-xs-6">
      RSDLGP Funding limit:
    </strong>
    <span class="col-md-8 col-xs-6">{!! number_format($approval->rsdlgp_funding_limit, 2, ".", "") !!} PGK</span>
  </div>

  <div class="row">
    <strong class="col-md-6 col-xs-12">
      Estimated cost of requested subproject:
    </strong>
    <span class="col-md-6 col-xs-12">{!! number_format($approval->fund_estimate_origin, 2, ".", "") !!} PGK</span>
  </div>

  <div class="row">
    <strong class="col-md-12 col-xs-6">
      Estimated sources of funds:
    </strong>
  </div>


  <div class="row">
    <div class="col-md-12 table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th></th>
            <th>Amount (PGK)</th>
            <th>% of total</th>
            <th>Letters of commitment</th>
          </tr>
        </thead>

        <tbody>
          @foreach($approval->funds as $fund)
          <tr>
            <td>{!! $fund->source == "" ? $fund->fund_type : $fund->source !!}</td>
            <td>{!! number_format($fund->origin_amount, 2, ".", "") !!} PGK</td>
            <td>{!! ($fund->origin_amount == 0 or $approval->fund_estimate_origin ==0) ? 0 : round($fund->origin_amount/$approval->fund_estimate_origin*100, 2) !!}%</td>
            @if($fund->source != NULL and $fund->has_commitment == 1)
              <td>yes</td>
            @elseif($fund->source != NULL and $fund->has_commitment == 0)
              <td>no</td>
            @else
              <td>--</td>
            @endif
          </tr>
          @endforeach
          <tr>
            <td>Total</td>
            <td>{!! number_format($total_fund, 2, ".", "") !!} PGK</td>
            <td>{!! ($total_fund == 0 or $approval->fund_estimate_origin == 0) ? 0 : round($total_fund/$approval->fund_estimate_origin*100, 2) !!}%</td>
            <td>--</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div> 

  <div class="row">
    <p class="col-md-12 bg-section">Attachments:</p>
  </div>

  @foreach($approval->attachments as $attachment)
    <div class="row">
      <a class="col-md-12 col-xs-12" href="{!! asset('/file/'.$attachment->id.'/download') !!}">{!! $attachment->name !!}</a>
    </div>
  @endforeach


  <div class="row"><hr></div>

  <div class="row" style="text-align: right;">
    <a class="btn btn-default" href="{!! asset('/approval/'.$approval->id.'/edit') !!}">Edit</a>
  </div>

  <div class="row">
    <hr>
  </div>
</div>
@stop