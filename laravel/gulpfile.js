// var elixir = require('laravel-elixir');
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
gulp.task('default', function(){
	gulp.src(['./resources/assets/bower_components/angular/angular.js', './resources/assets/bower_components/jquery/dist/jquery.js'])
	.pipe(concat('frameworks.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('./public/js'));
});
// elixir(function(mix) {
//     mix.scriptsIn('public/js');
// });
