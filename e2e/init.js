//-- Run spec files with: protractor init.js

exports.config = {
    //-- Selenium web-driver server address
    seleniumAddress: "http://localhost:3232/wd/hub",

    //-- The place to write result JSON file
    resultJsonOutputFile: 'result/'+Date.now()+'_result.json',

    //-- The specs, the test cases
    specs: [
        'specs/home/login.js',
        'specs/T1/UI_create_test/*.js',
        'specs/T1/create.js',
        'specs/T1/UI_edit_test/*.js',
        'specs/T1/edit.js',
        'specs/T1/index.js',
        'specs/T2/UI_create_test/*.js',
        'specs/T2/create.js',
        'specs/T2/UI_edit_test/*.js',
        'specs/T2/edit.js',
        'specs/T2/index.js',
        'specs/T4/UI_create_test/*.js',
        'specs/T4/create.js',
        'specs/T4/UI_edit_test/*.js',
        'specs/T4/edit.js',
        'specs/T4/index.js',
        'specs/T6/UI_create_test/*.js',
        'specs/T6/create.js',
        'specs/T6/UI_edit_test/*.js',
        'specs/T6/edit.js',
        'specs/T6/index.js',
        // 'specs/T7/create_UI_and_submit_test.js',
        // 'specs/T7/create_cancel_test.js',
        // 'specs/T7/edit_UI_and_submit_test.js',
        // 'specs/T7/edit_cancel_test.js',
        // 'specs/T7/show.js',
        // 'specs/T8/create_UI_and_submit_test.js',
        // 'specs/T8/create_cancel_test.js',
        // 'specs/T8/edit_UI_and_submit_test.js',
        // 'specs/T8/edit_cancel_test.js',
        // 'specs/T8/show.js',
    ],

    //-- The url to run specs
    baseUrl: 'http://192.168.3.178',
    //-- Developer url to run specs
    // baseUrl: 'http://argo.lizhe.tpt',

    rootElement: "[ng-app]",

    multiCapabilities: [
        //-- There are compatibility issue on teamcity agent.
        // { 'browserName': 'firefox' },
        { 'browserName': 'chrome' }
    ]
}
