describe('Add Proposal', function(){

    beforeEach(function(){
        browser.ignoreSynchronization = false;
        browser.get('/ranking/create');
    });

    var add_proposal_btn = $('a[ng-click="addProposal()"]');

    it('Normal Flow : Add proposal -> Delete proposal', function() {
        var EXPECT_NEEDS_COUNT = 1;
        expect( add_proposal_btn.isPresent() ).toBe(true);

        add_proposal_btn.click();
        var proposals = element.all(by.repeater('(index, proposal) in proposals'));
        expect( proposals.count() ).toEqual(EXPECT_NEEDS_COUNT);

        proposals.each(function(proposal, index) {
            expect( proposal.$('select[ng-model="proposal.vid"]').isPresent() ).toBe(true);
            expect( proposal.$('input[ng-model="proposal.name"]').isPresent() ).toBe(true);
            expect( proposal.$('input[ng-model="proposal.ranking"]').getAttribute('value') ).toEqual(index + 1 + '');
            proposal.$('button[ng-click="delProposal(index)"]').click();
        });

        expect( proposals.count() ).toEqual(0);
    });
});