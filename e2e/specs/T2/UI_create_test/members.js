describe('Add Members', function(){

    beforeEach(function(){
        browser.ignoreSynchronization = false;
        browser.get('/ranking/create');
    });

    var add_member_btn = $('a[ng-click="addMember()"]');

    it('Normal Flow : Add member -> Delete member', function() {
        var EXPECT_NEEDS_COUNT = 1;
        expect( add_member_btn.isPresent() ).toBe(true);

        add_member_btn.click();
        var members = element.all(by.repeater('(index, member) in members'));
        expect( members.count() ).toEqual(EXPECT_NEEDS_COUNT);

        members.each(function(member, index) {
            expect( member.$('input[ng-model="member.name"]').isPresent() ).toBe(true);
            expect( member.$$('input[ng-model="member.gender"]').count() ).toEqual(2);
            member.$('button[ng-click="delMember(index)"]').click();
        });

        expect( members.count() ).toEqual(0);
    });
});