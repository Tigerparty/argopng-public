describe('T2 index Page', function() {
	it('Click T2 Index Tab', function() {
		browser.sleep(3000);

		$('li[class="dropdown"] a').click().then(function() {
			$('li[id="ward-tab"] a').click();
		});

		browser.sleep(3000);
		var T2_list = element.all(by.repeater('(key, ranking) in rankings'));
		expect( T2_list.count() ).toBeGreaterThan(0);
	});

	var T2_list = element.all(by.repeater('(key, ranking) in rankings'));
	var keyword = $('input[ng-model="search_keyword"]');
	var prov_slt = $('select[ng-model="province"]');
	var ward_slt = $('select[ng-model="ward"]');

	describe('Display search keyword', function() {
		beforeEach( function() {
			browser.ignoreSynchronization = false;
			browser.navigate().refresh();
		});

		it('Display search by province name for not exist T2', function () {
			keyword.sendKeys('not exist');
			expect( T2_list.count() ).toEqual(0);
		});

		it('Display search by province name for exist T2', function () {
			keyword.sendKeys('Central');
			expect( T2_list.count() ).toBeGreaterThan(0);
		});

		it('Display search by ward name for exist T2', function () {
			keyword.sendKeys('Banika');
			expect( T2_list.count() ).toBeGreaterThan(0);
		});
	});

	describe('Display drop down list: province', function() {
		beforeEach( function() {
			browser.ignoreSynchronization = false;
			browser.navigate().refresh();
		});

		it('Display province selector for not exist T2', function() {
			prov_slt.$('option:nth-child(3)').click().then(function() {
				expect( T2_list.count() ).toEqual(0);
			});
		});

		it('Display province selector for exist T2', function() {
			prov_slt.$('option:nth-child(2)').click().then(function() {
				expect( T2_list.count() ).toBeGreaterThan(0);
			});
		});
	});

	describe('Display drop down list: province -> ward', function() {
		beforeEach( function() {
			browser.ignoreSynchronization = false;
			browser.navigate().refresh();
			prov_slt.$('option:nth-child(2)').click();
		});

		it('Display province selector for not exist T2', function() {
			ward_slt.$('option:nth-child(3)').click().then(function() {
				expect( T2_list.count() ).toEqual(0);
			});
		});

		it('Display province selector for exist T2', function() {
			ward_slt.$('option:nth-child(2)').click().then(function() {
				expect( T2_list.count() ).toBeGreaterThan(0);
			});
		});
	});
});


