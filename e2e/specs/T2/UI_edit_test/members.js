
describe('Edit Page', function() {
    it('Go to edit page', function() {
        browser.getCurrentUrl().then(function(url) {
            browser.ignoreSynchronization = false;
            browser.get(url + '/edit');
        });
    });

    describe('Add Members', function(){

        beforeEach(function(){
            browser.navigate().refresh();
        });

        var add_member_btn = $('a[ng-click="addMember()"]');


        it('Normal Flow :  Delete member -> Add member', function() {
            var EXPECT_NEEDS_COUNT = 1;

            var members = element.all(by.repeater('(index, member) in members'));

            members.each(function(member, index) {

                expect( member.$('input[ng-model="member.name"]').isPresent() ).toBe(true);
                expect( member.$$('input[ng-model="member.gender"]').count() ).toEqual(2);
                var delete_member = member.$('button[ng-click="delMember(index)"]');

                delete_member.click().then(function() {
                    expect( members.count() ).toEqual(0);
                });
            });

            expect( add_member_btn.isPresent() ).toBe(true);
            add_member_btn.click().then(function() {
                expect( members.count() ).toEqual(EXPECT_NEEDS_COUNT);
            });

        });
    });
});
