describe('Province->Ward->Add proposal->Village', function(){

    beforeEach(function(){
        browser.ignoreSynchronization = false;
        browser.navigate().refresh();
        browser.executeScript('window.scrollTo(0, 333);');
    });

    var prov_slt = element(by.name('provinceId'));
    var ward_slt = element(by.name('wardId'));
    var add_proposal_btn = $('a[ng-click="addProposal()"]');

    it('Normal Flow : P->W->Add Proposal->V', function(){
        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(true);
        add_proposal_btn.click();
        var proposals = element.all(by.repeater('(index, proposal) in proposals'));
        proposals.each(function(proposal, index) {
            expect( proposal.$('select[name="proposalVid[]"]').isEnabled() ).toBe(true);
        });

        prov_slt.$('option:nth-child(2)').click();
        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(true);
        proposals.each(function(proposal, index) {
            expect( proposal.$('select[name="proposalVid[]"]').isEnabled() ).toBe(true);
        });

        ward_slt.$('option:nth-child(2)').click();
        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(true);
        proposals.each(function(proposal, index) {
            expect( proposal.$('select[name="proposalVid[]"]').isEnabled() ).toBe(true);
        });
    });

    it('Abnormal Flow : P->W->Add Proposal->V->P', function(){
        prov_slt.$('option:nth-child(2)').click();
        ward_slt.$('option:nth-child(2)').click();
        add_proposal_btn.click();
        var proposals = element.all(by.repeater('(index, proposal) in proposals'));
        proposals.each(function(proposal, index) {
            proposal.$('select[name="proposalVid[]"]').$('option:nth-child(2)').click();
        });
        prov_slt.$('option:nth-child(1)').click();

        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(false);
        proposals.each(function(proposal, index) {
            expect( proposal.$('select[name="proposalVid[]"]').isEnabled() ).toBe(false);
        });
    });

    it('Abnormal Flow : P->W->Add Proposal->V->W', function(){
        prov_slt.$('option:nth-child(2)').click();
        ward_slt.$('option:nth-child(2)').click();
        add_proposal_btn.click();
        var proposals = element.all(by.repeater('(index, proposal) in proposals'));
        proposals.each(function(proposal, index) {
            proposal.$('select[name="proposalVid[]"]').$('option:nth-child(2)').click();
        });
        ward_slt.$('option:nth-child(1)').click();

        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(true);
        proposals.each(function(proposal, index) {
            expect( proposal.$('select[name="proposalVid[]"]').isEnabled() ).toBe(false);
        });
    });
});