
describe('Create T2', function() {
	beforeEach(function(){
		browser.ignoreSynchronization = false;
		browser.get('/ranking/create');
	});

	var meeting_date = element(by.name('meetingDate')).element(by.xpath('..'));
	var cycle_slt = element(by.name('cycleId'));
	var prov_slt = element(by.name('provinceId'));
	var ward_slt = element(by.name('wardId'));
	var submit = $('input[type="submit"]');
	var add_proposal_btn = element(by.xpath('//a[@ng-click="addProposal()"]'));
	var add_member_btn = element(by.xpath('//a[@ng-click="addMember()"]'));

	it('Press data for all', function() {
		meeting_date.$('img').click().then(function() {
			meeting_date.element(by.buttonText('Today')).click();
		});

		cycle_slt.$('option:nth-child(2)').click();
		prov_slt.$('option:nth-child(2)').click();
		ward_slt.$('option:nth-child(2)').click();

		//-- Add proposal
		add_proposal_btn.click().then(function() {
			var proposal = element(by.xpath('//div[@proposal-row]/div[1]'));
			var vill_slt = proposal.element(by.name('proposalVid[]'));
			var sp_name = proposal.element(by.name('proposalName[]'));

			vill_slt.$('option:nth-child(2)').click();
			sp_name.sendKeys('E2E Create');
		});

		add_member_btn.click().then(function() {
			var member = element(by.repeater('(index, member) in members'));
			var name = member.$('input[ng-model="member.name"]');

			name.sendKeys('E2E Create');
		});

		submit.click().then(function() {
			browser.ignoreSynchronization = true;
			browser.sleep(3000);
			expect( browser.getCurrentUrl() ).not.toContain('create');
		});
	});
});