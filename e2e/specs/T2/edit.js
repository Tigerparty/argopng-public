describe('Edit Page', function() {
	var edit_url = ""

	it('Set Edit URL', function() {
		browser.getCurrentUrl().then(function(url) {
			edit_url = url;
		});
	});


	describe('Edit T2 Test', function() {
		beforeEach(function(){
			browser.get(edit_url)
			browser.ignoreSynchronization = false;
		});

		var meeting_date = element(by.name('meetingDate')).element(by.xpath('..'));
		var cycle_slt = element(by.name('cycleId'));
		var prov_slt = element(by.name('provinceId'));
		var ward_slt = element(by.name('wardId'));
		var sumbit = $('input[type="submit"]');
		var proposals = element.all(by.repeater('(index, proposal) in proposals'))
		var members = element.all(by.repeater('(index, member) in members'))
		var add_proposal_btn = element(by.xpath('//a[@ng-click="addProposal()"]'));
		var add_member_btn = element(by.xpath('//a[@ng-click="addMember()"]'));

		it('Check edit data', function() {
			expect( meeting_date.getAttribute('value') ).not.toEqual('');
			expect( cycle_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( prov_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( ward_slt.getAttribute('value') ).toBeGreaterThan(0);
		});

		it('Edit data for all', function() {
			meeting_date.$('img').click().then(function() {
				meeting_date.element(by.buttonText('Today')).click();
			});

			cycle_slt.$('option:nth-child(2)').click();
			prov_slt.$('option:nth-child(2)').click();
			ward_slt.$('option:nth-child(2)').click();

			proposals.each(function(proposal, index) {
				var village_slt = proposal.$('select[ng-model="proposal.vid"]');
				var sp_name = proposal.$('input[ng-model="proposal.name"]');

				expect( village_slt.getAttribute('value') ).toBeGreaterThan(0);
				expect( sp_name.getAttribute('value') ).toEqual('E2E Create');
			});

			members.each(function(member, index) {
				var name = member.$('input[ng-model="member.name"]');

				expect( name.getAttribute('value') ).toEqual('E2E Create');
			});

			sumbit.click().then(function() {
				browser.ignoreSynchronization = true;
				browser.sleep(3000);
				expect( browser.getCurrentUrl() ).not.toContain('edit');
			});
		});
	});
});


