describe('Create T6 form', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.get('/subproject/create');
	})

	var name          = element(by.name('subproject[name]'));
	var latitude      = element(by.name('subproject[latitude]'));
	var longitude     = element(by.name('subproject[longitude]'));
	var province_slt  = element(by.name('subproject[province]'));
	var ward_slt      = element(by.name('subproject[ward]'));
	var village_slt   = element(by.name('subproject[village]'));
	var cycle_slt     = element(by.name('subproject[cycle]'));
	var nature        = element(by.name('subproject[nature]'));
	var objective     = element(by.name('subproject[objective]'));
	var description   = element(by.name('subproject[description]'));
	var approved_date = element(by.name('subproject[province_approved_date]')).element(by.xpath('..'));
	var tdf_date      = element(by.name('subproject[tdf_approved_date]')).element(by.xpath('..'));
	var helper_name   = element(by.name('subproject[community_helper_name]'));
	var sector_slt    = element(by.name('subproject[sector]'));
	var subsector_slt = element(by.name('subproject[subsector]'));
	var type_slt      = element(by.name('subproject[type]'));
	var sic_date      = element(by.name('subproject[sic_establish_date]')).element(by.xpath('..'));
	var sic_name      = element(by.name('subproject[sic_agreed_name]'));
	var sic_members   = element.all(by.repeater('sic in subproject.sics track by $index'));
	var bnf_vill_slt  = element(by.xpath('//select[@ng-model="benefitVillage"]'));
	var add_stage_btn = element(by.xpath('//a[@ng-click="addWorkStage()"]'));
	var add_pcm_btn   = element(by.xpath('//a[@ng-click="addProcurement()"]'));
	var rdp           = element(by.name('subproject[contributions][0][source_name]')).element(by.xpath('..'));
    var community     = element(by.name('subproject[contributions][1][source_name]')).element(by.xpath('..'));
	var add_ctrb_btn  = element(by.xpath('//a[@ng-click="addContribution()"]'));
	var sumbit 		  = $('input[type="submit"]');
	var cancel 	      = element(by.buttonText('Cancel'));

	it('Cancel create T6 form', function() {
		cancel.click().then(function() {
			browser.ignoreSynchronization = true;
			browser.sleep(3000);
			expect( browser.getCurrentUrl() ).not.toContain('create');
		});
	});

	it('Press data for all', function() {
		name.sendKeys('E2E Create');
		province_slt.$('option:nth-child(2)').click();
		ward_slt.$('option:nth-child(2)').click();
		village_slt.$('option:nth-child(2)').click();

		latitude.getAttribute("value").then(function(lat) {
			if(!(/[0-9]+\.*[0-9]*/.test(lat))) {
				latitude.sendKeys(123);
			}
		});

		longitude.getAttribute("value").then(function(lng) {
			if(!(/[0-9]+\.*[0-9]*/.test(lng))) {
				longitude.sendKeys(22);
			}
		});

		cycle_slt.$('option:nth-child(2)').click();
		nature.$('option:nth-child(2)').click();
		objective.sendKeys('E2E Create');
		description.sendKeys('E2E Create');

		approved_date.$('img').click().then(function() {
			approved_date.element(by.buttonText('Today')).click();
		});

		tdf_date.$('img').click().then(function() {
			tdf_date.element(by.buttonText('Today')).click();
		});

		helper_name.sendKeys('E2E Create');
		sector_slt.$('option:nth-child(2)').click();
		subsector_slt.$('option:nth-child(2)').click();
		type_slt.$('option:nth-child(2)').click();

		sic_date.$('img').click().then(function() {
			sic_date.element(by.buttonText('Today')).click();
		});

		sic_name.sendKeys('E2E Create');

		sic_members.each(function(sic_member, index) {
			sic_member.$('input[ng-model="sic.name"]').sendKeys('E2E Create' + index );
			sic_member.$('select[ng-model="sic.gender"]').$('option:nth-child(2)').click();
		});

		bnf_vill_slt.$('option:nth-child(2)').click();

		add_stage_btn.click().then(function() {
			var first_stg = element(by.repeater('(stg_idx, workstage) in subproject.workstages'));
			var add_act_btn = first_stg.$('a[ng-click="addWorkActivity(stg_idx)"]');
			add_act_btn.click().then(function() {
				var first_act = first_stg.element(by.repeater('(act_idx, activity) in workstage.workactivities'));
				var description = first_act.$('input[ng-model="activity.description"]');
				description.sendKeys('E2E Create');
			});
		});

		add_pcm_btn.click().then(function() {
			var first_pcm = element(by.repeater('(p_idx, procurement) in subproject.procurements'));
			var title = first_pcm.$('input[ng-model="procurement.contract_title"]');
			var contractor = first_pcm.$('input[ng-model="procurement.contractor"]');
			var type = first_pcm.$('select[ng-model="procurement.type"]');
			var cost = first_pcm.$('input[ng-model="procurement.cost"]');

			title.sendKeys('E2E Create');
			contractor.sendKeys('E2E Create');
			type.$('option:nth-child(2)').click();
			cost.sendKeys('10000');
		});

		rdp.$('input[ng-model="RDP.amount"]').sendKeys('10000');
		rdp.$('input[ng-model="RDP.description"]').sendKeys('E2E Create');
		community.$('input[ng-model="Community.amount"]').sendKeys('10000');
		community.$('input[ng-model="Community.description"]').sendKeys('E2E Create');

		add_ctrb_btn.click().then(function() {
			var first_ctrb = element(by.repeater('(c_idx, contribution) in contributions'));
			var source = first_ctrb.$('select[ng-model="contribution.source_name"]');
			var amount = first_ctrb.$('input[ng-model="contribution.amount"]');
			var description = first_ctrb.$('input[ng-model="contribution.description"]');

			source.$('option:nth-child(2)').click();
			amount.sendKeys('10000');
			description.sendKeys('E2E Create');
		});

		sumbit.click().then(function() {
			browser.ignoreSynchronization = true;
			browser.sleep(3000);
			expect( browser.getCurrentUrl() ).not.toContain('create');
		});
	});
});


