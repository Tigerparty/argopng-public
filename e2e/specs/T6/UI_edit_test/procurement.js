

describe('Add Procurement', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.navigate().refresh();
		browser.executeScript('window.scrollTo(0, 2468);');
	});

	var add_procurement_btn = element(by.xpath('//a[@ng-click="addProcurement()"]'));

	it('Normal Flow : Add Pcm', function() {
		var EXPECT_PCM_COUNT = 2;

		expect( add_procurement_btn.isPresent() ).toBe(true);

		add_procurement_btn.click();
		var procurements = element.all(by.repeater('(p_idx, procurement) in subproject.procurements'));
		expect( procurements.count() ).toEqual(EXPECT_PCM_COUNT);
		procurements.then(function(procurements) {
			var index = EXPECT_PCM_COUNT - 1;

			var first_pcm = procurements[index].$$('div').get(0).$$('div');
			var title = first_pcm.get(0).$('input');
			var contractor = first_pcm.get(1).$('input');
			var type = first_pcm.get(2).$('select');
			var cost = first_pcm.get(3).$('input');
			var del = procurements[index].$$('div').get(0).$('a');

			expect( title.getAttribute('name') ).toEqual('subproject[procurements][' + index + '][contract_title]');
			expect( contractor.getAttribute('name') ).toEqual('subproject[procurements][' + index + '][contractor]');
			expect( type.getAttribute('name') ).toEqual('subproject[procurements][' + index + '][type]');
			expect( type.$$('option').count() ).toEqual(4);
			expect( cost.getAttribute('name') ).toEqual('subproject[procurements][' + index + '][cost]');
			expect( del.isPresent() ).toBe(true);

			del.click();
		});
		expect( procurements.count() ).toEqual(EXPECT_PCM_COUNT - 1);
	});
});