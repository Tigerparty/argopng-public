describe('Add Contribution', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.navigate().refresh();
		browser.executeScript('window.scrollTo(0, 2475);');
	});

	var add_ctrb_btn = element(by.xpath('//a[@ng-click="addContribution()"]'));

	it('Normal Flow : Add contribution -> Delete contribution', function() {
		var EXPECT_CTRB_COUNT = 2;

		expect( add_ctrb_btn.isPresent() ).toBe(true);
		add_ctrb_btn.click();

		var contributions = element.all(by.repeater('(c_idx, contribution) in contributions'));
		expect( contributions.count() ).toEqual(EXPECT_CTRB_COUNT);
		contributions.then(function(contributions) {
			var index = EXPECT_CTRB_COUNT - 1;
			var ctbr_childs = contributions[index].$$('div').get(0).$$('div');
			var source = ctbr_childs.get(0).$('select').$$('option');
			var amount = ctbr_childs.get(1).$('input');
			var description = ctbr_childs.get(2).$('input');
			var del = contributions[index].$$('div').get(0).$('a[ng-click="deleteContribution(c_idx)"]');

			expect( source.count() ).toEqual(5);
			expect( amount.getAttribute('name') ).toEqual('subproject[contributions][' + (index+2) + '][amount]');
			expect( description.getAttribute('name') ).toEqual('subproject[contributions][' + (index+2) + '][description]');
			expect( del.isPresent() ).toBe(true);

			del.click();
		});
		expect( contributions.count() ).toEqual(EXPECT_CTRB_COUNT - 1);

	});
});