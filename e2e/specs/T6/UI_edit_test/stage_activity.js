
describe('Add Stage -> Add Activity', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.navigate().refresh();
		browser.executeScript('window.scrollTo(0, 2442);');
	});

	var add_stage_btn = element(by.xpath('//a[@ng-click="addWorkStage()"]'));

	it('Normal Flow : Add Stg -> Add Act', function() {
		var EXPECT_STAGE_COUNT = 2;

		expect( add_stage_btn.isPresent() ).toBe(true);
		add_stage_btn.click();
		var stages = element.all(by.repeater('(stg_idx, workstage) in subproject.workstages'));
		expect( stages.count() ).toEqual(EXPECT_STAGE_COUNT);

		stages.then(function(stages) {
			var index = EXPECT_STAGE_COUNT - 1;
			var add_act_btn = stages[index].$('div[class="panel-body"] a');
			var del_stg_btn = stages[index].$('div[class="panel-heading"] h3 button');

			add_act_btn.click().then(function() {
				activities = element.all(by.repeater('(act_idx, activity) in workstage.workactivities'));
				expect( activities.count() ).toEqual(EXPECT_STAGE_COUNT);
				activities.then(function(activities) {
					var first_act = activities[0].$('div[class="activity-row"]');
					var description = first_act.$('div').$('input');
					var completed = first_act.$('input[type="hidden"]');
					var del_act_btn = first_act.$('a');

					expect( description.getAttribute('name') ).toEqual('subproject[workstages][0][workactivities][0][description]');
					expect( completed.getAttribute('value') ).toEqual('0');

					del_act_btn.click();
				});
				expect( activities.count() ).toEqual(EXPECT_STAGE_COUNT - 1);
			});
			del_stg_btn.click();
		});
		expect( stages.count() ).toEqual(EXPECT_STAGE_COUNT - 1);
	});
});