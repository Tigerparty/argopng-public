
describe('Edit Page', function() {
	var edit_url = "";

	it('Set Edit URL', function() {
		browser.getCurrentUrl().then(function(url) {
			edit_url = url;
		});
	});

	describe('Edit T6 Test', function() {
		beforeEach(function(){
			browser.get(edit_url);
			browser.ignoreSynchronization = false;
		});

		var name          = element(by.name('subproject[name]'));
		var latitude      = element(by.name('subproject[latitude]'));
		var longitude     = element(by.name('subproject[longitude]'));
		var province_slt  = element(by.name('subproject[province]'));
		var ward_slt      = element(by.name('subproject[ward]'));
		var village_slt   = element(by.name('subproject[village]'));
		var cycle_slt     = element(by.name('subproject[cycle]'));
		var nature        = element(by.name('subproject[nature]'));
		var objective     = element(by.name('subproject[objective]'));
		var description   = element(by.name('subproject[description]'));
		var approved_date = element(by.name('subproject[province_approved_date]')).element(by.xpath('..'));
		var tdf_date      = element(by.name('subproject[tdf_approved_date]')).element(by.xpath('..'));
		var helper_name   = element(by.name('subproject[community_helper_name]'));
		var sector_slt    = element(by.name('subproject[sector]'));
		var subsector_slt = element(by.name('subproject[subsector]'));
		var type_slt      = element(by.name('subproject[type]'));
		var sic_date      = element(by.name('subproject[sic_establish_date]')).element(by.xpath('..'));
		var sic_name      = element(by.name('subproject[sic_agreed_name]'));
		var sic_members   = element.all(by.repeater('sic in subproject.sics track by $index'));
		var bnf_villages  = element.all(by.repeater('benefitVillageTable in benefitVillageTables track by $index'));
		var bnf_vill_slt  = element(by.xpath('//select[@ng-model="benefitVillage"]'));
		var stages 		  = element.all(by.repeater('(stg_idx, workstage) in subproject.workstages'));
		var add_stage_btn = element(by.xpath('//a[@ng-click="addWorkStage()"]'));
		var procurements  = element.all(by.repeater('(p_idx, procurement) in subproject.procurements'));
		var add_pcm_btn   = element(by.xpath('//a[@ng-click="addProcurement()"]'));
		var rdp           = element(by.name('subproject[contributions][0][source_name]')).element(by.xpath('..'));
	    var community     = element(by.name('subproject[contributions][1][source_name]')).element(by.xpath('..'));
		var add_ctrb_btn  = element(by.xpath('//a[@ng-click="addContribution()"]'));
		var sumbit 		  = $('input[type="submit"]');
		var cancel 	      = element(by.buttonText('Cancel'));

		it('Check edit data', function() {
			expect( name.getAttribute('value') ).toEqual('E2E Create');
			expect( latitude.getAttribute('value') ).not.toEqual('');
			expect( longitude.getAttribute('value') ).not.toEqual('');
			expect( province_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( ward_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( village_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( cycle_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( nature.getAttribute('value') ).toEqual('new_renovation');
			expect( objective.getAttribute('value') ).toEqual('E2E Create');
			expect( description.getAttribute('value') ).toEqual('E2E Create');
			expect( approved_date.getAttribute('value') ).not.toEqual('');
			expect( tdf_date.getAttribute('value') ).not.toEqual('');
			expect( helper_name.getAttribute('value') ).toEqual('E2E Create');
			expect( sector_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( subsector_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( type_slt.getAttribute('value') ).toBeGreaterThan(0);
			expect( sic_date.getAttribute('value') ).not.toEqual('');
			expect( sic_name.getAttribute('value') ).toEqual('E2E Create');

			sic_members.each(function(sic_member, index) {
				expect( sic_member.$('input[ng-model="sic.name"]').getAttribute('value') ).toEqual('E2E Create' + index);
				expect( sic_member.$('select[ng-model="sic.gender"]').getAttribute('value') ).toEqual('Male');
			});

			expect( bnf_villages.count() ).toEqual(1);
			expect( stages.count() ).toEqual(1);

			stages.then(function(stages) {
				var activities = stages[0].$$('div[ng-repeat="(act_idx, activity) in workstage.workactivities"]');
				expect( activities.count() ).toEqual(1);
				activities.then(function(activities) {
					var describe = activities[0].$('input[ng-model="activity.description"]');
					expect( describe.getAttribute('value') ).toEqual('E2E Create');
				});
			});

			expect( procurements.count() ).toEqual(1);

			procurements.then(function(procurements) {
				var title = procurements[0].$('input[ng-model="procurement.contract_title"]');
				var contractor = procurements[0].$('input[ng-model="procurement.contractor"]');
				var type = procurements[0].$('select[ng-model="procurement.type"]');
				var cost = procurements[0].$('input[ng-model="procurement.cost"]');

				expect( title.getAttribute('value') ).toEqual('E2E Create');
				expect( contractor.getAttribute('value') ).toEqual('E2E Create');
				expect( type.getAttribute('value') ).toEqual('goods');
				expect( cost.getAttribute('value') ).toBeGreaterThan(0);
			});

			expect( rdp.$('input[ng-model="RDP.amount"]').getAttribute('value') ).toBeGreaterThan(0);
			expect( rdp.$('input[ng-model="RDP.description"]').getAttribute('value') ).toEqual('E2E Create');
			expect( community.$('input[ng-model="Community.amount"]').getAttribute('value') ).toBeGreaterThan(0);
			expect( community.$('input[ng-model="Community.description"]').getAttribute('value') ).toEqual('E2E Create');
		});

		it('Edit data for all', function() {
			name.sendKeys(' Edit');
			province_slt.$('option:nth-child(3)').click();
			ward_slt.$('option:nth-child(3)').click();
			village_slt.$('option:nth-child(3)').click();

			latitude.getAttribute("value").then(function(lat) {
				if(!(/[0-9]+\.*[0-9]*/.test(lat))) {
					latitude.sendKeys(123);
				}
			});

			longitude.getAttribute("value").then(function(lng) {
				if(!(/[0-9]+\.*[0-9]*/.test(lng))) {
					longitude.sendKeys(22);
				}
			});

			cycle_slt.$('option:nth-child(3)').click();
			nature.$('option:nth-child(3)').click();
			objective.sendKeys(' Edit');
			description.sendKeys(' Edit');

			approved_date.$('img').click().then(function() {
				approved_date.element(by.buttonText('Today')).click();
			});

			tdf_date.$('img').click().then(function() {
				tdf_date.element(by.buttonText('Today')).click();
			});

			helper_name.sendKeys(' Edit');
			sector_slt.$('option:nth-child(3)').click();
			subsector_slt.$('option:nth-child(3)').click();
			type_slt.$('option:nth-child(3)').click();

			sic_date.$('img').click().then(function() {
				sic_date.element(by.buttonText('Today')).click();
			});

			sic_name.sendKeys(' Edit');

			sic_members.each(function(sic_member, index) {
				sic_member.$('input[ng-model="sic.name"]').sendKeys(' Edit' + index );
				sic_member.$('select[ng-model="sic.gender"]').$('option:nth-child(3)').click();
			});

			var first_stg = element(by.repeater('(stg_idx, workstage) in subproject.workstages'));
			var add_act_btn = first_stg.$('a[ng-click="addWorkActivity(stg_idx)"]');
			var first_act = first_stg.element(by.repeater('(act_idx, activity) in workstage.workactivities'));
			var act_dscpt = first_act.$('input[ng-model="activity.description"]');
			act_dscpt.sendKeys(' Edit');

			var first_pcm = element(by.repeater('(p_idx, procurement) in subproject.procurements'));
			var pcm_title = first_pcm.$('input[ng-model="procurement.contract_title"]');
			var pcm_contractor = first_pcm.$('input[ng-model="procurement.contractor"]');
			var pcm_type = first_pcm.$('select[ng-model="procurement.type"]');
			var pcm_cost = first_pcm.$('input[ng-model="procurement.cost"]');

			pcm_title.sendKeys('E2E Edit');
			pcm_contractor.sendKeys('E2E Edit');
			pcm_type.$('option:nth-child(2)').click();
			pcm_cost.sendKeys('10000');

			rdp.$('input[ng-model="RDP.amount"]').sendKeys('10000');
			rdp.$('input[ng-model="RDP.description"]').sendKeys('E2E Edit');
			community.$('input[ng-model="Community.amount"]').sendKeys('10000');
			community.$('input[ng-model="Community.description"]').sendKeys('E2E Edit');

			var first_ctrb = element(by.repeater('(c_idx, contribution) in contributions'));
			var ctrb_source = first_ctrb.$('select[ng-model="contribution.source_name"]');
			var ctrb_amount = first_ctrb.$('input[ng-model="contribution.amount"]');
			var ctrb_description = first_ctrb.$('input[ng-model="contribution.description"]');

			ctrb_source.$('option:nth-child(2)').click();
			ctrb_amount.sendKeys('10000');
			ctrb_description.sendKeys('E2E Edit');


			sumbit.click().then(function() {
				browser.ignoreSynchronization = true;
				browser.sleep(3000);
				expect( browser.getCurrentUrl() ).not.toContain('edit');
			});
		});

		it('Cancel edit T6 form', function() {
			cancel.click().then(function() {
				browser.ignoreSynchronization = true;
				browser.sleep(3000);
				expect( browser.getCurrentUrl() ).not.toContain('edit');
			});
		});
	});

});
