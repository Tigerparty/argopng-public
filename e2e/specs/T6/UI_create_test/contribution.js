describe('Add Contribution', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.get('/subproject/create');
		browser.executeScript('window.scrollTo(0, 2475);');
	});

	var add_ctrb_btn = element(by.xpath('//a[@ng-click="addContribution()"]'));

	it('Normal Flow : Add contribution -> Delete contribution', function() {
		expect( add_ctrb_btn.isPresent() ).toBe(true);

		add_ctrb_btn.click();

		var contributions = element.all(by.repeater('(c_idx, contribution) in contributions'));
		expect( contributions.count() ).toEqual(1);
		contributions.then(function(contributions) {
			var ctbr_childs = contributions[0].$$('div').get(0).$$('div');
			var source = ctbr_childs.get(0).$('select').$$('option');
			var amount = ctbr_childs.get(1).$('input');
			var description = ctbr_childs.get(2).$('input');
			var del = contributions[0].$$('div').get(0).$('a[ng-click="deleteContribution(c_idx)"]');

			expect( source.count() ).toEqual(5);
			expect( amount.getAttribute('name') ).toEqual('subproject[contributions][2][amount]');
			expect( description.getAttribute('name') ).toEqual('subproject[contributions][2][description]');
			expect( del.isPresent() ).toBe(true);

			del.click();
		});
		expect( contributions.count() ).toEqual(0);
	});
});