describe('Sector -> SubSector', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.get('/subproject/create');
		browser.executeScript('window.scrollTo(0, 1024);');
	});
	var sector_slt = element(by.name('subproject[sector]'));
	var subsector_slt = element(by.name('subproject[subsector]'));

	it('Normal Flow : S->SS', function() {
		expect( sector_slt.isEnabled() ).toBe(true);
		expect( subsector_slt.isEnabled() ).toBe(false);

		sector_slt.$('option:nth-child(2)').click();

		expect( sector_slt.isEnabled() ).toBe(true);
		expect( subsector_slt.isEnabled() ).toBe(true);

		subsector_slt.$('option:nth-child(2)').click();
	});

	it('Normal Flow : S->SS->S', function() {
		sector_slt.$('option:nth-child(2)').click();
		subsector_slt.$('option:nth-child(2)').click();
		sector_slt.$('option:nth-child(1)').click();

		expect( sector_slt.isEnabled() ).toBe(true);
		expect( subsector_slt.isEnabled() ).toBe(false);
	});
});