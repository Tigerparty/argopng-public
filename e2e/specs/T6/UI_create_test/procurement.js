

describe('Add Procurement', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.get('/subproject/create');
		browser.executeScript('window.scrollTo(0, 2468);');
	});

	var add_procurement_btn = element(by.xpath('//a[@ng-click="addProcurement()"]'));

	it('Normal Flow : Add Pcm', function() {

		expect( add_procurement_btn.isPresent() ).toBe(true);

		add_procurement_btn.click();
		var procurements = element.all(by.repeater('(p_idx, procurement) in subproject.procurements'));
		expect( procurements.count() ).toEqual(1);
		procurements.then(function(procurements) {
			var first_pcm = procurements[0].$$('div').get(0).$$('div');
			var title = first_pcm.get(0).$('input');
			var contractor = first_pcm.get(1).$('input');
			var type = first_pcm.get(2).$('select');
			var cost = first_pcm.get(3).$('input');
			var del = procurements[0].$$('div').get(0).$('a');

			expect( title.getAttribute('name') ).toEqual('subproject[procurements][0][contract_title]');
			expect( contractor.getAttribute('name') ).toEqual('subproject[procurements][0][contractor]');
			expect( type.getAttribute('name') ).toEqual('subproject[procurements][0][type]');
			expect( type.$$('option').count() ).toEqual(4);
			expect( cost.getAttribute('name') ).toEqual('subproject[procurements][0][cost]');
			expect( del.isPresent() ).toBe(true);

			del.click();
		});
		expect( procurements.count() ).toEqual(0);
	});
});