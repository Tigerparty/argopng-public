describe('Province -> Ward -> Benefit Village', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.get('/subproject/create');
		browser.executeScript('window.scrollTo(0, 2105);');
	});

	var prov_slt = element(by.name('subproject[province]'));
	var ward_slt = element(by.name('subproject[ward]'));
	var bnf_vill_sel = element(by.xpath('//select[@ng-change="addBenefitVillageTable()"]'));

	it('Normal Flow : P->W->Add BV 2 times->Del BV', function() {
		prov_slt.$('option:nth-child(2)').click();
		ward_slt.$('option:nth-child(2)').click();
		bnf_vill_sel.$('option:nth-child(2)').click();
		bnf_vill_sel.$('option:nth-child(3)').click();

		var bnf_vills = element.all(by.repeater('benefitVillageTable in benefitVillageTables track by $index'));
		expect( bnf_vills.count() ).toEqual(2);
		bnf_vills.then(function(bnf_vills) {
			var del = bnf_vills[0].$('bvt').$$('div').get(0).$('button');
			del.click();
		});
		expect( bnf_vills.count() ).toEqual(1);
	});

	it('Abnormal Flow : P->W->Add BV 1 times->Del BV->accept alert', function() {
		prov_slt.$('option:nth-child(2)').click();
		ward_slt.$('option:nth-child(2)').click();
		bnf_vill_sel.$('option:nth-child(2)').click();

		var bnf_vills = element.all(by.repeater('benefitVillageTable in benefitVillageTables track by $index'));
		expect( bnf_vills.count() ).toEqual(1);
		bnf_vills.then(function(bnf_vills) {
			var del = bnf_vills[0].$('bvt').$$('div').get(0).$('button');
			del.click();
			browser.sleep(3000);
			var alert = browser.switchTo().alert();
			expect( alert.getText() ).toEqual('Benefit villages at least 1');
			alert.accept();
		});
	});
});