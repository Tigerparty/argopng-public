
describe('Create T4', function() {
	beforeEach(function(){
		browser.ignoreSynchronization = false;
		browser.get('/approval/create');
	});

	var prov_slt = element(by.name('province'));
	var cycle_slt = element(by.name('cycle'))
	var date_picker = element(by.xpath('//div[@ng-click="openDatePicker($event)"]'));
	var budget = element(by.name('approvedBudget'));
	var cancel = element(by.buttonText('Cancel'));
	var submit = $('input[type="submit"]');

	it('Cancel create T4 form', function() {
		cancel.click().then(function() {
			browser.ignoreSynchronization = true;
			browser.sleep(3000);
			expect( browser.getCurrentUrl() ).not.toContain('create');
		});
	});


	it('Press data for all', function() {

		prov_slt.$('option:nth-child(2)').click();
		cycle_slt.$('option:nth-child(2)').click();

		//-- Date pick
		date_picker.click();
		var btn_bar = element(by.xpath('//li[@ng-if="showButtonBar"]'));
		btn_bar.element(by.buttonText('Today')).click();

		budget.sendKeys('10000');

		element(by.xpath('//a[@ng-click="addProposal()"]')).click();
		browser.sleep(500);

		var proposal = element(by.xpath('//div[@proposal-row]/div[1]'));
		var ward_slt = proposal.element(by.name('proposalWard[]'));
		var vill_slt = proposal.element(by.name('proposalVillage[]'));
		var sp_slt   = proposal.element(by.name('proposalSubproject[]'));

		expect( ward_slt.isEnabled() ).toBe(true);
		expect( vill_slt.isEnabled() ).toBe(false);
		expect( sp_slt.isEnabled() ).toBe(false);

		ward_slt.$('option:nth-child(2)').click();
		expect( ward_slt.isEnabled() ).toBe(true);
		expect( vill_slt.isEnabled() ).toBe(true);
		expect( sp_slt.isEnabled() ).toBe(false);

		vill_slt.$('option:nth-child(2)').click();
		expect( ward_slt.isEnabled() ).toBe(true);
		expect( vill_slt.isEnabled() ).toBe(true);
		expect( sp_slt.isEnabled() ).toBe(true);

		//-- Select proposal subproject
		// sp_slt.$('option:nth-child(2)').click();
		// expect(
		// 	proposal.element(
		// 		by.name('proposalSubprojectType[]')
		// 	).getAttribute('value')
		// ).toEqual('select');

		//-- Create subproject
		sp_slt.$('option:nth-last-child(1)').click();
		expect(
			proposal.element(
				by.name('proposalSubprojectType[]')
			).getAttribute('value')
		).toEqual('text');

		proposal.element(by.name('proposalSubproject[]')).sendKeys('E2E Create');


		submit.click().then(function() {
			browser.ignoreSynchronization = true;
			browser.sleep(3000);
			expect( browser.getCurrentUrl() ).not.toContain('create');
		});
	});
});