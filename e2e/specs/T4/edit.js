describe('Edit Page', function() {
    var edit_url = ""

    it('Set Edit URL', function() {
        browser.getCurrentUrl().then(function(url) {
            edit_url = url;
        });
    });

    describe('Edit T4 Test', function() {
        beforeEach(function(){
            browser.get(edit_url)
            browser.ignoreSynchronization = false;
        });

        var prov_slt = element(by.name('province'));
        var cycle_slt = element(by.name('cycle'))
        var meeting_date = element(by.xpath('//div[@ng-click="openDatePicker($event)"]'));
        var budget = element(by.name('approvedBudget'));
        var cancel = element(by.buttonText('Cancel'));
        var submit = $('input[type="submit"]');

        it('Cancel create T4 form', function() {
            cancel.click().then(function() {
                browser.ignoreSynchronization = true;
                browser.sleep(3000);
                expect( browser.getCurrentUrl() ).not.toContain('create');
            });
        });

        it('Check edit data', function() {
            var proposal = element(by.xpath('//div[@proposal-row]/div[1]'));
            var ward_slt = proposal.element(by.name('proposalWard[]'));
            var vill_slt = proposal.element(by.name('proposalVillage[]'));
            var sp_slt   = proposal.element(by.name('proposalSubproject[]'));

            expect( prov_slt.getAttribute('value') ).toBeGreaterThan(0);
            expect( cycle_slt.getAttribute('value') ).toBeGreaterThan(0);
            expect( meeting_date.getAttribute('value') ).not.toEqual('');
            expect( budget.getAttribute('value') ).toEqual('10000');
            expect( ward_slt.getAttribute('value') ).toBeGreaterThan(0);
            expect( vill_slt.getAttribute('value') ).toBeGreaterThan(0);
        });


        it('Press data for all', function() {

            prov_slt.$('option:nth-child(2)').click();
            cycle_slt.$('option:nth-child(2)').click();

            //-- Date pick
            meeting_date.click();
            var btn_bar = element(by.xpath('//li[@ng-if="showButtonBar"]'));
            btn_bar.element(by.buttonText('Today')).click();

            budget.sendKeys('10000');

            var proposal = element(by.xpath('//div[@proposal-row]/div[1]'));
            var ward_slt = proposal.element(by.name('proposalWard[]'));
            var vill_slt = proposal.element(by.name('proposalVillage[]'));
            var sp_slt   = proposal.element(by.name('proposalSubproject[]'));

            ward_slt.$('option:nth-child(2)').click();
            vill_slt.$('option:nth-child(2)').click();

            //-- Select proposal subproject
            // sp_slt.$('option:nth-child(2)').click();
            // expect(
            //  proposal.element(
            //      by.name('proposalSubprojectType[]')
            //  ).getAttribute('value')
            // ).toEqual('select');

            //-- Create subproject
            sp_slt.$('option:nth-last-child(1)').click();
            expect(
                proposal.element(
                    by.name('proposalSubprojectType[]')
                ).getAttribute('value')
            ).toEqual('text');

            proposal.element(by.name('proposalSubproject[]')).sendKeys('E2E Create Edit');

            submit.click().then(function() {
                browser.ignoreSynchronization = true;
                browser.sleep(3000);
                expect( browser.getCurrentUrl() ).not.toContain('edit');
            });
        });
    });
});