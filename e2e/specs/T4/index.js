describe('T4 index Page', function() {
	it('Click T4 Index Tab', function() {
		browser.sleep(3000);

		$('li[class="dropdown"] a').click().then(function() {
			$('li[id="provincial-tab"] a').click();
		});

		browser.sleep(3000);
		var T4_list = element.all(by.repeater('pageApproval in pageApprovals'));
		expect( T4_list.count() ).toBeGreaterThan(0);
	});

	var T4_list = element.all(by.repeater('pageApproval in pageApprovals'));
	var prov_slt = $('select[ng-model="province"]');
	var search_btn = $('a[ng-click="approvalFilter()"]');

	describe('Display drop down list: province', function() {
		beforeEach( function() {
			browser.ignoreSynchronization = false;
			browser.navigate().refresh();
		});

		it('Display province selector for not exist T4', function() {
			prov_slt.$('option:nth-child(2)').click().then(function() {
				search_btn.click().then(function() {
					expect( T4_list.count() ).toEqual(0);
				});

			});
		});

		it('Display province selector for exist T4', function() {
			prov_slt.$('option:nth-child(4)').click().then(function() {
				search_btn.click().then(function() {
					expect( T4_list.count() ).toBeGreaterThan(0);
				});
			});
		});
	});

});


