describe('Edit Page', function() {
	var edit_url = ""

	it("Set Edit URL", function() {
		browser.getCurrentUrl().then(function(url) {
			browser.ignoreSynchronization = false;
			browser.get(url + '/edit');
		});
	});

	describe('Province -> Add Subproject -> Ward -> Village', function() {
		var prov_slt = element(by.name('province'));
		var delete_sp_btn = $('button[ng-click="deleteProposal(index)"]');

		beforeEach(function() {
			browser.navigate().refresh();
			delete_sp_btn.click();
			prov_slt.$('option:nth-child(1)').click().then(function() {
				$('button[ng-click="yesOnClick()"]').click();
			});
		});


		it('P->Add SP->W->V->SP->P', function() {
			prov_slt.$('option:nth-child(2)').click();
			element(by.xpath('//a[@ng-click="addProposal()"]')).click();

			var proposal = element(by.xpath('//div[@proposal-row]/div[1]'));
			var ward_slt = proposal.element(by.name('proposalWard[]'));
			var vill_slt = proposal.element(by.name('proposalVillage[]'));
			var sp_slt   = proposal.element(by.name('proposalSubproject[]'));

			ward_slt.$('option:nth-child(2)').click();
			vill_slt.$('option:nth-child(2)').click();
			sp_slt.$('option:nth-last-child(1)').click();
			prov_slt.$('option:nth-child(1)').click();

			browser.sleep(5000);

			expect( ward_slt.isEnabled() ).toBe(false);
			expect( vill_slt.isEnabled() ).toBe(false);
			expect( sp_slt.isEnabled() ).toBe(false);
		});

		it('P->Add SP->W->V->SP->W', function() {
			prov_slt.$('option:nth-child(2)').click();
			element(by.xpath('//a[@ng-click="addProposal()"]')).click();

			var proposal = element(by.xpath('//div[@proposal-row]/div[1]'));
			var ward_slt = proposal.element(by.name('proposalWard[]'));
			var vill_slt = proposal.element(by.name('proposalVillage[]'));
			var sp_slt   = proposal.element(by.name('proposalSubproject[]'));

			ward_slt.$('option:nth-child(2)').click();
			vill_slt.$('option:nth-child(2)').click();
			sp_slt.$('option:nth-last-child(1)').click();
			ward_slt.$('option:nth-child(1)').click();

			expect( ward_slt.isEnabled() ).toBe(true);
			expect( vill_slt.isEnabled() ).toBe(false);
			expect( sp_slt.isEnabled() ).toBe(false);
		});

		it('P->Add SP->W->V->SP->V', function() {
			prov_slt.$('option:nth-child(2)').click();

			element(by.xpath('//a[@ng-click="addProposal()"]')).click();

			var proposal = element(by.xpath('//div[@proposal-row]/div[1]'));
			var ward_slt = proposal.element(by.name('proposalWard[]'));
			var vill_slt = proposal.element(by.name('proposalVillage[]'));
			var sp_slt   = proposal.element(by.name('proposalSubproject[]'));

			ward_slt.$('option:nth-child(2)').click();
			vill_slt.$('option:nth-child(2)').click();
			sp_slt.$('option:nth-last-child(1)').click();
			vill_slt.$('option:nth-child(1)').click();

			expect( ward_slt.isEnabled() ).toBe(true);
			expect( vill_slt.isEnabled() ).toBe(true);
			expect( sp_slt.isEnabled() ).toBe(false);
		});

	});
})

