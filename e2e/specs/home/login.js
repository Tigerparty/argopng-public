

describe('Login', function(){
    browser.ignoreSynchronization = true;
    it('Login', function(){

        browser.get('/login');

        element(by.id('username'))
            .sendKeys('developer');
        element(by.id('password'))
            .sendKeys('developer');
        element.all(by.tagName('form')).all(by.tagName('input')).last()
            .click();
    });

    it('Dropdown menu', function(){
        browser.sleep(3000);
        expect(
            element(by.id('overall-header-logout-link')).getText()
        ).toBe('Logout');

        element.all(by.css('#dropdownMenu1')).click();
        element.all(by.css('#overall-header ul.dropdown-menu li a')).each(function(element, index){
            element.getAttribute('href').then(function(text){
                switch(index){
                    case 0:
                        expect(text).toMatch(/village\/create/);
                        break;
                    case 1:
                        expect(text).toMatch(/request\/create/);
                        break;
                    case 2:
                        expect(text).toMatch(/ranking\/create/);
                        break;
                    case 3:
                        expect(text).toMatch(/approval\/create/);
                        break;
                    case 4:
                        expect(text).toMatch(/subproject\/create/);
                        break;
                }
            });
        });
        element.all(by.css('#dropdownMenu1')).click();
    });

    // it('Home tab', function(){
    //     var tab_btn = element(by.id('home-tab'));
    //     tab_btn.click();
    //     browser.sleep(3000);
    //     expect( tab_btn.getAttribute('class') ).toEqual('overall-menu-button active');
    // });

    // it('Community Prioritisation tab', function(){
    //     var tab_btn = element(by.id('home-tab'));
    //     tab_btn.click();
    //     browser.sleep(3000);
    //     expect( tab_btn.getAttribute('class') ).toEqual('overall-menu-button active');
    // });

    // it('Ward Ranking Meetings tab', function(){
    //     var tab_btn = element(by.id('home-tab'));
    //     tab_btn.click();
    //     browser.sleep(3000);
    //     expect( tab_btn.getAttribute('class') ).toEqual('overall-menu-button active');
    // });

    // it('Provincial Approval tab', function(){
    //     var tab_btn = element(by.id('home-tab'));
    //     tab_btn.click();
    //     browser.sleep(3000);
    //     expect( tab_btn.getAttribute('class') ).toEqual('overall-menu-button active');
    // });

    // it('Subproject Information tab', function(){
    //     var tab_btn = element(by.id('home-tab'));
    //     tab_btn.click();
    //     browser.sleep(3000);
    //     expect( tab_btn.getAttribute('class') ).toEqual('overall-menu-button active');
    // });
});
