describe('Create T1 form', function(){

    beforeEach(function(){
        browser.ignoreSynchronization = false;
        browser.get('/request/create');
    });

    var prov_slt          = element(by.name('province'));
    var ward_slt          = element(by.name('ward'));
    var vill_slt          = element(by.name('village'));
    var meeting_date      = element(by.name('meeting[date]')).element(by.xpath('..'));
    var meeting_boys      = element(by.name('meeting[boys]'));
    var meeting_girls     = element(by.name('meeting[girls]'));
    var meeting_males     = element(by.name('meeting[males]'));
    var meeting_females   = element(by.name('meeting[females]'));
    var meeting_men       = element(by.name('meeting[men]'));
    var meeting_women     = element(by.name('meeting[women]'));
    var meeting_old_men   = element(by.name('meeting[old_men]'));
    var meeting_old_women = element(by.name('meeting[old_women]'));
    var meeting_disabled  = element(by.name('meeting[disabled]'));
    var cycle_slt         = element(by.name('cycle'));
    var needs             = element.all(by.repeater('need in needs track by $index'));
    var sector_slt        = element(by.name('sector'));
    var subsector_slt     = element(by.name('subsector'));
    var sumbit            = $('input[type="submit"]');
    var cancel            = $('button[onclick="window.location=\'/request\'"]');

    it('Cancel create T1 form', function() {
        cancel.click().then(function() {
            browser.ignoreSynchronization = true;
            browser.sleep(3000);
            expect( browser.getCurrentUrl() ).not.toContain('create');
        });
    });

    it('Press data for all', function(){
        prov_slt.$('option:nth-child(2)').click();
        ward_slt.$('option:nth-child(2)').click();
        vill_slt.$('option:nth-child(2)').click();

        meeting_date.$('img').click().then(function() {
            meeting_date.element(by.buttonText('Today')).click();
        });

        meeting_boys.sendKeys(121);
        meeting_girls.sendKeys(121);
        meeting_males.sendKeys(121);
        meeting_females.sendKeys(121);
        meeting_men.sendKeys(121);
        meeting_women.sendKeys(121);
        meeting_old_men.sendKeys(121);
        meeting_old_women.sendKeys(121);
        meeting_disabled.sendKeys(121);

        cycle_slt.$('option:nth-child(2)').click();

        needs.each(function(need, index) {
            need.$('input[ng-model="need.description"]').sendKeys('E2E Create' + index);
            need.$('input[value="yes"]').click();
        });

        sector_slt.$('option:nth-child(2)').click();
        subsector_slt.$('option:nth-child(2)').click();

        sumbit.click().then(function() {
            browser.ignoreSynchronization = true;
            browser.sleep(3000);
            expect( browser.getCurrentUrl() ).not.toContain('create');
        });
    });
});

