describe('T1 index Page', function() {
	it('Click T1 Index Tab', function() {
		browser.sleep(3000);

		$('li[class="dropdown"] a').click().then(function() {
			$('li[id="priority-tab"] a').click();
		});

		browser.sleep(3000);
		var T1_list = element.all(by.repeater('request in pageRequests'));
		expect( T1_list.count() ).toBeGreaterThan(0);
	});

	var T1_list = element.all(by.repeater('request in pageRequests'));
	var search_icon = $('img[ng-click="searchByFilter()"]');
	var keyword = $('input[ng-model="keyword_search"]');
	var prov_slt = $('select[ng-model="province"]');
	var ward_slt = $('select[ng-model="ward"]');
	var vill_slt = $('select[ng-model="village"]');

	describe('Display search keyword', function() {
		beforeEach( function() {
			browser.ignoreSynchronization = false;
			browser.navigate().refresh();
		});

		it('Display search by province name for not exist T1', function () {
			keyword.sendKeys('not exist');
			search_icon.click().then(function() {
				expect( T1_list.count() ).toBeLessThan(1);
			});
		});

		it('Display search by province name for exist T1', function () {
			keyword.sendKeys('Choiseul');

			search_icon.click().then(function() {
				expect( T1_list.count() ).toBeGreaterThan(0);
			});
		});

		it('Display search by ward name for exist T1', function () {
			keyword.sendKeys('Bangera');

			search_icon.click().then(function() {
				expect( T1_list.count() ).toBeGreaterThan(0);
			});
		});

		it('Display search by village name for exist T1', function () {
			keyword.sendKeys('Zalezale');

			search_icon.click().then(function() {
				expect( T1_list.count() ).toBeGreaterThan(0);
			});
		});
	});

	describe('Display drop down list: province', function() {
		beforeEach( function() {
			browser.ignoreSynchronization = false;
			browser.navigate().refresh();
		});

		it('Display province selector for not exist T1', function() {
			prov_slt.$('option:nth-child(2)').click().then(function() {
				expect( T1_list.count() ).toBeLessThan(1);
			});
		});

		it('Display province selector for exist T1', function() {
			prov_slt.$('option:nth-child(3)').click().then(function() {
				expect( T1_list.count() ).toBeGreaterThan(0);
			});
		});
	});

	describe('Display drop down list: province -> ward', function() {
		beforeEach( function() {
			browser.ignoreSynchronization = false;
			browser.navigate().refresh();
			prov_slt.$('option:nth-child(3)').click();
		});

		it('Display province selector for not exist T1', function() {
			ward_slt.$('option:nth-child(2)').click().then(function() {
				expect( T1_list.count() ).toBeLessThan(1);
			});
		});

		it('Display province selector for exist T1', function() {
			ward_slt.$('option:nth-child(3)').click().then(function() {
				expect( T1_list.count() ).toBeGreaterThan(0);
			});
		});
	});

	describe('Display drop down list: province -> ward -> village', function() {
		beforeEach( function() {
			browser.ignoreSynchronization = false;
			browser.navigate().refresh();
			prov_slt.$('option:nth-child(3)').click();
			ward_slt.$('option:nth-child(3)').click();
		});

		it('Display province selector for not exist T1', function() {
			vill_slt.$('option:nth-child(2)').click().then(function() {
				expect( T1_list.count() ).toBeLessThan(1);
			});
		});

		it('Display province selector for exist T1', function() {
			vill_slt.$('option:nth-child(3)').click().then(function() {
				expect( T1_list.count() ).toBeGreaterThan(0);
			});
		});
	});
});


