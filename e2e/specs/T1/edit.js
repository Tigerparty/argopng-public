describe('Edit Page', function() {
	var edit_url = "";

	it('Set Edit URL', function() {
		browser.getCurrentUrl().then(function(url) {
			edit_url = url;
		});
	});
	describe('Edit T1 from', function() {
		beforeEach(function(){
			browser.get(edit_url);
			browser.ignoreSynchronization = false;
		});

		var prov_slt          = element(by.name('province'));
	    var ward_slt          = element(by.name('ward'));
	    var vill_slt          = element(by.name('village'));
	    var meeting_date      = element(by.name('meeting[date]')).element(by.xpath('..'));
	    var meeting_boys      = element(by.name('meeting[boys]'));
	    var meeting_girls     = element(by.name('meeting[girls]'));
	    var meeting_males     = element(by.name('meeting[males]'));
	    var meeting_females   = element(by.name('meeting[females]'));
	    var meeting_men       = element(by.name('meeting[men]'));
	    var meeting_women     = element(by.name('meeting[women]'));
	    var meeting_old_men   = element(by.name('meeting[old_men]'));
	    var meeting_old_women = element(by.name('meeting[old_women]'));
	    var meeting_disabled  = element(by.name('meeting[disabled]'));
	    var cycle_slt         = element(by.name('cycle'));
	    var needs             = element.all(by.repeater('need in needs track by $index'));
	    var sector_slt        = element(by.name('sector'));
	    var subsector_slt     = element(by.name('subsector'));
	    var sumbit            = $('input[type="submit"]');
	    var cancel 	          = element(by.buttonText('Cancel'));

	    it('Check edit data', function() {
	    	expect( prov_slt.getAttribute('value') ).toBeGreaterThan(0);
		    expect( ward_slt.getAttribute('value') ).toBeGreaterThan(0);
		    expect( vill_slt.getAttribute('value') ).toBeGreaterThan(0);
		    expect( meeting_date.getAttribute('value') ).not.toEqual('');
		    expect( meeting_boys.getAttribute('value') ).toEqual('121');
		    expect( meeting_girls.getAttribute('value') ).toEqual('121');
		    expect( meeting_males.getAttribute('value') ).toEqual('121');
		    expect( meeting_females.getAttribute('value') ).toEqual('121');
		    expect( meeting_men.getAttribute('value') ).toEqual('121');
		    expect( meeting_women.getAttribute('value') ).toEqual('121');
		    expect( meeting_old_men.getAttribute('value') ).toEqual('121');
		    expect( meeting_old_women.getAttribute('value') ).toEqual('121');
		    expect( meeting_disabled.getAttribute('value') ).toEqual('121');
		    expect( cycle_slt.getAttribute('value') ).toBeGreaterThan(0);

		    needs.each(function(need, index) {
		    	expect( need.$$('label').get(0).getText() ).toEqual(index + 1 + '');
				expect( need.$('input[ng-model="need.description"]').getAttribute('value') ).toEqual('E2E Create' + index);
		    });

		    expect( sector_slt.getAttribute('value') ).toBeGreaterThan(0);
		    expect( subsector_slt.getAttribute('value') ).toBeGreaterThan(0);
	    });

	    it('Edit data for all', function(){
	        prov_slt.$('option:nth-child(3)').click();
	        ward_slt.$('option:nth-child(3)').click();
	        vill_slt.$('option:nth-child(3)').click();

	        meeting_date.$('img').click().then(function() {
	            meeting_date.element(by.buttonText('Today')).click();
	        });

	        meeting_boys.sendKeys(121);
	        meeting_girls.sendKeys(121);
	        meeting_males.sendKeys(121);
	        meeting_females.sendKeys(121);
	        meeting_men.sendKeys(121);
	        meeting_women.sendKeys(121);
	        meeting_old_men.sendKeys(121);
	        meeting_old_women.sendKeys(121);
	        meeting_disabled.sendKeys(121);

	        cycle_slt.$('option:nth-child(3)').click();

	        needs.each(function(need, index) {
	            need.$('input[ng-model="need.description"]').sendKeys(' Edit' + index);
	            need.$('input[value="yes"]').click();
	        });

	        sector_slt.$('option:nth-child(3)').click();
	        subsector_slt.$('option:nth-child(3)').click();

	        sumbit.click().then(function() {
	            browser.ignoreSynchronization = true;
	            browser.sleep(3000);
	            expect( browser.getCurrentUrl() ).not.toContain('edit');
	        });
	    });

		// it('Cancel edit T1 form', function() {
	    //     cancel.click().then(function() {
	    //         browser.ignoreSynchronization = true;
	    //         browser.sleep(3000);
	    //         expect( browser.getCurrentUrl() ).not.toContain('edit');
	    //     });
	    // });
	});
});