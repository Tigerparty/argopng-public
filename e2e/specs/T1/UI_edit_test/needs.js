describe('Edit Page', function() {
    it('Go to Edit page', function() {
        browser.getCurrentUrl().then(function(url) {
            browser.ignoreSynchronization = false;
            browser.get(url + '/edit');
        });
    });
    describe('Add Need', function(){
        beforeEach(function(){
            browser.navigate().refresh();
        });

        var add_need_btn = element(by.xpath('//a[@ng-click="addNewNeed()"]'));
        var needs = element.all(by.repeater('need in needs track by $index'));

        it('Normal Flow : Add need -> Delete need', function() {
            var EXPECT_NEEDS_COUNT = 2;

            expect( add_need_btn.isPresent() ).toBe(true);

            expect( needs.count() ).toEqual(EXPECT_NEEDS_COUNT - 1);
            add_need_btn.click();
            expect( needs.count() ).toEqual(EXPECT_NEEDS_COUNT);

            needs.then(function(needs) {
                var index = EXPECT_NEEDS_COUNT - 1;
                var need_child_set = needs[index].$$('div').get(0);
                var rank = need_child_set.$('label');
                var description = need_child_set.$$('div').get(0).$('input');
                var eligible = need_child_set.$$('div').get(1).$$('label');  //-- Need to add hidden input to check init value
                var del = need_child_set.$('a');

                expect( rank.getText() ).toEqual('' + EXPECT_NEEDS_COUNT);
                expect( description.getAttribute('name') ).toEqual('needs[1][description]');
                expect( eligible.count() ).toEqual(2);
                expect( del.getAttribute('ng-click') ).toEqual('deleteNeed($index)');
            });
        });
    });
});