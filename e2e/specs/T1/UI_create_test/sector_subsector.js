describe('Sector -> SubSector', function() {
	beforeEach(function() {
		browser.ignoreSynchronization = false;
		browser.get('/request/create');
		browser.executeScript('window.scrollTo(0, 1454);');
	});
	var sector_slt = element(by.name('sector'));
	var subsector_slt = element(by.name('subsector'));

	it('Normal Flow : S->SS', function() {
		expect( sector_slt.isEnabled() ).toBe(true);
		expect( subsector_slt.isEnabled() ).toBe(false);

		sector_slt.$('option:nth-child(2)').click();

		expect( sector_slt.isEnabled() ).toBe(true);
		expect( subsector_slt.isEnabled() ).toBe(true);

		subsector_slt.$('option:nth-child(2)').click();
	});

	it('Normal Flow : S->SS->S', function() {
		sector_slt.$('option:nth-child(2)').click();
		subsector_slt.$('option:nth-child(2)').click();
		sector_slt.$('option:nth-child(1)').click();

		expect( sector_slt.isEnabled() ).toBe(true);
		expect( subsector_slt.isEnabled() ).toBe(false);
	});
});