describe('Province->Ward->Village', function(){

    beforeEach(function(){
        browser.ignoreSynchronization = false;
        browser.get('/request/create');
    });

    var prov_slt = element(by.name('province'));
    var ward_slt = element(by.name('ward'));
    var vill_slt = element(by.name('village'));

    it('Normal Flow : P->W->V', function(){
        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(false);
        expect( vill_slt.isEnabled() ).toBe(false);

        prov_slt.$('option:nth-child(2)').click();
        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(true);
        expect( vill_slt.isEnabled() ).toBe(false);

        ward_slt.$('option:nth-child(2)').click();
        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(true);
        expect( vill_slt.isEnabled() ).toBe(true);
    });

    it('Abnormal Flow : P->W->V->P', function(){
        prov_slt.$('option:nth-child(2)').click();
        ward_slt.$('option:nth-child(2)').click();
        vill_slt.$('option:nth-child(2)').click();
        prov_slt.$('option:nth-child(1)').click();

        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(false);
        expect( vill_slt.isEnabled() ).toBe(false);
    });

    it('Abnormal Flow : P->W->V->W', function(){
        prov_slt.$('option:nth-child(2)').click();
        ward_slt.$('option:nth-child(2)').click();
        vill_slt.$('option:nth-child(2)').click();
        ward_slt.$('option:nth-child(1)').click();

        expect( prov_slt.isEnabled() ).toBe(true);
        expect( ward_slt.isEnabled() ).toBe(true);
        expect( vill_slt.isEnabled() ).toBe(false);
    });
});