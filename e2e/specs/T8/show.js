describe('T8 Show', function(){
    beforeEach(function(){
        browser.ignoreSynchronization = false;
    });

    it('Click Subproject Information tab', function(){
        browser.ignoreSynchronization = true;
        element(by.id('subproject-tab')).click();
    });

    it('Click first subproject read more', function(){
        var first_subproject = element.all(by.repeater("(key, value) in pageSubprojects | groupBy: 'group'")).first();
        first_subproject.all(by.xpath('div[@ng-repeat="subproject in value"]//a')).first().click();
    });

    it('Click first report', function(){
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        element.all(by.xpath('//table[@class="col-md-12 table table-striped table-bordered"]//a')).first().click();
    });

    it('Click edit button', function(){
        browser.ignoreSynchronization = true;
        element.all(by.xpath('//div[@class="row t8-hide-tag"]//a')).first().click();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).toContain('edit');
    });

    it('Test download the file', function(){

    });
});