describe('T8 Edit', function(){
    beforeEach(function(){
        browser.ignoreSynchronization = false;
    });

    it('Click Subproject Information tab', function(){
        browser.ignoreSynchronization = true;
        element(by.id('subproject-tab')).click();
    });

    it('Click first subproject read more', function(){
        var first_subproject = element.all(by.repeater("(key, value) in pageSubprojects | groupBy: 'group'")).first();
        first_subproject.all(by.xpath('div[@ng-repeat="subproject in value"]//a')).first().click();
    });

    it('Click first report', function(){
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        element.all(by.xpath('//table[@class="col-md-12 table table-striped table-bordered"]//a')).first().click();
    });

    it('Click edit button', function(){
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        element.all(by.xpath('//div[@class="row t8-hide-tag"]//a')).first().click();
    });

    it('Show subproject name', function(){
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        var subproject_name = element(by.xpath('//div[@class="row"]//label[@class="col-md-4 col-xs-9 control-label"]'));
        expect(subproject_name.getText()).not.toBe('');
    });

    it('Select the date of monitoring form', function(){
        element(by.xpath('//div[@ng-click="openMonitoringDatePicker($event)"]')).click();
        element(by.xpath('//div[@class="col-md-4 col-xs-12"]//li[@ng-if="showButtonBar"]')).element(by.buttonText('Today')).click();
        var monitoring_date = element(by.model('report.monitoring_date'));
        expect(monitoring_date.getAttribute('value')).not.toBe('');
    });

    it('Enter the report name', function(){
        var completed_by = element(by.id('completed_by'));
        completed_by.clear();
        completed_by.sendKeys('CompleteByTest');
        expect(completed_by.getAttribute('value')).toBe('CompleteByTest');
    });

    it('Add issue button onclick', function(){
        element(by.xpath('//a[@ng-click="addIssues()"]')).click();
    });

    it('Test add issue - issue', function(){
        var date_time = new Date();

        var issue = element.all(by.model('issue.issue')).last();
        issue.sendKeys(date_time.toString());
        expect(issue.getAttribute('value')).not.toBe('');
    });

    it('Test add issue - create', function(){
        var last_issue = element.all(by.repeater('(index, issue) in issuesList track by $index')).last();
        element.all(by.xpath('//div[@ng-click="openIssueCreatedDatePicker($event, $index)"]')).last().click();
        last_issue.all(by.buttonText('Today')).first().click();
        expect(last_issue.element(by.model('issue.createDate')).getAttribute('value')).not.toBe('');
    });

    it('Test add issue - update', function(){
        var last_issue = element.all(by.repeater('(index, issue) in issuesList track by $index')).last();
        element.all(by.xpath('//div[@ng-click="openIssueUpdatedDatePicker($event, $index)"]')).last().click();
        last_issue.all(by.buttonText('Today')).last().click();
        expect(last_issue.element(by.model('issue.updateDate')).getAttribute('value')).not.toBe('');
    });

    it('Test add issue - resolved', function(){
        var last_issue = element.all(by.repeater('(index, issue) in issuesList track by $index')).last();
        var resolved = last_issue.all(by.model('issue.resolved')).get(0);
        resolved.click();
        expect(resolved.getAttribute('value')).toBe('1');
    });

    it('Test "financial" field', function(){
        //-- test yes onclick
        element.all(by.xpath('//input[@name="report[financial_kept_check]"]')).get(0).click();
        expect(element(by.id('financial_kept_detail')).isDisplayed()).toBe(false);

        //-- test no onclick
        element.all(by.xpath('//input[@name="report[financial_kept_check]"]')).get(1).click();
        expect(element(by.id('financial_kept_detail')).isDisplayed()).toBe(true);

        var financial_kept_detail = element(by.xpath('//textarea[@name="report[financial_kept]"]'));
        financial_kept_detail.clear();
        financial_kept_detail.sendKeys('FinancialTest');
        expect(financial_kept_detail.getAttribute('value')).toBe('FinancialTest');
    });

    it('Test "community" field', function(){
        var yes = element.all(by.xpath('//input[@name="report[is_contributions_updated]"]')).get(0).click();
        expect(yes.getAttribute('value')).toBe('1');
    });

    it('Test "procurement" field', function(){
        //-- test no onclick
        element.all(by.xpath('//input[@name="report[procurement_change_check]"]')).get(1).click();
        expect(element(by.id('procurement_change_detail')).isDisplayed()).toBe(false);

        //-- test yes onclick
        element.all(by.xpath('//input[@name="report[procurement_change_check]"]')).get(0).click();
        expect(element(by.id('procurement_change_detail')).isDisplayed()).toBe(true);

        var procurement_change_detail = element(by.xpath('//textarea[@name="report[procurement_change]"]'));
        procurement_change_detail.clear();
        procurement_change_detail.sendKeys('ProcurementTest');
        expect(procurement_change_detail.getAttribute('value')).toBe('ProcurementTest');
    });

    it('Test "safeguard" field', function(){
        //-- test no onclick
        element.all(by.xpath('//input[@name="report[safeguard_check]"]')).get(1).click();
        expect(element(by.id('safeguard_detail')).isDisplayed()).toBe(false);

        //-- test yes onclick
        element.all(by.xpath('//input[@name="report[safeguard_check]"]')).get(0).click();
        expect(element(by.id('safeguard_detail')).isDisplayed()).toBe(true);

        var safeguard_detail = element(by.xpath('//textarea[@name="report[safeguard]"]'));
        safeguard_detail.clear();
        safeguard_detail.sendKeys('SafeguardTest');
        expect(safeguard_detail.getAttribute('value')).toBe('SafeguardTest');
    });

    it('Test "contributions" field', function(){
        //-- test no onclick
        element.all(by.xpath('//input[@name="report[contribution_changing_check]"]')).get(1).click();
        expect(element(by.id('contribution_changing_detail')).isDisplayed()).toBe(false);

        //-- test yes onclick
        element.all(by.xpath('//input[@name="report[contribution_changing_check]"]')).get(0).click();
        expect(element(by.id('contribution_changing_detail')).isDisplayed()).toBe(true);

        var contribution_changing_detail = element(by.xpath('//textarea[@name="report[contribution_changing]"]'));
        contribution_changing_detail.clear();
        contribution_changing_detail.sendKeys('ContributionsTest');
        expect(contribution_changing_detail.getAttribute('value')).toBe('ContributionsTest');
    });

    it('Test Submit button', function(){
        browser.ignoreSynchronization = true;
        element(by.buttonText('Submit')).click();
        browser.sleep(3000);
        expect(browser.getCurrentUrl()).not.toContain('edit');
    });
});