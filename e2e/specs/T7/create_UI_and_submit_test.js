describe('T7 Create', function(){
    beforeEach(function(){
        browser.ignoreSynchronization = false;
    });

    it('Click Subproject Information tab', function(){
        browser.ignoreSynchronization = true;
        element(by.id('subproject-tab')).click();
    });

    it('Click first subproject read more', function(){
        var first_subproject = element.all(by.repeater("(key, value) in pageSubprojects | groupBy: 'group'")).first();
        first_subproject.all(by.xpath('div[@ng-repeat="subproject in value"]//a')).first().click();
    });

    it('Click create T7 button', function(){
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        element.all(by.xpath('//div[@class="row"]//span[@class="col-md-offset-8 col-md-4"]//a')).last().click();
    });

    it('Show subproject name', function(){
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        var subproject_name = element(by.xpath('//div[@class="row"]//div[@class="col-md-8 col-xs-8"]//span'));
        expect(subproject_name.getText()).not.toBe('');
    });

    it('Select the Date Financing Agreement signed', function(){
        element(by.xpath('//div[@class="col-md-4 col-xs-12"]//div[@ng-click="openDatePicker($event)"]')).click();
        element(by.xpath('//div[@class="col-md-4 col-xs-12"]//li[@ng-if="showButtonBar"]')).element(by.buttonText('Today')).click();;
        var date = element(by.model('signedDate'));
        expect(date.getAttribute('value')).not.toBe('');
    });

    it('Enter the Original amount', function(){
        var original_amount = element(by.model('originalAmount'));
        original_amount.clear();
        original_amount.sendKeys(100);
        expect(original_amount.getAttribute('value')).toBeGreaterThan(0);
    });


    //-- test Amendment History
    it('Click Add Amendment History', function(){
        element(by.xpath('//button[@ng-click="addAmendment()"]')).click();

        var amendments = element.all(by.repeater('amendment in amendmentList track by $index'));
        expect(amendments.count()).toBe(2);
    });

    it('Test add Amendment History - Delete', function(){
        var last_amendment = element.all(by.repeater('amendment in amendmentList track by $index')).last();
        last_amendment.all(by.xpath('//a[@ng-click="deleteAmendment($index)"]')).last().click();

        var amendments = element.all(by.repeater('amendment in amendmentList track by $index'));
        expect(amendments.count()).toBe(1);
    });

    it('Test add Amendment History - Date of Amendment', function(){
        var last_amendment = element.all(by.repeater('amendment in amendmentList track by $index')).last();
        last_amendment.element(by.xpath('//div[@ng-click="openAmendmentDatePicker($event, $index)"]')).click();
        last_amendment.element(by.buttonText('Today')).click();

        var date = last_amendment.element(by.xpath('//input[@ng-model="amendment.date"]'));
        expect(date.getAttribute('value')).not.toBe('');
    });

    it('Test add Amendment History - Amend Amount', function(){
        var last_amendment = element.all(by.repeater('amendment in amendmentList track by $index')).last();

        var amount = last_amendment.element(by.xpath('//input[@ng-model="amendment.amount"]'));
        amount.clear();
        amount.sendKeys(100);
        expect(amount.getAttribute('value')).toBeGreaterThan(0);
    });

    //-- test Co-financing Agreements
    it('Click add Co-financing Agreements', function(){
        element(by.xpath('//button[@ng-click="addCoFinancing()"]')).click();

        var agreements = element.all(by.repeater('coFinancing in coFinancingList track by $index'));
        expect(agreements.count()).toBe(2);
    });

    it('Test add Co-financing Agreements - Delete', function(){
        var last_agreement = element.all(by.repeater('coFinancing in coFinancingList track by $index')).last();
        last_agreement.all(by.xpath('//a[@ng-click="deleteCoFinancing($index)"]')).last().click();

        var agreements = element.all(by.repeater('coFinancing in coFinancingList track by $index'));
        expect(agreements.count()).toBe(1);
    });

    it('Test add Co-financing Agreements - Enter Source', function(){
        var last_agreement = element.all(by.repeater('coFinancing in coFinancingList track by $index')).last();

        var source = last_agreement.element(by.model('coFinancing.source'));
        source.sendKeys('SourceNameTest');
        expect(source.getAttribute('value')).toBe('SourceNameTest');
    });

    it('Test add Co-financing Agreements - Due Date', function(){
        var last_agreement = element.all(by.repeater('coFinancing in coFinancingList track by $index')).last();
        last_agreement.element(by.xpath('//div[@ng-click="openCoFinancingDatePicker($event, $index)"]')).click();
        last_agreement.element(by.buttonText('Today')).click();

        var date = last_agreement.element(by.model('coFinancing.date'));
        expect(date.getAttribute('value')).not.toBe('');
    });

    it('Test add Co-financing Agreements - Amount or Value SBD', function(){
        var last_agreement = element.all(by.repeater('coFinancing in coFinancingList track by $index')).last();

        var amount = last_agreement.element(by.model('coFinancing.amount'));
        amount.clear();
        amount.sendKeys(100);
        expect(amount.getAttribute('value')).toBe('100');
    });

    it('Test add Co-financing Agreements - MP/MPA/Provincial Gov\'t', function(){
        var last_agreement = element.all(by.repeater('coFinancing in coFinancingList track by $index')).last();
        var mpa = last_agreement.all(by.model('coFinancing.type')).get(1);
        mpa.click();
        expect(mpa.getAttribute('value')).toBe('1');
    });

    it('Test Submit button', function(){
        browser.ignoreSynchronization = true;
        element(by.xpath('//input[@ng-click="btnSubmitClick();"]')).click();
        browser.sleep(3000);
        expect( browser.getCurrentUrl() ).not.toContain('create');
    });
});