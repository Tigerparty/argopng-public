describe('T7 Edit', function(){
    beforeEach(function(){
        browser.ignoreSynchronization = false;
    });

    it('Click Subproject Information tab', function(){
        browser.ignoreSynchronization = true;
        element(by.id('subproject-tab')).click();
    });

    it('Click first subproject read more', function(){
        var first_subproject = element.all(by.repeater("(key, value) in pageSubprojects | groupBy: 'group'")).first();
        first_subproject.all(by.xpath('div[@ng-repeat="subproject in value"]//a')).first().click();
    });

    it('Click last agreement', function(){
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        element.all(by.xpath('//tbody//tr//td//a')).last().click();
    });

    it('Click edit agreement', function(){
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        element(by.xpath('//div[@class="row t8-hide-tag"]//a')).click();
    });

    it('Click cancel button', function(){
        browser.ignoreSynchronization = true;
        element(by.buttonText('Cancel')).click();
        browser.sleep(3000);
        expect( browser.getCurrentUrl() ).not.toContain('create');
    });
});