CREATE DATABASE  IF NOT EXISTS `taarifa` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `taarifa`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: 127.0.0.1    Database: taarifa
-- ------------------------------------------------------
-- Server version	5.5.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `origin_name` varchar(255) DEFAULT NULL,
  `store_name` varchar(255) DEFAULT '',
  `type` varchar(255) DEFAULT NULL,
  `upload_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachment`
--

LOCK TABLES `attachment` WRITE;
/*!40000 ALTER TABLE `attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contribution`
--

DROP TABLE IF EXISTS `contribution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contribution` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `source_name` text,
  `type` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `contribution_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contribution`
--

LOCK TABLES `contribution` WRITE;
/*!40000 ALTER TABLE `contribution` DISABLE KEYS */;
INSERT INTO `contribution` VALUES (1,1,'name1','Other',10000),(2,1,'name2','Other',10000),(3,1,'name3','RDP',10000),(4,1,'name4','Community',10000);
/*!40000 ALTER TABLE `contribution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `needs`
--

DROP TABLE IF EXISTS `needs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `needs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `needs_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `needs`
--

LOCK TABLES `needs` WRITE;
/*!40000 ALTER TABLE `needs` DISABLE KEYS */;
/*!40000 ALTER TABLE `needs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procurement`
--

DROP TABLE IF EXISTS `procurement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procurement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(10) unsigned DEFAULT NULL,
  `contract_title` varchar(255) DEFAULT '',
  `contractor` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreign_sp_id_subproject_idx` (`sp_id`),
  CONSTRAINT `foreign_sp_id_subproject` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procurement`
--

LOCK TABLES `procurement` WRITE;
/*!40000 ALTER TABLE `procurement` DISABLE KEYS */;
/*!40000 ALTER TABLE `procurement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relation_subproject_attachment`
--

DROP TABLE IF EXISTS `relation_subproject_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relation_subproject_attachment` (
  `sp_id` int(11) unsigned NOT NULL,
  `ath_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sp_id`,`ath_id`),
  KEY `ath_id` (`ath_id`),
  CONSTRAINT `relation_subproject_attachment_ibfk_2` FOREIGN KEY (`ath_id`) REFERENCES `attachment` (`id`),
  CONSTRAINT `relation_subproject_attachment_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relation_subproject_attachment`
--

LOCK TABLES `relation_subproject_attachment` WRITE;
/*!40000 ALTER TABLE `relation_subproject_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `relation_subproject_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relation_subproject_village`
--

DROP TABLE IF EXISTS `relation_subproject_village`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relation_subproject_village` (
  `sp_id` int(11) unsigned NOT NULL,
  `v_id` int(11) unsigned NOT NULL,
  `men` int(11) DEFAULT NULL,
  `women` int(11) DEFAULT NULL,
  `male_youths` int(11) DEFAULT NULL,
  `female_youths` int(11) DEFAULT NULL,
  `boys` int(11) DEFAULT NULL,
  `girls` int(11) DEFAULT NULL,
  `disabled` int(11) DEFAULT NULL,
  PRIMARY KEY (`sp_id`,`v_id`),
  KEY `v_id` (`v_id`),
  CONSTRAINT `relation_subproject_village_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`),
  CONSTRAINT `relation_subproject_village_ibfk_2` FOREIGN KEY (`v_id`) REFERENCES `village` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relation_subproject_village`
--

LOCK TABLES `relation_subproject_village` WRITE;
/*!40000 ALTER TABLE `relation_subproject_village` DISABLE KEYS */;
INSERT INTO `relation_subproject_village` VALUES (1,1,11,11,11,11,11,11,11),(1,2,12,12,12,12,12,12,12),(2,1,21,21,21,21,21,21,21),(2,2,22,22,22,22,22,22,22);
/*!40000 ALTER TABLE `relation_subproject_village` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sector`
--

DROP TABLE IF EXISTS `sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sector` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `sector_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `sector` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sector`
--

LOCK TABLES `sector` WRITE;
/*!40000 ALTER TABLE `sector` DISABLE KEYS */;
INSERT INTO `sector` VALUES (0,0,'0'),(1,0,'main_sector1'),(2,1,'sector1-1'),(3,1,'sector1-2'),(4,0,'main_sector2'),(5,4,'sector2-1'),(6,4,'sector2-2');
/*!40000 ALTER TABLE `sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subproject`
--

DROP TABLE IF EXISTS `subproject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subproject` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) unsigned DEFAULT NULL,
  `sector_id` int(11) unsigned DEFAULT NULL,
  `subsector_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `objective` text,
  `description` text,
  `nature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `sector_id` (`sector_id`),
  KEY `subsector_id` (`subsector_id`),
  CONSTRAINT `subproject_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `subproject_type` (`id`),
  CONSTRAINT `subproject_ibfk_3` FOREIGN KEY (`sector_id`) REFERENCES `sector` (`id`),
  CONSTRAINT `subproject_ibfk_4` FOREIGN KEY (`subsector_id`) REFERENCES `sector` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subproject`
--

LOCK TABLES `subproject` WRITE;
/*!40000 ALTER TABLE `subproject` DISABLE KEYS */;
INSERT INTO `subproject` VALUES (1,1,1,2,'subproject1','2014-10-30','objective1','description1','new_renovation'),(2,2,4,5,'subproject2','2014-10-30','objective2','description2','repair_expansion');
/*!40000 ALTER TABLE `subproject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subproject_type`
--

DROP TABLE IF EXISTS `subproject_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subproject_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subproject_type`
--

LOCK TABLES `subproject_type` WRITE;
/*!40000 ALTER TABLE `subproject_type` DISABLE KEYS */;
INSERT INTO `subproject_type` VALUES (1,'subproject_type1'),(2,'subproject_type2'),(3,'subproject_type3');
/*!40000 ALTER TABLE `subproject_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `village`
--

DROP TABLE IF EXISTS `village`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `village` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `village`
--

LOCK TABLES `village` WRITE;
/*!40000 ALTER TABLE `village` DISABLE KEYS */;
INSERT INTO `village` VALUES (1,'village1'),(2,'village2'),(3,'village3');
/*!40000 ALTER TABLE `village` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_activity`
--

DROP TABLE IF EXISTS `work_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stg_id` int(11) unsigned DEFAULT NULL,
  `descripton` text,
  `completed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stg_id` (`stg_id`),
  CONSTRAINT `work_activity_ibfk_1` FOREIGN KEY (`stg_id`) REFERENCES `work_stage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_activity`
--

LOCK TABLES `work_activity` WRITE;
/*!40000 ALTER TABLE `work_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `work_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_stage`
--

DROP TABLE IF EXISTS `work_stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_stage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `work_stage_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_stage`
--

LOCK TABLES `work_stage` WRITE;
/*!40000 ALTER TABLE `work_stage` DISABLE KEYS */;
/*!40000 ALTER TABLE `work_stage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-14 18:57:44
