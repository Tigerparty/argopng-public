# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.19-log)
# Database: project_taarifa_forms
# Generation Time: 2014-10-16 06:46:20 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table attachment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attachment`;

CREATE TABLE `attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `origin_name` varchar(255) DEFAULT NULL,
  `store_name` varchar(255) DEFAULT '',
  `type` varchar(255) DEFAULT NULL,
  `upload_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table contribution
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contribution`;

CREATE TABLE `contribution` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `source_name` text,
  `type` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `contribution_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `contribution` WRITE;
/*!40000 ALTER TABLE `contribution` DISABLE KEYS */;

INSERT INTO `contribution` (`id`, `sp_id`, `source_name`, `type`, `amount`)
VALUES
	(1,36,'','RDP',50000),
	(2,36,'','Community',50000),
	(3,36,'Louis1','Other',10000),
	(4,36,'Louis2','Other',20000),
	(5,37,'','RDP',0),
	(6,37,'','Community',0),
	(7,37,'','Other',0),
	(8,38,'','RDP',0),
	(9,38,'','Community',0),
	(10,38,'','Other',0),
	(11,39,'','RDP',1),
	(12,39,'','Community',1),
	(13,39,'1','Other',1);

/*!40000 ALTER TABLE `contribution` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table needs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `needs`;

CREATE TABLE `needs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `needs_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table procurement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `procurement`;

CREATE TABLE `procurement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(10) unsigned DEFAULT NULL,
  `contract_title` varchar(255) DEFAULT '',
  `contractor` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreign_sp_id_subproject_idx` (`sp_id`),
  CONSTRAINT `foreign_sp_id_subproject` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `procurement` WRITE;
/*!40000 ALTER TABLE `procurement` DISABLE KEYS */;

INSERT INTO `procurement` (`id`, `sp_id`, `contract_title`, `contractor`, `type`, `cost`)
VALUES
	(1,36,'Louis1','Louis1','goods',10000),
	(2,36,'Louis2','Louis2','work',20000),
	(3,36,'Louis3','Louis3','services',30000),
	(4,37,'','','goods',0),
	(5,38,'','','goods',0),
	(6,39,'1','1','goods',1);

/*!40000 ALTER TABLE `procurement` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table relation_report_attachment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relation_report_attachment`;

CREATE TABLE `relation_report_attachment` (
  `spr_id` int(11) unsigned NOT NULL,
  `ath_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`spr_id`,`ath_id`),
  KEY `ath_id` (`ath_id`),
  CONSTRAINT `relation_report_attachment_ibfk_2` FOREIGN KEY (`ath_id`) REFERENCES `attachment` (`id`),
  CONSTRAINT `relation_report_attachment_ibfk_1` FOREIGN KEY (`spr_id`) REFERENCES `subproject_report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table relation_subproject_attachment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relation_subproject_attachment`;

CREATE TABLE `relation_subproject_attachment` (
  `sp_id` int(11) unsigned NOT NULL,
  `ath_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sp_id`,`ath_id`),
  KEY `ath_id` (`ath_id`),
  CONSTRAINT `relation_subproject_attachment_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`),
  CONSTRAINT `relation_subproject_attachment_ibfk_2` FOREIGN KEY (`ath_id`) REFERENCES `attachment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table relation_subproject_village
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relation_subproject_village`;

CREATE TABLE `relation_subproject_village` (
  `sp_id` int(11) unsigned NOT NULL,
  `v_id` int(11) unsigned NOT NULL,
  `men` int(11) DEFAULT NULL,
  `women` int(11) DEFAULT NULL,
  `male_youths` int(11) DEFAULT NULL,
  `female_youths` int(11) DEFAULT NULL,
  `boys` int(11) DEFAULT NULL,
  `girls` int(11) DEFAULT NULL,
  `disabled` int(11) DEFAULT NULL,
  PRIMARY KEY (`sp_id`,`v_id`),
  KEY `v_id` (`v_id`),
  CONSTRAINT `relation_subproject_village_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`),
  CONSTRAINT `relation_subproject_village_ibfk_2` FOREIGN KEY (`v_id`) REFERENCES `village` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `relation_subproject_village` WRITE;
/*!40000 ALTER TABLE `relation_subproject_village` DISABLE KEYS */;

INSERT INTO `relation_subproject_village` (`sp_id`, `v_id`, `men`, `women`, `male_youths`, `female_youths`, `boys`, `girls`, `disabled`)
VALUES
	(36,1,11,12,13,14,15,16,17),
	(36,2,21,22,23,24,25,26,27),
	(36,3,31,32,33,34,35,36,37),
	(37,1,0,0,0,0,0,0,0),
	(38,1,2,2,2,2,2,2,2),
	(38,2,1,1,1,1,1,1,1),
	(38,3,2,2,2,2,2,2,2),
	(39,1,1,1,1,1,1,1,1),
	(40,1,1,1,1,1,1,1,1),
	(41,1,0,0,0,0,0,0,0);

/*!40000 ALTER TABLE `relation_subproject_village` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sector
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sector`;

CREATE TABLE `sector` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `sector_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `sector` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sector` WRITE;
/*!40000 ALTER TABLE `sector` DISABLE KEYS */;

INSERT INTO `sector` (`id`, `parent_id`, `name`)
VALUES
	(0,0,'0'),
	(1,0,'main_sector1'),
	(2,1,'sector1-1'),
	(3,1,'sector1-2'),
	(4,0,'main_sector2'),
	(5,4,'sector2-1'),
	(6,4,'sector2-2');

/*!40000 ALTER TABLE `sector` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subproject
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subproject`;

CREATE TABLE `subproject` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) unsigned DEFAULT NULL,
  `sector_id` int(11) unsigned DEFAULT NULL,
  `subsector_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `objective` text,
  `description` text,
  `nature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `sector_id` (`sector_id`),
  KEY `subsector_id` (`subsector_id`),
  CONSTRAINT `subproject_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `subproject_type` (`id`),
  CONSTRAINT `subproject_ibfk_3` FOREIGN KEY (`sector_id`) REFERENCES `sector` (`id`),
  CONSTRAINT `subproject_ibfk_4` FOREIGN KEY (`subsector_id`) REFERENCES `sector` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `subproject` WRITE;
/*!40000 ALTER TABLE `subproject` DISABLE KEYS */;

INSERT INTO `subproject` (`id`, `type_id`, `sector_id`, `subsector_id`, `name`, `due_date`, `objective`, `description`, `nature`)
VALUES
	(36,2,1,3,'Louis','2014-10-27','Louis','Louis','extension'),
	(37,1,1,2,'','0000-00-00','','','new_renovation'),
	(38,2,1,3,'test','2014-10-08','test','test','new_renovation'),
	(39,1,1,2,'123','2014-10-07','123','123','new_renovation'),
	(40,1,1,2,'2','2014-10-15','2','2','new_renovation'),
	(41,1,1,2,'','0000-00-00','','','new_renovation');

/*!40000 ALTER TABLE `subproject` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subproject_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subproject_report`;

CREATE TABLE `subproject_report` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `content` text,
  `oustanding_resolved` text,
  `implementation_problems` text,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `subproject_report_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table subproject_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subproject_type`;

CREATE TABLE `subproject_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `subproject_type` WRITE;
/*!40000 ALTER TABLE `subproject_type` DISABLE KEYS */;

INSERT INTO `subproject_type` (`id`, `type`)
VALUES
	(1,'subproject_type1'),
	(2,'subproject_type2'),
	(3,'subproject_type3');

/*!40000 ALTER TABLE `subproject_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table village
# ------------------------------------------------------------

DROP TABLE IF EXISTS `village`;

CREATE TABLE `village` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `village` WRITE;
/*!40000 ALTER TABLE `village` DISABLE KEYS */;

INSERT INTO `village` (`id`, `name`)
VALUES
	(1,'village1'),
	(2,'village2'),
	(3,'village3'),
	(4,'village4'),
	(5,'village5'),
	(6,'village6'),
	(7,'village7'),
	(8,'village8'),
	(9,'village9'),
	(10,'village10');

/*!40000 ALTER TABLE `village` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table work_activity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `work_activity`;

CREATE TABLE `work_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stg_id` int(11) unsigned DEFAULT NULL,
  `description` text,
  `completed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stg_id` (`stg_id`),
  CONSTRAINT `work_activity_ibfk_1` FOREIGN KEY (`stg_id`) REFERENCES `work_stage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `work_activity` WRITE;
/*!40000 ALTER TABLE `work_activity` DISABLE KEYS */;

INSERT INTO `work_activity` (`id`, `stg_id`, `description`, `completed`)
VALUES
	(1,24,'Louis11',0),
	(2,24,'Louis12',0),
	(3,25,'Louis21',0),
	(4,25,'Louis22',0),
	(5,26,'123',0),
	(6,26,'234',0),
	(9,29,'1',0),
	(10,29,'2',1),
	(63,66,'1',1),
	(64,66,'2',0),
	(65,67,'1',1),
	(66,67,'2',0);

/*!40000 ALTER TABLE `work_activity` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table work_stage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `work_stage`;

CREATE TABLE `work_stage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `work_stage_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `work_stage` WRITE;
/*!40000 ALTER TABLE `work_stage` DISABLE KEYS */;

INSERT INTO `work_stage` (`id`, `sp_id`, `stage`, `description`)
VALUES
	(24,36,'1','Louis1'),
	(25,36,'2','Louis2'),
	(26,37,'1','123'),
	(29,39,'1','1'),
	(66,41,'1','1'),
	(67,41,'2','2'),
	(68,41,'3','4');

/*!40000 ALTER TABLE `work_stage` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
