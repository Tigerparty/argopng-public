# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.19-log)
# Database: project_taarifa_forms
# Generation Time: 2014-10-09 02:32:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table activity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table attachment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attachment`;

CREATE TABLE `attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `origin_name` varchar(255) DEFAULT NULL,
  `store_name` varchar(255) DEFAULT '',
  `type` varchar(255) DEFAULT NULL,
  `upload_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table contribution
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contribution`;

CREATE TABLE `contribution` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `source_name` text,
  `type` varchar(255) DEFAULT NULL,
  `funds` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `contribution_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table needs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `needs`;

CREATE TABLE `needs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sp_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `sp_id` (`sp_id`),
  CONSTRAINT `needs_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table procurement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `procurement`;

CREATE TABLE `procurement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contract_title` varchar(255) DEFAULT '',
  `contractor` varchar(255) DEFAULT NULL,
  `goods_work_services` varchar(255) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table relation_subproject_activities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relation_subproject_activities`;

CREATE TABLE `relation_subproject_activities` (
  `sp_id` int(11) unsigned NOT NULL,
  `act_id` int(11) unsigned NOT NULL DEFAULT '0',
  `stage` int(11) DEFAULT NULL,
  `completed` bit(1) DEFAULT NULL,
  PRIMARY KEY (`sp_id`,`act_id`),
  KEY `act_id` (`act_id`),
  CONSTRAINT `relation_subproject_activities_ibfk_2` FOREIGN KEY (`act_id`) REFERENCES `activity` (`id`),
  CONSTRAINT `relation_subproject_activities_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table relation_subproject_attachment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relation_subproject_attachment`;

CREATE TABLE `relation_subproject_attachment` (
  `sp_id` int(11) unsigned NOT NULL,
  `ath_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sp_id`,`ath_id`),
  KEY `ath_id` (`ath_id`),
  CONSTRAINT `relation_subproject_attachment_ibfk_2` FOREIGN KEY (`ath_id`) REFERENCES `attachment` (`id`),
  CONSTRAINT `relation_subproject_attachment_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table relation_subproject_village
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relation_subproject_village`;

CREATE TABLE `relation_subproject_village` (
  `sp_id` int(11) unsigned NOT NULL,
  `v_id` int(11) unsigned NOT NULL,
  `men` int(11) DEFAULT NULL,
  `women` int(11) DEFAULT NULL,
  `male_youths` int(11) DEFAULT NULL,
  `female_youths` int(11) DEFAULT NULL,
  `boys` int(11) DEFAULT NULL,
  `girls` int(11) DEFAULT NULL,
  `disabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`sp_id`,`v_id`),
  KEY `v_id` (`v_id`),
  CONSTRAINT `relation_subproject_village_ibfk_2` FOREIGN KEY (`v_id`) REFERENCES `village` (`id`),
  CONSTRAINT `relation_subproject_village_ibfk_1` FOREIGN KEY (`sp_id`) REFERENCES `subproject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sector
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sector`;

CREATE TABLE `sector` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `name` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `sector_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `sector` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table subject_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subject_type`;

CREATE TABLE `subject_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table subproject
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subproject`;

CREATE TABLE `subproject` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) unsigned DEFAULT NULL,
  `village_id` int(11) unsigned DEFAULT NULL,
  `sector_id` int(11) unsigned DEFAULT NULL,
  `subsector_id` int(11) unsigned DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `objective` text,
  `description` text,
  `nature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `village_id` (`village_id`),
  KEY `sector_id` (`sector_id`),
  KEY `subsector_id` (`subsector_id`),
  CONSTRAINT `subproject_ibfk_4` FOREIGN KEY (`subsector_id`) REFERENCES `sector` (`id`),
  CONSTRAINT `subproject_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `subject_type` (`id`),
  CONSTRAINT `subproject_ibfk_2` FOREIGN KEY (`village_id`) REFERENCES `village` (`id`),
  CONSTRAINT `subproject_ibfk_3` FOREIGN KEY (`sector_id`) REFERENCES `sector` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table village
# ------------------------------------------------------------

DROP TABLE IF EXISTS `village`;

CREATE TABLE `village` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table work_plan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `work_plan`;

CREATE TABLE `work_plan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
